import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSettingEditPageComponent } from './admin-setting-edit-page.component';

describe('AdminSettingEditPageComponent', () => {
  let component: AdminSettingEditPageComponent;
  let fixture: ComponentFixture<AdminSettingEditPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSettingEditPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSettingEditPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
