import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Router } from '@angular/router';
import * as jspdf from 'jspdf';  
import html2canvas from 'html2canvas';  
import { ApproveRejectDialogComponent } 
from '../../dialogs/approve-reject-dialog/approve-reject-dialog.component';
import { SuccessOkDialogComponent } 
from '../../dialogs/success-ok-dialog/success-ok-dialog.component';

import { Observable, Subscription } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import { AuthService } from '../../services/auth.service';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-workers-dialog',
  templateUrl: './workers-dialog.component.html',
  styleUrls: ['./workers-dialog.component.css']
})
export class WorkersDialogComponent implements OnInit {
  profile: any;

  totalShiftCount = 0;
  bookingCount = 0;
  pendingCount = 0;
  openDisputeCount = 0;
  closeDisputeCount = 0;

  private bookingSubscription: Subscription;
  private bookingOngoingSubscription: Subscription;
  private bookingPendingSubscription: Subscription;
  private bookingOpenDisputeSubscription: Subscription;
  private bookingCloseDisputeSubscription: Subscription;

  constructor(public dialog: MatDialog, public dialogRef: MatDialogRef<WorkersDialogComponent>, @Inject(MAT_DIALOG_DATA) public data:string,
  private router: Router, private firestore: AngularFirestore,private afAuth: AngularFireAuth, 
  private authService: AuthService) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    if(this.bookingSubscription != undefined){
        this.bookingSubscription.unsubscribe();
    }

    if(this.bookingOngoingSubscription != undefined){
        this.bookingOngoingSubscription.unsubscribe();
    }

    if(this.bookingPendingSubscription != undefined){
        this.bookingPendingSubscription.unsubscribe();
    }

    if(this.bookingOpenDisputeSubscription != undefined){
        this.bookingOpenDisputeSubscription.unsubscribe();
    }

    if(this.bookingCloseDisputeSubscription != undefined){
        this.bookingCloseDisputeSubscription.unsubscribe();
    }
  }

  public updateBookingsList(){
    var aCollection = this.firestore.collection('bookings', ref => 
    ref.where('workingStatus','==','completed')
    .where('workerId','==',this.profile.userId)
    );
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.bookingSubscription = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(shiftslist => {
      if (shiftslist != undefined) {
        this.totalShiftCount = shiftslist.length;
      } else {
        this.totalShiftCount = 0;
      }
    });
     
   }

   public updateOngoingBookingsList(){
    var aCollection = this.firestore.collection('bookings', ref => 
    // ref.where('workingStatus','<','completed')
    // .where('workingStatus','>','completed')
    ref.where('workerId','==',this.profile.userId)
    );
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.bookingOngoingSubscription = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(shiftslist => {
      var temp = [];
      temp = shiftslist;
      var incompletes = [];
      for(const aBook of temp){
        if((aBook.workingStatus == undefined) || (aBook.workingStatus != 'completed')){
          incompletes.push(aBook);
        }
      }
      if (shiftslist != undefined) {
        this.bookingCount = shiftslist.length;
      } else {
        this.bookingCount = 0;
      }
    });
     
   }

   public updatePendingBookingsList(){
    var aCollection = this.firestore.collection('bookings', ref => 
    // ref.where('paymentStatus','<','paid')
    // .where('paymentStatus','>','paid')
    ref.where('workerId','==',this.profile.userId)
    );
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.bookingPendingSubscription = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(shiftslist => {
      var temp = [];
      temp = shiftslist;
      var pendings = [];
      for(const aBook of temp){
        if((aBook.paymentStatus == undefined) || (aBook.paymentStatus != 'paid')){
          pendings.push(aBook);
        }
      }
      if (shiftslist != undefined) {
        this.pendingCount = shiftslist.length;
      } else {
        this.pendingCount = 0;
      }
    });
     
   }

   public updateOpenDisputeBookingsList(){
    var aCollection = this.firestore.collection('bookings', ref => 
    ref.where('timesheetStatus','==','disputed')
    .where('workerId','==',this.profile.userId)
    );
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.bookingOpenDisputeSubscription = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(shiftslist => {
      if (shiftslist != undefined) {
        this.openDisputeCount = shiftslist.length;
      } else {
        this.openDisputeCount = 0;
      }
    });
     
   }

   public updateCloseDisputeBookingsList(){
    var aCollection = this.firestore.collection('bookings', ref => 
    ref.where('timesheetStatus','==','disputeresolved')
    .where('workerId','==',this.profile.userId)
    );
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.bookingCloseDisputeSubscription = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(shiftslist => {
      if (shiftslist != undefined) {
        this.closeDisputeCount = shiftslist.length;
      } else {
        this.closeDisputeCount = 0;
      }
    });
     
   }

  cancelClick(): void {
    this.dialogRef.close();
  }

  // generatePrint() {

  //   var title = "";
  //   if (this.profile != undefined) {
  //     // profile exists
  //     title = this.profile.firstName+"-"+this.profile.lastName;
  //   } else if (this.profile != undefined) {
  //     // company exists
  //     title = this.profile.name;
  //   }

  //   var innerContents = document.getElementById("workerPrintableContainerId").innerHTML;
  //   var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
  //   popupWinindow.document.open();
  //   popupWinindow.document.write(`
  //     <html>
  //       <head>
  //         <title>${title}</title>
  //         <style>
  //         .example-spacer {
  //           .example-spacer {
  //             flex: 1 1 auto;
  //         }
  //         .mat-dialog-content{
  //             margin: 0;
  //             padding: 0;
  //         }
  //         .dialog-toolbar{
  //             background-color: #c3ccf7;
  //             padding: 0px 30px;
  //             position: relative;
  //         }
  //         .clear-icon{
  //             cursor: pointer;
  //             position: absolute;
  //             right: 40px;
  //             top: 25px;
  //             font-size: 30px;
  //             font-weight: 600;
  //         }
  //         .text-bold{
  //             font-size: 16px;
  //             font-weight: 600;
  //             color: #000;
  //          }
  //          .text-bold span{
  //              padding: 0 5px;
  //          }
  //          .breaking-word{
  //              word-break: break-word;
  //              font-size: 14px;
  //              font-weight: 500;
  //              color: #000;
  //          }
  //         .mat-toolbar-row{
  //             height: 100px;
  //         }
  //         .worker-image{
  //             width: 60px;
  //             height: 60px;
  //             position: relative;
  //         }
  //         .worker-image img{
  //             width: 100%;
  //             height: 100%;
  //             border-radius: 50%;
  //         }
  //         .inline{
  //             display: inline-block;
  //         }
  //         .no-padding{
  //             margin: 0;
  //             padding: 0;
  //         }
  //         .worker-col{
  //             max-width: 12.666667%;
  //         }
  //         .admin-online{
  //             position: absolute;
  //             width: 25px;
  //             height: 27px;
  //             bottom: 7px;
  //             left: 45px;
  //         }
  //         .admin-online img{
  //             width: 100%;
  //             height: 100%;
  //         }
  //         .worker-name-address{
  //             padding-top: 6px;
  //         } 
  //         .worker-name-address h3{
  //             color: #323232;
  //             font-size: 23px;
  //             font-weight: 400;
  //             font-family: Roboto;
  //             word-wrap: break-word;
  //             margin-bottom: 0;
  //         }
  //         .worker-name-address p{
  //             color: #323232;
  //             font-size: 16px;
  //             font-weight: 500;
  //             font-family: Roboto;
  //             word-wrap: break-word;
  //         }
  //         .email-phone{
  //             padding-top: 6px;
  //             color: #323232;
  //             font-size: 15px;
  //             font-weight: 500;
  //             font-family: Roboto;
  //         }
  //         .email-phone p{
  //             margin-bottom: 0;
  //         }
  //         .email_address_phone_no{
  //             padding-top: 6px;
  //             color: #323232;
  //             font-size: 15px;
  //             font-weight: 500;
  //             font-family: Roboto;
  //             word-wrap: break-word;
  //         }
  //         .email_address_phone_no p{
  //             margin-bottom: 0;
  //         }
  //         .dialog-subject{
  //             padding: 30px 80px;
  //             color: #323232;
  //         }
  //         .worker_detail{
  //             border-bottom: 1px solid #e2e2e2;
  //         }
  //         .worker_detail_data p{
  //             margin-bottom: 0;
  //             padding-bottom: 10px;
  //             font-size: 14px;
  //             color: #323232;
  //             font-family: Roboto;
  //             font-weight: 400;
  //             line-height: 25px;
  //         }
  //         .deletesuspendWorker button{
  //             background-color: #DF8C40;
  //             color: #fff;
  //             border-radius: 3px;
  //             padding: 9px 25px;
  //             box-shadow: 0 0 40px rgba(223,140,64, .2);
  //             border: 0;
  //             outline: none;
  //             margin: 20px 5px 30px;
  //             font-size: 14px;
  //             font-weight: 300;
  //             letter-spacing: 1px;
  //             cursor: pointer;
  //         }
          
  //         .top-space{
  //             padding-top: 10px;
  //         }
          
          
  //         .personal-description-heading{
  //             font-size: 23px;
  //             color: #323232;
  //             font-family: Montserrat;
  //             font-weight: 400;
  //             padding: 5px 0;
  //         }
  //         .document-heading{
  //             font-size: 23px;
  //             color: #323232;
  //             font-family: Montserrat;
  //             font-weight: 400;
  //             padding: 5px 0;
  //         }
  //         .personal-description{
  //             border-bottom: 1px solid #e2e2e2;
  //             padding: 0 10px;
  //         }
  //         .personal-description-text{
  //             font-size: 14px;
  //             color: #323232;
  //             font-family: Roboto;
  //             font-weight: 500;
  //         }
  //         .personal-description-data{
  //             font-size: 14px;
  //             color: #323232;
  //             font-family: Roboto;
  //             font-weight: 400;
  //         } 
          
          
  //         .document-data-text p{
  //             font-size: 14px;
  //             color: #b44634;
  //             font-family: Roboto;
  //             font-weight: 500;
  //         }
  //         .document-data-download p{
  //             font-size: 14px;
  //             color: #0f38f7;
  //             font-family: Roboto;
  //             font-weight: 500;
  //             text-decoration: underline;
  //         }
  //         .print-pdf-sec{
  //             padding-top: 80px;
  //         }
  //         .print-btn-image {
  //            padding-right: 6px;
  //         }
  //         .pdf-btn-image{
  //             padding-left: 6px;
  //         }
  //         .print-pdf-btn{
  //             border: 0;
  //             outline: 0;
  //             background-color: #465f9e;
  //             color: #fff; 
  //             font-size: 12px;
  //             font-weight: 100;
  //             padding: 3px 7px;
  //             cursor: pointer;
  //         }
          
  //         @media (max-width: 768px){
  //             .mat-toolbar-row{
  //                 height: 150px;
  //             }
  //             .worker-col{
  //                 max-width: 18.66667%;
  //             }
  //             .dialog-subject{
  //                 padding: 30px 40px;
  //             }
  //         }
  //         @media (max-width: 555px){
  //             .worker-col{
  //                 max-width: 27.66667%;
  //             }
  //         }
  //         </style>
  //       </head>
  //   <body onload="window.print();window.close()">${innerContents}</body>
  //     </html>`
  //   );    
  //   popupWinindow.document.close();

  // }  
  // generatePDF(){
  //   var data = document.getElementById('workerPrintableContainerId'); 

  //   var pdfName = "MYPdf.pdf";
  //   if (this.profile != undefined) {
  //     // profile exists
  //     pdfName = this.profile.firstName.replace(/\s/g,'')+"-"+this.profile.lastName.replace(/\s/g,'')+".pdf";
  //   } else if (this.profile != undefined) {
  //     // company exists
  //     pdfName = this.profile.name.replace(/\s/g,'')+".pdf";
  //   }

  //   html2canvas(data).then(canvas => {  
  //     // Few necessary setting options  
  //     var imgWidth = 208;   
  //     var pageHeight = 295;    
  //     var imgHeight = canvas.height * imgWidth / canvas.width;  
  //     var heightLeft = imgHeight;  
  
  //     const contentDataURL = canvas.toDataURL('image/png')  
  //     let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
  //     var position = 0;  
  //     pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
  //     pdf.save(pdfName); // Generated PDF   
  //   }); 
  // }


  suspendButtonClicked(){
    const dialogRef = this.dialog.open(ApproveRejectDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = 'Are you sure?';
    dialogRef.componentInstance.descr = 
    "Do you really want to suspend this worker";

    dialogRef.componentInstance.approveTitle = 'Yes, Suspend';
    dialogRef.componentInstance.rejectTitle = 'No, Cancel';

    dialogRef.afterClosed().subscribe(result => {
        if(result == dialogRef.componentInstance.approveTitle){

            this.dialogRef.close();

            this.firestore.collection('profiles').doc(this.profile.userId)
            .set({approvalStatus: 'suspended'}, {merge: true})
            .then(() => {
                this.firestore.collection('suspensions').doc(this.profile.userId)
                .set({
                    firstName: this.profile.firstName,
                    lastName: this.profile.lastName,
                    email: this.profile.email,
                    userId: this.profile.userId,
                    updatedAt: firebase.firestore.FieldValue.serverTimestamp()
                }, {merge: true})
                .then(()=>{
                    this.openInformDialog('Sucess', 'Worker has been suspended sucessfully');
                })
                .catch((err) => {
                    console.log(err.message);
                    this.openInformDialog('Error', err.message)
                })

            })
            .catch((err) => {
                console.log(err.message);
                this.openInformDialog('Error', err.message)
            })
        }
    });
  }

  deleteButtonClicked(){
    const dialogRef = this.dialog.open(ApproveRejectDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = 'Are you sure?';
    dialogRef.componentInstance.descr = 
    "Do you really want to delete this worker";

    dialogRef.componentInstance.approveTitle = 'Yes, delete';
    dialogRef.componentInstance.rejectTitle = 'No, Cancel';

    dialogRef.afterClosed().subscribe(result => {
        if(result == dialogRef.componentInstance.approveTitle){

            this.dialogRef.close();

            this.firestore.collection('profiles').doc(this.profile.userId)
            .set({approvalStatus: 'deleted'}, {merge: true})
            .then(() => {
                this.firestore.collection('deletions').doc(this.profile.userId)
                .set({
                    firstName: this.profile.firstName,
                    lastName: this.profile.lastName,
                    email: this.profile.email,
                    userId: this.profile.userId,
                    updatedAt: firebase.firestore.FieldValue.serverTimestamp()
                }, {merge: true})
                .then(()=>{
                    this.openInformDialog('Sucess', 'Worker has been marked for delete');
                })
                .catch((err) => {
                    console.log(err.message);
                    this.openInformDialog('Error', err.message)
                })

            })
            .catch((err) => {
                console.log(err.message);
                this.openInformDialog('Error', err.message)
            })
        }
    });
  }

  openInformDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
    
  }

}
