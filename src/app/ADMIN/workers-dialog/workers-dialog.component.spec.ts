import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkersDialogComponent } from './workers-dialog.component';

describe('WorkersDialogComponent', () => {
  let component: WorkersDialogComponent;
  let fixture: ComponentFixture<WorkersDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkersDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkersDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
