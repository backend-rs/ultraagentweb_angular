import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';
import { PendingDetailDialogComponent } from '../../dialogs/pending-detail-dialog/pending-detail-dialog.component';
import { SuccessOkDialogComponent } 
from '../../dialogs/success-ok-dialog/success-ok-dialog.component';

@Component({
  selector: 'app-timesheet',
  templateUrl: './timesheet.component.html',
  styleUrls: ['./timesheet.component.css']
})
export class TimesheetComponent {

  filterDate:Date = undefined;
  sortKey = "";
  
  dialogResult="";

  shiftslist:any;
  profileslist:any;
  companieslist:any;
  uamargins:any;

  sub:any;
  passedBookingId:any;
  alreadyProcessedPassedBookingId = false;

  activeTab = 'pending';

  blist = [];

  hidePendingShiftCountTitle = false;
  hidePaidShiftCountTitle = true;

  NewShiftsThisWeek = 0;

  
  private shiftSubscription: Subscription;
  private profileSubscription: Subscription;
  private bookingSubscription: Subscription;

  private companiesSubscription: Subscription;
  private uamarginsSubscription: Subscription;



  constructor(public dialog: MatDialog, private authService: AuthService,
    private router: Router,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    private route: ActivatedRoute,
    ) {}


  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.passedBookingId = params['bookingId']; 
      //console.log(params);
    });

    this.updateShiftsList();

    this.uamarginsSubscription = this.firestore.collection('globalsettings').doc('uamargins')
    .snapshotChanges().subscribe(aSnap => {
      if (aSnap.payload.exists) {
        // console.log('uamargins');
        // console.log(aSnap);
        // console.log(aSnap.payload.data());
        this.uamargins = aSnap.payload.data();

        this.calculateFees();
      }
      
    });
  }
  
  ngOnDestroy() {
    if (this.shiftSubscription != undefined){
      this.shiftSubscription.unsubscribe();
    }
    if (this.profileSubscription != undefined){
      this.profileSubscription.unsubscribe();
    }
    if (this.bookingSubscription != undefined){
      this.bookingSubscription.unsubscribe();
    }
    if (this.companiesSubscription != undefined){
      this.companiesSubscription.unsubscribe();
    }
    if (this.uamarginsSubscription != undefined){
      this.uamarginsSubscription.unsubscribe();
    }

    this.sub.unsubscribe();

  }

  updateBookings(){
    const aCollection = this.firestore.collection('bookings');
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.bookingSubscription = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(someList => {
      if(someList != undefined){
        this.blist = someList;
      }

      var bCollection = this.firestore.collection('profiles');
      this.profileSubscription = bCollection.snapshotChanges().pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        }))
      ).subscribe(profileslist => {
          this.profileslist = profileslist;
          //console.log(this.profileslist);

          for(var i=0; i<this.blist.length; i++){
            var workerId = this.blist[i].workerId;
            for(var j=0; j<this.profileslist.length; j++){
              if(this.profileslist[j].id == workerId){
                this.blist[i].worker = this.profileslist[j];
              }
            }
          }

          for(var j=0; j<this.blist.length; j++){
            for(var i=0; i<this.shiftslist.length; i++){
              if(this.blist[j].shiftId == this.shiftslist[i].id){
                this.blist[j].shift = this.shiftslist[i];
              }
            }
          }

          if ((this.passedBookingId != undefined) && (this.passedBookingId.length > 0) && (this.alreadyProcessedPassedBookingId == false)) {
            var found = false;
            this.alreadyProcessedPassedBookingId = true;
            for (var i=0; i< this.blist.length; i++) {
              if (this.blist[i].id == this.passedBookingId) {
                const aBooking = this.blist[i];
                found = true;
    
                if (((aBooking.timesheetStatus == 'approved') || 
                      (aBooking.timesheetStatus == 'amendresolved') || 
                      (aBooking.timesheetStatus == 'disputeresolved')) && 
                    ((aBooking.paymentStatus == undefined) || (aBooking.paymentStatus == 'pending'))
                    )
                {
                  this.activeTab = 'pending';
                  this.openPendingDetailsDialog(aBooking);
                  return;
                } else if ((aBooking.paymentStatus != undefined) && (aBooking.paymentStatus == 'paid'))
                {
                  this.activeTab = 'paid';
                  this.openPaidDetailsDialog(aBooking);
                  return;
                }
    
                break;
              }
            }
    
            if (found == false) {
              this.openDialog('Oops!','Booking you are looking for no longer available here.\n\nIt might have been deleted or archived.');
            }
    
          }

      });
      
     
      //console.log('booking fetched');
      //console.log(this.blist);

     

      this.calculateFees();

    });
  }

  updateShiftsList(){
    var aCollection = this.firestore.collection('shifts');
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.shiftSubscription = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(shiftslist => {
        this.shiftslist = shiftslist;
        //console.log(this.shiftslist);

        this.NewShiftsThisWeek = 0;
        if(this.shiftslist != undefined) {
          this.NewShiftsThisWeek += this.shiftslist.length;
        }

      var bCollection = this.firestore.collection('companies');
      this.companiesSubscription = bCollection.snapshotChanges().pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        }))
      ).subscribe(companieslist => {
          this.companieslist = companieslist;
          //console.log(this.companieslist);

          for(var i=0; i<this.shiftslist.length; i++){
            var owner = this.shiftslist[i].owner;
            for(var j=0; j<this.companieslist.length; j++){
              if(this.companieslist[j].id == owner){
                this.shiftslist[i].company = this.companieslist[j];
              }
            }
          }

      });

      this.updateBookings();

    });
     
   }

   calculateFees() {

    for(var i=0; i<this.blist.length; i++){
      var catId = this.blist[i].shiftCategory;

      if (this.blist[i].approvedMins == undefined) {
        this.blist[i].approvedMins = this.blist[i].workedMins;
      }
      if (this.blist[i].approvedFee == undefined) {
        this.blist[i].approvedFee = this.blist[i].fee;
      }

      if ((this.blist[i].approvedMins != undefined) && (this.blist[i].approvedFee != undefined)) {
        if (this.uamargins[catId] == undefined) {
          // uamargins not found, take zero

          this.blist[i].calculatedWorkerFee = parseFloat(((parseInt(this.blist[i].approvedMins+'')/60.00)*
            parseInt(this.blist[i].approvedFee+''))+'').toFixed(2);
          this.blist[i].calculatedUAMargin = 0.00;
          this.blist[i].calculatedEmployerFee = parseFloat((parseFloat(this.blist[i].calculatedWorkerFee)+parseFloat(this.blist[i].calculatedUAMargin))+'').toFixed(2);

        } else {
          this.blist[i].calculatedWorkerFee = parseFloat(((parseInt(this.blist[i].approvedMins+'')/60.00)*
            parseInt(this.blist[i].approvedFee+''))+'').toFixed(2);
          this.blist[i].calculatedUAMargin = parseFloat((this.blist[i].calculatedWorkerFee * 
            parseInt(this.uamargins[catId]+'')/100.00)+'').toFixed(2);
            this.blist[i].calculatedEmployerFee = parseFloat((parseFloat(this.blist[i].calculatedWorkerFee)+parseFloat(this.blist[i].calculatedUAMargin))+'').toFixed(2);
          }
      } else {
        this.blist[i].calculatedWorkerFee = 0;
        this.blist[i].calculatedUAMargin = 0;
        this.blist[i].calculatedEmployerFee = 0;
      }
    }

   }


   onPendingTabSelected() {
    //console.log('onPendingTabSelected clicked');
      this.hidePendingShiftCountTitle = false;
      this.hidePaidShiftCountTitle = true;
  }

  onPaidTabSelected() {
    //console.log('onPaidTabSelected clicked');
      this.hidePendingShiftCountTitle = true;
      this.hidePaidShiftCountTitle = false;
  }

  // DIALOG FOR PENDING SHIFT DETAILS
  openPendingDetailsDialog(booking): void {
   //console.log('openPendingDetailsDialog');
   //console.log(booking);
    const dialogRef = this.dialog.open(PendingDetailDialogComponent, {
      panelClass: 'my-panel',
      width: '900px',
    });

    dialogRef.componentInstance.title='PENDING SHIFT';
    dialogRef.componentInstance.subtitle='SHIFT NO: '+booking.shift.subcategory+' #'+booking.shiftNo;
    dialogRef.componentInstance.booking = booking;
    dialogRef.componentInstance.hidePayTimesheetButton = false;
    dialogRef.componentInstance.hidePaidTimeText = true;

     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
    });
  }

  openPaidDetailsDialog(booking): void {

    //console.log('openPaidDetailsDialog');
    //console.log(booking);

    const dialogRef = this.dialog.open(PendingDetailDialogComponent, {
      panelClass: 'my-panel',
      width: '900px',
    });

    dialogRef.componentInstance.title='PAID SHIFT';
    dialogRef.componentInstance.subtitle='SHIFT NO: '+booking.shift.subcategory+' #'+booking.shiftNo;
    dialogRef.componentInstance.booking = booking;
    dialogRef.componentInstance.hidePayTimesheetButton = true;
    dialogRef.componentInstance.hidePaidTimeText = false;

    this.firestore.collection('profiles').
    doc(booking.workerId).collection('ratings').doc(booking.id).ref.get()
      .then((adoc) => {
        if (!adoc.exists) { 
          return; 
        }         
        var adata = adoc.data();
        adata.id = adoc.id;
        dialogRef.componentInstance.employerRating = adata;
        dialogRef.componentInstance.averageRating = (adata.commRating+adata.deliveryRating+adata.hireAgainRating)/3;
        
      })
      .catch((err) => {
        console.log(err);
      });

     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
    });
  }

  openDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
    
  }

}
