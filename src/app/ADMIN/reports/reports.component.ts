import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Location } from '@angular/common';
import {MatDialog} from '@angular/material';

import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { HttpClient, HttpParams } from '@angular/common/http';

import {UAGlobals} from '../../globals/uaglobals';
import {UAHelpers} from '../../globals/uahelpers';

import * as jspdf from 'jspdf';  
import html2canvas from 'html2canvas'; 

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

  categories = [];

  datePipe: DatePipe;

  usersReportType:any;
  usersReportStartDate:Date;
  usersReportEndDate:Date;

  timesheetReportType:any;
  timesheetReportStartDate:Date;
  timesheetReportEndDate:Date;

  shiftsReportType:any;
  shiftsReportStartDate:Date;
  shiftsReportEndDate:Date;

  registrationReportType:any;
  registrationReportStartDate:Date;
  registrationReportEndDate:Date;

  usersSubscription:Subscription;
  companiesSubscription:Subscription;

  constructor(
    private router: Router, 
    public dialog: MatDialog, private location: Location, 
    private authService: AuthService, 
    private afAuth: AngularFireAuth, 
    private firestore: AngularFirestore,
    private route: ActivatedRoute,
    private httpClient:HttpClient
  ) { 
    this.datePipe = new DatePipe('en-GB');
    this.categories = Object.keys(UAGlobals.MASTER_CATEGORIES);

  }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.usersSubscription != undefined) {
      this.usersSubscription.unsubscribe();
      this.usersSubscription = undefined;
    }
    if (this.companiesSubscription != undefined) {
      this.companiesSubscription.unsubscribe();
      this.companiesSubscription = undefined;
    }
  }

  activeTab = 'timesheet';

  timesheet(activeTab){
    this.activeTab = activeTab;
  }

  shifts(activeTab){
    this.activeTab = activeTab;
  }

  registration(activeTab){
    this.activeTab = activeTab;
  }

  users(activeTab){
    this.activeTab = activeTab;
  }

//Generate Print
  generatePrint(){
    // if (this.activeTab == 'users') {
    //   this.router.navigate(['Users_report']);
    // } else if (this.activeTab == 'timesheet') {
    //   this.router.navigate(['Timesheet_report']);
    // } else if (this.activeTab == 'shifts') {
    //   this.router.navigate(['Shifts_report']);
    // } else if (this.activeTab == 'registration') {
    //   this.router.navigate(['Registration_report']);
    // }

  }

//Generate PDF
  generatePDF(){
  }

  generateClicked() {
    if (this.activeTab == 'users') {
      if ((this.usersReportType == undefined) || (this.usersReportType.length < 1) || (this.usersReportType == 'none')) {
        this.openDialog('Type on specified', 'Please select a valid report type from dropdown menu');
        return;
      }

      if (this.usersReportStartDate == undefined) {
        this.openDialog('Start date on specified', 'Please select a valid start date');
        return;
      }
      if (this.usersReportEndDate == undefined) {
        this.openDialog('End date on specified', 'Please select a valid end date');
        return;
      }
      if (this.usersReportStartDate.getTime() > this.usersReportEndDate.getTime()) {
        this.openDialog('Invalid End date', 'Please select a end date after start date');
        return;
      }

      const start = this.datePipe.transform(this.usersReportStartDate, 'yyyy-MM-dd');
      const end = this.datePipe.transform(this.usersReportEndDate, 'yyyy-MM-dd');;

      this.router.navigate(['Users_report', 
        { type: this.usersReportType, 
          start:start,
          end:end,
          skipLocationChange: true}]);

      // this.queryUsersData();

    } else  if (this.activeTab == 'timesheet') {
      if ((this.timesheetReportType == undefined) || (this.timesheetReportType.length < 1) || (this.timesheetReportType == 'none')) {
        this.openDialog('Type on specified', 'Please select a valid report type from dropdown menu');
        return;
      }

      if (this.timesheetReportStartDate == undefined) {
        this.openDialog('Start date on specified', 'Please select a valid start date');
        return;
      }
      if (this.timesheetReportEndDate == undefined) {
        this.openDialog('End date on specified', 'Please select a valid end date');
        return;
      }
      if (this.timesheetReportStartDate.getTime() > this.timesheetReportEndDate.getTime()) {
        this.openDialog('Invalid End date', 'Please select a end date after start date');
        return;
      }

      const start = this.datePipe.transform(this.timesheetReportStartDate, 'yyyy-MM-dd');
      const end = this.datePipe.transform(this.timesheetReportEndDate, 'yyyy-MM-dd');;

      this.router.navigate(['Timesheet_report', 
        { type: this.timesheetReportType, 
          start:start,
          end:end,
          skipLocationChange: true}]);

      
    } else  if (this.activeTab == 'shifts') {
      if ((this.shiftsReportType == undefined) || (this.shiftsReportType.length < 1) || (this.shiftsReportType == 'none')) {
        this.openDialog('Type on specified', 'Please select a valid report type from dropdown menu');
        return;
      }

      if (this.shiftsReportStartDate == undefined) {
        this.openDialog('Start date on specified', 'Please select a valid start date');
        return;
      }
      if (this.shiftsReportEndDate == undefined) {
        this.openDialog('End date on specified', 'Please select a valid end date');
        return;
      }
      if (this.shiftsReportStartDate.getTime() > this.shiftsReportEndDate.getTime()) {
        this.openDialog('Invalid End date', 'Please select a end date after start date');
        return;
      }

      const start = this.datePipe.transform(this.shiftsReportStartDate, 'yyyy-MM-dd');
      const end = this.datePipe.transform(this.shiftsReportEndDate, 'yyyy-MM-dd');;

      this.router.navigate(['Shifts_report', 
        { type: this.shiftsReportType, 
          start:start,
          end:end,
          skipLocationChange: true}]);

    } else  if (this.activeTab == 'registration') {
      if ((this.registrationReportType == undefined) || (this.registrationReportType.length < 1) || (this.registrationReportType == 'none')) {
        this.openDialog('Type on specified', 'Please select a valid report type from dropdown menu');
        return;
      }

      if (this.registrationReportStartDate == undefined) {
        this.openDialog('Start date on specified', 'Please select a valid start date');
        return;
      }
      if (this.registrationReportEndDate == undefined) {
        this.openDialog('End date on specified', 'Please select a valid end date');
        return;
      }
      if (this.registrationReportStartDate.getTime() > this.registrationReportEndDate.getTime()) {
        this.openDialog('Invalid End date', 'Please select a end date after start date');
        return;
      }

      const start = this.datePipe.transform(this.registrationReportStartDate, 'yyyy-MM-dd');
      const end = this.datePipe.transform(this.registrationReportEndDate, 'yyyy-MM-dd');;

      this.router.navigate(['Registration_report', 
        { type: this.registrationReportType, 
          start:start,
          end:end,
          skipLocationChange: true}]);
    }
  }

  queryUsersData() {

    this.usersReportStartDate.setHours(0,0,0,0);
    var beginningOfStartDay = this.usersReportStartDate.getTime();

    this.usersReportEndDate.setHours(23,59,59,999);
    var endOfEndDay = this.usersReportEndDate.getTime();

    var aCollection = this.firestore.collection('profiles', ref => 
      ref.where('approvalStatus','==','approved')
      .where('profileType', '==', 'worker')
    );
  
    this.usersSubscription = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(aList => {
      var workers = [];
      var companies = [];

      for (const aWorker of aList) {
        var timeStamp = aWorker['createdAt'].toDate().getTime();
        if ((timeStamp >= beginningOfStartDay) && (timeStamp <= endOfEndDay)) {
          workers.push(aWorker);
        }
      }

      if (this.usersSubscription != undefined) {
        this.usersSubscription.unsubscribe();
        this.usersSubscription = undefined;
      }

      var bCollection = this.firestore.collection('companies', ref => 
        ref.where('approvalStatus','==','approved')
      );
    
      this.companiesSubscription = bCollection.snapshotChanges().pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        }))
      ).subscribe(bList => {

        var companies = [];

        for (const aCompany of bList) {
          var timeStamp = aCompany['createdAt'].toDate().getTime();
          if ((timeStamp >= beginningOfStartDay) && (timeStamp <= endOfEndDay)) {
            companies.push(aCompany);
          }
        }

        if (this.companiesSubscription != undefined) {
          this.companiesSubscription.unsubscribe();
          this.companiesSubscription = undefined;
        }

        this.generateUsersPDF(workers, companies);

      });

    });

  }

  generateUsersPDF(workers, companies) {

    var workerByCategoriesMap = {};
    var companiesByCategoriesMap = {};

    for (const aCat of this.categories) {
      workerByCategoriesMap[aCat] = [];
      companiesByCategoriesMap[aCat] = [];
    }


    if ((workers != undefined) && (workers.length > 0)) {
      for (const aWorker of workers) {
        const category = aWorker.wProfession;
        var arra = workerByCategoriesMap[category];
        if (arra == undefined) {
          arra = [];
        }
        arra.push(aWorker);
        workerByCategoriesMap[category] = arra;
      }
    } 

    if ((companies != undefined) && (companies.length > 0)) {
      for (const aCompany of companies) {
        const category = aCompany.industry;
        var arra = companiesByCategoriesMap[category];
        if (arra == undefined) {
          arra = [];
        }
        arra.push(aCompany);
        companiesByCategoriesMap[category] = arra;
      }
    } 


    console.log(workerByCategoriesMap);
    console.log(companiesByCategoriesMap);

    this.httpClient.get("https://ultraagent.co.uk/assets/reporttemplates/usersReport.html", { responseType: 'text' })
    .subscribe(
      data => {
        console.log("GET Request is successful ");
        console.log(data);

        var pdf = new jspdf('p', 'in', 'letter');
        // source can be HTML-formatted string, or a reference
        // to an actual DOM element from which the text will be scraped.
        var source = data;

        // we support special element handlers. Register them with jQuery-style 
        // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
        // There is no support for any other type of selectors 
        // (class, of compound) at this time.
        var specialElementHandlers = {
            // element with id of "bypass" - jQuery style selector
            '#bypassme': function (element, renderer) {
                // true = "handled elsewhere, bypass text extraction"
                return true
            }
        };

        var margins = {
          top: 0.5,
          bottom: 0.5,
          left: 0.5,
          width: 7.5
        };
        // all coords and widths are in jsPDF instance's declared units
        // 'inches' in this case
        pdf.fromHTML(
        source, // HTML string or DOM elem ref.
        margins.left, // x coord
        margins.top, { // y coord
            'width': margins.width, // max width of content on PDF
            'elementHandlers': specialElementHandlers
        }, function (dispose) {
            // dispose: object with X, Y of the last line add to the PDF 
            //          this allow the insertion of new lines after html
            pdf.save('users.pdf');
        }, margins);

          

      },
      error => {
        console.log("Error", error);
        this.openDialog("Error", error.message);
      }
    );   
    

  }

  openDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
    
  }

}
