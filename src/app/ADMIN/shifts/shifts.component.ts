import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';
import { AmendDialogComponent } from '../../dialogs/amend-dialog/amend-dialog.component';

import { PostedBookingsCancelledOngoingApprovedDisputedDetailsDialogComponent } 
from '../../dialogs/posted-bookings-cancelled-ongoing-approved-disputed-details-dialog/posted-bookings-cancelled-ongoing-approved-disputed-details-dialog.component';

@Component({
  selector: 'app-shifts',
  templateUrl: './shifts.component.html',
  styleUrls: ['./shifts.component.css']
})
export class ShiftsComponent implements OnInit {

  filterDate:Date = undefined;

  sortKey = "";
  
  shiftslist:any;
  profileslist:any;
  companieslist:any;

  blist = [];


  hideNewShiftsThisWeekTitle = false;
  hideBookingsShiftsTitle = true;
  hideCancelledShiftsTitle = true;
  hideOngoingShiftsTitle = true;
  hideApprovedShiftsTitle = true;
  hideOpenDisputesShiftsTitle = true;
  hideResolvedDisputesShiftsTitle = true;

  NewShiftsThisWeek = 0;

  private shiftSubscription: Subscription;
  private profileSubscription: Subscription;
  private bookingSubscription: Subscription;

  private companiesSubscription: Subscription;

  constructor(public dialog: MatDialog, private authService: AuthService,
    private router: Router,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,) { }

    // DIALOG FOR POSTED SHIFT DETAILS
    openPostedDetailsDialog(aShift): void {

      const dialogRef = this.dialog.open(PostedBookingsCancelledOngoingApprovedDisputedDetailsDialogComponent, {
        panelClass: 'my-panel',
        width: '900px',
      });
  
      dialogRef.componentInstance.title='POSTED SHIFT';
      dialogRef.componentInstance.subtitle='SHIFT NO: '+aShift.subcategory+' #'+aShift.shiftNo;
      dialogRef.componentInstance.shift = aShift;
      dialogRef.componentInstance.interestedWorkersHidden = true;
      dialogRef.componentInstance.selectedWorkersHidden = true;
      dialogRef.componentInstance.workerDetailHidden = true;
      dialogRef.componentInstance.startEndTimeHidden = true;
      dialogRef.componentInstance.reasonForCancellationHidden = true;
      dialogRef.componentInstance.employerReviewHidden = true;
      dialogRef.componentInstance.penaliseWorkerHidden = true;
      dialogRef.componentInstance.tracktimesheetHidden = true;

       dialogRef.afterClosed().subscribe(result => {
        console.log(`The dialog was closed: ${result}`);
      });
    }

    ngOnInit() {
      this.updateShiftsList();
    }

    ngOnDestroy() {
      if (this.shiftSubscription != undefined){
        this.shiftSubscription.unsubscribe();
      }
      if (this.profileSubscription != undefined){
        this.profileSubscription.unsubscribe();
      }
      if (this.bookingSubscription != undefined){
        this.bookingSubscription.unsubscribe();
      }
      if (this.companiesSubscription != undefined){
        this.companiesSubscription.unsubscribe();
      }
    }

    updateBookings(){
      const aCollection = this.firestore.collection('bookings');
      // .snapshotChanges() returns a DocumentChangeAction[], which contains
      // a lot of information about "what happened" with each change. If you want to
      // get the data and the id use the map operator.
      this.bookingSubscription = aCollection.snapshotChanges().pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        }))
      ).subscribe(someList => {
        if(someList != undefined){
          this.blist = someList;
        }

        var bCollection = this.firestore.collection('profiles');
        this.profileSubscription = bCollection.snapshotChanges().pipe(
          map(actions => actions.map(a => {
            const data = a.payload.doc.data();
            const id = a.payload.doc.id;
            return { id, ...data };
          }))
        ).subscribe(profileslist => {
            this.profileslist = profileslist;
            //console.log(this.profileslist);
  
            //Associate incomplete bookings with each profile
            for(var i=0; i<this.profileslist.length; i++){
              var workerId = this.profileslist[i].userId;
              var pArr = [];
              for(var j=0; j<this.blist.length; j++){
                if(this.blist[j].workerId == workerId){
                  if((this.blist[j].workingStatus == undefined) || (this.blist[j].workingStatus != 'completed')){
                    pArr.push(this.blist[j]); 
                  }
                }
              }
              this.profileslist[i].incompleteBookings = pArr; 
            }

            for(var i=0; i<this.blist.length; i++){
              var workerId = this.blist[i].workerId;
              for(var j=0; j<this.profileslist.length; j++){
                if(this.profileslist[j].id == workerId){
                  this.blist[i].worker = this.profileslist[j];
                }
              }
            }

            for(var j=0; j<this.blist.length; j++){
              for(var i=0; i<this.shiftslist.length; i++){
                if(this.blist[j].shiftId == this.shiftslist[i].id){
                  this.blist[j].shift = this.shiftslist[i];
                }
              }
            }
  
        });
        
       
        //console.log('booking fetched');
        //console.log(this.blist);
  
      });
    }
  
    updateShiftsList(){
      var aCollection = this.firestore.collection('shifts');
      // .snapshotChanges() returns a DocumentChangeAction[], which contains
      // a lot of information about "what happened" with each change. If you want to
      // get the data and the id use the map operator.
      this.shiftSubscription = aCollection.snapshotChanges().pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        }))
      ).subscribe(shiftslist => {
          this.shiftslist = shiftslist;
          //console.log(this.shiftslist);

          this.NewShiftsThisWeek = 0;
          if(this.shiftslist != undefined) {
            this.NewShiftsThisWeek += this.shiftslist.length;
          }

        var bCollection = this.firestore.collection('companies');
        this.companiesSubscription = bCollection.snapshotChanges().pipe(
          map(actions => actions.map(a => {
            const data = a.payload.doc.data();
            const id = a.payload.doc.id;
            return { id, ...data };
          }))
        ).subscribe(companieslist => {
            this.companieslist = companieslist;
            //console.log(this.companieslist);
  
            for(var i=0; i<this.shiftslist.length; i++){
              var owner = this.shiftslist[i].owner;
              for(var j=0; j<this.companieslist.length; j++){
                if(this.companieslist[j].id == owner){
                  this.shiftslist[i].company = this.companieslist[j];
                }
              }
            }
  
        });

        this.updateBookings();

      });
       
     }

     onPostedTabSelected() {
      //console.log('onPostedTabSelected clicked');
      this.hideNewShiftsThisWeekTitle = false;
      this.hideBookingsShiftsTitle = true;
      this.hideCancelledShiftsTitle = true;
      this.hideOngoingShiftsTitle = true;
      this.hideApprovedShiftsTitle = true;
      this.hideOpenDisputesShiftsTitle = true;
      this.hideResolvedDisputesShiftsTitle = true;
    }
    onBookingsTabSelected() {
      //console.log('onBookingsTabSelected clicked');
      this.hideNewShiftsThisWeekTitle = true;
      this.hideBookingsShiftsTitle = false;
      this.hideCancelledShiftsTitle = true;
      this.hideOngoingShiftsTitle = true;
      this.hideApprovedShiftsTitle = true;
      this.hideOpenDisputesShiftsTitle = true;
      this.hideResolvedDisputesShiftsTitle = true;
    }
    onCancelledTabSelected() {
      //console.log('onCancelledTabSelected clicked');
      this.hideNewShiftsThisWeekTitle = true;
      this.hideBookingsShiftsTitle = true;
      this.hideCancelledShiftsTitle = false;
      this.hideOngoingShiftsTitle = true;
      this.hideApprovedShiftsTitle = true;
      this.hideOpenDisputesShiftsTitle = true;
      this.hideResolvedDisputesShiftsTitle = true;
    }
    onOnGoingTabSelected() {
      //console.log('onOnGoingTabSelected clicked');
      this.hideNewShiftsThisWeekTitle = true;
      this.hideBookingsShiftsTitle = true;
      this.hideCancelledShiftsTitle = true;
      this.hideOngoingShiftsTitle = false;
      this.hideApprovedShiftsTitle = true;
      this.hideOpenDisputesShiftsTitle = true;
      this.hideResolvedDisputesShiftsTitle = true;
    }
    onApprovedTabSelected() {
      //console.log('onApprovedTabSelected clicked');
      this.hideNewShiftsThisWeekTitle = true;
      this.hideBookingsShiftsTitle = true;
      this.hideCancelledShiftsTitle = true;
      this.hideOngoingShiftsTitle = true;
      this.hideApprovedShiftsTitle = false;
      this.hideOpenDisputesShiftsTitle = true;
      this.hideResolvedDisputesShiftsTitle = true;
    }
    onOpenDisputesTabSelected() {
      //console.log('onOpenDisputesTabSelected clicked');
      this.hideNewShiftsThisWeekTitle = true;
      this.hideBookingsShiftsTitle = true;
      this.hideCancelledShiftsTitle = true;
      this.hideOngoingShiftsTitle = true;
      this.hideApprovedShiftsTitle = true;
      this.hideOpenDisputesShiftsTitle = false;
      this.hideResolvedDisputesShiftsTitle = true;
    }
    onResolvedDisputesTabSelected() {
      //console.log('onResolvedDisputesTabSelected clicked');
      this.hideNewShiftsThisWeekTitle = true;
      this.hideBookingsShiftsTitle = true;
      this.hideCancelledShiftsTitle = true;
      this.hideOngoingShiftsTitle = true;
      this.hideApprovedShiftsTitle = true;
      this.hideOpenDisputesShiftsTitle = true;
      this.hideResolvedDisputesShiftsTitle = false;
    }

    openBookingDetailsDialog(booking) {
      const dialogRef = this.dialog.open(PostedBookingsCancelledOngoingApprovedDisputedDetailsDialogComponent, {
        panelClass: 'my-panel',
        width: '900px',
      });
  
      dialogRef.componentInstance.title='BOOKED SHIFT';
      dialogRef.componentInstance.subtitle='SHIFT NO: '+booking.shift.subcategory+' #'+booking.shiftNo;
      dialogRef.componentInstance.shift = booking.shift;
      dialogRef.componentInstance.booking = booking;
      dialogRef.componentInstance.bookedWorker = booking.worker;
      dialogRef.componentInstance.interestedWorkersHidden = true;
      dialogRef.componentInstance.selectedWorkersHidden = false;
      dialogRef.componentInstance.workerDetailHidden = true;
      dialogRef.componentInstance.startEndTimeHidden = true;
      dialogRef.componentInstance.reasonForCancellationHidden = true;
      dialogRef.componentInstance.employerReviewHidden = true;
      dialogRef.componentInstance.penaliseWorkerHidden = true;
      dialogRef.componentInstance.tracktimesheetHidden = true;

       dialogRef.afterClosed().subscribe(result => {
        //console.log(`The dialog was closed: ${result}`);
      });
    }

    openCancelledBookingDetailsDialog(booking) {
      const dialogRef = this.dialog.open(PostedBookingsCancelledOngoingApprovedDisputedDetailsDialogComponent, {
        panelClass: 'my-panel',
        width: '900px',
      });
  
      dialogRef.componentInstance.title='CANCELLED SHIFT';
      dialogRef.componentInstance.subtitle='SHIFT NO: '+booking.shift.subcategory+' #'+booking.shiftNo;
      dialogRef.componentInstance.shift = booking.shift;
      dialogRef.componentInstance.penaliseWorkerbutton = 'Penalise Worker';
      dialogRef.componentInstance.penaliseEmployerbutton = 'Penalise Employer';
      dialogRef.componentInstance.booking = booking;
      dialogRef.componentInstance.bookedWorker = booking.worker;
      dialogRef.componentInstance.interestedWorkersHidden = true;
      dialogRef.componentInstance.selectedWorkersHidden = true;
      dialogRef.componentInstance.workerDetailHidden = false;
      dialogRef.componentInstance.startEndTimeHidden = true;
      dialogRef.componentInstance.reasonForCancellationHidden = false;
      dialogRef.componentInstance.employerReviewHidden = true;
      dialogRef.componentInstance.penaliseWorkerHidden = false;
      dialogRef.componentInstance.tracktimesheetHidden = true;

       dialogRef.afterClosed().subscribe(result => {
        //console.log(`The dialog was closed: ${result}`);
      });
    }

    openOngoingBookingDetailsDialog(booking) {
      const dialogRef = this.dialog.open(PostedBookingsCancelledOngoingApprovedDisputedDetailsDialogComponent, {
        panelClass: 'my-panel',
        width: '900px',
      });
      var max = 2;

      var a = (booking.duration || '').split(':');

      for (var i = 0; i < max; i++) {
        a[i] = isNaN(parseInt(a[i])) ? 0 : parseInt(a[i]);
      }

      var hours = a[0];
      var minutes = a[1];
      var expectedEndTime = new Date();
      expectedEndTime.setTime (booking.workStartTime.toDate().getTime() + (hours*60+minutes)*60*1000);

      dialogRef.componentInstance.title='ONGOING SHIFT';
      dialogRef.componentInstance.subtitle='SHIFT NO: '+booking.shift.subcategory+' #'+booking.shiftNo;
      dialogRef.componentInstance.shift = booking.shift;
      dialogRef.componentInstance.booking = booking;
      dialogRef.componentInstance.expectedEndTime = expectedEndTime;
      dialogRef.componentInstance.bookedWorker = booking.worker;
      dialogRef.componentInstance.interestedWorkersHidden = true;
      dialogRef.componentInstance.selectedWorkersHidden = true;
      dialogRef.componentInstance.workerDetailHidden = false;
      dialogRef.componentInstance.startEndTimeHidden = false;
      dialogRef.componentInstance.reasonForCancellationHidden = false;
      dialogRef.componentInstance.employerReviewHidden = true;
      dialogRef.componentInstance.penaliseWorkerHidden = true;
      dialogRef.componentInstance.reasonForCancellationHidden = true;
      dialogRef.componentInstance.tracktimesheetHidden = true;

       dialogRef.afterClosed().subscribe(result => {
        //console.log(`The dialog was closed: ${result}`);
      });
    }

  openApprovedBookingDetailsDialog(booking) {
    const dialogRef = this.dialog.open(PostedBookingsCancelledOngoingApprovedDisputedDetailsDialogComponent, {
      panelClass: 'my-panel',
      width: '900px',
    });

    dialogRef.componentInstance.title='APPROVED SHIFT';
    dialogRef.componentInstance.subtitle='SHIFT NO: '+booking.shift.subcategory+' #'+booking.shiftNo;
    dialogRef.componentInstance.shift = booking.shift;
    dialogRef.componentInstance.buttonTitle = 'Track Shift in Timesheet'
    dialogRef.componentInstance.booking = booking;
    dialogRef.componentInstance.bookedWorker = booking.worker;
    dialogRef.componentInstance.interestedWorkersHidden = true;
    dialogRef.componentInstance.selectedWorkersHidden = true;
    dialogRef.componentInstance.workerDetailHidden = false;
    dialogRef.componentInstance.startEndTimeHidden = true;
    dialogRef.componentInstance.reasonForCancellationHidden = false;
    dialogRef.componentInstance.employerReviewHidden = false;
    dialogRef.componentInstance.penaliseWorkerHidden = true;
    dialogRef.componentInstance.reasonForCancellationHidden = true;
    dialogRef.componentInstance.tracktimesheetHidden = false;

    this.firestore.collection('profiles').
    doc(booking.workerId).collection('ratings').doc(booking.id).ref.get()
      .then((adoc) => {
        if (!adoc.exists) { 
          return; 
        }         
        var adata = adoc.data();
        adata.id = adoc.id;
        dialogRef.componentInstance.employerRating = adata;
        dialogRef.componentInstance.averageRating = (adata.commRating+adata.deliveryRating+adata.hireAgainRating)/3;
        
      })
      .catch((err) => {
        console.log(err);
      });

    //  dialogRef.afterClosed().subscribe(result => {
    //   console.log(`The dialog was closed: ${result}`);
    //   if (result == dialogRef.componentInstance.buttonTitle){
    //     // view details clicked
    //     this.router.navigate(['timesheet', {shiftId:booking.shiftId}]);
    //   }
    // });
    dialogRef.afterClosed().subscribe(result => {

      if (result == dialogRef.componentInstance.buttonTitle){
        // view details clicked
        this.router.navigate(['timesheet', {bookingId:booking.id}]);
      }
    });
  }

    openDisputeOpenBookingDetailsDialog(booking) {
      if ((booking.activeAmendId == undefined) || (booking.activeAmendId.length < 1)) { return; }

      const dialogRef = this.dialog.open(AmendDialogComponent, {
        panelClass: 'my-panel',
        width: '800px',
      });
  
      dialogRef.componentInstance.booking = booking;
      dialogRef.componentInstance.amend = undefined;
      dialogRef.componentInstance.hidePreviousProposalSection = true;
      dialogRef.componentInstance.hideApprovedProposalSection = true;
      dialogRef.componentInstance.hideProposalSubmitSection = false;
      dialogRef.componentInstance.title = 'OPEN DISPUTE SHIFT',
      dialogRef.componentInstance.proposalTitle = 'EMPLOYER PROPOSED CHANGES',

      this.firestore.collection('bookings').doc(booking.id)
      .collection('amends').doc(booking.activeAmendId)
      .ref
      .get()
      .then((adoc) => {
        if (!adoc.exists) { 
          return; 
        }
        var aAmend = adoc.data();
        aAmend.id = adoc.id;

        dialogRef.componentInstance.amend = aAmend;
        dialogRef.componentInstance.hidePreviousProposalSection = false;
        
      })
      .catch((err) => {
        console.log(err.message);
      })

      dialogRef.afterClosed().subscribe(result => {
        //console.log(`The dialog was closed: ${result}`);
       
      });
    }

    openDisputeResolvedBookingDetailsDialog(booking) {
      if ((booking.activeAmendId == undefined) || (booking.activeAmendId.length < 1)) { return; }

      const dialogRef = this.dialog.open(AmendDialogComponent, {
        panelClass: 'my-panel',
        width: '800px',
      });
  
      dialogRef.componentInstance.booking = booking;
      dialogRef.componentInstance.amend = undefined;
      dialogRef.componentInstance.hidePreviousProposalSection = true;
      dialogRef.componentInstance.hideApprovedProposalSection = true;
      dialogRef.componentInstance.hideProposalSubmitSection = true;
      dialogRef.componentInstance.title = 'RESOLVED DISPUTE SHIFT'
      dialogRef.componentInstance.proposalTitle = 'EMPLOYER PROPOSAL',

      this.firestore.collection('bookings').doc(booking.id)
      .collection('amends').doc(booking.activeAmendId)
      .ref
      .get()
      .then((adoc) => {
        if (!adoc.exists) { 
          return; 
        }
        var aAmend = adoc.data();
        aAmend.id = adoc.id;

        dialogRef.componentInstance.amend = aAmend;
        dialogRef.componentInstance.hidePreviousProposalSection = false;
        dialogRef.componentInstance.hideApprovedProposalSection = false;
        
      })
      .catch((err) => {
        console.log(err.message);
      })

      dialogRef.afterClosed().subscribe(result => {
        //console.log(`The dialog was closed: ${result}`);
       
      });
    }

}
