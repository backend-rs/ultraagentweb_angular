import { Component, ViewChild, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { MessagingService } from "../../services/messaging.service";

import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {

  message=null;

  @ViewChild('sidenav') sidenav: any;
  navMode = 'side';
  profile:any;

  unreadNoticesCount = 0;

  private noticeCollectionSub: Subscription;

  constructor(private router: Router, public authService: AuthService,
    private messagingService: MessagingService,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth){
    this.profile = this.authService.loggedInProfile;
  }

  ngOnInit() {
    const userId = this.profile.userId;
    this.messagingService.requestPermission(userId)
    this.messagingService.receiveMessage()
    this.message = this.messagingService.currentMessage
    console.log(this.message.getValue());

    this.updateNoticesList();
  }

  ngOnDestroy() {
    this.noticeCollectionSub.unsubscribe();
  }

  updateNoticesList(){
    const aCollection = this.firestore.collection('profiles')
    .doc(this.authService.loggedInProfile.userId)
    .collection<any>('notices');
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.noticeCollectionSub = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(noticeslist => {
      let cnt = 0;
      if ((noticeslist != undefined) && (noticeslist.length > 0)) {
        for (const aNotice of noticeslist) {
          if (aNotice.isRead == false) {
            cnt++;
          }
        }
      }
      this.unreadNoticesCount = cnt;
    });
  }

  clickOnLogOut(){
    this.authService.doLogout().then(() =>{
      this.router.navigate(['Admin_login']);
    });
  }
  
  notificationClick() {
    this.router.navigate(['admin_dashboard', {show: 'notifications'}]);
  }


  @HostListener('window:resize', ['$event'])
  onResize(event) {
      if (event.target.innerWidth < 768) {
          this.navMode = 'over';
          this.sidenav.close();
      }
      if (event.target.innerWidth > 768) {
         this.navMode = 'side';
         this.sidenav.open();
      }
  }

}
