import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DetailsDialogBoxComponent } from '../details-dialog-box/details-dialog-box.component';
import { NoticeDetailsDialogComponent } from '../../dialogs/notice-details-dialog/notice-details-dialog.component';
import { ApproveRejectDialogComponent } 
from '../../dialogs/approve-reject-dialog/approve-reject-dialog.component';
import { SuccessOkDialogComponent } 
from '../../dialogs/success-ok-dialog/success-ok-dialog.component';

import { AuthService } from '../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators }from '@angular/forms';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';
import * as moment from 'moment';

import { AdminShiftsDisputesOpenFilterPipe } from '../../filters/adminshiftsdisputesopen-filter.pipe';
import { AdminShiftsDisputesResolvedFilterPipe } from '../../filters/adminshiftsdisputesresolved-filter.pipe';
import { AdminTimesheetPaidFilterPipe } from '../../filters/admintimesheetpaid-filter.pipe';
import { AdminTimesheetPendingFilterPipe } from '../../filters/admintimesheetpending-filter.pipe';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [AdminShiftsDisputesOpenFilterPipe, AdminShiftsDisputesResolvedFilterPipe, 
    AdminTimesheetPaidFilterPipe, AdminTimesheetPendingFilterPipe]
})
export class DashboardComponent {
  dialogResult="";
  noticeslist:any;

  totalShiftCount =0;
  monthlyShiftCount = 0;
  pendingShiftCount = 0;
  completedShiftCount = 0;
  disputedShiftCount = 0;
  pendingShiftPerce = 0;
  completedShiftPerce = 0;
  disputedShiftPerce = 0;


  workersCount = 0;
  employersCount=0;

  topWorkers =[];
  topEmployers = [];

  unreadNoticesCount = 0;

  
  sub: any;

  private noticeCollectionSub: Subscription;
  private shiftSubscription: Subscription;
  private workersSubscription: Subscription;
  private employersSubscription: Subscription;
  private bookingSubscription: Subscription;


  // lineChart
  public shiftsLineChartData:Array<any> = [
    {data: [0], label: 'This Month'},
    {data: [0], label: 'Previous Month'},
   
  ];

  public shiftsLineChartLabels:Array<any> = ['1', '2', '3', '4', '5', '6', '7', '8', '9','10', '11', '12', '13', '14', '15',
  '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30'];

  public shiftsLineChartOptions:any = {
    responsive: false,
    events:[]
  };

  public shiftsLineChartColors:Array<any> = [
   
    { // brown
      backgroundColor: 'rgba(212,143,79,0.8)',
      borderColor: 'rgba(212,143,79,1)',
      pointBackgroundColor: 'rgba(212,143,79,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(212,143,79,1)'
    },
    { // blue
      backgroundColor: 'rgba(86, 116, 246,0.8)',
      borderColor: 'rgba(86, 116, 246,1)',
      pointBackgroundColor: 'rgba(86, 116, 246,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(86, 116, 246,0.8)'
    }
  ];

  public shiftsLineChartLegend:boolean = true;
  public shiftsLineChartType:string = 'line';


  // Bookings Line Chart
  public thisMonthEmployersLineChartData:Array<any> = [
    {data: [0, 7, 9, 10, 2], label: ''},   
  ];
  public thisMonthEmployersChartLabels:Array<any> = ['1', '2', '3', '4', '5', '6', '7', '8', '9','10', '11', '12', '13', '14', '15',
  '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];

  public thisMonthWorkersLineChartData:Array<any> = [
    {data: [0, 7, 9, 10, 2], label: ''},   
  ];
  public thisMonthWorkersLineChartLabels:Array<any> = ['1', '2', '3', '4', '5', '6', '7', '8', '9','10', '11', '12', '13', '14', '15',
  '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];


  public bookingsLineChartOptions:any = {
    responsive: false,
    events:[],
    legend:false,
    scales: {
      xAxes: [{
          gridLines: {
              display:false
          },
          display: false
      }],
      yAxes: [{
          gridLines: {
              display:false
          },
          display: false   
      }]
    },
    elements: {
      point:{
          radius: 0
      }
    }

  };

  public bookingsLineChartColors:Array<any> = [
    { // blue
      backgroundColor: 'rgba(117, 142, 248,1)',
      borderColor: 'rgba(117, 142, 248,0)',
      pointBackgroundColor: 'rgba(117, 142, 248,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(117, 142, 248,0.8)'
    }
  ];

  public bookingsLineChartLegend:boolean = false;
  public bookingsLineChartType:string = 'line';
  
  // Doughnut
  public doughnutCompletedChartLabels:string[] = ['Completed', 'Completed-Inverse'];
  public doughnutCompletedChartData:number[] = [0, 100];
  public doughnutPendingChartLabels:string[] = ['Pending', 'Pending-Inverse'];
  public doughnutPendingChartData:number[] = [0, 100];
  public doughnutDisputedChartLabels:string[] = ['Disputed', 'Disputed-Inverse'];
  public doughnutDisputedChartData:number[] = [0, 100];
  public doughnutChartType:string = 'doughnut';
  public doughnutOptions =  {
    // This chart will not respond to mousemove, etc
    events: []
  }
  public doughnutColors:any[] = [
    { backgroundColor: ['rgba(212,143,79,1)', 'rgba(218,218,218,1)', ] },
    { borderColor: ["#AEEBF2", "#FEFFC9"] }];

  constructor(public dialog: MatDialog,
    private authService: AuthService,
    private fb: FormBuilder,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    private route: ActivatedRoute,
    private adminShiftsDisputesOpenFilterPipe: AdminShiftsDisputesOpenFilterPipe, 
    private adminShiftsDisputesResolvedFilterPipe: AdminShiftsDisputesResolvedFilterPipe, 
    private adminTimesheetPaidFilterPipe: AdminTimesheetPaidFilterPipe, 
    private adminTimesheetPendingFilterPipe: AdminTimesheetPendingFilterPipe,) {

    }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      console.log(params);
      if(params['show'] != undefined){
        if(params['show'] == 'notifications'){
          console.log('setting active tab');
          this.activeTab = 'notifications';
        }
      }
   });

    this.updateNoticesList();
    this.updateShiftsList();
    this.updateWorkersList();
    this.updateEmployersList();
    this.updateBookings();


  }

  ngOnDestroy() {
    this.sub.unsubscribe();
    this.noticeCollectionSub.unsubscribe();
    this.shiftSubscription.unsubscribe();
    this.workersSubscription.unsubscribe();
    this.employersSubscription.unsubscribe();
    this.bookingSubscription.unsubscribe();
  }

 
  // events on slice click
  public chartClicked(e:any):void {
    console.log(e);
  }
 
 // event on pie chart slice hover
  public chartHovered(e:any):void {
    console.log(e);
  }

  updateEmployersList(){
    var aCollection = this.firestore.collection('companies', ref => 
    ref.where('approvalStatus','==','approved')
  );
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.employersSubscription = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(employersList => {
      if (employersList != undefined) {
        this.employersCount = employersList.length;
        if (employersList.length > 0) {
          this.topEmployers[0] = employersList[0];
        }
        if (employersList.length > 1) {
          this.topEmployers[1] = employersList[1];
        }


        var employers = [];
        employers = employersList;

        var thisMonthEmployersByDateLabel = {};
        var beginningOfThisMonth = moment().startOf('month').toDate().getTime();
        var endOfThisMonth = moment().endOf('month').toDate().getTime();

        for (const aCompany of employers) {
  
          var timeStamp = aCompany['createdAt'].toDate().getTime();
          var dateStr = moment(aCompany['createdAt'].toDate()).format('YYYY/MM/DD');
          if ((timeStamp >= beginningOfThisMonth) && (timeStamp <= endOfThisMonth)) {
            var set = thisMonthEmployersByDateLabel[dateStr];
            if (set == undefined) { set = new Set(); }
            set.add(aCompany.id);
            thisMonthEmployersByDateLabel[dateStr] = set;
          }
  
        }

        var todayTime = moment().toDate().getTime();
        var thisMonthDayWiseCount = [];
        for (var aDate = moment().startOf('month').toDate(); 
          aDate.getTime() < todayTime; 
          aDate = moment(aDate).add(1,'days').toDate()) {
          var dateStr = moment(aDate).format('YYYY/MM/DD');
          if (thisMonthEmployersByDateLabel[dateStr] != undefined) {
            thisMonthDayWiseCount.push(thisMonthEmployersByDateLabel[dateStr].size);
          } else {
            thisMonthDayWiseCount.push(0);
          }
        } 
        //console.log('this month companies');
        //console.log(thisMonthDayWiseCount);
        if (thisMonthDayWiseCount.length < 1) {
          this.thisMonthEmployersLineChartData = [{data: [0], label: ''}];
        } else {
          this.thisMonthEmployersLineChartData = [{data: thisMonthDayWiseCount, label: ''}];
        }

      } else {
        this.employersCount = 0;
        this.topEmployers = [];
      }
    });
     
   }

  updateWorkersList(){
    var aCollection = this.firestore.collection('profiles', ref => 
    ref.where('profileType','==','worker')
      .where('approvalStatus','==','approved')
  );
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.workersSubscription = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(workerslist => {
      if (workerslist != undefined) {
        this.workersCount = workerslist.length;

        if (workerslist.length > 0) {
          this.topWorkers[0] = workerslist[0];
        }
        if (workerslist.length > 1) {
          this.topWorkers[1] = workerslist[1];
        }

        var workers = [];
        workers = workerslist;

        var thisMonthWorkersByDateLabel = {};
        var beginningOfThisMonth = moment().startOf('month').toDate().getTime();
        var endOfThisMonth = moment().endOf('month').toDate().getTime();

        for (const aWorker of workers) {
  
          var timeStamp = aWorker['createdAt'].toDate().getTime();
          var dateStr = moment(aWorker['createdAt'].toDate()).format('YYYY/MM/DD');
          if ((timeStamp >= beginningOfThisMonth) && (timeStamp <= endOfThisMonth)) {
            var set = thisMonthWorkersByDateLabel[dateStr];
            if (set == undefined) { set = new Set(); }
            set.add(aWorker.userId);
            thisMonthWorkersByDateLabel[dateStr] = set;
          }
  
        }

        var todayTime = moment().toDate().getTime();
        var thisMonthDayWiseCount = [];
        for (var aDate = moment().startOf('month').toDate(); 
          aDate.getTime() < todayTime; 
          aDate = moment(aDate).add(1,'days').toDate()) {
          var dateStr = moment(aDate).format('YYYY/MM/DD');
          if (thisMonthWorkersByDateLabel[dateStr] != undefined) {
            thisMonthDayWiseCount.push(thisMonthWorkersByDateLabel[dateStr].size);
          } else {
            thisMonthDayWiseCount.push(0);
          }
        } 
        //console.log('this month booking');
        //console.log(thisMonthDayWiseCount);
        if (thisMonthDayWiseCount.length < 1) {
          this.thisMonthWorkersLineChartData = [{data: [0], label: ''}];
        } else {
          this.thisMonthWorkersLineChartData = [{data: thisMonthDayWiseCount, label: ''}];
        }

      } else {
        this.workersCount = 0;
        this.topWorkers = [];
      }
    });
     
   }

  updateShiftsList(){
    var aCollection = this.firestore.collection('shifts');
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.shiftSubscription = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(shiftslist => {
      var shifts = [];
      var thisMonthShiftsByDateLabel = {};
      var prevMonthShiftsByDateLabel = {};
      shifts = shiftslist;

     if (shiftslist != undefined) {
        this.totalShiftCount = shiftslist.length;
        this.monthlyShiftCount = 0;

        var beginningOfThisMonth = moment().startOf('month').toDate().getTime();
        var endOfThisMonth = moment().endOf('month').toDate().getTime();

        var beginningOfPrevMonth = moment().subtract(1,'months').startOf('month').toDate().getTime();
        var endOfPrevMonth = moment().subtract(1,'months').endOf('month').toDate().getTime();
      
    
        var thisMonthCount = 0;
        for (const aShift of shifts) {
          var timeStamp = aShift['createdAt'].toDate().getTime();
          var dayOfMonth = aShift['createdAt'].toDate().getDate() + '';
          if ((timeStamp >= beginningOfThisMonth) && (timeStamp <= endOfThisMonth)) {
            thisMonthCount++;
            var arr = thisMonthShiftsByDateLabel[dayOfMonth];
            if (arr == undefined) { arr = []; }
            arr.push(aShift);
            thisMonthShiftsByDateLabel[dayOfMonth] = arr;
          } else if ((timeStamp >= beginningOfPrevMonth) && (timeStamp <= endOfPrevMonth)) {
            var arr = prevMonthShiftsByDateLabel[dayOfMonth];
            if (arr == undefined) { arr = []; }
            arr.push(aShift);
            prevMonthShiftsByDateLabel[dayOfMonth] = arr;
          }
        }
        this.monthlyShiftCount = thisMonthCount;

        // Prepare previous month and this month day-wise data
        const thisMonthDays = moment().daysInMonth();
        const prevMonthDays = moment().subtract(1,'months').daysInMonth();
  
        var maxDays = thisMonthDays;
        if (prevMonthDays > thisMonthDays) {
          maxDays = prevMonthDays;
        }
  
        this.shiftsLineChartLabels = [];
        for (var i=0; i<maxDays; i++) {
          this.shiftsLineChartLabels.push((i+1)+'');
        }

        //console.log(thisMonthShiftsByDateLabel);
        //console.log(prevMonthShiftsByDateLabel);

        var todayDateOfMonth = moment().toDate().getDate();
        var thisMonthDayWiseCount = [];
        for (var i=0; i<todayDateOfMonth; i++) {
          var dateStr = (i+1)+'';
          if (thisMonthShiftsByDateLabel[dateStr] != undefined) {
            thisMonthDayWiseCount.push(thisMonthShiftsByDateLabel[dateStr].length);
          } else {
            thisMonthDayWiseCount.push(0);
          }
        } 

        var prevMonthDayWiseCount = [];
        for (var i=0; i<prevMonthDays; i++) {
          var dateStr = (i+1)+'';
          if (prevMonthShiftsByDateLabel[dateStr] != undefined) {
            prevMonthDayWiseCount.push(prevMonthShiftsByDateLabel[dateStr].length);
          } else {
            prevMonthDayWiseCount.push(0);
          }
        } 

        /*
        public shiftsLineChartData:Array<any> = [
          {data: [28, 48, 40, 19, 86, 27, 90], label: 'This Month'},
          {data: [65, 59, 80, 81, 56, 55, 40, 
            65, 59, 80, 81, 56, 55, 40, 
            65, 59, 80, 81, 56, 55, 40, 
            65, 59, 80, 81, 56, 55, 40,
            65, 59], label: 'Previous Month'},
        
        ];
        */

       var shiftsLineChartDataTemp = [];
       shiftsLineChartDataTemp.push({data: thisMonthDayWiseCount, label: 'This Month'});
       shiftsLineChartDataTemp.push({data: prevMonthDayWiseCount, label: 'Previous Month'});

       this.shiftsLineChartData = shiftsLineChartDataTemp;
      } else {
        this.totalShiftCount = 0;
        this.monthlyShiftCount = 0;
        this.pendingShiftCount = 0;
        this.disputedShiftCount = 0;
        this.completedShiftCount = 0;
        this.pendingShiftPerce = 0;
        this.completedShiftPerce = 0;
        this.disputedShiftPerce = 0;
      }
    });
     
   }

   updateBookings(){
    const aCollection = this.firestore.collection('bookings');
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.bookingSubscription = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(someList => {

      var blist = [];
      var thisMonthBookingsByDateLabel = {};
      var totalBookingsByDateLabel = {};

      if(someList != undefined){
        blist = someList;
      }
      
      var workerCompletedBlist = [];
      for (const aBook of blist) {
        if ((aBook.workingStatus != undefined) && (aBook.workingStatus == 'completed')) {
          workerCompletedBlist.push(aBook);
        }
      }

      var compls = this.adminTimesheetPaidFilterPipe.transform(workerCompletedBlist);
      var pends = this.adminTimesheetPendingFilterPipe.transform(workerCompletedBlist);
      var disptsClosed = this.adminShiftsDisputesResolvedFilterPipe.transform(workerCompletedBlist);
      var disptsOpen = this.adminShiftsDisputesOpenFilterPipe.transform(workerCompletedBlist);

      var completedSet = new Set();
      var pendingSet = new Set();
      var disputedSet = new Set();

      for (const aBook of disptsClosed) {
        disputedSet.add(aBook.shiftId);
      }
      for (const aBook of disptsOpen) {
        disputedSet.add(aBook.shiftId);
      }

      for (const aBook of pends) {
        if (disputedSet.has(aBook) == false) {
          pendingSet.add(aBook.shiftId);
        }
      }

      for (const aBook of compls) {
        if ((disputedSet.has(aBook) == false)  && (pendingSet.has(aBook) == false)) {
          completedSet.add(aBook.shiftId);
        }
      }


      this.pendingShiftCount = pendingSet.size;
      this.disputedShiftCount = disputedSet.size;
      this.completedShiftCount = completedSet.size;

      
      // console.log('boobking totalShiftCount');
      // console.log(this.totalShiftCount);

      if(this.totalShiftCount>0){
        this.pendingShiftPerce = parseInt((this.pendingShiftCount*100/this.totalShiftCount)+'');
        this.completedShiftPerce = parseInt((this.completedShiftCount*100/this.totalShiftCount)+'');
        this.disputedShiftPerce = parseInt((this.disputedShiftCount*100/this.totalShiftCount)+'');
      } else {
        this.pendingShiftPerce = 0;
        this.completedShiftPerce = 0;
        this.disputedShiftPerce = 0;
      }
      

      this.doughnutCompletedChartData = [this.completedShiftPerce, 100-this.completedShiftPerce];
      this.doughnutPendingChartData = [this.pendingShiftPerce, 100-this.pendingShiftPerce];
      this.doughnutDisputedChartData = [this.disputedShiftPerce, 100-this.disputedShiftPerce];

      
    });
  }

  updateNoticesList(){
    const aCollection = this.firestore.collection('profiles')
    .doc(this.authService.loggedInProfile.userId)
    .collection<any>('notices', ref => ref.orderBy('createdAt','desc'));
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.noticeCollectionSub = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(noticeslist => {
        this.noticeslist = noticeslist;
        let cnt = 0;
        if ((noticeslist != undefined) && (noticeslist.length > 0)) {
          for (const aNotice of noticeslist) {
            if (aNotice.isRead == false) {
              cnt++;
            }
          }
        }
        this.unreadNoticesCount = cnt;
        //console.log(this.noticeslist);
    });
  }
  
  openDialog(): void {
    const dialogRef = this.dialog.open(DetailsDialogBoxComponent, {
      panelClass: 'my-panel',
      width: '600px',
    });

     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      this.dialogResult = result;
    });
  }

  activeTab = 'summary';

  summary(activeTab){
    this.activeTab = activeTab;
  }

  notifications(activeTab){
    this.activeTab = activeTab;
  }

  openNoticeDetailsDialog(aNotice): void {
    const dialogRef = this.dialog.open(NoticeDetailsDialogComponent, {
      panelClass: 'my-panel',
      width: '600px',
    });

    if (aNotice.type == 'registrationNewWorker'){
      if ((aNotice.alreadyActed == undefined) || (aNotice.alreadyActed == false)) {
        dialogRef.componentInstance.buttonDisabled = false;
      } else {
        dialogRef.componentInstance.buttonDisabled = true;
      }

      dialogRef.componentInstance.buttonDisabled = true;
      dialogRef.componentInstance.subjectTitle = aNotice.workerTitle;
      dialogRef.componentInstance.p1 = aNotice.workerTitle + ' registered as new worker.';
    }

    if (aNotice.type == 'registrationNewCompany'){
      if ((aNotice.alreadyActed == undefined) || (aNotice.alreadyActed == false)) {
        dialogRef.componentInstance.buttonDisabled = false;
      } else {
        dialogRef.componentInstance.buttonDisabled = true;
      }

      dialogRef.componentInstance.buttonDisabled = true;
      dialogRef.componentInstance.subjectTitle = aNotice.companyTitle;
      dialogRef.componentInstance.p1 = aNotice.companyTitle + ' registered as new company.';
    }

    if (aNotice.type == 'shiftApproved'){
      if ((aNotice.alreadyActed == undefined) || (aNotice.alreadyActed == false)) {
        dialogRef.componentInstance.buttonDisabled = false;
      } else {
        dialogRef.componentInstance.buttonDisabled = true;
      }

      dialogRef.componentInstance.buttonDisabled = true;
      dialogRef.componentInstance.subjectTitle = aNotice.shiftTitle;
      dialogRef.componentInstance.p1 = aNotice.shiftTitle + ' approved.';
    }

    if (aNotice.type == 'workerRaisedDispute'){
      if ((aNotice.alreadyActed == undefined) || (aNotice.alreadyActed == false)) {
        dialogRef.componentInstance.buttonDisabled = false;
      } else {
        dialogRef.componentInstance.buttonDisabled = true;
      }

      dialogRef.componentInstance.buttonDisabled = true;
      dialogRef.componentInstance.subjectTitle = aNotice.shiftTitle;
      dialogRef.componentInstance.p1 = aNotice.shiftTitle + ' dispute raised.';
    }

    if (aNotice.isRead == false) {
      this.firestore.collection('profiles')
      .doc(this.authService.loggedInProfile.userId)
      .collection<any>('notices').doc(aNotice.id).update({isRead: true})
      .then()
      .catch((err) => {console.error(err)});
    }
  } 

  deleteButtonClicked(aNotice){
    const dialogRef = this.dialog.open(ApproveRejectDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = 'Are you sure?';
    dialogRef.componentInstance.descr = 
    "Do you really want to remove this entry";

    dialogRef.componentInstance.approveTitle = 'Yes, Remove';
    dialogRef.componentInstance.rejectTitle = 'No, Cancel';

    dialogRef.afterClosed().subscribe(result => {
      if (result == dialogRef.componentInstance.approveTitle) {
        this.firestore.collection('profiles').doc(this.authService.loggedInProfile.userId)
        .collection('notices').doc(aNotice.id).delete()
          .then(() => {
            // user created. now create profile in firestore.
            this.openInformDialog("SUCCESS","Entry removed successfully.");
          })
          .catch((error) => {
            console.log("Error", error);
            this.openInformDialog("Error", error.message);
          });
      }
    });
  }

  openInformDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
    
  }

}
