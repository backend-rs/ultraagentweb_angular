import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { WorkersDialogComponent } from '../workers-dialog/workers-dialog.component';
import { CompaniesDialogComponent } from '../companies-dialog/companies-dialog.component';

import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { Observable, Subscription, from } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';

import {UAGlobals} from '../../globals/uaglobals';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent {

  dialogResult="";
  filterSearchText="";
  filterCategoryText="";
  filterSubCategoryText="";
  workerslist=[];
  companieslist=[];
  filterSelections=[];
  
  hideAllWorkers = false;
  hideAllEmployers = true;

  private workersSubscription:Subscription;
  private companiesSubscription: Subscription;

  constructor(public dialog: MatDialog,
    private authService: AuthService,
    private router: Router,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    ) {

      const categories = Object.keys(UAGlobals.MASTER_CATEGORIES);
      for (const aCat of categories) {
        this.filterSelections.push({display:aCat,category:aCat,subcategory:''});
        // const subcats = UAGlobals.MASTER_CATEGORIES[aCat];
        // for (const aSubCat of subcats) {
        //   this.filterSelections.push({display:'    '+aSubCat,category:aCat,subcategory:aSubCat});
        // }
      }

      console.log(this.filterSelections);
    }

    ngOnInit() {
      this.updateWorkersList();
      this.updateCompaniesList();
    }
    ngOnDestroy() {
      this.workersSubscription.unsubscribe();
      this.companiesSubscription.unsubscribe();
    }

    filterWorkersSelected(selectedCat, selectedSubCat) {
      console.log('filterWorkersSelected='+selectedCat+", "+selectedSubCat);
      if (selectedCat.toLowerCase() == 'none') {
        this.filterCategoryText = "";
        this.filterSubCategoryText = "";
      } else {
        this.filterCategoryText = selectedCat;
        this.filterSubCategoryText = selectedSubCat;
      }
      return true;
    }
  
    updateWorkersList(){
      //console.log('updateWorkersList');
      //console.log(this.authService.companyProfile);
      var aCollection = this.firestore.collection('profiles', ref => 
      ref.where('profileType','==','worker')
        .where('approvalStatus','==','approved')
    );
      // .snapshotChanges() returns a DocumentChangeAction[], which contains
      // a lot of information about "what happened" with each change. If you want to
      // get the data and the id use the map operator.
      this.workersSubscription = aCollection.snapshotChanges().pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        }))
      ).subscribe(workerslist => {
        if (workerslist != undefined) {
         this.workerslist = workerslist;
         //console.log('workers');
         //console.log(workerslist);
        } else {
          this.workerslist = [];
        }
      });
       
     }

     updateCompaniesList(){
      var aCollection = this.firestore.collection('companies', ref => 
      ref.where('approvalStatus','==','approved')
    );
      // .snapshotChanges() returns a DocumentChangeAction[], which contains
      // a lot of information about "what happened" with each change. If you want to
      // get the data and the id use the map operator.
      this.companiesSubscription = aCollection.snapshotChanges().pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        }))
      ).subscribe(employersList => {
        if (employersList != undefined) {
         this.companieslist = employersList;
        } else {
         this.companieslist = [];
        }
      });
       
     }

onWorkersTabSelected() {
  console.log('onWorkersTabSelected clicked');
  this.hideAllWorkers = false;
  this.hideAllEmployers = true;
}
onCompaniesTabSelected() {
  console.log('onCompaniesTabSelected clicked');
  this.hideAllWorkers = true;
  this.hideAllEmployers = false;
}

  openWorkersDialog(aprofile): void {
    const dialogRef = this.dialog.open(WorkersDialogComponent, {
      panelClass: 'my-panel',
      width: '800px',
    });

    dialogRef.componentInstance.profile = aprofile;
    dialogRef.componentInstance.updateBookingsList();
    dialogRef.componentInstance.updateOngoingBookingsList();
    dialogRef.componentInstance.updatePendingBookingsList();
    dialogRef.componentInstance.updateOpenDisputeBookingsList();
    dialogRef.componentInstance.updateCloseDisputeBookingsList();

    dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      this.dialogResult = result;
    });
  }

  openCompaniesDialog(acompany): void {
    const dialogRef = this.dialog.open(CompaniesDialogComponent, {
      panelClass: 'my-panel',
      width: '800px',
    });

    dialogRef.componentInstance.company = acompany;
    dialogRef.componentInstance.updateBookingsList();
    dialogRef.componentInstance.updateOngoingBookingsList();
    dialogRef.componentInstance.updatePendingBookingsList();
    dialogRef.componentInstance.updateCompletedBookingsList();
    dialogRef.componentInstance.updateOpenDisputeBookingsList();
    dialogRef.componentInstance.updateCloseDisputeBookingsList();
    dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      this.dialogResult = result;
    });
  }

}
