import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-delete-dialogbox',
  templateUrl: './delete-dialogbox.component.html',
  styleUrls: ['./delete-dialogbox.component.css']
})
export class DeleteDialogboxComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeleteDialogboxComponent>, @Inject(MAT_DIALOG_DATA) public data:string) { }

  ngOnInit() {
  }

  cancelClick(): void {
    this.dialogRef.close();
  }

}
