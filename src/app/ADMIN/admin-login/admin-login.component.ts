import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { AdminGuard } from '../../services/adminGuard.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators }from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { NewshiftDoneDialogComponent } from '../../EMPLOYER/newshift-done-dialog/newshift-done-dialog.component';

//import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
import { AngularFirestore } from 'angularfire2/firestore';

import { Profile } from '../../models/profile.model';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  // public email='admin@ultraagent.co.uk';
  // public password = 'password';

  public email='';
  public password = '';

  constructor(
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    public dialog: MatDialog,
    private adminGuard: AdminGuard,
  ) { 
   
  }

  ngOnInit() {
    //console.log('calling adminGuard.canActivate');
    this.adminGuard.canActivate(null,null)
    .then((val) => {
      //console.log('adminGuard.canActivate returned '+val);
      if (val == true) {
        // User already logged-in why, show the login page
        this.router.navigate(['admin_dashboard']);
      }
    });
  }

  ngOnDestroy(){
  }

  clickOnLogin(){
    // For Empty email and password
    if ((this.email.trim().length < 1 && this.password.length < 1)) {
      this.openDialog('Invalid', "Please enter a valid email address and password.");
      return;
    }

    // For Empty email or lessthan 3 character
    if ((this.email == undefined) || (this.email.trim().length < 3)) {
      this.openDialog('Invalid', "Please enter a valid email address.");
      return;
    }

    // // For Empty password or lessthan 3 character
    // if ((this.password == undefined) || (this.password.length < 6)) {
    //   this.openDialog('Invalid', "Password should be minimum 6 characters.");
    //   return;
    // }

    // For Empty email or lessthan 8 character
    if ((this.password == undefined) || (this.password.length < 1)) {
      this.openDialog('Invalid', "Please enter a valid password.");
      return;
    }

    //For Empty password or lessthan 8 character
    // if ((this.password == undefined) || (!/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/.test(this.password))) {
    //   this.openDialog('Error', "The username or password you entered is incorrect");
    //   return;
    // }

    this.authService.doLogin(this.email.trim(), this.password)
    .then((auser) => {
      console.log(auser);
      if((auser != undefined) && (auser.uid != undefined )){
        this.firestore.collection('profiles').doc(auser.uid).ref.get()
        .then((adoc) => {
          if (!adoc.exists) { 
            this.openDialog('Error', 'Account has been deleted. Contact support.');
            return; 
          }         
          var aprofile = adoc.data();
          console.log(aprofile);
          //aprofile:Profile = aObject as Profile;
          if(aprofile != undefined){
            if(aprofile.approvalStatus != 'approved'){
              //dialog 
              if(aprofile.approvalStatus == 'pending'){
                this.openDialog('Oops!', "Your profile is in pending state.\n"+
                "We are verifying your documents. Once ready, we will intimate you via email.\n"+
                "Please contact admin if you have any queries.\nadmin@ultraagent.co.uk");
              }else{
                this.openDialog('Error', "Your account is in " + aprofile.approvalStatus + " state. Please contact support.");
              }
              this.authService.doLogout().then().catch();
              return;
            }
            if(aprofile.profileType == 'worker'){
              //dialog 
              this.openDialog('Error', "You don't have access to this portal. Please login from the worker app.");
              this.authService.doLogout().then().catch();
              return;
            }
            if(aprofile.profileType == 'company'){
              //dialog 
              this.openDialog('Error', "You are registered as an employer. Please login from employer portal.");
              this.authService.doLogout().then().catch();
              return;
            }
            this.router.navigate(['admin_dashboard']);
          }
        })
        .catch((err) => {
          console.log(err);
          this.openDialog('Error', err.message);
        });
      }
    })
    .catch((err) => {
      console.log(err);
      if ((err.code != undefined) && (err.code == "auth/wrong-password")) {
        this.openDialog('Error', 'The username or password you entered is incorrect');
      } else if ((err.code != undefined) && (err.code == "auth/user-not-found")) {
        this.openDialog('Error', 'There is no record corresponding to this username. Please contact your administrator to request access');
      } else {
        this.openDialog('Error', err.message);
      }
    });
  }

  openDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(NewshiftDoneDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    
  }


}
