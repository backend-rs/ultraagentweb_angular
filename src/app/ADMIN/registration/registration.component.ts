import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AdminRegistrationFullProfileDialogboxComponent } from '../../dialogs/admin-registration-full-profile-dialogbox/admin-registration-full-profile-dialogbox.component';
import { AdminRegistrationDeleteDialogboxComponent } from '../../dialogs/admin-registration-delete-dialogbox/admin-registration-delete-dialogbox.component';
import { AdminRegistrationApproveDialogboxComponent } from '../../dialogs/admin-registration-approve-dialogbox/admin-registration-approve-dialogbox.component';
import { AdminRegistrationRejectDialogboxComponent } from '../../dialogs/admin-registration-reject-dialogbox/admin-registration-reject-dialogbox.component';

import { HttpClient, HttpParams } from '@angular/common/http';

import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { ApproveRejectDialogComponent } from '../../dialogs/approve-reject-dialog/approve-reject-dialog.component';

//import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { Observable, Subscribable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';
import * as moment from 'moment';
import { TouchSequence } from 'selenium-webdriver';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent {

  filterDate:Date = undefined;
  dialogResult="";
 // profile: Profile = new Profile();
  profileslist:any;
  companieslist:any;
  workerRegistrationsThisWeek = 0;
  companyRegistrationsThisWeek = 0;
  companiesIdHashMap = {};

  hideNewWorkerRegistration = false;
  hideNewEmployerRegistration = true;

  private profilesSubscription:Subscription;
  private companiesSubscription:Subscription;
  constructor(public dialog: MatDialog,
    private authService: AuthService,
    private router: Router,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    private httpClient:HttpClient,
    ) {}

  ngOnInit() {
      this.updateProfileList();
      this.updateCompanyList();
  }

  ngOnDestroy() {
    this.profilesSubscription.unsubscribe();
    this.companiesSubscription.unsubscribe();
  }

  updateProfileList(){
    var aCollection = this.firestore.collection('profiles');
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.profilesSubscription =  aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(profileslist => {
        this.profileslist = profileslist;
        //console.log(this.profileslist);
        this.workerRegistrationsThisWeek = 0;
        if(this.profileslist != undefined) {
          // this.registrationsThisWeek += this.profileslist.length;
          var beginningOfThisWeek = moment().startOf('week').toDate().getTime();

          var profsThisWeek = [];
          for (const aProfile of this.profileslist) {
            var timeStamp = aProfile['createdAt'].toDate().getTime();
            if (timeStamp >= beginningOfThisWeek) {
              profsThisWeek.push(aProfile);
            }
          }
          this.workerRegistrationsThisWeek = profsThisWeek.length;
  
        }
       
    });
   }

   updateCompanyList(){
    var aCollection = this.firestore.collection('companies');
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.companiesSubscription =  aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(list => {
        this.companieslist = list;
        //console.log(this.companieslist);

        for (var i=0; i < this.companieslist.length; i++) {
          var aCompany = this.companieslist[i];
          this.companiesIdHashMap[aCompany.id] = aCompany;
        }

        this.companyRegistrationsThisWeek = 0;
        if(this.companieslist != undefined) {
          // this.registrationsThisWeek += this.profileslist.length;
          var beginningOfThisWeek = moment().startOf('week').toDate().getTime();

          var profsThisWeek = [];
          for (const aCompany of this.companieslist) {
            var timeStamp = aCompany['createdAt'].toDate().getTime();
            if (timeStamp >= beginningOfThisWeek) {
              profsThisWeek.push(aCompany);
            }
          }
          this.companyRegistrationsThisWeek = profsThisWeek.length;
  
        }

       

    });
   }

  //  recomputesAgentsList() {
  //   var tempArr = new Array();

  //   for (var i=0; i < this.profileslist.length; i++) {
  //     var aProf = this.profileslist[i];
  //     if ((aProf.companyId == undefined) || (aProf.companyId.length < 1)) { continue; }
  //     if (this.companiesIdHashMap[aProf.companyId])
  //   }
  //  }

  onWorkerTabSelected() {
    this.hideNewWorkerRegistration = false;
    this.hideNewEmployerRegistration = true;
  }

  onEmployerTabSelected() {
    this.hideNewWorkerRegistration = true;
    this.hideNewEmployerRegistration = false;
  }
   
//Workers full profile Dialog

  openWorkerFullprofileDialog(aProfile): void {
    console.log('openWorkerFullprofileDialog');
    const dialogRef = this.dialog.open(AdminRegistrationFullProfileDialogboxComponent, {
      panelClass: 'my-panel',
      width: '750px',
    });

     dialogRef.componentInstance.title='WORKER DETAILS';
     dialogRef.componentInstance.buttonTitle='Activate Worker';
     dialogRef.componentInstance.buttonTitle2='Decline Worker';

     dialogRef.componentInstance.profile = aProfile;
    //console.log(aProfile);
    dialogRef.afterClosed().subscribe(result => 
    {
      console.log(`The dialog was closed: ${result}`);
      if (result == dialogRef.componentInstance.buttonTitle) 
      {
        // Activate profile
        const dialogRef2 = this.dialog.open(ApproveRejectDialogComponent, {
          width: '600px',
        });
        dialogRef2.componentInstance.title = 'Confirmation';
        dialogRef2.componentInstance.descr = 
        "Do you really want to activate "+aProfile.firstName+" "+aProfile.lastName+" as worker?";
    
        dialogRef2.componentInstance.approveTitle = 'Yes, Activate';
        dialogRef2.componentInstance.rejectTitle = 'No, Cancel';
    
        dialogRef2.afterClosed().subscribe(aRes => {
          if (aRes == dialogRef2.componentInstance.approveTitle) {
            this.firestore.collection('profiles').doc(aProfile.id).set({approvalStatus:'approved'},{merge: true})
            .then(() => {
              this.sendAccountActivatedEmail(aProfile)
              .then(() => {
                this.openDialog("SUCCESS","Profile has been updated successfully");
              })
              .catch((err) => {
                console.log(err);
                //this.openDialog("SUCCESS","Profile has been updated successfully.\n\nBut, could not send activation intimation to user.");
                this.openDialog("SUCCESS","Profile has been updated successfully");
              });
            })
            .catch((error) => {
              console.log(error);
              this.openDialog("Error",error.message);
            });
          }
        });
      } else  if (result == dialogRef.componentInstance.buttonTitle2) 
      {
        // Declined profile
        const dialogRef2 = this.dialog.open(ApproveRejectDialogComponent, {
          width: '600px',
        });
        dialogRef2.componentInstance.title = 'Confirmation';
        dialogRef2.componentInstance.descr = 
        "Do you really want to decline "+aProfile.firstName+" "+aProfile.lastName+" as worker?";
    
        dialogRef2.componentInstance.approveTitle = 'Yes, Decline';
        dialogRef2.componentInstance.rejectTitle = 'No, Cancel';
    
        dialogRef2.afterClosed().subscribe(aRes => {
          if (aRes == dialogRef2.componentInstance.approveTitle) {
            this.firestore.collection('profiles').doc(aProfile.id).set({approvalStatus:'rejected'},{merge: true})
            .then(() => {
              this.sendAccountActivatedEmail(aProfile)
              .then(() => {
                this.openDialog("SUCCESS","Profile has been declined successfully");
              })
              .catch((err) => {
                console.log(err);
                //this.openDialog("SUCCESS","Profile has been updated successfully.\n\nBut, could not send activation intimation to user.");
                this.openDialog("SUCCESS","Profile has been declined successfully");
              });
            })
            .catch((error) => {
              console.log(error);
              this.openDialog("Error",error.message);
            });
          }
        });
      }
    });
  }

//Companies full profile Dialog

  openAgentsFullprofileDialog(aProfile): void {

    var aCompany = this.companiesIdHashMap[aProfile.companyId];
    if (aCompany == undefined) { return; }

    const dialogRef = this.dialog.open(AdminRegistrationFullProfileDialogboxComponent, {
      panelClass: 'my-panel',
      width: '750px',
    });

     dialogRef.componentInstance.title='AGENT DETAILS';
     dialogRef.componentInstance.buttonTitle='Activate Agent';
     dialogRef.componentInstance.profile = aProfile;
     dialogRef.componentInstance.company = aCompany;

     dialogRef.afterClosed().subscribe(result => 
      {
        console.log(`The dialog was closed: ${result}`);
        if (result == dialogRef.componentInstance.buttonTitle) 
        {
          // Activate profile
          const dialogRef2 = this.dialog.open(ApproveRejectDialogComponent, {
            width: '600px',
          });
          dialogRef2.componentInstance.title = 'Confirmation';
          dialogRef2.componentInstance.descr = 
          "Do you really want to activate "+aProfile.firstName+" "+aProfile.lastName+" as agent?";
      
          dialogRef2.componentInstance.approveTitle = 'Yes, Activate';
          dialogRef2.componentInstance.rejectTitle = 'No, Cancel';
      
          dialogRef2.afterClosed().subscribe(aRes => {
            if (aRes == dialogRef2.componentInstance.approveTitle) {
              this.firestore.collection('profiles').doc(aProfile.id).set({approvalStatus:'approved'},{merge: true})
              .then(() => {
                this.sendAccountActivatedEmail(aProfile)
                .then(() => {
                  this.openDialog("SUCCESS","Profile has been updated successfully");
                })
                .catch((err) => {
                  console.log(err);
                  //this.openDialog("SUCCESS","Profile has been updated successfully.\n\nBut, could not send activation intimation to user.");
                  this.openDialog("SUCCESS","Profile has been updated successfully");
                });
              })
              .catch((error) => {
                console.log(error);
                this.openDialog("Error",error.message);
              });
            }
          });
        }
      });
  }

  openCompaniesFullprofileDialog(aCompany): void {

    const dialogRef = this.dialog.open(AdminRegistrationFullProfileDialogboxComponent, {
      panelClass: 'my-panel',
      width: '750px',
    });

     dialogRef.componentInstance.title='COMPANY DETAILS';
     dialogRef.componentInstance.buttonTitle='Activate Company';
     dialogRef.componentInstance.buttonTitle2='Decline Company';
     dialogRef.componentInstance.company = aCompany;

     dialogRef.afterClosed().subscribe(result => 
      {
        console.log(`The dialog was closed: ${result}`);
        if (result == dialogRef.componentInstance.buttonTitle) 
        {
          // Activate profile
          const dialogRef2 = this.dialog.open(ApproveRejectDialogComponent, {
            width: '600px',
          });
          dialogRef2.componentInstance.title = 'Confirmation';
          dialogRef2.componentInstance.descr = 
          "Do you really want to activate "+aCompany.name+" as company?";
      
          dialogRef2.componentInstance.approveTitle = 'Yes, Activate';
          dialogRef2.componentInstance.rejectTitle = 'No, Cancel';
      
          dialogRef2.afterClosed().subscribe(aRes => {
            if (aRes == dialogRef2.componentInstance.approveTitle) {
              this.firestore.collection('profiles').doc(aCompany.id).set({approvalStatus:'approved'},{merge: true})
              .then(() => {
                this.firestore.collection('companies').doc(aCompany.id).set({approvalStatus:'approved'},{merge: true})
                .then(() => {
                  this.sendAccountActivatedEmail(aCompany)
                  .then(() => {
                    this.openDialog("SUCCESS","Profile has been updated successfully");
                  })
                  .catch((err) => {
                    console.log(err);
                    this.openDialog("SUCCESS","Profile has been updated successfully");
                    //this.openDialog("SUCCESS","Profile has been updated successfully.\n\nBut, could not send activation intimation to user.");
                  });
                })
                .catch((error) => {
                  console.log(error);
                  this.openDialog("Error",error.message);
                });
              })
              .catch((error) => {
                console.log(error);
                this.openDialog("Error",error.message);
              });
            }
          });
        } else  if (result == dialogRef.componentInstance.buttonTitle2) 
        {
          // Decline profile
          const dialogRef2 = this.dialog.open(ApproveRejectDialogComponent, {
            width: '600px',
          });
          dialogRef2.componentInstance.title = 'Confirmation';
          dialogRef2.componentInstance.descr = 
          "Do you really want to decline "+aCompany.name+" as company?";
      
          dialogRef2.componentInstance.approveTitle = 'Yes, Decline';
          dialogRef2.componentInstance.rejectTitle = 'No, Cancel';
      
          dialogRef2.afterClosed().subscribe(aRes => {
            if (aRes == dialogRef2.componentInstance.approveTitle) {
              this.firestore.collection('profiles').doc(aCompany.id).set({approvalStatus:'rejected'},{merge: true})
              .then(() => {
                this.firestore.collection('companies').doc(aCompany.id).set({approvalStatus:'rejected'},{merge: true})
                .then(() => {
                  this.sendAccountActivatedEmail(aCompany)
                  .then(() => {
                    this.openDialog("SUCCESS","Profile has been declined successfully");
                  })
                  .catch((err) => {
                    console.log(err);
                    this.openDialog("SUCCESS","Profile has been declined successfully");
                    //this.openDialog("SUCCESS","Profile has been updated successfully.\n\nBut, could not send activation intimation to user.");
                  });
                })
                .catch((error) => {
                  console.log(error);
                  this.openDialog("Error",error.message);
                });
              })
              .catch((error) => {
                console.log(error);
                this.openDialog("Error",error.message);
              });
            }
          });
        }
      });
  }

  openDeleteCompanyDialog(aCompany): void {

    const dialogRef2 = this.dialog.open(ApproveRejectDialogComponent, {
      width: '600px',
    });
    dialogRef2.componentInstance.title = 'Are you sure?';
    dialogRef2.componentInstance.descr = 
    "Do you really want to delete company by name "+aCompany.name+"?";

    dialogRef2.componentInstance.approveTitle = 'Yes, Delete';
    dialogRef2.componentInstance.rejectTitle = 'No, Cancel';

    dialogRef2.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      if (result == dialogRef2.componentInstance.approveTitle) {

        this.httpClient.post("https://x784f653.ultraagent.co.uk/deleteuser",
        {
            "uid": aCompany.id,
            "email": aCompany.email,
            "displayName": aCompany.name
        })
        .subscribe(
            data => {
                console.log("POST Request is successful ");
                //console.log(data);
                if ((data == undefined) || (data["message"] != undefined)) {
                  // show if any error message
                  this.openDialog("Error", data["code"]+'\n'+data["message"]);
                  // Now delete profile
                  this.firestore.collection('profiles').doc(aCompany.id).delete()
                  .then(() => {
                    this.firestore.collection('companies').doc(aCompany.id).delete()
                    .then(() => {
                      // user created. now create profile in firestore.
                      this.openDialog("SUCCESS","Company removed successfully.");
                    })
                    .catch((error) => {
                      console.log("Error", error);
                      this.openDialog("Error", error.message);
                    });
                  })
                  .catch((error) => {
                    console.log("Error", error);
                    this.openDialog("Error", error.message);
                  });
                } else {
                   // Now delete profile
                   this.firestore.collection('profiles').doc(aCompany.id).delete()
                   .then(() => {
                     this.firestore.collection('companies').doc(aCompany.id).delete()
                     .then(() => {
                       // user created. now create profile in firestore.
                       this.openDialog("SUCCESS","Company removed successfully.");
                     })
                     .catch((error) => {
                       console.log("Error", error);
                       this.openDialog("Error", error.message);
                     });
                   })
                   .catch((error) => {
                     console.log("Error", error);
                     this.openDialog("Error", error.message);
                   });
                }
            },
            error => {
                console.log("Error", error);
                this.openDialog("Error", error.message);
            }
        ); 
      }
    });
  }



  // Delete Dialog
  openDeleteProfileDialog(aUser): void {

    const dialogRef2 = this.dialog.open(ApproveRejectDialogComponent, {
      width: '600px',
    });
    dialogRef2.componentInstance.title = 'Are you sure?';
    dialogRef2.componentInstance.descr = 
    "Do you really want to delete "+aUser.firstName+" "+aUser.lastName+"?";

    dialogRef2.componentInstance.approveTitle = 'Yes, Delete';
    dialogRef2.componentInstance.rejectTitle = 'No, Cancel';

    dialogRef2.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      if (result == dialogRef2.componentInstance.approveTitle) {

        this.httpClient.post("https://x784f653.ultraagent.co.uk/deleteuser",
        {
            "uid": aUser.id,
            "email": aUser.email,
            "displayName": aUser.firstName+' '+aUser.lastName
        })
        .subscribe(
            data => {
                console.log("POST Request is successful ");
                //console.log(data);
                if ((data == undefined) || (data["message"] != undefined)) {
                  // show if any error message
                  this.openDialog("Error", data["code"]+'\n'+data["message"]);
                  // Now delete profile
                  this.firestore.collection('profiles').doc(aUser.id).delete()
                  .then(() => {
                    // user created. now create profile in firestore.
                   this.openDialog("SUCCESS","User removed successfully.");
                  })
                  .catch((error) => {
                   console.log("Error", error);
                   this.openDialog("Error", error.message);
                  });
                } else {
                   // Now delete profile
                   this.firestore.collection('profiles').doc(aUser.id).delete()
                   .then(() => {
                     // user created. now create profile in firestore.
                    this.openDialog("SUCCESS","User removed successfully.");
                   })
                   .catch((error) => {
                    console.log("Error", error);
                    this.openDialog("Error", error.message);
                   });
                  
                }
            },
            error => {
                console.log("Error", error);
                this.openDialog("Error", error.message);
            }
        ); 
      }
    });
  }

// Approve Dialog
  openApproveDialog(): void {

    const dialogRef = this.dialog.open(AdminRegistrationApproveDialogboxComponent, {
      panelClass: 'my-panel',
      width: '600px',
    });

     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      this.dialogResult = result;
    });
  }

//Reject Dialog

  openRejectDialog(): void {

    const dialogRef = this.dialog.open(AdminRegistrationRejectDialogboxComponent, {
      panelClass: 'my-panel',
      width: '600px',
    });

     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      this.dialogResult = result;
    });
  }

  openDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
    
  }

  sendAccountActivatedEmail(aProfile) {
    console.log("sendAccountActivatedEmail");
    //console.log(aProfile);
    let promise = new Promise((resolve, reject) => {
      const name =  aProfile.name != undefined ? aProfile.name : aProfile.firstName+" "+aProfile.lastName

      this.httpClient.post("https://x784f653.ultraagent.co.uk/accountactivated",
      {
          "email": aProfile.email,
          "displayName": name
      })
      .subscribe(
          data => {
              console.log("POST Request is successful ");
              //console.log(data);
              resolve();
          },
          error => {
              console.log("Error", error);
              this.openDialog("Error", error.message);
              reject(error);
          }
      ); 
    });
    return promise;
  }

}
