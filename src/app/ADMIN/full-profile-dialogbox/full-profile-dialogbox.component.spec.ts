import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FullProfileDialogboxComponent } from './full-profile-dialogbox.component';

describe('FullProfileDialogboxComponent', () => {
  let component: FullProfileDialogboxComponent;
  let fixture: ComponentFixture<FullProfileDialogboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FullProfileDialogboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullProfileDialogboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
