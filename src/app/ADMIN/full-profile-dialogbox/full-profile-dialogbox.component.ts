import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


@Component({
  selector: 'app-full-profile-dialogbox',
  templateUrl: './full-profile-dialogbox.component.html',
  styleUrls: ['./full-profile-dialogbox.component.css']
})
export class FullProfileDialogboxComponent implements OnInit{

  constructor(public dialogRef: MatDialogRef<FullProfileDialogboxComponent>, @Inject(MAT_DIALOG_DATA) public data:string) { }

  ngOnInit() {
  }

  cancelClick(): void {
    this.dialogRef.close();
  }

  generatePrint(){}
  generatePDF(){}
  
}
