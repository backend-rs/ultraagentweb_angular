import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Location } from '@angular/common';

import { AuthService } from '../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators }from '@angular/forms';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';

import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { ApproveRejectDialogComponent } from '../../dialogs/approve-reject-dialog/approve-reject-dialog.component';

@Component({
  selector: 'app-pay-timesheet-page',
  templateUrl: './pay-timesheet-page.component.html',
  styleUrls: ['./pay-timesheet-page.component.css']
})
export class PayTimesheetPageComponent implements OnInit {

  sub:any;
  booking:any;
  calculatedInvoiceNo='';
  calculatedPaymentNo='';

  constructor(public dialog: MatDialog,
    private authService: AuthService,
    private fb: FormBuilder,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,) {

  }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      console.log(params);
      if(params['bookingId'] != undefined){
        this.firestore.collection('bookings').doc(params['bookingId']).ref.get()
        .then((adoc) => {
          if (!adoc.exists) { 
            //this.openDialog('Error', 'Account has been deleted. Contact support.');
            return; 
          }         
          this.booking = adoc.data();
          this.booking.id = adoc.id;
          this.calculatedInvoiceNo = this.booking.shiftNo;
          this.calculatedPaymentNo = this.booking.shiftNo;

          console.log(this.booking);
        })
        .catch((err) => {
          console.log(err.message);
        });
      }
   });

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  saveClicked() {
    if((this.calculatedInvoiceNo == undefined) || (this.calculatedInvoiceNo.length < 1) ){
      this.openDialog('Invalid', "Please enter a valid Invoice No");
      return;
    }
    if((this.calculatedPaymentNo == undefined) || (this.calculatedPaymentNo.length < 1) ){
      this.openDialog('Invalid', "Please enter a valid Payment No");
      return;
    }

    const dialogRef = this.dialog.open(ApproveRejectDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = 'Are you sure?';
    dialogRef.componentInstance.descr = 
    "Do you really want to complete payment for this timesheet?";

    dialogRef.componentInstance.approveTitle = 'Yes, Complete Payment';
    dialogRef.componentInstance.rejectTitle = 'No, Cancel';

    dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      if (result == dialogRef.componentInstance.approveTitle) {
        this.firestore.collection("bookings").doc(this.booking.id).set({
          paymentStatus:'paid',
          calculatedInvoiceNo: this.calculatedInvoiceNo,
          calculatedPaymentNo: this.calculatedPaymentNo,
          paidAt: firebase.firestore.FieldValue.serverTimestamp()
        }, {merge:true})
        .then(() => {
          this.openDialog('Sucess','Timesheet paid successfully.');
          this.location.back();
          
        })
        .catch((err) => {
          console.log(err.message);
          this.openDialog('Error', err.message);
        });
      }
    });

  }

  openDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
    
  }

}
