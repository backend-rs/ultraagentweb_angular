import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayTimesheetPageComponent } from './pay-timesheet-page.component';

describe('PayTimesheetPageComponent', () => {
  let component: PayTimesheetPageComponent;
  let fixture: ComponentFixture<PayTimesheetPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayTimesheetPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayTimesheetPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
