import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-approve-dialogbox',
  templateUrl: './approve-dialogbox.component.html',
  styleUrls: ['./approve-dialogbox.component.css']
})
export class ApproveDialogboxComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ApproveDialogboxComponent>, @Inject(MAT_DIALOG_DATA) public data:string) { }

  ngOnInit() {
  }

  cancelClick(): void {
    this.dialogRef.close();
  }

}
