import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveDialogboxComponent } from './approve-dialogbox.component';

describe('ApproveDialogboxComponent', () => {
  let component: ApproveDialogboxComponent;
  let fixture: ComponentFixture<ApproveDialogboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveDialogboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveDialogboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
