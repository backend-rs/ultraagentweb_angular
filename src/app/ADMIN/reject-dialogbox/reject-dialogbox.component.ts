import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-reject-dialogbox',
  templateUrl: './reject-dialogbox.component.html',
  styleUrls: ['./reject-dialogbox.component.css']
})
export class RejectDialogboxComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<RejectDialogboxComponent>, @Inject(MAT_DIALOG_DATA) public data:string) { }

  ngOnInit() {
  }

  cancelClick(): void {
    this.dialogRef.close();
  }
}
