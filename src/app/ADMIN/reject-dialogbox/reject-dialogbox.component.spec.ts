import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectDialogboxComponent } from './reject-dialogbox.component';

describe('RejectDialogboxComponent', () => {
  let component: RejectDialogboxComponent;
  let fixture: ComponentFixture<RejectDialogboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectDialogboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectDialogboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
