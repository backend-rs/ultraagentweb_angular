import {Component} from '@angular/core';
import {MatDialog} from '@angular/material';
import { Location } from '@angular/common';
//import {ForgotPasswordDialogComponent} from '../forgot-password-dialog/forgot-password-dialog.component';
import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent {
  dialogResult="";
  email = '';
  sub:any;

  hideControlHeading: boolean = false;
  hideEmployerHeading: boolean = false;

  constructor(public dialog: MatDialog, private location: Location, 
    private authService: AuthService, 
    private afAuth: AngularFireAuth, 
    private firestore: AngularFirestore,
    private route: ActivatedRoute) {}

    ngOnInit() {
      this.sub = this.route.params.subscribe(params => {
         console.log(params);
          this.hideControlHeading = false;
          this.hideEmployerHeading = true;
         if(params['employer'] != undefined){
          if((params['employer'] == true) || (params['employer'] == 'true')){
            this.hideControlHeading = true;
            this.hideEmployerHeading = false;
          }
         }

      });
    }
  
    ngOnDestroy() {
      this.sub.unsubscribe();
    }

  // openDialog(): void {
  //   const dialogRef = this.dialog.open(ForgotPasswordDialogComponent, {
  //     width: '600px',
  //     data: 'this text will display in dialog'
  //   });

  //    dialogRef.afterClosed().subscribe(result => {
  //     console.log(`The dialog was closed: ${result}`);
  //     this.dialogResult = result;
  //   });
  // }

  onLoginClick() {
    this.location.back();
  }

  onSendCodeClick() {
    if((this.email == undefined) || (this.email.length < 1)) {
      return;
    }
    firebase.auth().sendPasswordResetEmail(this.email)
    .then(() => {
      this.openDialog('SUCCESS','\nPassword reset code has been sent.\nPlease check your email and follow the instructions.\n');
    })
    .catch((err) => {
      console.log(err);
      this.openDialog('Error', err.message);
    });

  }

  openDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
    
  }

}

