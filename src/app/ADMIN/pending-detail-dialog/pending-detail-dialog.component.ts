import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-pending-detail-dialog',
  templateUrl: './pending-detail-dialog.component.html',
  styleUrls: ['./pending-detail-dialog.component.css']
})
export class PendingDetailDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<PendingDetailDialogComponent>, @Inject(MAT_DIALOG_DATA) public data:string) { }

  ngOnInit() {
  }

  cancelClick(): void {
    this.dialogRef.close();
  }

}
