import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { HttpClient, HttpParams } from '@angular/common/http';

import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { ApproveRejectDialogComponent } from '../../dialogs/approve-reject-dialog/approve-reject-dialog.component';
import { UAGlobals } from '../../globals/uaglobals';

import * as firebase from 'firebase/app';

@Component({
  selector: 'app-admin-settings',
  templateUrl: './admin-settings.component.html',
  styleUrls: ['./admin-settings.component.css']
})
export class AdminSettingsComponent implements OnInit {

  selectedIndustryOnMargin:string='Select Industry';
  previousuamargin:string = '';
  newuamargin:string = '';
  uamarginsprevious: Map<string, string> = new Map<string, string>();
  uamargins: Map<string, string> = new Map<string, string>();
  oldPassword = '';
  newPassword = '';
  confirmNewPassword = '';
  newUser= {
    firstName: "",
    lastName: "",
    email: "",
    phoneNumber: "",
    jobTitle: "",
    password: "",
    permsShifts:true,
    permsRegistrations: false,
    permsTimesheets: false,
    permsUsers: false,
  };

  admins: any[];
  categories: any[];
  
  private adminsCollection: AngularFirestoreCollection;

  private adminSubscription:Subscription;
  private uamarginsSubscription:Subscription;

  constructor(public dialog: MatDialog,
    private authService: AuthService,
    private router: Router,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    private httpClient:HttpClient,) 
  {
      this.categories = Object.keys(UAGlobals.MASTER_CATEGORIES);
      for (const aCat of this.categories) {
        this.uamargins.set(aCat, '');
        this.uamarginsprevious.set(aCat, '');
      }
  }

    ngOnInit() {

      console.log(this.authService.companyProfile);
      // Query for Company Users
      this.adminsCollection = this.firestore.collection('profiles', ref => 
        ref.where('profileType','==','adminuser')
      );
      this.adminSubscription = this.adminsCollection
      .snapshotChanges().pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        }))
      ).subscribe(profileslist => {
          this.admins = profileslist;
          console.log(this.admins);
      });
  
    this.uamarginsSubscription = this.firestore.collection('globalsettings').doc('uamargins')
    .snapshotChanges().subscribe(aSnap => {
      if (aSnap.payload.exists) {
        console.log('uamargins');
        console.log(aSnap);
        console.log(aSnap.payload.data());
        const data = aSnap.payload.data();
        for (const aCat of this.categories) {
          if (data[aCat] != undefined) {
            this.uamarginsprevious.set(aCat, data[aCat]+'');
            if ((this.selectedIndustryOnMargin != undefined) &&
                (this.selectedIndustryOnMargin != 'Select Industry') && 
                (this.selectedIndustryOnMargin == aCat)) {  
              this.previousuamargin = data[aCat]+'';      
            }
          } else {
            this.uamarginsprevious.set(aCat, '');
          }
        }
      }
      
    });
  
    }
  
    ngOnDestroy() {
      this.adminSubscription.unsubscribe();
      //this.adminSubscription.unsubscribe();
      this.uamarginsSubscription.unsubscribe();
    }

    onMarginChangesSave() {
      console.log('onMarginChangesSave');

      let changes: Map<string, string> = new Map<string, string>();
      this.uamargins.forEach((value: string, key: string) => {
        console.log(key, value);
        if ((value != undefined) && (value != null) && (value.length > 0)) {
          changes.set(key, value);
        }
      });

      console.log('changes');
      console.log(changes);

      if (changes.size > 0) {
        var changeObject = {};
        changes.forEach((value: string, key: string) => {
          changeObject[key] = value;
        });
        this.firestore.collection('globalsettings').doc('uamargins').set(
          changeObject, {merge:true}
        )
        .then(() => {
          // Reset changes
          for (const aCat of this.categories) {
            this.uamargins.set(aCat, '');
          }
          this.newuamargin = '';

          this.openDialog('SUCCESS','Changes saved successfully.');
        })
        .catch((err) => {
          console.log(err.message);
          this.openDialog('Error', err.message);
        });
      }

    }

    onChangeSelectedIndustry(seletedIndustry) {
      console.log('onChangeSelectedIndustry: '+seletedIndustry);
      if (this.uamarginsprevious.get(seletedIndustry) != undefined) {
        this.previousuamargin = this.uamarginsprevious.get(seletedIndustry);
      } else {
        this.previousuamargin = '';
      }

      if (this.uamargins.get(seletedIndustry) != undefined) {
        this.newuamargin = this.uamargins.get(seletedIndustry);
      } else {
        this.newuamargin = '';
      }

    }

    onChangeNewMargin(newValue) {
      console.log('onChangeNewMargin: '+newValue);
      if ((this.selectedIndustryOnMargin == undefined) || 
          (this.selectedIndustryOnMargin == 'Select Industry')) {
        this.openDialog('Industry not selected', 'Select an industry first, and then enter the margin.');
        return;
      }
      if ((newValue == undefined)) {
        this.openDialog('Invalid margin', 'Please enter a value between 1 to 100');
        return;
      }
      if ((newValue.length > 0) && ((newValue < 1) || (newValue > 100))) {
        this.openDialog('Invalid margin', 'Please enter a value between 1 to 100');
        return;
      }
      if (this.uamargins.get(this.selectedIndustryOnMargin) != undefined) {
        this.uamargins.set(this.selectedIndustryOnMargin,newValue);
      }   
    }

    onNewUserSaveClicked() {
      console.log("onNewUserSaveClicked called");
      console.log(this.newUser);

      if ((this.newUser.email == undefined) || (this.newUser.email.length < 1)) {
        this.openDialog("Email not specified","Please enter a valid email.");
        return;
      }
      if ((this.newUser.firstName == undefined) || (this.newUser.firstName.length < 1)) {
        this.openDialog("First name not specified","Please enter a valid first name.");
        return;
      }
      if (!/^[a-zA-Z ]*$/.test(this.newUser.firstName)) {
        this.openDialog("Invalid","Please enter a valid first name.");
        return;
      }
      if ((this.newUser.lastName == undefined) || (this.newUser.lastName.length < 1)) {
        this.openDialog("Last name not specified","Please enter a valid last name.");
        return;
      }
      if (!/^[a-zA-Z ]*$/.test(this.newUser.lastName)) {
        this.openDialog("Invalid","Please enter a valid last name.");
        return;
      }
      if ((this.newUser.phoneNumber == undefined) || (this.newUser.phoneNumber.length < 10)) {
        this.openDialog("Phone number not specified","Please enter a valid phone number.");
        return;
      }
      if ((this.newUser.password == undefined) || (this.newUser.password.length < 1)) {
        this.openDialog("Password not specified","Please enter a valid password.");
        return;
      }
      if ((!/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/.test(this.newUser.password))) {
        this.openDialog("Invalid","Password should be Minimum 8 characters, at least one uppercase letter, one lowercase letter, one digit and special character");
        return;
      }
      if ((this.newUser.jobTitle == undefined) || (this.newUser.jobTitle.length < 1)) {
        this.openDialog("Job title not specified","Please enter a valid job title.");
        return;
      }
      if ((this.newUser.permsShifts == false) && (this.newUser.permsRegistrations == false)
        && (this.newUser.permsTimesheets == false) && (this.newUser.permsUsers == false)){
          this.openDialog("Rights not checked","Please check Rights.");
          return;
        }
  
      this.httpClient.post("https://x784f653.ultraagent.co.uk/createuser",
      {
          "email": this.newUser.email.trim().toLowerCase(),
          "phoneNumber": this.newUser.phoneNumber,
          "displayName": this.newUser.firstName+' '+this.newUser.lastName,
          "password": this.newUser.password,
          "senderName": this.authService.loggedInProfile.firstName+' '+this.authService.loggedInProfile.lastName,
          "loginUrl":"https://admin.ultraagent.co.uk",
          "jobTitle":this.newUser.jobTitle,
      })
      .subscribe(
          data => {
              console.log("POST Request is successful ");
              console.log(data);
              if ((data == undefined) || (data["uid"] == undefined) || (data["uid"].length < 1)) {
                // show if any error message
                this.openDialog("Error", data["code"]+'\n'+data["message"]);
              } else {
                // user created. now create profile in firestore.
  
                this.firestore.collection("profiles").doc(data["uid"]).set({
                  email: this.newUser.email,
                  firstName:this.newUser.firstName,
                  lastName: this.newUser.lastName,
                  profileType:'adminuser',
                  approvalStatus:'approved', //defaults to approved
                  userId: data["uid"],
                  phoneNumber: this.newUser.phoneNumber,
                  city: '',
                  address: '',
                  jobTitle: this.newUser.jobTitle,
                  permsShifts: this.newUser.permsShifts,
                  permsRegistrations: this.newUser.permsRegistrations,
                  permsTimesheets: this.newUser.permsTimesheets,
                  permsUsers: this.newUser.permsUsers,
                  sendNotifications: true,
                  createdAt: firebase.firestore.FieldValue.serverTimestamp()
                }, {merge: true})
                .then(_ => {
                  this.openDialog("SUCCESS","Admin user added successfully.");
                  this.newUser.email = '';
                  this.newUser.firstName = '';
                  this.newUser.lastName = '';
                  this.newUser.jobTitle = '';
                  this.newUser.phoneNumber = '';
                  this.newUser.password = '';
                  this.newUser.permsShifts = false;
                  this.newUser.permsRegistrations = false;
                  this.newUser.permsTimesheets = false;
                  this.newUser.permsUsers = false;

                  var adminTab = document.getElementById('admin-tab'); 
                  adminTab.click();
                })
                .catch((err) => {
                  console.log(err);
                  this.openDialog("Error",err.message);
                }); 
  
              }
          },
          error => {
              console.log("Error", error);
              this.openDialog("Error", error.message);
          }
      );     
    }

    clickOnChangePassword() {
      if ((this.oldPassword == undefined) || (this.oldPassword.length < 1)) {
        this.openDialog("Old password not specified","Please enter a valid old password.");
        return;
      }
      // if ((this.newPassword == undefined) || (this.confirmNewPassword == undefined) || 
      //   (this.newPassword.length < 6) || (this.confirmNewPassword.length < 6)) 
      // {
      //   this.openDialog("Password not valid","Password should be minimum 6 chars long.");
      //   return;
      // }

      if ((this.newPassword == undefined) || (this.confirmNewPassword == undefined) ||
      (!/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/.test(this.newPassword)) || 
      (!/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/.test(this.confirmNewPassword))) {
        this.openDialog("Password not valid","Password and Confirm password should be Minimum 8 characters, at least one uppercase letter, one lowercase letter, one digit and one special character.");
        return;
      }
  
      if (this.newPassword != this.confirmNewPassword) {
        this.openDialog("Password not valid","Password and Confirm password do not match.");
        return;
      }

      var cred = firebase.auth.EmailAuthProvider.credential(this.authService.loggedInProfile.email, this.oldPassword);
      this.afAuth.auth.currentUser.reauthenticateWithCredential(cred)
      .then(() => {
        this.afAuth.auth.currentUser.updatePassword(this.newPassword)
        .then(() => {
          this.openDialog("SUCCESS","Password changed successfully.");
        })
        .catch((err) => {
          console.log(err);
          this.openDialog("Error", err.message);
        });
      })
      .catch((err) => {
        console.log(err);
        this.openDialog("Error", "Old Password: "+err.message);
      });
  
  
      // this.httpClient.post("https://x784f653.ultraagent.co.uk/57few674",
      // {
      //     "uid": this.authService.loggedInProfile.id,
      //     "password": this.newPassword
      // })
      // .subscribe(
      //     data => {
      //         console.log("POST Request is successful ");
      //         console.log(data);
      //         if ((data == undefined) || (data["uid"] == undefined) || (data["uid"].length < 1)) {
      //           // show if any error message
      //           this.openDialog("Error", data["code"]+'\n'+data["message"]);
      //         } else {
      //           // user created. now create profile in firestore.
      //           this.openDialog("SUCCESS","Password changed successfully.");
      //         }
      //     },
      //     error => {
      //         console.log("Error", error);
      //         this.openDialog("Error", error.message);
      //     }
      // ); 
    }
  
    clickOnRemoveUser(aUser) {
      
      const dialogRef = this.dialog.open(ApproveRejectDialogComponent, {
        width: '600px',
      });
      dialogRef.componentInstance.title = 'Are you sure?';
      dialogRef.componentInstance.descr = 
      "Do you really want to remove "+aUser.firstName+" "+aUser.lastName+" as admin?";
  
      dialogRef.componentInstance.approveTitle = 'Yes, Remove';
      dialogRef.componentInstance.rejectTitle = 'No, Cancel';
  
      dialogRef.afterClosed().subscribe(result => {
        console.log(`The dialog was closed: ${result}`);
        if (result == dialogRef.componentInstance.approveTitle) {
  
          this.httpClient.post("https://x784f653.ultraagent.co.uk/deleteuser",
          {
              "uid": aUser.id,
              "email": aUser.email,
              "displayName": aUser.firstName+' '+aUser.lastName
          })
          .subscribe(
              data => {
                  console.log("POST Request is successful ");
                  console.log(data);
                  if ((data == undefined) || (data["message"] != undefined)) {
                    // show if any error message
                    this.openDialog("Error", data["code"]+'\n'+data["message"]);
                    // Now delete profile
                    this.firestore.collection('profiles').doc(aUser.id).delete()
                    .then(() => {
                      // user created. now create profile in firestore.
                     this.openDialog("SUCCESS","User removed successfully.");
                    })
                    .catch((error) => {
                     console.log("Error", error);
                     this.openDialog("Error", error.message);
                    });
                  } else {
                     // Now delete profile
                     this.firestore.collection('profiles').doc(aUser.id).delete()
                     .then(() => {
                       // user created. now create profile in firestore.
                      this.openDialog("SUCCESS","User removed successfully.");
                     })
                     .catch((error) => {
                      console.log("Error", error);
                      this.openDialog("Error", error.message);
                     });
                    
                  }
              },
              error => {
                  console.log("Error", error);
                  this.openDialog("Error", error.message);
              }
          ); 
        }
      });
    }

    clickOnEditUser(aUser) {
      this.router.navigate(['admin_setting_edit', {profile:JSON.stringify(aUser), skipLocationChange: true}]);
    }

  

    openDialog(title='', msg=''): void {
      const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
        width: '600px',
      });
      dialogRef.componentInstance.title = title;
      dialogRef.componentInstance.descr = msg;
      dialogRef.componentInstance.buttonTitle="OK";
      
    }
}
