import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsDialogBoxComponent } from './details-dialog-box.component';

describe('DetailsDialogBoxComponent', () => {
  let component: DetailsDialogBoxComponent;
  let fixture: ComponentFixture<DetailsDialogBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsDialogBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsDialogBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
