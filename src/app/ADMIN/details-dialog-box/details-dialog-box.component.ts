import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-details-dialog-box',
  templateUrl: './details-dialog-box.component.html',
  styleUrls: ['./details-dialog-box.component.css']
})
export class DetailsDialogBoxComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DetailsDialogBoxComponent>, @Inject(MAT_DIALOG_DATA) public data:string) { }

  ngOnInit() {
  }

  cancelClick(): void {
    this.dialogRef.close();
  }

  suspendWorkerCancelClick(): void {
    this.dialogRef.close();
  }

}
