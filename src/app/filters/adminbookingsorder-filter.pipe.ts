import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
    name: 'adminbookingsorderfilter',
    pure: false
})

export class AdminBookingsOrderFilterPipe implements PipeTransform {

  transform(items: any[], sortBy: string = undefined): any[] {
      //console.log("AdminBookingsOrderFilterPipe transform called");
    if (!items || items == undefined || items.length == 0) {
      return items;
    }

    items.sort( (a: any, b: any) => {

        if((sortBy == undefined) ||(sortBy.length<1) || (sortBy == 'Date')) {
            if(a.shiftStartDateTime == undefined){
                return -1;
            }
            if(b.shiftStartDateTime == undefined){
                return -1;
            }
            const aD = a.shiftStartDateTime.toDate();
            const bD = b.shiftStartDateTime.toDate();
            //console.log(aD+" "+a.shiftNo);
            //console.log(bD+" "+b.shiftNo);
            if (aD < bD) {
                return -1;
            } else if (aD > bD) {
                return 1;
            } else {
                return 0;
            }
        } else if (sortBy == 'Fee') {
            if(a.fee == undefined){
                return -1;
            }
            if(b.fee == undefined){
                return -1;
            }
            const aD = parseInt(a.fee+'');
            const bD = parseInt(b.fee+'');
            //console.log(aD+" "+a.shiftNo);
            //console.log(bD+" "+b.shiftNo);
            if (aD < bD) {
                return -1;
            } else if (aD > bD) {
                return 1;
            } else {
                return 0;
            }
        } else if (sortBy == 'City') {
            if(a.city == undefined){
                return -1;
            }
            if(b.city == undefined){
                return -1;
            }
            if (a.city < b.city) {
                return -1;
            } else if (a.city > b.city) {
                return 1;
            } else {
                return 0;
            }
        } else if (sortBy == 'Industry') {
            if(a.category == undefined){
                return -1;
            }
            if(b.category == undefined){
                return -1;
            }
            if (a.category < b.category) {
                return -1;
            } else if (a.category > b.category) {
                return 1;
            } else {
                return 0;
            }
        }
        
    });
    return items;
  }
  
}