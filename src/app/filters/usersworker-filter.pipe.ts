import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
    name: 'usersworkerfilter',
    pure: false
})
export class UsersWorkerFilterPipe implements PipeTransform {
  transform(items: any[], filterSearchText:string ="", filterCategoryText:string = "", filterSubCategoryText:string = ""): any[] {
     // console.log("UsersWorkerFilterPipe transform called");
    if (!items) {
      return items;
    }
    // filter items array, items which match and return true will be kept, false will be filtered out
    return items.filter((item: any) => {
       if ((item.profileType != undefined) && (item.profileType == 'worker') && 
        (item.approvalStatus != undefined) && (item.approvalStatus == 'approved')){

          if ((filterSearchText != undefined) && (filterSearchText.length > 0)) {
            if ( (item.firstName.toLowerCase().indexOf(filterSearchText.toLowerCase()) >= 0) ||
                 (item.lastName.toLowerCase().indexOf(filterSearchText.toLowerCase()) >= 0) ) {
              // good it matches
            } else {
              return false;
            } 
          }

          if ((filterCategoryText != undefined) && (filterCategoryText.length > 0)) {
            if (item.wProfession.toLowerCase().indexOf(filterCategoryText.toLowerCase()) >= 0) {
              // good it matches
            } else {
              return false;
            } 
          }

          if ((filterSubCategoryText != undefined) && (filterSubCategoryText.length > 0)) {
            if (item.wSubCategory.toLowerCase().indexOf(filterSubCategoryText.toLowerCase()) >= 0) {
              // good it matches
            } else {
              return false;
            } 
          }
          
          return true;
       }
       return false;
    });
  }
  
}