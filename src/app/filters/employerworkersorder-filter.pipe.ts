import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
    name: 'employerworkersorderfilter',
    pure: false
})
export class EmployerWorkersOrderFilterPipe implements PipeTransform {
  transform(items: any[]): any[] {
    //  console.log("EmployerWorkersOrderFilterPipe transform called");
    if (!items || items == undefined || items.length == 0) {
      return items;
    }

    items.sort( (a: any, b: any) => {
        if(a.createdAt == undefined){
            return -1;
        }
        if(b.createdAt == undefined){
            return -1;
        }
        if (a.createdAt < b.createdAt) {
            return -1;
        } else if (a.createdAt > b.createdAt) {
            return 1;
        } else {
            return 0;
        }
    });
    return items;
  }
  
}