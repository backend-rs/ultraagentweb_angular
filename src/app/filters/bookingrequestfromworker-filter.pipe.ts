import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
    name: 'bookingrequestfromworker',
    pure: false
})
export class BookingRequestFromWorkerFilterPipe implements PipeTransform {

  transform(items: any[], filterDate:Date = undefined): any[] {
    //  console.log("ShiftsAdvancedBookingFilterPipe transform called");
    if (!items) {
      return items;
    }
    // filter items array, items which match and return true will be kept, false will be filtered out
    return items.filter((item: any) => {

       if ((item.isAdvanceBooking != undefined) && (item.isAdvanceBooking == true)){
            return false;
       }
       return true;
    });
  }
  
}