import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
    name: 'userscompanyfilter',
    pure: false
})
export class UsersCompanyFilterPipe implements PipeTransform {
  transform(items: any[], filterSearchText:string ="", filterCategoryText:string = "", filterSubCategoryText:string = ""): any[] {
    // console.log("UsersWorkerFilterPipe transform called");
   if (!items) {
     return items;
   }
   // filter items array, items which match and return true will be kept, false will be filtered out
   return items.filter((item: any) => {
      if ((item.approvalStatus != undefined) && (item.approvalStatus == 'approved')){

         if ((filterSearchText != undefined) && (filterSearchText.length > 0)) {
           if (item.name.toLowerCase().indexOf(filterSearchText.toLowerCase()) >= 0) {
             // good it matches
           } else {
             return false;
           } 
         }

         if ((filterCategoryText != undefined) && (filterCategoryText.length > 0)) {
           if (item.industry.toLowerCase().indexOf(filterCategoryText.toLowerCase()) >= 0) {
             // good it matches
           } else {
             return false;
           } 
         }

         // Bharat: Company has no subcategory to compare
        //  if ((filterSubCategoryText != undefined) && (filterSubCategoryText.length > 0)) {
        //    if (item.wSubCategory.toLowerCase().indexOf(filterSubCategoryText.toLowerCase()) >= 0) {
        //      // good it matches
        //    } else {
        //      return false;
        //    } 
        //  }
         
         return true;
      }
      return false;
   });
 }
  
}