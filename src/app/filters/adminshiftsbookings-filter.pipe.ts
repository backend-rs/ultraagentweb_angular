import { Pipe, PipeTransform } from '@angular/core';
import {UAHelpers} from '../globals/uahelpers';


@Pipe({
    name: 'adminshiftsbookingsfilter',
    pure: false
})
export class AdminShiftsBookingsFilterPipe implements PipeTransform {

  transform(items: any[], filterDate:Date = undefined): any[] {
     // console.log("AdminShiftsBookingsFilterPipe transform called");
    if (!items) {
      return items;
    }
    // filter items array, items which match and return true will be kept, false will be filtered out
    return items.filter((item: any) => {
      
      if ((item.shiftStartDateTime == undefined) || (item.shiftStartDateTime == null)) {
        return false;
      }

      if ((filterDate != undefined) && (filterDate != null)) {
        // chek if date matches
        if ((item.shiftStartDateTime != undefined) && (item.shiftStartDateTime != null)) {
          const aDate = item.shiftStartDateTime.toDate();
          /*console.log('shiftDate');*/ //console.log(aDate+" "+item.shiftNo);
          //console.log('filterDate'); console.log(filterDate);
          if (UAHelpers.sameDay(filterDate, aDate)) {
            // matches
          } else {
            return false;
          }
        } else {
          return false;
        }
      }
      
      // Should be approvalStatus=bookings
      if ((item.status != undefined) && (item.status == 'active')){
          return true;
      }
       return false;
    });
  }
  
}