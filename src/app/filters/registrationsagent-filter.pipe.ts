import { Pipe, PipeTransform } from '@angular/core';
import {UAHelpers} from '../globals/uahelpers';


@Pipe({
    name: 'registrationsagentfilter',
    pure: false
})
export class RegistrationsAgentFilterPipe implements PipeTransform {

  transform(items: any[], filterDate:Date = undefined): any[] {
     // console.log("RegistrationsAgentFilterPipe transform called");
    if (!items) {
      return items;
    }
    // filter items array, items which match and return true will be kept, false will be filtered out
    return items.filter((item: any) => {
      if ((filterDate != undefined) && (filterDate != null)) {
        // chek if date matches
        if ((item.createdAt != undefined) && (item.createdAt != null)) {
          const aDate = item.createdAt.toDate();// convert Firestore Timestamp to javascript Date object
          if (UAHelpers.sameDay(filterDate, aDate)) {
            // matches
          } else {
            return false;
          }
        }
      }
       if ((item.companyId != undefined) && (item.companyId.length > 0) && 
        (item.approvalStatus != undefined) && (item.approvalStatus == 'pending')){
            return true;
       }
       return false;
    });
  }
  
}