import { Pipe, PipeTransform } from '@angular/core';
import { UAHelpers } from '../globals/uahelpers';


@Pipe({
  name: 'shiftsadvancedbookingfilter',
  pure: false
})
export class ShiftsAdvancedBookingFilterPipe implements PipeTransform {

  transform(items: any[], filterDate: Date = undefined): any[] {
    //  console.log("ShiftsAdvancedBookingFilterPipe transform called");
    if (!items) {
      return items;
    }
    // filter items array, items which match and return true will be kept, false will be filtered out
    return items.filter((item: any) => {

      if ((item.startDateTime == undefined) || (item.startDateTime == null)) {
        return false;
      }

      if ((filterDate != undefined) && (filterDate != null)) {
        // chek if date matches
        if ((item.startDateTime != undefined) && (item.startDateTime != null)) {
          const aDate = item.startDateTime.toDate();
          /*console.log('shiftDate');*/ //console.log(aDate+" "+item.shiftNo);
          //console.log('filterDate'); console.log(filterDate);
          if (UAHelpers.sameDay(filterDate, aDate)) {
            // matches
          } else {
            return false;
          }
        } else {
          return false;
        }
      }

      if ((item.isAdvanceBooking != undefined) && (item.isAdvanceBooking == true)) {
        if ((item.recruitmentStatus == undefined) || (item.recruitmentStatus != 'recruited')) {
          return true;
        }
        return false;
      }
      return false;
    });
  }

}