import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
    name: 'bookingsorderfilter',
    pure: false
})
export class BookingsOrderFilterPipe implements PipeTransform {

  transform(items: any[]): any[] {
     // console.log("ShiftsOrderFilterPipe transform called");
    if (!items || items == undefined || items.length == 0) {
      return items;
    }

    items.sort( (a: any, b: any) => {
       if(a.shiftStartDateTime == undefined){
            return -1;
        }
        if(b.shiftStartDateTime == undefined){
            return -1;
        }
        const aD = a.shiftStartDateTime.toDate();
        const bD = b.shiftStartDateTime.toDate();
        //console.log(aD+" "+a.shiftNo);
        //console.log(bD+" "+b.shiftNo);
        if (aD < bD) {
            return -1;
        } else if (aD > bD) {
            return 1;
        } else {
            return 0;
        }
    });
    return items;
  }
  
}