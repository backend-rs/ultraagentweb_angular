// import { Pipe,PipeTransform } from "angular/core";
import { Pipe, } from '@angular/core';
@Pipe({
    name: "orderby"
})
export class OrderByPipe {
    transform(array: Array<string>, args: string): Array<string> {
        console.log("orderby", array)
        array.sort((a: any, b: any) => {
            console.log('a', a.startDateTime)
            if (a.startDateTime < b.startDateTime) {
                return 1;
            } else if (a.startDateTime > b.startDateTime) {
                return -1;
            } else {
                return 0;
            }
        });
        return array;
    }
}
// export class OrderByPipe implements PipeTransform {

//     transform(array: Array<string>, args: string): Array<string> {
//         console.log("orderby", array)

//         array.sort((a: any, b: any) => {
//             if (a < b) {
//                 return 1;
//             } else if (a > b) {
//                 return -1;
//             } else {
//                 return 0;
//             }
//         });
//         return array;
//     }
// }