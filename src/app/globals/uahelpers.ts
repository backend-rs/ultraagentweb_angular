import {UAGlobals} from '../globals/uaglobals';

export const UAHelpers = Object.freeze({
    sameDay: function (d1: Date, d2: Date) {
        return d1.getFullYear() === d2.getFullYear() &&
          d1.getMonth() === d2.getMonth() &&
          d1.getDate() === d2.getDate();
    
          // return d1.getUTCFullYear() === d2.getUTCFullYear() &&
          // d1.getUTCMonth() === d2.getUTCMonth() &&
          // d1.getUTCDate() === d2.getUTCDate();
      },
      stringToDate: function(_date,_format,_delimiter)
      {
        var formatLowerCase=_format.toLowerCase();
        var formatItems=formatLowerCase.split(_delimiter);
        var dateItems=_date.split(_delimiter);
        var monthIndex=formatItems.indexOf("mm");
        var dayIndex=formatItems.indexOf("dd");
        var yearIndex=formatItems.indexOf("yyyy");
        var month=parseInt(dateItems[monthIndex]);
        month-=1;
        var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
        return formatedDate;
      },
      cityDropdownData: function() {
        let data = [];
        const zones = Object.keys(UAGlobals.MASTER_CITIES);
        for (const aZone of zones) {
          data.push({display:aZone,zone:aZone,city:''});
          const cities = UAGlobals.MASTER_CITIES[aZone];
          for (const aCity of cities) {
            data.push({display:aCity,zone:aZone,city:aCity});
          }
        }
        return data;
      }
});