import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-newshift-done-dialog',
  templateUrl: './newshift-done-dialog.component.html',
  styleUrls: ['./newshift-done-dialog.component.css']
})
export class NewshiftDoneDialogComponent implements OnInit {

  title:string = 'SUCCESS';
  descr:string = 'SHIFT ADDED';

  constructor(public dialogRef: MatDialogRef<NewshiftDoneDialogComponent>, @Inject(MAT_DIALOG_DATA) public data:string) {
    // this.title = data.title;
    // this.descr = data.descr;
   }

  ngOnInit() {
  }

  cancelClick(): void {
    this.dialogRef.close();
  }


}
