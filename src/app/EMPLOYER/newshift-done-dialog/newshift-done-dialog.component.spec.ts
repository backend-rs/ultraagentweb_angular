import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewshiftDoneDialogComponent } from './newshift-done-dialog.component';

describe('NewshiftDoneDialogComponent', () => {
  let component: NewshiftDoneDialogComponent;
  let fixture: ComponentFixture<NewshiftDoneDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewshiftDoneDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewshiftDoneDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
