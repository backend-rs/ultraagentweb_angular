import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';

import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { HttpClient, HttpParams } from '@angular/common/http';

import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { ApproveRejectDialogComponent } from '../../dialogs/approve-reject-dialog/approve-reject-dialog.component';

import * as firebase from 'firebase/app';

@Component({
  selector: 'app-employer-users-edit-page',
  templateUrl: './employer-users-edit-page.component.html',
  styleUrls: ['./employer-users-edit-page.component.css']
})
export class EmployerUsersEditPageComponent implements OnInit {

  profile: any;
  sub:any;

  constructor(public dialog: MatDialog,
    private authService: AuthService,
    private router: Router,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    private httpClient:HttpClient,private route: ActivatedRoute,) { 
    }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.profile = JSON.parse(params['profile']); 
      console.log(params);
   });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  onProfileSave() {
    console.log('profileSave clicked');

    // this.router.navigate(['settings', {show: 'users'}]);


    if ((this.profile.email == undefined) || (this.profile.email.length < 1)) {
      this.openDialog("Email not specified","Please enter a valid email.");
      return;
    }
    if ((this.profile.firstName == undefined) || (this.profile.firstName.length < 1)) {
      this.openDialog("First name not specified","Please enter a valid first name.");
      return;
    }
    if ((this.profile.lastName == undefined) || (this.profile.lastName.length < 1)) {
      this.openDialog("Last name not specified","Please enter a valid last name.");
      return;
    }
    if ((this.profile.phoneNumber == undefined) || (this.profile.phoneNumber.length < 1)) {
      this.openDialog("Phone number not specified","Please enter a valid phone number.");
      return;
    }
    if ((this.profile.jobTitle == undefined) || (this.profile.jobTitle.length < 1)) {
      this.openDialog("Job title not specified","Please enter a valid job title.");
      return;
    }
    if ((this.profile.city == undefined) || (this.profile.city.length < 1)) {
      this.openDialog("City not specified","Please enter a valid city.");
      return;
    }
    if ((this.profile.address == undefined) || (this.profile.address.length < 1)) {
      this.openDialog("Address not specified","Please enter a valid address.");
      return;
    }

    const dialogRef = this.dialog.open(ApproveRejectDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = 'Are you sure?';
    dialogRef.componentInstance.descr = 
    "Do you really want to save change to this profile?";

    dialogRef.componentInstance.approveTitle = 'Yes, Save';
    dialogRef.componentInstance.rejectTitle = 'No, Cancel';

    dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      if (result == dialogRef.componentInstance.approveTitle) {

        this.firestore.collection("profiles").doc(this.profile.userId).set({
          firstName:this.profile.firstName,
          lastName: this.profile.lastName,
          phoneNumber: this.profile.phoneNumber,
          city: this.profile.city,
          address: this.profile.address,
          jobTitle: this.profile.jobTitle,
          permsShifts: this.profile.permsShifts,
          permsBookings: this.profile.permsBookings,
          permsTimesheets: this.profile.permsTimesheets,
          permsUsers: this.profile.permsUsers,
          createdAt: firebase.firestore.FieldValue.serverTimestamp()
        }, {merge: true})
        .then(_ => {
          this.openDialog("SUCCESS","Profile updated successfully.");
        })
        .catch((err) => {
          console.log(err);
          this.openDialog("Error",err.message);
        }); 

      }
    });
  }


  openDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    
    // this.router.navigate(['employer_dashboard', {show: 'notifications'}]);

    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
    
  }

}
