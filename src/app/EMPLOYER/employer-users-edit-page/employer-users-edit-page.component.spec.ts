import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerUsersEditPageComponent } from './employer-users-edit-page.component';

describe('EmployerUsersEditPageComponent', () => {
  let component: EmployerUsersEditPageComponent;
  let fixture: ComponentFixture<EmployerUsersEditPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerUsersEditPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerUsersEditPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
