import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerReportsComponent } from './employer-reports.component';

describe('EmployerReportsComponent', () => {
  let component: EmployerReportsComponent;
  let fixture: ComponentFixture<EmployerReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
