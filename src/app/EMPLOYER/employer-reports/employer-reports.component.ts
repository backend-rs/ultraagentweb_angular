import { SearchBoxService } from "../../services/searchbox.service";
import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Location } from '@angular/common';
import {MatDialog} from '@angular/material';

import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { HttpClient, HttpParams } from '@angular/common/http';

import {UAGlobals} from '../../globals/uaglobals';
import {UAHelpers} from '../../globals/uahelpers';

@Component({
  selector: 'app-employer-reports',
  templateUrl: './employer-reports.component.html',
  styleUrls: ['./employer-reports.component.css']
})
export class EmployerReportsComponent implements OnInit {

  datePipe: DatePipe;

  checked = true;
  searchword = '';
  private searchBoxSubscription: Subscription;
  
  shiftsReportType:any;
  shiftsReportStartDate:Date;
  shiftsReportEndDate:Date;

  bookingsReportType:any;
  bookingsReportStartDate:Date;
  bookingsReportEndDate:Date;

  timesheetReportType:any;
  timesheetReportStartDate:Date;
  timesheetReportEndDate:Date;

  constructor(
    private router: Router, 
    public dialog: MatDialog, private location: Location, 
    private authService: AuthService, 
    private afAuth: AngularFireAuth, 
    private firestore: AngularFirestore,
    private route: ActivatedRoute,
    private httpClient:HttpClient, 
    private searchBoxService: SearchBoxService
  ) {
    this.datePipe = new DatePipe('en-GB');
   }

  ngOnInit() {

    // Reset search box in navigation
    this.searchBoxService.clearMessage();

    this.searchBoxSubscription = this.searchBoxService.getMessage().subscribe((msg) => {
      this.searchword = msg.searchword;
    });

  }

  ngOnDestroy() {
    this.searchBoxSubscription.unsubscribe();
  }

  activeTab = 'postedshift';

  postedshift(activeTab){
    this.activeTab = activeTab;
  }

  bookings(activeTab){
    this.activeTab = activeTab;
  }

  timesheet(activeTab){
    this.activeTab = activeTab;
  }

  generateReportClicked(){
    if (this.activeTab == 'postedshift') {

      if ((this.shiftsReportType == undefined) || (this.shiftsReportType.length < 1) || (this.shiftsReportType == 'none')) {
        this.openDialog('Type on specified', 'Please select a valid report type from dropdown menu');
        return;
      }

      if (this.shiftsReportStartDate == undefined) {
        this.openDialog('Start date on specified', 'Please select a valid start date');
        return;
      }
      if (this.shiftsReportEndDate == undefined) {
        this.openDialog('End date on specified', 'Please select a valid end date');
        return;
      }
      if (this.shiftsReportStartDate.getTime() > this.shiftsReportEndDate.getTime()) {
        this.openDialog('Invalid End date', 'Please select a end date after start date');
        return;
      }

      const start = this.datePipe.transform(this.shiftsReportStartDate, 'yyyy-MM-dd');
      const end = this.datePipe.transform(this.shiftsReportEndDate, 'yyyy-MM-dd');;

      this.router.navigate(['Employer_postedshift_report', 
        { type: this.shiftsReportType, 
          start:start,
          end:end,
          companyId:this.authService.companyProfile.id,
          skipLocationChange: true}]);

    } else if (this.activeTab == 'bookings') {

      if ((this.bookingsReportType == undefined) || (this.bookingsReportType.length < 1) || (this.bookingsReportType == 'none')) {
        this.openDialog('Type on specified', 'Please select a valid report type from dropdown menu');
        return;
      }

      if (this.bookingsReportStartDate == undefined) {
        this.openDialog('Start date on specified', 'Please select a valid start date');
        return;
      }
      if (this.bookingsReportEndDate == undefined) {
        this.openDialog('End date on specified', 'Please select a valid end date');
        return;
      }
      if (this.bookingsReportStartDate.getTime() > this.bookingsReportEndDate.getTime()) {
        this.openDialog('Invalid End date', 'Please select a end date after start date');
        return;
      }

      const start = this.datePipe.transform(this.bookingsReportStartDate, 'yyyy-MM-dd');
      const end = this.datePipe.transform(this.bookingsReportEndDate, 'yyyy-MM-dd');;

      this.router.navigate(['Employer_bookings_report', 
        { type: this.bookingsReportType, 
          start:start,
          end:end,
          companyId:this.authService.companyProfile.id,
          skipLocationChange: true}]);

    } else if (this.activeTab == 'timesheet') {

      if ((this.timesheetReportType == undefined) || (this.timesheetReportType.length < 1) || (this.timesheetReportType == 'none')) {
        this.openDialog('Type on specified', 'Please select a valid report type from dropdown menu');
        return;
      }

      if (this.timesheetReportStartDate == undefined) {
        this.openDialog('Start date on specified', 'Please select a valid start date');
        return;
      }
      if (this.timesheetReportEndDate == undefined) {
        this.openDialog('End date on specified', 'Please select a valid end date');
        return;
      }
      if (this.timesheetReportStartDate.getTime() > this.timesheetReportEndDate.getTime()) {
        this.openDialog('Invalid End date', 'Please select a end date after start date');
        return;
      }

      const start = this.datePipe.transform(this.timesheetReportStartDate, 'yyyy-MM-dd');
      const end = this.datePipe.transform(this.timesheetReportEndDate, 'yyyy-MM-dd');;

      this.router.navigate(['Employer_timesheet_report', 
        { type: this.timesheetReportType, 
          start:start,
          end:end,
          companyId:this.authService.companyProfile.id,
          skipLocationChange: true}]);
    } 
  }

  openDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
    
  }
}
