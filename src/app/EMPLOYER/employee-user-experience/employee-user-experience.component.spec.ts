import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeUserExperienceComponent } from './employee-user-experience.component';

describe('EmployeeUserExperienceComponent', () => {
  let component: EmployeeUserExperienceComponent;
  let fixture: ComponentFixture<EmployeeUserExperienceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeUserExperienceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeUserExperienceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
