import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


@Component({
  selector: 'app-newshift-add-dialog',
  templateUrl: './newshift-add-dialog.component.html',
  styleUrls: ['./newshift-add-dialog.component.css']
})

export class NewshiftAddDialogComponent implements OnInit {

  dialogResult="";
  shift: any;

  constructor(public dialog: MatDialog, public dialogRef: MatDialogRef<NewshiftAddDialogComponent>, @Inject(MAT_DIALOG_DATA) public data:string) { }

  addClick(): void {
    this.dialogRef.close(true);

  
  }

  ngOnInit() {
  }

  cancelClick(): void {
    this.dialogRef.close(false);
  }

}
