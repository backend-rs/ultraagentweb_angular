import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewshiftAddDialogComponent } from './newshift-add-dialog.component';

describe('NewshiftAddDialogComponent', () => {
  let component: NewshiftAddDialogComponent;
  let fixture: ComponentFixture<NewshiftAddDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewshiftAddDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewshiftAddDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
