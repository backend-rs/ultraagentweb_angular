import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MybookingDetailsDialogComponent } from './mybooking-details-dialog.component';

describe('MybookingDetailsDialogComponent', () => {
  let component: MybookingDetailsDialogComponent;
  let fixture: ComponentFixture<MybookingDetailsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MybookingDetailsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MybookingDetailsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
