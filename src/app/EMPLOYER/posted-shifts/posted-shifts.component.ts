import { Component, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatDialog } from '@angular/material';
import { NewshiftAddDialogComponent } from '../newshift-add-dialog/newshift-add-dialog.component';
import { DeleteShiftDialogComponent } from '../delete-shift-dialog/delete-shift-dialog.component';
import { ViewDetailsDialogComponent } from '../view-details-dialog/view-details-dialog.component';
import { ViewWorkerDetailsComponent } from '../view-worker-details/view-worker-details.component';
import { NewshiftDoneDialogComponent } from '../newshift-done-dialog/newshift-done-dialog.component';
import { ViewAdvanceBookingDetailsDialogComponent } from '../view-advance-booking-details-dialog/view-advance-booking-details-dialog.component';
import { MyshiftEditDialogComponent } from '../myshift-edit-dialog/myshift-edit-dialog.component';
import { PostedBookingsCancelledOngoingApprovedDisputedDetailsDialogComponent } from '../../dialogs/posted-bookings-cancelled-ongoing-approved-disputed-details-dialog/posted-bookings-cancelled-ongoing-approved-disputed-details-dialog.component';

import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { LocationLookupDialogComponent } from '../../dialogs/location-lookup-dialog/location-lookup-dialog.component';

import { HttpClient, HttpParams } from '@angular/common/http';

import { SearchBoxService } from "../../services/searchbox.service";

import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';

import { UAGlobals } from '../../globals/uaglobals';
import { UAHelpers } from '../../globals/uahelpers';
import { DateTimeAdapter } from 'ng-pick-datetime';
import { OrderByPipe } from 'src/app/filters/orderby';

@Component({
  selector: 'app-posted-shifts',
  templateUrl: './posted-shifts.component.html',
  styleUrls: ['./posted-shifts.component.css']
})
export class PostedShiftsComponent {
  pipes: [OrderByPipe]
  searchword = '';
  filterDate: Date = undefined;
  newShiftDateFromPicker: Date = undefined;
  hideFilterDate = false;
  hideShiftTitle = false;
  hideNewConstructionShiftTitle = true;
  hideMap = false;
  hideSaveButton = false;
  hidePostDeleteShiftButton = false;
  hideExperience: boolean = false;

  dialogResult = "";
  registerForm: FormGroup;
  errorMessage: string = '';
  successMessage: string = '';

  totalShiftCount = 0;
  monthlyShiftCount = 0;

  minShiftStartTime = new Date((new Date()).getTime() + 30 * 60000);
  shift = {
    category: '',
    subcategory: '',
    duration: '',
    durationHH: '',
    durationMM: '',
    startDateTime: this.minShiftStartTime,
    fee: '',
    contactpername: '',
    phoneNumber: '',
    address: '',
    postcode: '',
    city: '',
    description: '',
    expNeeded: '',
    lat: 51.509865,
    lng: -0.118092,
    shiftNo: ''
  };
  shiftslist: any;
  profileslist: any;
  blist: any;

  categories: any[];
  subcategories: any[];
  citySelectionData: any[];

  private shiftSubscription: Subscription;
  private profileSubscription: Subscription;
  private bookingRequestsSubscription: Subscription;

  private searchBoxSubscription: Subscription;

  constructor(public dialog: MatDialog,
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    private httpClient: HttpClient,
    private dateTimeAdapter: DateTimeAdapter<any>,
    private searchBoxService: SearchBoxService

  ) {

    this.categories = Object.keys(UAGlobals.MASTER_CATEGORIES);
    // this.subcategories = UAGlobals.MASTER_CATEGORIES[this.categories[0]];
    //console.log(this.categories);
    //console.log(this.subcategories);

    this.shift.category = this.authService.companyProfile.industry;
    this.subcategories = UAGlobals.MASTER_CATEGORIES[this.shift.category];
    this.citySelectionData = UAHelpers.cityDropdownData();

    // update to 30 mins from now
    this.minShiftStartTime = new Date((new Date()).getTime() + 30 * 60000);

    this.dateTimeAdapter.setLocale('en-UK');

  }

  ngOnInit() {

    // Reset search box in navigation
    this.searchBoxService.clearMessage();

    this.searchBoxSubscription = this.searchBoxService.getMessage().subscribe((msg) => {
      this.searchword = msg.searchword;
    });

    this.updateShiftsList();

    // update to 30 mins from now
    this.minShiftStartTime = new Date((new Date()).getTime() + 30 * 60000);
  }

  ngOnDestroy() {

    this.searchBoxSubscription.unsubscribe();

    if (this.shiftSubscription != undefined) {
      this.shiftSubscription.unsubscribe();
    }
    if (this.profileSubscription != undefined) {
      this.profileSubscription.unsubscribe();
    }
    if (this.bookingRequestsSubscription != undefined) {
      this.bookingRequestsSubscription.unsubscribe();
    }
  }

  onCategoryChange(newValue) {
    //console.log('onCategoryChange:'+newValue);
    this.subcategories = UAGlobals.MASTER_CATEGORIES[newValue];
    if (this.subcategories.length > 0) {
      this.shift.subcategory = this.subcategories[0];
    }
    //console.log(this.subcategories);
  }



  // updateShiftsList(){
  //   this.shiftCollection = this.firestore.collection<Shift>('shifts');
  //   // .snapshotChanges() returns a DocumentChangeAction[], which contains
  //   // a lot of information about "what happened" with each change. If you want to
  //   // get the data and the id use the map operator.
  //   this.shiftCollection.snapshotChanges().pipe(
  //     map(actions => actions.map(a => {
  //       const data = a.payload.doc.data() as Shift;
  //       const id = a.payload.doc.id;
  //       return { id, ...data };
  //     }))
  //   ).subscribe(shiftslist => {
  //       this.shiftslist = shiftslist;
  //       console.log(this.shiftslist);
  //   });

  //  }

  updateBookingRequests() {
    const aCollection = this.firestore.collection('bookingrequests', ref =>
      ref.where('companyId', '==', this.authService.loggedInProfile.companyId));
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.bookingRequestsSubscription = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(someList => {
      this.blist = someList;

      for (var i = 0; i < this.blist.length; i++) {
        var companyId = this.blist[i].companyId;
        for (var j = 0; j < this.profileslist.length; j++) {
          if (this.profileslist[j].id == companyId) {
            this.blist[i].company = this.profileslist[j];
          }
        }
      }

      for (var i = 0; i < this.blist.length; i++) {
        var workerId = this.blist[i].workerId;
        for (var j = 0; j < this.profileslist.length; j++) {
          if (this.profileslist[j].id == workerId) {
            this.blist[i].worker = this.profileslist[j];
          }
        }
      }

      for (var i = 0; i < this.shiftslist.length; i++) {
        var arr = new Array;
        for (var j = 0; j < this.blist.length; j++) {
          if (this.blist[j].shiftId == this.shiftslist[i].id) {
            arr.push(this.blist[j]);
          }
        }
        this.shiftslist[i].bookingrequests = arr;
      }
      console.log('shiftslist', this.shiftslist);

    }
    );
  }

  updateShiftsList() {
    var aCollection = this.firestore.collection('shifts', ref =>
      ref.where('companyId', '==', this.authService.loggedInProfile.companyId)
    );
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.shiftSubscription = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(shiftslist => {

      this.shiftslist = shiftslist;
      // const sortedActivities = shiftslist.sort((a, b) => b.startDateTime - a.startDateTime)
      console.log(this.shiftslist);

      if (shiftslist != undefined) {
        this.totalShiftCount = shiftslist.length;
        this.monthlyShiftCount = shiftslist.length;
      } else {
        this.totalShiftCount = 0;
        this.monthlyShiftCount = 0;
      }

      var bCollection = this.firestore.collection('profiles');
      this.profileSubscription = bCollection.snapshotChanges().pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        }))
      ).subscribe(profileslist => {
        this.profileslist = profileslist;
        //console.log(this.profileslist);

        for (var i = 0; i < this.shiftslist.length; i++) {
          var owner = this.shiftslist[i].owner;
          for (var j = 0; j < this.profileslist.length; j++) {
            if (this.profileslist[j].id == owner) {
              this.shiftslist[i].company = this.profileslist[j];
            }
          }
        }
        this.updateBookingRequests();
      });

    });

  }



  openDialog(): void {

    if ((this.shift.subcategory == undefined) || (this.shift.subcategory.length < 1)) {
      this.openInformDialog("Subcategory not specified", "Please select a valid subcategory.");
      return;
    }
    // if ((this.shift.duration == undefined) || (this.shift.duration.length < 1)) {
    //   this.openInformDialog("Duration not specified","Please enter a valid duration.");
    //   return;
    // }
    if ((this.shift.durationHH == undefined) || (this.shift.durationHH.length < 1)) {
      this.openInformDialog("Duration not specified", "Please enter a valid duration.");
      return;
    }
    if ((this.shift.durationMM == undefined) || (this.shift.durationMM.length < 1)) {
      this.openInformDialog("Duration not specified", "Please enter a valid duration.");
      return;
    }
    if (this.newShiftDateFromPicker == undefined) {
      this.openInformDialog("Date not specified", "Please select a valid date.");
      return;
    }
    if ((this.shift.fee == undefined) || (this.shift.fee.length < 1)) {
      this.openInformDialog("Fee not specified", "Please enter a valid fee.");
      return;
    }
    let feeMinMax = parseInt(this.shift.fee);
    if (feeMinMax < 8 || feeMinMax > 999) {
      this.openInformDialog("Invalid", "Fee should be minimum £8/h to maximum £999/h");
      return;
    }
    if ((this.shift.contactpername == undefined) || (this.shift.contactpername.length < 1)) {
      this.openInformDialog("Contact person not specified", "Please enter a valid Contact person.");
      return;
    }
    if (!/^[a-zA-Z ]*$/.test(this.shift.contactpername)) {
      this.openInformDialog("Invalid", "Please enter a valid Contact person");
      return;
    }

    if ((this.shift.phoneNumber == undefined) || (this.shift.phoneNumber.length < 1)) {
      this.openInformDialog("Phone number not specified", "Please enter a valid Phone number.");
      return;
    }
    if ((this.shift.phoneNumber == undefined) || (this.shift.phoneNumber.length < 10)) {
      this.openInformDialog("Invalid", "Phone number should be minimum 10 digits.");
      return;
    }
    if ((this.shift.address == undefined) || (this.shift.address.length < 1)) {
      this.openInformDialog("Address not specified", "Please enter a valid address.");
      return;
    }
    if ((this.shift.postcode == undefined) || (this.shift.postcode.length < 1)) {
      this.openInformDialog("postcode not specified", "Please enter a valid postcode.");
      return;
    }
    if ((this.shift.city == undefined) || (this.shift.city.length < 1)) {
      this.openInformDialog("City not specified", "Please enter a valid city.");
      return;
    }
    // if ((this.shift.expNeeded == undefined) || (this.shift.expNeeded.length < 1)) {
    //   this.openInformDialog("Experience not specified","Please enter a valid experience.");
    //   return;
    // }
    // if ((this.shift.description == undefined) || (this.shift.description.length < 1)) {
    //   this.openInformDialog("Description not specified","Please enter a valid description.");
    //   return;
    // }

    this.shift.startDateTime = this.newShiftDateFromPicker;

    this.shift.duration = this.shift.durationHH + ':' + this.shift.durationMM;

    const dialogRef = this.dialog.open(NewshiftAddDialogComponent, {
      panelClass: 'my-panel',
      width: '850px',
    });

    dialogRef.componentInstance.shift = this.shift;
    dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      if (result == true) {
        // add shift to firestore
        this.shift.shiftNo = Math.round(new Date().getTime() / 1000) + "";
        console.log(this.authService.loggedInProfile);
        this.firestore.collection("shifts").add({
          email: this.authService.loggedInProfile.email,
          approvalStatus: 'pending',
          companyId: this.authService.loggedInProfile.companyId,
          companyName: this.authService.companyProfile.name,
          createdBy: this.authService.loggedInProfile.id,
          owner: this.authService.loggedInProfile.id,
          category: this.shift.category,
          subcategory: this.shift.subcategory,
          duration: this.shift.duration,
          startDateTime: this.shift.startDateTime,
          fee: this.shift.fee,
          contactpername: this.shift.contactpername,
          phoneNumber: this.shift.phoneNumber,
          address: this.shift.address,
          postcode: this.shift.postcode,
          city: this.shift.city,
          lat: this.shift.lat,
          lng: this.shift.lng,
          description: this.shift.description,
          expNeeded: this.shift.expNeeded,
          createdAt: firebase.firestore.FieldValue.serverTimestamp(),
          shiftNo: this.shift.shiftNo,
          disputedStatus: '',
          isAdvanceBooking: false,
        })
          .then((aObj) => {
            //this.router.navigate(['login'])

            console.log(aObj);
            // Send Push to all workers

            var aCollection = this.firestore.collection('profiles', ref =>
              ref.where('profileType', '==', 'worker')
                .where('approvalStatus', '==', 'approved')
                .where('wProfession', '==', this.shift.category)
                .where('wSubCategory', '==', this.shift.subcategory)
                .where('wAvailable', '==', true)
            );

            var aSubscription: Subscription = aCollection.snapshotChanges().pipe(
              map(actions => actions.map(a => {
                const data = a.payload.doc.data();
                const id = a.payload.doc.id;
                return { id, ...data };
              }))
            ).subscribe(matches => {
              var workers: any = matches;
              var aTitle = this.shift.subcategory + ' #' + this.shift.shiftNo
                + '￡' + this.shift.fee + '/h - ' + this.shift.city + ' by '
                + this.authService.companyProfile.name;
              var aDescr = aTitle + '\n' + this.shift.description;

              for (var i = 0; i < workers.length; i++) {
                var aWorker = workers[i];
                console.log('Sending push to worker: ' + aWorker.email);
                this.httpClient.post("https://x784f653.ultraagent.co.uk/sendPush",
                  {
                    "userId": aWorker.id,
                    "title": aTitle,
                    "description": aDescr,
                    "data": { shiftId: aObj.id, type: 'newshiftadded' }
                  })
                  .subscribe(data => { console.log(data); }, error => { console.log("Error", error); });
              }

              aSubscription.unsubscribe();

              const dialogRef = this.dialog.open(NewshiftDoneDialogComponent, {
                panelClass: 'my-panel',
                width: '750px',
              });
              //this.shift.category = '';
              this.shift.subcategory = '';
              this.shift.durationHH = '';
              this.shift.durationMM = '';
              this.newShiftDateFromPicker = undefined;
              this.shift.fee = '';
              this.shift.contactpername = '';
              this.shift.phoneNumber = '';
              this.shift.address = '';
              this.shift.postcode = '';
              this.shift.city = '';
              this.shift.description = '';
              this.shift.expNeeded = '';
              this.newShiftDateFromPicker = undefined;

              dialogRef.afterClosed().subscribe(result => {
                var myshiftTab = document.getElementById('myshifts-tab');
                myshiftTab.click();
              });

            });
          })
          .catch(err => console.log(err))
      }
    });
  }

  openDeleteShiftDialog(aShift): void {

    const dialogRef = this.dialog.open(DeleteShiftDialogComponent, {
      panelClass: 'my-panel',
      width: '750px',
    });

    dialogRef.componentInstance.title = "DELETE " + aShift.subcategory.toUpperCase() + " #" + aShift.shiftNo;
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {

        // Delete associated bookingrequests
        this.firestore.collection('bookingrequests', ref =>
          ref.where('companyId', '==', this.authService.loggedInProfile.companyId)
        ).ref
          .get()
          .then((querySnapshot) => {

            let bookingIds = [];
            querySnapshot.forEach((doc) => {
              // doc.data() is never undefined for query doc snapshots
              if (doc.exists) {
                console.log("Deleting booking request " + doc.id);
                bookingIds.push(doc.id);
              }
            });
            for (let idx = 0; idx < bookingIds.length; idx++) {
              const aId = bookingIds[idx];
              this.firestore.collection('bookingrequests').doc(aId).delete();

            }
          })
          .catch((err) => {
            console.log(err);
            this.openInformDialog("Error", err.message);
          });

        this.firestore.collection('shifts').doc(aShift.id).delete()
          .then(() => {
            this.openInformDialog("SUCCESS", "Shift deleted successfully.");
          })
          .catch((err) => {
            console.log(err);
            this.openInformDialog("Error", err.message);
          });

      }
    });
  }

  openViewDetailsDialog(aShift): void {

    // const dialogRef = this.dialog.open(ViewDetailsDialogComponent, {
    //   panelClass: 'my-panel',
    //   width: '800px',
    // });

    // dialogRef.componentInstance.shift = aShift;
    //  dialogRef.afterClosed().subscribe(result => {
    //   console.log(`The dialog was closed: ${result}`);
    //   this.dialogResult = result;
    // });


    console.log('openViewAdvanceBookingDetailsDialog called');
    console.log(aShift);
    const dialogRef = this.dialog.open(ViewAdvanceBookingDetailsDialogComponent, {
      panelClass: 'my-panel',
      width: '990px',
    });

    var listBookedWorkers = new Array();
    var listRequestedWorkers = new Array();
    var listInterestedWorkers = new Array();
    var listDeclinedWorkers = new Array();
    if (aShift.bookingrequests != undefined) {
      for (var i = 0; i < aShift.bookingrequests.length; i++) {
        const aReq = aShift.bookingrequests[i];
        if ((aReq.isAdvanceBooking == undefined) || (aReq.isAdvanceBooking == false)) {
          if (aReq.approvalStatus == 'pending') {
            listInterestedWorkers.push(aShift.bookingrequests[i].worker);
          }
        } else {
          if (aReq.approvalStatus == 'pending') {
            listRequestedWorkers.push(aShift.bookingrequests[i].worker);
          }
        }

        if ((aReq.approvalStatus != undefined) && (aReq.approvalStatus == 'approved')) {
          listBookedWorkers.push(aShift.bookingrequests[i].worker);
        }

        if ((aReq.approvalStatus != undefined) && (aReq.approvalStatus == 'rejected')) {
          listDeclinedWorkers.push(aShift.bookingrequests[i].worker);
        }
      }
    }

    dialogRef.componentInstance.shift = aShift;
    dialogRef.componentInstance.shiftDate = aShift.startDateTime.toDate();
    dialogRef.componentInstance.requestedWorkers = listRequestedWorkers;
    dialogRef.componentInstance.bookedWorkers = listBookedWorkers;
    dialogRef.componentInstance.declinedWorkers = listDeclinedWorkers;
    dialogRef.componentInstance.interestedWorkers = listInterestedWorkers;
    dialogRef.componentInstance.hideDeclinedWorkersList = listDeclinedWorkers.length == 0 ? true : false;
    dialogRef.componentInstance.hideBookedWorkersList = listBookedWorkers.length == 0 ? true : false;
    dialogRef.componentInstance.hideRequestedWorkersList = listRequestedWorkers.length == 0 ? true : false;
    dialogRef.componentInstance.hideInterestedWorkersList = listInterestedWorkers.length == 0 ? true : false;
    dialogRef.componentInstance.isRequestedWorkersListActionable = false;
    dialogRef.componentInstance.isInterestedWorkersListActionable = true;
    dialogRef.componentInstance.isBookedWorkersListActionable = true;
    dialogRef.componentInstance.hideWaitingApproval = false;
    dialogRef.componentInstance.hideWorkerDeclined = true;
    dialogRef.componentInstance.hideneedExperience = false;

    dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      this.dialogResult = result;
    });

  }

  openViewWorkersDetailsDialog(): void {

    const dialogRef = this.dialog.open(ViewWorkerDetailsComponent, {
      panelClass: 'my-panel',
      width: '750px',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      this.dialogResult = result;
    });
  }

  openViewAdvanceBookingDetailsDialog(aShift): void {
    console.log('openViewAdvanceBookingDetailsDialog called');
    console.log(aShift);
    const dialogRef = this.dialog.open(ViewAdvanceBookingDetailsDialogComponent, {
      panelClass: 'my-panel',
      width: '990px',
    });

    var listBookedWorkers = new Array();
    var listRequestedWorkers = new Array();
    var listInterestedWorkers = new Array();
    var listDeclinedWorkers = new Array();
    if (aShift.bookingrequests != undefined) {
      for (var i = 0; i < aShift.bookingrequests.length; i++) {
        const aReq = aShift.bookingrequests[i];
        if ((aReq.isAdvanceBooking == undefined) || (aReq.isAdvanceBooking == false)) {
          listInterestedWorkers.push(aShift.bookingrequests[i].worker);
        } else {
          listRequestedWorkers.push(aShift.bookingrequests[i].worker);
        }

        if ((aReq.approvalStatus != undefined) && (aReq.approvalStatus == 'approved')) {
          listBookedWorkers.push(aShift.bookingrequests[i].worker);
        }

        if ((aReq.approvalStatus != undefined) && (aReq.approvalStatus == 'rejected')) {
          listDeclinedWorkers.push(aShift.bookingrequests[i].worker);
        }
      }
    }

    dialogRef.componentInstance.shift = aShift;
    dialogRef.componentInstance.requestedWorkers = listRequestedWorkers;
    dialogRef.componentInstance.bookedWorkers = listBookedWorkers;
    dialogRef.componentInstance.declinedWorkers = listDeclinedWorkers;
    dialogRef.componentInstance.interestedWorkers = listInterestedWorkers;
    dialogRef.componentInstance.hideBookedWorkersList = true;
    dialogRef.componentInstance.hideRequestedWorkersList = true;
    dialogRef.componentInstance.hideDeclinedWorkersList = listDeclinedWorkers.length == 0 ? true : false;
    dialogRef.componentInstance.hideInterestedWorkersList = true;
    dialogRef.componentInstance.isRequestedWorkersListActionable = false;
    dialogRef.componentInstance.isInterestedWorkersListActionable = false;
    dialogRef.componentInstance.isBookedWorkersListActionable = false;
    dialogRef.componentInstance.hideWaitingApproval = false;
    dialogRef.componentInstance.hideWorkerDeclined = true;
    dialogRef.componentInstance.hideneedExperience = true;

    dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      this.dialogResult = result;
    });
  }

  openEditShiftDialog(aShift): void {
    if ((aShift.lat == undefined) || (aShift.lng == undefined)) {
      aShift.lat = 51.509865;
      aShift.lng = -0.118092;
    }
    if (aShift.duration != undefined) {
      var arr = aShift.duration.split(':');
      aShift.durationHH = arr[0];
      aShift.durationMM = arr[1];
      console.log(aShift.durationHH);
      console.log(aShift.durationMM);
    }
    const dialogRef = this.dialog.open(MyshiftEditDialogComponent, {
      panelClass: 'my-panel',
      width: '990px',
      data: { shift: aShift }
    });

    dialogRef.componentInstance.hideExperience = false;
    dialogRef.componentInstance.hideMap = false;
    dialogRef.componentInstance.hideSaveButton = false;
    dialogRef.componentInstance.hidePostDeleteShiftButton = true;


    dialogRef.componentInstance.title = 'SHIFT NO: ' + aShift.subcategory + ' #' + aShift.shiftNo;
    dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      this.dialogResult = result;
    });
  }

  openAdvanceBookingEditDialog(aShift): void {
    if ((aShift.lat == undefined) || (aShift.lng == undefined)) {
      aShift.lat = 51.509865;
      aShift.lng = -0.118092;
    }
    if (aShift.duration != undefined) {
      var arr = aShift.duration.split(':');
      aShift.durationHH = arr[0];
      aShift.durationMM = arr[1];
      console.log(aShift.durationHH);
      console.log(aShift.durationMM);
    }
    const dialogRef = this.dialog.open(MyshiftEditDialogComponent, {
      panelClass: 'my-panel',
      width: '990px',
      data: { shift: aShift }
    });

    dialogRef.componentInstance.hideExperience = true;
    dialogRef.componentInstance.hideMap = true;
    dialogRef.componentInstance.hideSaveButton = true;
    dialogRef.componentInstance.hidePostDeleteShiftButton = false;


    dialogRef.componentInstance.title = 'SHIFT NO: ' + aShift.subcategory + ' #' + aShift.shiftNo;
    dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      this.dialogResult = result;
    });
  }

  openInformDialog(title = '', msg = ''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle = "OK";

  }

  openLocationLookupDialog(): void {
    const dialogRef = this.dialog.open(LocationLookupDialogComponent, {
      panelClass: 'my-panel',
      width: '750px',
      data: {
        lat: this.shift.lat,
        lng: this.shift.lng
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      //console.log(`The dialog was closed: ${result}`);
      this.dialogResult = result;

      this.shift.address = result.address;
      this.shift.city = result.city;
      this.shift.lat = result.lat;
      this.shift.lng = result.lng;

    });
  }

  onMyShiftsTabSelected() {
    console.log('onMyShiftsTabSelected clicked');
    this.hideFilterDate = false;
    this.hideShiftTitle = false;
    this.hideNewConstructionShiftTitle = true;
  }
  onNewShiftTabSelected() {
    console.log('onNewShiftTabSelected clicked');
    this.hideFilterDate = true;
    this.hideShiftTitle = true;
    this.hideNewConstructionShiftTitle = false;
  }
  onAdvanceBookingsTabSelected() {
    console.log('onAdvanceBookingsTabSelected clicked');
    this.hideFilterDate = false;
    this.hideShiftTitle = false;
    this.hideNewConstructionShiftTitle = true;
  }
}
