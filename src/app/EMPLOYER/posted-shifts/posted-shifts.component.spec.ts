import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostedShiftsComponent } from './posted-shifts.component';

describe('PostedShiftsComponent', () => {
  let component: PostedShiftsComponent;
  let fixture: ComponentFixture<PostedShiftsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostedShiftsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostedShiftsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
