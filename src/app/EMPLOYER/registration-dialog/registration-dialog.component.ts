import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration-dialog',
  templateUrl: './registration-dialog.component.html',
  styleUrls: ['./registration-dialog.component.css']
})
export class RegistrationDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<RegistrationDialogComponent>, @Inject(MAT_DIALOG_DATA) public data:string,
  private router: Router,
  ) { }

  ngOnInit() {
  }

  // cancelClick(): void {
  //   this.dialogRef.close();
  // }

  openEmployeeUserExp() {
    this.dialogRef.close();
    this.router.navigate(['employee_user_experience']);
  }

}
