import { Component } from '@angular/core';
import { DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material';
// import { EmployerDetailsDialogComponent } from '../employer-details-dialog/employer-details-dialog.component';
import { NoticeDetailsDialogComponent } from '../../dialogs/notice-details-dialog/notice-details-dialog.component';

import { ApproveRejectDialogComponent } 
from '../../dialogs/approve-reject-dialog/approve-reject-dialog.component';
import { SuccessOkDialogComponent } 
from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { CompletedPendingDisputedViewdetailsDialogComponent } 
from '../../dialogs/completed-pending-disputed-viewdetails-dialog/completed-pending-disputed-viewdetails-dialog.component';

import { SearchBoxService } from "../../services/searchbox.service";

import { AuthService } from '../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators }from '@angular/forms';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';
import * as moment from 'moment';

import { EmployerTimesheetCompletedFilterPipe } from '../../filters/employertimesheetcompleted-filter.pipe';
import { EmployerTimesheetDisputedFilterPipe } from '../../filters/employertimesheetdisputed-filter.pipe';
import { EmployerTimesheetPendingFilterPipe } from '../../filters/employertimesheetpending-filter.pipe';

@Component({
  selector: 'app-employer-dashboard',
  templateUrl: './employer-dashboard.component.html',
  styleUrls: ['./employer-dashboard.component.css'],
  providers: [EmployerTimesheetCompletedFilterPipe,
    EmployerTimesheetDisputedFilterPipe,
    EmployerTimesheetPendingFilterPipe]
})
export class EmployerDashboardComponent {
  profile: any;

  dialogResult="";
  noticeslist = [];

  workerslist= [];
  
  monthlyBookingShiftsCount = 0;
  totalBookingShiftsCount = 0;

  totalShiftCount =0;
  monthlyShiftCount = 0;
  pendingShiftCount = 0;
  completedShiftCount = 0;
  disputedShiftCount = 0;
  pendingShiftPerce = 0;
  completedShiftPerce = 0;
  disputedShiftPerce = 0;


  unreadNoticesCount = 0;


  sub: any;

  hideDashboardTopTab = false;
  hideGraph = true;
  hideDashboardBelowTab = true;

  private noticeCollectionSub: Subscription;
  private workersSubscription: Subscription;
  private shiftSubscription: Subscription;
  private bookingSubscription: Subscription;

  // lineChart
  public shiftsLineChartData:Array<any> = [
    {data: [0], label: 'This Month'},
    {data: [0], label: 'Previous Month'},
   
  ];

  public shiftsLineChartLabels:Array<any> = ['1', '2', '3', '4', '5', '6', '7', '8', '9','10', '11', '12', '13', '14', '15',
  '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30'];

  public shiftsLineChartOptions:any = {
    responsive: false,
    events:[]
  };

  public shiftsLineChartColors:Array<any> = [
   
    { // brown
      backgroundColor: 'rgba(212,143,79,0.8)',
      borderColor: 'rgba(212,143,79,1)',
      pointBackgroundColor: 'rgba(212,143,79,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(212,143,79,1)'
    },
    { // blue
      backgroundColor: 'rgba(86, 116, 246,0.8)',
      borderColor: 'rgba(86, 116, 246,1)',
      pointBackgroundColor: 'rgba(86, 116, 246,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(86, 116, 246,0.8)'
    }
  ];

  public shiftsLineChartLegend:boolean = true;
  public shiftsLineChartType:string = 'line';

  // Bookings Line Chart
  public totalBookingsLineChartData:Array<any> = [
    {data: [0, 7, 9, 10, 2], label: ''},   
  ];
  public totalBookingsLineChartLabels:Array<any> = ['1', '2', '3', '4', '5', '6', '7', '8', '9','10', '11', '12', '13', '14', '15',
  '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];

  public thisMonthBookingsLineChartData:Array<any> = [
    {data: [0, 7, 9, 10, 2], label: ''},   
  ];
  public thisMonthBookingsLineChartLabels:Array<any> = ['1', '2', '3', '4', '5', '6', '7', '8', '9','10', '11', '12', '13', '14', '15',
  '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];


  public bookingsLineChartOptions:any = {
    responsive: false,
    events:[],
    legend:false,
    scales: {
      xAxes: [{
          gridLines: {
              display:false
          },
          display: false
      }],
      yAxes: [{
          gridLines: {
              display:false
          },
          display: false   
      }]
    },
    elements: {
      point:{
          radius: 0
      }
    }

  };

  public bookingsLineChartColors:Array<any> = [
    { // blue
      backgroundColor: 'rgba(117, 142, 248,1)',
      borderColor: 'rgba(117, 142, 248,0)',
      pointBackgroundColor: 'rgba(117, 142, 248,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(117, 142, 248,0.8)'
    }
  ];

  public bookingsLineChartLegend:boolean = false;
  public bookingsLineChartType:string = 'line';

            // Doughnut
  public doughnutCompletedChartLabels:string[] = ['Completed', 'Completed-Inverse'];
  public doughnutCompletedChartData:number[] = [40, 60];
  public doughnutPendingChartLabels:string[] = ['Pending', 'Pending-Inverse'];
  public doughnutPendingChartData:number[] = [10, 90];
  public doughnutDisputedChartLabels:string[] = ['Disputed', 'Disputed-Inverse'];
  public doughnutDisputedChartData:number[] = [20, 80];
  public doughnutChartType:string = 'doughnut';
  public doughnutOptions =  {
    // This chart will not respond to mousemove, etc
    events: []
  }
  public doughnutColors:any[] = [
    { backgroundColor: ['rgba(212,143,79,1)', 'rgba(218,218,218,1)', ] },
    { borderColor: ["#AEEBF2", "#FEFFC9"] }];


  constructor(public dialog: MatDialog,
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    private route: ActivatedRoute,
    private searchBoxService: SearchBoxService,
    private employerTimesheetCompletedFilterPipe: EmployerTimesheetCompletedFilterPipe,
    private employerTimesheetDisputedFilterPipe: EmployerTimesheetDisputedFilterPipe,
    private employerTimesheetPendingFilterPipe: EmployerTimesheetPendingFilterPipe,
    ) {

      

    }

  ngOnInit() {
    // Reset search box in navigation
    this.searchBoxService.clearMessage();

    this.sub = this.route.params.subscribe(params => {
      //console.log(params);
      if(params['show'] != undefined){
        if(params['show'] == 'notifications'){
          //console.log('setting active tab');
          this.activeTab = 'notifications';
        }
      }
   });



    this.updateNoticesList();
    this.updateWorkersList();
    this.updateShiftsList();
    this.updateBookings();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
    this.noticeCollectionSub.unsubscribe();
    this.workersSubscription.unsubscribe();
    this.shiftSubscription.unsubscribe();
    this.bookingSubscription.unsubscribe();
  }

  // events on slice click
  public chartClicked(e:any):void {
    console.log(e);
  }
 
 // event on pie chart slice hover
  public chartHovered(e:any):void {
    console.log(e);
  }
  
  updateShiftsList(){
    var aCollection = this.firestore.collection('shifts', ref => 
      ref.where('companyId','==',this.authService.companyProfile.id)
    );
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.shiftSubscription = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(shiftslist => {
      var shifts = [];
      var thisMonthShiftsByDateLabel = {};
      var prevMonthShiftsByDateLabel = {};
      shifts = shiftslist;
      if (shiftslist != undefined) {
        this.totalShiftCount = shiftslist.length;
        this.monthlyShiftCount = 0;

        var beginningOfThisMonth = moment().startOf('month').toDate().getTime();
        var endOfThisMonth = moment().endOf('month').toDate().getTime();

        var beginningOfPrevMonth = moment().subtract(1,'months').startOf('month').toDate().getTime();
        var endOfPrevMonth = moment().subtract(1,'months').endOf('month').toDate().getTime();
      
    
        var thisMonthCount = 0;
        for (const aShift of shifts) {
          var timeStamp = aShift['createdAt'].toDate().getTime();
          var dayOfMonth = aShift['createdAt'].toDate().getDate() + '';
          if ((timeStamp >= beginningOfThisMonth) && (timeStamp <= endOfThisMonth)) {
            thisMonthCount++;
            var arr = thisMonthShiftsByDateLabel[dayOfMonth];
            if (arr == undefined) { arr = []; }
            arr.push(aShift);
            thisMonthShiftsByDateLabel[dayOfMonth] = arr;
          } else if ((timeStamp >= beginningOfPrevMonth) && (timeStamp <= endOfPrevMonth)) {
            var arr = prevMonthShiftsByDateLabel[dayOfMonth];
            if (arr == undefined) { arr = []; }
            arr.push(aShift);
            prevMonthShiftsByDateLabel[dayOfMonth] = arr;
          }
        }
        this.monthlyShiftCount = thisMonthCount;

        // Prepare previous month and this month day-wise data
        const thisMonthDays = moment().daysInMonth();
        const prevMonthDays = moment().subtract(1,'months').daysInMonth();
  
        var maxDays = thisMonthDays;
        if (prevMonthDays > thisMonthDays) {
          maxDays = prevMonthDays;
        }
  
        this.shiftsLineChartLabels = [];
        for (var i=0; i<maxDays; i++) {
          this.shiftsLineChartLabels.push((i+1)+'');
        }

        //console.log(thisMonthShiftsByDateLabel);
        //console.log(prevMonthShiftsByDateLabel);

        var todayDateOfMonth = moment().toDate().getDate();
        var thisMonthDayWiseCount = [];
        for (var i=0; i<todayDateOfMonth; i++) {
          var dateStr = (i+1)+'';
          if (thisMonthShiftsByDateLabel[dateStr] != undefined) {
            thisMonthDayWiseCount.push(thisMonthShiftsByDateLabel[dateStr].length);
          } else {
            thisMonthDayWiseCount.push(0);
          }
        } 

        var prevMonthDayWiseCount = [];
        for (var i=0; i<prevMonthDays; i++) {
          var dateStr = (i+1)+'';
          if (prevMonthShiftsByDateLabel[dateStr] != undefined) {
            prevMonthDayWiseCount.push(prevMonthShiftsByDateLabel[dateStr].length);
          } else {
            prevMonthDayWiseCount.push(0);
          }
        } 

        /*
        public shiftsLineChartData:Array<any> = [
          {data: [28, 48, 40, 19, 86, 27, 90], label: 'This Month'},
          {data: [65, 59, 80, 81, 56, 55, 40, 
            65, 59, 80, 81, 56, 55, 40, 
            65, 59, 80, 81, 56, 55, 40, 
            65, 59, 80, 81, 56, 55, 40,
            65, 59], label: 'Previous Month'},
        
        ];
        */

       var shiftsLineChartDataTemp = [];
       shiftsLineChartDataTemp.push({data: thisMonthDayWiseCount, label: 'This Month'});
       shiftsLineChartDataTemp.push({data: prevMonthDayWiseCount, label: 'Previous Month'});

       this.shiftsLineChartData = shiftsLineChartDataTemp;
      }
    });
  }

  updateWorkersList(){
    //console.log('updateWorkersList');
    //console.log(this.authService.companyProfile);
    var aCollection = this.firestore.collection('profiles', ref => 
    ref.where('profileType','==','worker')
      .where('approvalStatus','==','approved')
      .where('wProfession','==',this.authService.companyProfile.industry)
  );
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.workersSubscription = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(workerslist => {
      if (workerslist != undefined) {
       this.workerslist = workerslist;
       //console.log('workers');
       //console.log(workerslist);
      } else {
        this.workerslist = [];
      }
    });
     
   }

  updateNoticesList(){
    const aCollection = this.firestore.collection('profiles')
    .doc(this.authService.loggedInProfile.userId)
    .collection<any>('notices', ref => ref.orderBy('createdAt','desc'));
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.noticeCollectionSub = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(noticeslist => {
        this.noticeslist = noticeslist;
        let cnt = 0;
        if ((noticeslist != undefined) && (noticeslist.length > 0)) {
          for (const aNotice of noticeslist) {
            if (aNotice.isRead == false) {
              cnt++;
            }
          }
        }
        this.unreadNoticesCount = cnt;
          //console.log(this.noticeslist);
    });
  }

  updateBookings(){
    const aCollection = this.firestore.collection('bookings',ref => 
      ref.where('companyId','==',this.authService.loggedInProfile.companyId)
    );
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.bookingSubscription = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(someList => {

      var blist = [];
      var thisMonthBookingsByDateLabel = {};
      var totalBookingsByDateLabel = {};

      if(someList != undefined){
        blist = someList;
      }
      //console.log('boobking eceived');
      //console.log(blist);

      var beginningOfThisMonth = moment().startOf('month').toDate().getTime();
      var endOfThisMonth = moment().endOf('month').toDate().getTime();

      var beginningOfPrevMonth = moment().subtract(1,'months').startOf('month').toDate().getTime();
      var endOfPrevMonth = moment().subtract(1,'months').endOf('month').toDate().getTime();
    
  
      var thisMonthShiftsSet = new Set();
      var totalShiftsSet = new Set();

      for (const aBook of blist) {
        totalShiftsSet.add(aBook.shiftId);

        var timeStamp = aBook['createdAt'].toDate().getTime();
        var dateStr = moment(aBook['createdAt'].toDate()).format('YYYY/MM/DD');
        if ((timeStamp >= beginningOfThisMonth) && (timeStamp <= endOfThisMonth)) {
          thisMonthShiftsSet.add(aBook.shiftId);
          var set = thisMonthBookingsByDateLabel[dateStr];
          if (set == undefined) { set = new Set(); }
          set.add(aBook.shiftId);
          thisMonthBookingsByDateLabel[dateStr] = set;
        }

        var set = totalBookingsByDateLabel[dateStr];
        if (set == undefined) { set = new Set(); }
        set.add(aBook.shiftId);
        totalBookingsByDateLabel[dateStr] = set;
        
      }

      this.monthlyBookingShiftsCount = thisMonthShiftsSet.size;
      this.totalBookingShiftsCount = totalShiftsSet.size;

      // Prepare this month and total bookings day-wise data
      const thisMonthDays = moment().daysInMonth();
      this.thisMonthBookingsLineChartLabels = [];
      for (var i=0; i<thisMonthDays; i++) {
        this.thisMonthBookingsLineChartLabels.push((i+1)+'');
      }

      var todayTime = moment().toDate().getTime();
      var thisMonthDayWiseCount = [];
      for (var aDate = moment().startOf('month').toDate(); 
        aDate.getTime() < todayTime; 
        aDate = moment(aDate).add(1,'days').toDate()) {
        var dateStr = moment(aDate).format('YYYY/MM/DD');
        if (thisMonthBookingsByDateLabel[dateStr] != undefined) {
          thisMonthDayWiseCount.push(thisMonthBookingsByDateLabel[dateStr].size);
        } else {
          thisMonthDayWiseCount.push(0);
        }
      } 
      //console.log('this month booking');
      //console.log(thisMonthDayWiseCount);
      if (thisMonthDayWiseCount.length < 1) {
        this.thisMonthBookingsLineChartData = [{data: [0], label: ''}];
      } else {
        this.thisMonthBookingsLineChartData = [{data: thisMonthDayWiseCount, label: ''}];
      }


      var totalDatesArr = Object.keys(totalBookingsByDateLabel).sort();
      if (totalDatesArr.length > 0) {
        var firstDate = moment(totalDatesArr[0]).toDate();
        var lastDate = moment(totalDatesArr[totalDatesArr.length - 1]).toDate();

        var totalBookingDayWiseCount = [];
        var totalBookingDayLabels = [];
        var counter = 0;
        for (var aDate = firstDate; 
          aDate.getTime() <= lastDate.getTime(); 
          aDate = moment(aDate).add(1,'days').toDate()) {
          counter++;
          totalBookingDayLabels.push(counter+'');
          var dateStr = moment(aDate).format('YYYY/MM/DD');
          if (totalBookingsByDateLabel[dateStr] != undefined) {
            totalBookingDayWiseCount.push(totalBookingsByDateLabel[dateStr].size);
          } else {
            totalBookingDayWiseCount.push(0);
          }
        } 
        //console.log('total booking');
        //console.log(totalBookingDayWiseCount);
        this.totalBookingsLineChartData = [{data: totalBookingDayWiseCount, label: ''}];
        this.totalBookingsLineChartLabels = totalBookingDayLabels;
      } else {
        this.totalBookingsLineChartData = [{data: [0], label: ''}];
        this.totalBookingsLineChartLabels = ['0'];
      }


      var workerCompletedBlist = [];
      for (const aBook of blist) {
        if ((aBook.workingStatus != undefined) && (aBook.workingStatus == 'completed')) {
          workerCompletedBlist.push(aBook);
        }
      }

      var compls = this.employerTimesheetCompletedFilterPipe.transform(workerCompletedBlist);
      var pends = this.employerTimesheetPendingFilterPipe.transform(workerCompletedBlist);
      var dispts = this.employerTimesheetDisputedFilterPipe.transform(workerCompletedBlist);

      var completedSet = new Set();
      var pendingSet = new Set();
      var disputedSet = new Set();

      for (const aBook of dispts) {
        disputedSet.add(aBook.shiftId);
      }

      for (const aBook of pends) {
        if (disputedSet.has(aBook) == false) {
          pendingSet.add(aBook.shiftId);
        }
      }

      for (const aBook of compls) {
        if ((disputedSet.has(aBook) == false)  && (pendingSet.has(aBook) == false)) {
          completedSet.add(aBook.shiftId);
        }
      }

      // console.log('boobking completed');
      // console.log(completedSet);

      // console.log('boobking pending');
      // console.log(pendingSet);

      // console.log('boobking dispts');
      // console.log(disputedSet);

      this.pendingShiftCount = pendingSet.size;
      this.disputedShiftCount = disputedSet.size;
      this.completedShiftCount = completedSet.size;

      
      // console.log('boobking totalShiftCount');
      // console.log(this.totalShiftCount);

      if(this.totalShiftCount>0){
        this.pendingShiftPerce = parseInt((this.pendingShiftCount*100/this.totalShiftCount)+'');
        this.completedShiftPerce = parseInt((this.completedShiftCount*100/this.totalShiftCount)+'');
        this.disputedShiftPerce = parseInt((this.disputedShiftCount*100/this.totalShiftCount)+'');
      } else {
        this.pendingShiftPerce = 0;
        this.completedShiftPerce = 0;
        this.disputedShiftPerce = 0;
      }
      

      this.doughnutCompletedChartData = [this.completedShiftPerce, 100-this.completedShiftPerce];
      this.doughnutPendingChartData = [this.pendingShiftPerce, 100-this.pendingShiftPerce];
      this.doughnutDisputedChartData = [this.disputedShiftPerce, 100-this.disputedShiftPerce];

      
    });
  }

  // openDialog(): void {
  //   const dialogRef = this.dialog.open(EmployerDetailsDialogComponent, {
  //     panelClass: 'my-panel',
  //     width: '600px',
  //   });

  //    dialogRef.afterClosed().subscribe(result => {
  //     console.log(`The dialog was closed: ${result}`);
  //     this.dialogResult = result;
  //   });
  // }

  advanceBookingClick(profile): void {
    //this.dialogRef.close();
   // this.dialogRef.close();
    this.router.navigate(['worker_advance_booking', {'workerId':profile.id,
      'workerFirstName':profile.firstName,
      'workerLastName':profile.lastName,
      'workerNumber':profile.wProfessionBodyNumber,
      'workerSubCategory':profile.wSubCategory,
    }]);  
  }

  activeTab = 'summary';

  summary(activeTab){
    this.activeTab = activeTab;
    //console.log('onSummaryTabSelected click');
    this.hideGraph = true;
    this.hideDashboardBelowTab = true;
    this.hideDashboardTopTab = false;
  }

  notifications(activeTab){
    this.activeTab = activeTab;
    //console.log('onSummaryTabSelected click');
    this.hideGraph = false;
    this.hideDashboardBelowTab = false;
    this.hideDashboardTopTab = true;
  }

  // summary() {
  //   console.log('onPostedTabSelected clicked');
  //   this.hideNewShiftsThisWeekTitle = false;
  //   this.hideBookingsShiftsTitle = true;
  //   this.hideCancelledShiftsTitle = true;
  //   this.hideOngoingShiftsTitle = true;
  //   this.hideApprovedShiftsTitle = true;
  //   this.hideOpenDisputesShiftsTitle = true;
  //   this.hideResolvedDisputesShiftsTitle = true;
  // }

  openNoticeDetailsDialog(aNotice): void {

    const dialogRef = this.dialog.open(NoticeDetailsDialogComponent, {
      panelClass: 'my-panel',
      width: '600px',
    });
    
    if (aNotice.type == 'workerAppliedToShift'){
      if ((aNotice.alreadyActed == undefined) || (aNotice.alreadyActed == false)) {
        dialogRef.componentInstance.buttonDisabled = false;
      } else {
        dialogRef.componentInstance.buttonDisabled = true;
      }
      dialogRef.componentInstance.buttonTitle = 'accept worker';
      dialogRef.componentInstance.subjectTitle = aNotice.shiftTitle;
      dialogRef.componentInstance.p1 = aNotice.shiftTitle;
      dialogRef.componentInstance.p2 = aNotice.title;
      dialogRef.componentInstance.p3 = '';

      dialogRef.componentInstance.buttonDisabled = true;

      this.firestore.collection('bookingrequests').doc(aNotice.bookingRequestId).ref.get()
        .then((adoc) => {
          if (!adoc.exists) { 
            return; 
          }         
          var adata = adoc.data();
          adata.id = adoc.id;

          if ((adata.approvalStatus == undefined) || (adata.approvalStatus == 'pending')) {
            dialogRef.componentInstance.buttonDisabled = false;
          } else {
            dialogRef.componentInstance.buttonDisabled = true;
          }


        })
        .catch((err) => {
          console.log(err);
        });

      dialogRef.afterClosed().subscribe(result => {
        //console.log(`The dialog was closed: ${result}`);
        this.dialogResult = result;
  
        if(result == dialogRef.componentInstance.buttonTitle){
          if (aNotice.type == 'workerAppliedToShift'){
            // Perform Accept worker
  
            const dialogRef = this.dialog.open(ApproveRejectDialogComponent, {
              panelClass: 'my-panel',
              width: '600px',
            });
            dialogRef.componentInstance.title='Confirmation';
            dialogRef.componentInstance.descr = 'Do you really want to confirm booking with '+
              aNotice.workerTitle+'?';
            dialogRef.componentInstance.approveTitle = 'Yes, Confirm';
            dialogRef.componentInstance.rejectTitle = 'No, Cancel';
  
            dialogRef.afterClosed().subscribe(result => {
              //console.log(`The dialog was closed: ${result}`);
              if (result == dialogRef.componentInstance.approveTitle) {
                // Change approvalStatus to approved on bookingrequest
                const reqId = aNotice.bookingRequestId;
                const splitted = reqId.split('-');
                var shiftId = splitted[0];
                var workerId = splitted[1];
  
                this.authService.doAcceptWorkerForBookingRequest(
                  reqId, 
                  shiftId, 
                  workerId, 
                  this.authService.companyProfile.id)
                .then((aObj) => {
  
                  this.firestore.collection('profiles').doc(this.authService.loggedInProfile.userId)
                  .collection('notices').doc(aNotice.id).set(
                    {alreadyActed:true},
                    {merge:true}
                  )
                  .then((aObj) => {
                    this.openDialog('SUCCESS', 'Worker booked successfully');
                  })
                  .catch((err)=>{
                    console.log(err);
                    this.openDialog("Error", err.message);
                  });
  
                }).catch((err)=>{
                  console.log(err);
                  this.openDialog("Error", err.message);
                }) ;
              }
            });
          }
        }
      });
  
    }

    if (aNotice.type == 'workerAcceptedBookingRequest'){
      if ((aNotice.alreadyActed == undefined) || (aNotice.alreadyActed == false)) {
        dialogRef.componentInstance.buttonDisabled = false;
      } else {
        dialogRef.componentInstance.buttonDisabled = true;
      }

      var datePipe = new DatePipe('en-GB');

      dialogRef.componentInstance.buttonTitle = 'view details';
      dialogRef.componentInstance.subjectTitle = aNotice.shiftTitle;
      dialogRef.componentInstance.p1 = aNotice.shiftTitle;
      dialogRef.componentInstance.p2 = aNotice.title;

      dialogRef.afterClosed().subscribe(result => {

        if (result == dialogRef.componentInstance.buttonTitle){
          // view details clicked
          this.router.navigate(['bookings', {bookingId:aNotice.bookingId}]);
        }
      });

    }

    if (aNotice.type == 'employerAcceptedBookingRequest'){
      if ((aNotice.alreadyActed == undefined) || (aNotice.alreadyActed == false)) {
        dialogRef.componentInstance.buttonDisabled = false;
      } else {
        dialogRef.componentInstance.buttonDisabled = true;
      }

      var datePipe = new DatePipe('en-GB');

      dialogRef.componentInstance.buttonTitle = 'view details';
      dialogRef.componentInstance.subjectTitle = aNotice.shiftTitle;
      dialogRef.componentInstance.p1 = aNotice.shiftTitle;
      dialogRef.componentInstance.p2 = aNotice.title;

      dialogRef.afterClosed().subscribe(result => {

        if (result == dialogRef.componentInstance.buttonTitle){
          // view details clicked
          this.router.navigate(['bookings', {bookingId:aNotice.bookingId}]);
        }
      });

    }

    if (aNotice.type == 'employerCancelledBooking'){
      if ((aNotice.alreadyActed == undefined) || (aNotice.alreadyActed == false)) {
        dialogRef.componentInstance.buttonDisabled = false;
      } else {
        dialogRef.componentInstance.buttonDisabled = true;
      }

      var datePipe = new DatePipe('en-GB');

      dialogRef.componentInstance.buttonTitle = 'view details';
      dialogRef.componentInstance.subjectTitle = aNotice.shiftTitle;
      dialogRef.componentInstance.p1 = aNotice.shiftTitle;
      dialogRef.componentInstance.p2 = aNotice.title;

      dialogRef.afterClosed().subscribe(result => {

        if (result == dialogRef.componentInstance.buttonTitle){
          // view details clicked
          this.router.navigate(['bookings', {bookingId:aNotice.bookingId}]);
        }
      });

    }

    if (aNotice.type == 'employerRejectedBookingRequest'){
      if ((aNotice.alreadyActed == undefined) || (aNotice.alreadyActed == false)) {
        dialogRef.componentInstance.buttonDisabled = false;
      } else {
        dialogRef.componentInstance.buttonDisabled = true;
      }

      var datePipe = new DatePipe('en-GB');

      dialogRef.componentInstance.buttonDisabled = true;
      dialogRef.componentInstance.subjectTitle = aNotice.shiftTitle;
      dialogRef.componentInstance.p1 = aNotice.shiftTitle;
      dialogRef.componentInstance.p2 = 'declined booking request ' + aNotice.shiftTitle;

    }

    if (aNotice.type == 'workerCancelledBooking'){
      if ((aNotice.alreadyActed == undefined) || (aNotice.alreadyActed == false)) {
        dialogRef.componentInstance.buttonDisabled = false;
      } else {
        dialogRef.componentInstance.buttonDisabled = true;
      }

      var datePipe = new DatePipe('en-GB');

      dialogRef.componentInstance.buttonTitle = 'view details';
      dialogRef.componentInstance.subjectTitle = aNotice.shiftTitle;
      dialogRef.componentInstance.p1 = aNotice.shiftTitle;
      dialogRef.componentInstance.p2 = aNotice.title;

      dialogRef.afterClosed().subscribe(result => {

        if (result == dialogRef.componentInstance.buttonTitle){
          // view details clicked
          this.router.navigate(['bookings', {bookingId:aNotice.bookingId}]);
        }
      });

    }

    if (aNotice.type == 'workerRejectedDirectBooking'){
      if ((aNotice.alreadyActed == undefined) || (aNotice.alreadyActed == false)) {
        dialogRef.componentInstance.buttonDisabled = false;
      } else {
        dialogRef.componentInstance.buttonDisabled = true;
      }

      var datePipe = new DatePipe('en-GB');

      dialogRef.componentInstance.buttonDisabled = true;
      dialogRef.componentInstance.subjectTitle = aNotice.shiftTitle;
      dialogRef.componentInstance.p1 = aNotice.shiftTitle;
      dialogRef.componentInstance.p2 = aNotice.title;

    }


    if (aNotice.type == 'workerCompletedWork'){
      if ((aNotice.alreadyActed == undefined) || (aNotice.alreadyActed == false)) {
        dialogRef.componentInstance.buttonDisabled = false;
      } else {
        dialogRef.componentInstance.buttonDisabled = true;
      }

      var datePipe = new DatePipe('en-GB');

      dialogRef.componentInstance.buttonTitle = 'approve shift';
      dialogRef.componentInstance.subjectTitle = aNotice.shiftTitle;
      dialogRef.componentInstance.p1 = aNotice.shiftTitle;
      dialogRef.componentInstance.p2 = 'Shift submited @' + datePipe.transform(aNotice.createdAt.toDate() , 'HH:mm MMM dd, yyyy' );

      dialogRef.componentInstance.buttonDisabled = true;

      this.firestore.collection('bookings').doc(aNotice.bookingId).ref.get()
        .then((adoc) => {
          if (!adoc.exists) { 
            return; 
          }         
          var adata = adoc.data();
          adata.id = adoc.id;

          dialogRef.componentInstance.booking = adata;
          dialogRef.componentInstance.p3 = 'Start time: ' + 
          datePipe.transform(adata.workStartTime.toDate() , 'HH:mm' ) + 
          ' End time: ' + 
          datePipe.transform(adata.workEndTime.toDate() , 'HH:mm' ) + 
          ' Duration: ' + parseInt((adata.workedMins/60)+'')+'hrs ' +
          parseInt((adata.workedMins%60)+'')+'mins ';

          if (((adata.workingStatus != undefined) && (adata.workingStatus == 'completed'))&&
          ((adata.timesheetStatus == undefined) || (adata.timesheetStatus == 'pending'))){
            dialogRef.componentInstance.buttonDisabled = false;
          } else {
            dialogRef.componentInstance.buttonDisabled = true;
          }
          
        })
        .catch((err) => {
          console.log(err);
        });
      dialogRef.afterClosed().subscribe(result => {

        if (result == dialogRef.componentInstance.buttonTitle){
          var dialogRef2 = this.dialog.open(ApproveRejectDialogComponent, {
            width: '600px',
          });

          dialogRef2.componentInstance.refObject = dialogRef.componentInstance.booking;
          dialogRef2.componentInstance.title = 'Are you sure?';
          dialogRef2.componentInstance.descr = 
          "Do you really want to approve this timesheet?";
      
          dialogRef2.componentInstance.approveTitle = 'Yes, Approve';
          dialogRef2.componentInstance.rejectTitle = 'No, Cancel';

          dialogRef2.afterClosed().subscribe(result => {
            if (result == dialogRef2.componentInstance.approveTitle){
              this.authService.doApproveShift(dialogRef2.componentInstance.refObject.id,
                dialogRef2.componentInstance.refObject.workedMins, dialogRef2.componentInstance.refObject.fee)
              .then((aObj) => {
                this.openDialog("SUCCESS","Shift approved successfully.");
              })
              .catch((err) => {
                console.log(err);
                this.openDialog("Error",err.message);
              });
            }
          });

        }
      
      });
    }

    if (aNotice.type == 'workerPausedWork'){
      
      dialogRef.componentInstance.buttonDisabled = true;
      

      var datePipe = new DatePipe('en-GB');

      dialogRef.componentInstance.buttonDisabled = true;
      dialogRef.componentInstance.subjectTitle = aNotice.shiftTitle;
      dialogRef.componentInstance.p1 = aNotice.shiftTitle;
      dialogRef.componentInstance.p2 = aNotice.workerTitle+' paused shift ' + aNotice.shiftTitle;

    }

    if (aNotice.type == 'workerResumedWork'){
      
      dialogRef.componentInstance.buttonDisabled = true;
      

      var datePipe = new DatePipe('en-GB');

      dialogRef.componentInstance.buttonDisabled = true;
      dialogRef.componentInstance.subjectTitle = aNotice.shiftTitle;
      dialogRef.componentInstance.p1 = aNotice.shiftTitle;
      dialogRef.componentInstance.p2 = aNotice.workerTitle+' resumed shift ' + aNotice.shiftTitle;

    }

    if (aNotice.type == 'workerDisputeResolved'){
      if ((aNotice.alreadyActed == undefined) || (aNotice.alreadyActed == false)) {
        dialogRef.componentInstance.buttonDisabled = false;
      } else {
        dialogRef.componentInstance.buttonDisabled = true;
      }

      var datePipe = new DatePipe('en-GB');

      dialogRef.componentInstance.buttonTitle = 'view details';
      dialogRef.componentInstance.subjectTitle = aNotice.shiftTitle;
      dialogRef.componentInstance.p1 = aNotice.shiftTitle;
      dialogRef.componentInstance.p2 = 'Dispute resolved. Filed on' + datePipe.transform(aNotice.createdAt.toDate() , ' MMM dd, yyyy @ HH:mm' );

      dialogRef.afterClosed().subscribe(result => {

        if (result == dialogRef.componentInstance.buttonTitle){
          // view details clicked
          this.router.navigate(['employer_timesheet', {bookingId:aNotice.bookingId}]);
        }
      });

    }

    if (aNotice.type == 'workerRaisedDispute'){
      if ((aNotice.alreadyActed == undefined) || (aNotice.alreadyActed == false)) {
        dialogRef.componentInstance.buttonDisabled = false;
      } else {
        dialogRef.componentInstance.buttonDisabled = true;
      }

      var datePipe = new DatePipe('en-GB');

      dialogRef.componentInstance.buttonTitle = 'view details';
      dialogRef.componentInstance.subjectTitle = aNotice.shiftTitle;
      dialogRef.componentInstance.p1 = aNotice.shiftTitle;
      dialogRef.componentInstance.p2 = 'Dispute raised. Filed on' + datePipe.transform(aNotice.createdAt.toDate() , ' MMM dd, yyyy @ HH:mm' );

      dialogRef.afterClosed().subscribe(result => {

        if (result == dialogRef.componentInstance.buttonTitle){
          // view details clicked
          this.router.navigate(['employer_timesheet', {bookingId:aNotice.bookingId}]);
        }
      });

    }

    if (aNotice.type == 'employerAmendAcceptedByWorker'){
      if ((aNotice.alreadyActed == undefined) || (aNotice.alreadyActed == false)) {
        dialogRef.componentInstance.buttonDisabled = false;
      } else {
        dialogRef.componentInstance.buttonDisabled = true;
      }

      var datePipe = new DatePipe('en-GB');

      dialogRef.componentInstance.buttonTitle = 'view details';
      dialogRef.componentInstance.subjectTitle = aNotice.shiftTitle;
      dialogRef.componentInstance.p1 = aNotice.shiftTitle;
      dialogRef.componentInstance.p2 = 'Amend accepted by worker on' + datePipe.transform(aNotice.createdAt.toDate() , ' MMM dd, yyyy @ HH:mm' );

      dialogRef.afterClosed().subscribe(result => {

        if (result == dialogRef.componentInstance.buttonTitle){
          // view details clicked
          this.router.navigate(['employer_timesheet', {bookingId:aNotice.bookingId}]);
        }
      });

    }

    if (aNotice.type == 'shiftApproved'){
      if ((aNotice.alreadyActed == undefined) || (aNotice.alreadyActed == false)) {
        dialogRef.componentInstance.buttonDisabled = false;
      } else {
        dialogRef.componentInstance.buttonDisabled = true;
      }

      var datePipe = new DatePipe('en-GB');

      dialogRef.componentInstance.buttonTitle = 'view details';
      dialogRef.componentInstance.subjectTitle = aNotice.shiftTitle;
      dialogRef.componentInstance.p1 = aNotice.shiftTitle;
      dialogRef.componentInstance.p2 = 'Timesheet approved on' + datePipe.transform(aNotice.createdAt.toDate() , ' MMM dd, yyyy @ HH:mm' );

      dialogRef.afterClosed().subscribe(result => {

        if (result == dialogRef.componentInstance.buttonTitle){
          // view details clicked
          this.router.navigate(['employer_timesheet', {bookingId:aNotice.bookingId}]);
        }
      });

    }

    if (aNotice.type == 'shiftPaid'){
      if ((aNotice.alreadyActed == undefined) || (aNotice.alreadyActed == false)) {
        dialogRef.componentInstance.buttonDisabled = false;
      } else {
        dialogRef.componentInstance.buttonDisabled = true;
      }

      var datePipe = new DatePipe('en-GB');

      dialogRef.componentInstance.buttonTitle = 'view details';
      dialogRef.componentInstance.subjectTitle = aNotice.shiftTitle;
      dialogRef.componentInstance.p1 = aNotice.shiftTitle;
      dialogRef.componentInstance.p2 = 'Timesheet paid on' + datePipe.transform(aNotice.createdAt.toDate() , ' MMM dd, yyyy @ HH:mm' );

      dialogRef.afterClosed().subscribe(result => {

        if (result == dialogRef.componentInstance.buttonTitle){
          // view details clicked
          this.router.navigate(['employer_timesheet', {bookingId:aNotice.bookingId}]);
        }
      });

    }

    if (aNotice.isRead == false) {
      this.firestore.collection('profiles')
      .doc(this.authService.loggedInProfile.userId)
      .collection<any>('notices').doc(aNotice.id).update({isRead: true})
      .then()
      .catch((err) => {console.error(err)});
    }
  } 

  openDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
    
  }
  deleteButtonClicked(aNotice){
    const dialogRef = this.dialog.open(ApproveRejectDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = 'Are you sure?';
    dialogRef.componentInstance.descr = 
    "Do you really want to remove this entry";

    dialogRef.componentInstance.approveTitle = 'Yes, Remove';
    dialogRef.componentInstance.rejectTitle = 'No, Cancel';

    dialogRef.afterClosed().subscribe(result => {
      if (result == dialogRef.componentInstance.approveTitle) {
        this.firestore.collection('profiles').doc(this.authService.loggedInProfile.userId)
        .collection('notices').doc(aNotice.id).delete()
          .then(() => {
            // user created. now create profile in firestore.
            this.openDialog("SUCCESS","Entry removed successfully.");
          })
          .catch((error) => {
            console.log("Error", error);
            this.openDialog("Error", error.message);
          });
      }
    });
  }

}
