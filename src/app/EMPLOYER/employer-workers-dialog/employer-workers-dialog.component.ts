import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Profile } from '../../models/profile.model';
import { Router } from '@angular/router';

import { Observable, Subscription } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';

import { AuthService } from '../../services/auth.service';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-employer-workers-dialog',
  templateUrl: './employer-workers-dialog.component.html',
  styleUrls: ['./employer-workers-dialog.component.css']
})
export class EmployerWorkersDialogComponent implements OnInit {
  profile: any;

  totalShiftCount = 0;

  private bookingSubscription: Subscription;

  
  constructor(public dialogRef: MatDialogRef<EmployerWorkersDialogComponent>, @Inject(MAT_DIALOG_DATA) public data:string, 
  private router: Router, private firestore: AngularFirestore,private afAuth: AngularFireAuth, 
  private authService: AuthService) { }

  ngOnInit() {
    
  }

  ngOnDestroy() {
    if(this.bookingSubscription != undefined){
      this.bookingSubscription.unsubscribe();
    }
  }

  public updateBookingsList(){
    var aCollection = this.firestore.collection('bookings', ref => 
    ref.where('workingStatus','==','completed')
    .where('workerId','==',this.profile.userId)
    );
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.bookingSubscription = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(shiftslist => {
      if (shiftslist != undefined) {
        this.totalShiftCount = shiftslist.length;
      } else {
        this.totalShiftCount = 0;
      }
    });
     
   }

  advanceBookingClick(): void {
    this.dialogRef.close();
    this.router.navigate(['worker_advance_booking', {'workerId':this.profile.id,
   'workerFirstName':this.profile.firstName, 
   'workerLastName':this.profile.lastName,
   'workerNumber':this.profile.wProfessionBodyNumber,
   'workerSubCategory':this.profile.wSubCategory,
   skipLocationChange:true}]);
  }

  cancelClick(): void {
    this.dialogRef.close();
  }

}
