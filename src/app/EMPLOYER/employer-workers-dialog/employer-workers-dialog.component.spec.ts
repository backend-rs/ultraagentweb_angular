import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerWorkersDialogComponent } from './employer-workers-dialog.component';

describe('EmployerWorkersDialogComponent', () => {
  let component: EmployerWorkersDialogComponent;
  let fixture: ComponentFixture<EmployerWorkersDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerWorkersDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerWorkersDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
