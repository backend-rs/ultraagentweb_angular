import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { NewshiftDoneDialogComponent } from '../../EMPLOYER/newshift-done-dialog/newshift-done-dialog.component';

import { AngularFirestore } from 'angularfire2/firestore';

import { Location } from '@angular/common';

import * as firebase from 'firebase/app';

@Component({
  selector: 'app-review-worker',
  templateUrl: './review-worker.component.html',
  styleUrls: ['./review-worker.component.css']
})
export class ReviewWorkerComponent implements OnInit {

  // workerFirstName: string;
  // workerLastName: string;
  // workerProfilePictureUrl: string;
  profile: any;
  workerId: any;
  bookingId: any;

  sub: any;

  commRating = 0;
  deliveryRating = 5;
  hireAgainRating = 0;
  whyHiredBrief = '';

  constructor(private route: ActivatedRoute,     
              private firestore: AngularFirestore,
              public dialog: MatDialog,
              private router: Router,
              private location: Location,
    ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      // this.workerFirstName = params['workerFirstName']; 
      // this.workerLastName = params['workerLastName']; 
      // this.workerProfilePictureUrl = params['workerProfilePictureUrl']; 
      this.workerId = params['workerId']; 
      this.bookingId = params['bookingId']; 

      if((this.workerId != undefined)){
        this.firestore.collection('profiles').doc(this.workerId).ref.get()
        .then((adoc) => {
          if (!adoc.exists) { 
            this.openDialog('Error', 'Account has been deleted. Contact support.');
            return; 
          }         
          var aprofile = adoc.data();
          this.profile = aprofile;
          // console.log(aprofile);
          //aprofile:Profile = aObject as Profile;
          
        })
        .catch((err) => {
          console.log(err);
          this.openDialog('Error', err.message);
        });
      }
   });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  onCommRatingChange(newValue){
    this.commRating = newValue.rating;
    // console.log(this.commRating);
  }
  onDeliveryRatingChange(newValue){
    this.deliveryRating = newValue.rating;
    // console.log(this.deliveryRating);
  }
  onWorkAgainRatingChange(newValue){
    this.hireAgainRating = newValue.rating;
    // console.log(this.hireAgainRating);
  }

  
  
  onSubmitClick(){
    if(this.commRating<1){
      this.openDialog("Communication Rating", 
      "Please select appropriate star rating for communication skills.");
      return;
    }
    if(this.deliveryRating<1){
      this.openDialog("Delivery Rating", 
      "Please select appropriate star rating for delivery skills.");
      return;
    }
    if(this.hireAgainRating<1){
      this.openDialog("Hire again Rating", 
      "Please select appropriate star rating for hire again recomendations.");
      return;
    }
    if((this.whyHiredBrief == undefined) || (this.whyHiredBrief.length < 1)){
      this.openDialog("Enter a brief", 
      "Please enter a brief for the worker.");
      return;
    }
    this.firestore.collection('profiles').doc(this.workerId).collection('ratings').doc(this.bookingId).set(
      {
        commRating:this.commRating,
        deliveryRating: this.deliveryRating,
        hireAgainRating: this.hireAgainRating,
        whyHiredBrief: this.whyHiredBrief,
        updatedAt: firebase.firestore.FieldValue.serverTimestamp()
      },
      {merge: true}
    )
    .then(() => {
      const dialogRef = this.dialog.open(NewshiftDoneDialogComponent, {
        width: '600px',
      });
      dialogRef.componentInstance.title = 'SUCCESS';
      dialogRef.componentInstance.descr = 'Rating has been updated successfully.';
      dialogRef.afterClosed().subscribe(result => {
        this.location.back();
      });
      
    })
    .catch((err) => {
      console.log(err);
      this.openDialog('Error', err.message);
    });
  }

  openDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(NewshiftDoneDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    
  }


}
