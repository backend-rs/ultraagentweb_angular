import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvanceBookingPageComponent } from './advance-booking-page.component';

describe('AdvanceBookingPageComponent', () => {
  let component: AdvanceBookingPageComponent;
  let fixture: ComponentFixture<AdvanceBookingPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvanceBookingPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvanceBookingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
