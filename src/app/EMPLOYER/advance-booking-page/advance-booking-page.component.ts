import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { Profile } from '../../models/profile.model';
import { LocationLookupDialogComponent } from '../../dialogs/location-lookup-dialog/location-lookup-dialog.component';
import { EmployerDateAdvanceBookingDialogComponent } from '../../dialogs/employer-date-advance-booking-dialog/employer-date-advance-booking-dialog.component'; 

import { AuthService } from '../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators }from '@angular/forms';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';

import {UAGlobals} from '../../globals/uaglobals';
import {UAHelpers} from '../../globals/uahelpers';

@Component({
  selector: 'app-advance-booking-page',
  templateUrl: './advance-booking-page.component.html',
  styleUrls: ['./advance-booking-page.component.css']
})
export class AdvanceBookingPageComponent implements OnInit {
  workerId: string;
  workerFirstName:string;
  workerLastName:string;
  workerNumber:string;
  newShiftDateFromPicker:Date = undefined;
  minShiftStartTime = new Date((new Date()).getTime() + 30*60000);
  shift = {
    category:'',
    subcategory:'',
    duration:'',
    durationHH:'',
    durationMM:'',
    startDateTime: this.minShiftStartTime,
    fee:'',
    contactpername:'',
    phoneNumber:'',
    address:'',
    postcode:'',
    city:'',
    description:'',
    expNeeded:'',
    lat:51.509865,
    lng:-0.118092,
    shiftNo:''
  };
  sub:any;
  
  dialogResult="";

  categories: any[];
  subcategories: any[];
  citySelectionData: any[];

  constructor(
    public dialog: MatDialog,
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    private route: ActivatedRoute) {

      this.categories = Object.keys(UAGlobals.MASTER_CATEGORIES);
  
      this.shift.category = this.authService.companyProfile.industry;
      this.subcategories = UAGlobals.MASTER_CATEGORIES[this.shift.category];
      this.citySelectionData = UAHelpers.cityDropdownData();


     // update to 30 mins from now
     this.minShiftStartTime = new Date((new Date()).getTime() + 30*60000);
    }

    ngOnInit() {
      this.sub = this.route.params.subscribe(params => {
         this.workerId = params['workerId']; 
         this.workerFirstName = params['workerFirstName']; 
         this.workerLastName = params['workerLastName']; 
         this.workerNumber = params['workerNumber'];
         this.shift.subcategory = params['workerSubCategory'] 
         console.log(params);
      });

      // update to 30 mins from now
      this.minShiftStartTime = new Date((new Date()).getTime() + 30*60000);
    }
  
    ngOnDestroy() {
      this.sub.unsubscribe();
    }

  openAdvanceBookingDialog(refWorker={}): void {

    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      panelClass: 'my-panel',
      width: '600px',
    });

    dialogRef.componentInstance.title='SUCCESS';
    dialogRef.componentInstance.subtitle = 'SHIFT ADDED';
    dialogRef.componentInstance.descr = 'You will be notified once the worker accepts your offer.';
    dialogRef.componentInstance.buttonTitle = 'OK';

     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      this.router.navigate(['workers']);
    });
  }

  //Calender Dilog
  openCalenderDialog(): void {
    const dialogRef = this.dialog.open(EmployerDateAdvanceBookingDialogComponent, {
      panelClass: 'my-panel',
      width: '700px',
    });

     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
    });
  }

bookClicked(){

  if ((this.shift.subcategory == undefined) || (this.shift.subcategory.length < 1)) {
    this.openInformDialog("Subcategory not specified","Please select a valid subcategory.");
    return;
  }
  // if ((this.shift.duration == undefined) || (this.shift.duration.length < 1)) {
  //   this.openInformDialog("Duration not specified","Please enter a valid duration.");
  //   return;
  // }
  if ((this.shift.durationHH == undefined) || (this.shift.durationHH.length < 1)) {
    this.openInformDialog("Duration not specified","Please enter a valid duration.");
    return;
  }
  if ((this.shift.durationMM == undefined) || (this.shift.durationMM.length < 1)) {
    this.openInformDialog("Duration not specified","Please enter a valid duration.");
    return;
  }
  if (this.newShiftDateFromPicker == undefined)  {
    this.openInformDialog("Date not specified","Please select a valid date.");
    return;
  }
  // let feeMinMax = parseInt(this.shift.fee);
  // if(feeMinMax < 8 || feeMinMax > 999){
  //   this.openInformDialog("Invalid","Fee should be minimum £8/h to maximum £999/h");
  //   return;
  // }
  if ((this.shift.fee == undefined) || (this.shift.fee.length < 1)) {
    this.openInformDialog("Fee not specified","Please enter a valid fee.");
    return;
  }
  let feeMinMax = parseInt(this.shift.fee);
  if(feeMinMax < 8 || feeMinMax > 999){
    this.openInformDialog("Invalid","Fee should be minimum £8/h to maximum £999/h");
    return;
  }
  if ((this.shift.contactpername == undefined) || (this.shift.contactpername.length < 1)) {
    this.openInformDialog("Contact person not specified","Please enter a valid Contact person.");
    return;
  }
  if (!/^[a-zA-Z ]*$/.test(this.shift.contactpername)) {
    this.openInformDialog("Invalid","Please enter a valid Contact person");
    return;
  }
  if ((this.shift.phoneNumber == undefined) || (this.shift.phoneNumber.length < 1)) {
    this.openInformDialog("Phone number not specified","Please enter a valid Phone number.");
    return;
  }
  if ((this.shift.phoneNumber == undefined) || (this.shift.phoneNumber.length < 10)) {
    this.openInformDialog("Invalid","Phone number should be minimum 10 digits.");
    return;
  }
  if ((this.shift.address == undefined) || (this.shift.address.length < 1)) {
    this.openInformDialog("Address not specified","Please enter a valid address.");
    return;
  }
  if ((this.shift.postcode == undefined) || (this.shift.postcode.length < 1)) {
    this.openInformDialog("Postcode not specified","Please enter a valid Postcode.");
    return;
  }
  if ((this.shift.city == undefined) || (this.shift.city.length < 1)) {
    this.openInformDialog("City not specified","Please enter a valid city.");
    return;
  }
  // if ((this.shift.expNeeded == undefined) || (this.shift.expNeeded.length < 1)) {
  //   this.openInformDialog("Experience not specified","Please enter a valid experience.");
  //   return;
  // }
  // if ((this.shift.description == undefined) || (this.shift.description.length < 1)) {
  //   this.openInformDialog("Description not specified","Please enter a valid description.");
  //   return;
  // }
    
  this.shift.duration = this.shift.durationHH + ':' + this.shift.durationMM;


  // add shift to firestore
  this.shift.shiftNo = Math.round(new Date().getTime() / 1000)+"";
  console.log(this.authService.loggedInProfile.userId);
  this.firestore.collection("shifts").add({
    email: this.authService.loggedInProfile.email,
    approvalStatus:'pending',
    companyId:this.authService.loggedInProfile.companyId,
    companyName:this.authService.companyProfile.name,
    createdBy: this.authService.loggedInProfile.userId,
    owner: this.authService.loggedInProfile.userId,
    category:this.shift.category,
    subcategory:this.shift.subcategory,
    duration:this.shift.duration,
    startDateTime:this.newShiftDateFromPicker,
    fee:this.shift.fee,
    contactpername:this.shift.contactpername,
    phoneNumber:this.shift.phoneNumber,
    address:this.shift.address,
    postcode:this.shift.postcode,
    city:this.shift.city,
    lat:this.shift.lat,
    lng:this.shift.lng,          
    description:this.shift.description,
    expNeeded: this.shift.expNeeded,
    createdAt: firebase.firestore.FieldValue.serverTimestamp(),
    shiftNo: this.shift.shiftNo,
    disputedStatus:'',
    isAdvanceBooking:true,
  })
  .then((aShift) => {
    console.log("Newly created shifts");
    console.log(aShift);

      this.firestore.collection("bookingrequests").add({
        createdBy: this.authService.loggedInProfile.userId,
        shiftId: aShift.id,
        workerId: this.workerId,
        companyId: this.authService.loggedInProfile.userId,
        approvalStatus: 'pending',
        createdAt: firebase.firestore.FieldValue.serverTimestamp(),
        isAdvanceBooking: true,
      })
      .then((aBooking) => {
        console.log("Newly created booking");
        console.log(aBooking);

        this.openAdvanceBookingDialog();


      })
      .catch(err=>console.log(err)) 
    
  })
  .catch(err=>console.log(err)) 

  }

  openLocationLookupDialog(): void {
    const dialogRef = this.dialog.open(LocationLookupDialogComponent, {
      panelClass: 'my-panel',
      width: '750px',
      data: {
        lat:this.shift.lat,
        lng:this.shift.lng
      }
    });

     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      this.dialogResult = result;

      this.shift.address = result.address;
      this.shift.city = result.city;
      this.shift.lat = result.lat;
      this.shift.lng = result.lng;
    });
  }

  openInformDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
      
  }

}
