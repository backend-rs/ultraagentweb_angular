import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-workers-advance-booking-dialog',
  templateUrl: './workers-advance-booking-dialog.component.html',
  styleUrls: ['./workers-advance-booking-dialog.component.css']
})
export class WorkersAdvanceBookingDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<WorkersAdvanceBookingDialogComponent>, @Inject(MAT_DIALOG_DATA) public data:string) { }

  ngOnInit() {
  }

  cancelClick(): void {
    this.dialogRef.close();
  }

}
