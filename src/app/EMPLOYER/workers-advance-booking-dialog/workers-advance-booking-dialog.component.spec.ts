import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkersAdvanceBookingDialogComponent } from './workers-advance-booking-dialog.component';

describe('WorkersAdvanceBookingDialogComponent', () => {
  let component: WorkersAdvanceBookingDialogComponent;
  let fixture: ComponentFixture<WorkersAdvanceBookingDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkersAdvanceBookingDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkersAdvanceBookingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
