import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MybookingDetailsDialogComponent } from '../../dialogs/mybooking-details-dialog/mybooking-details-dialog.component';

import { SuccessOkDialogComponent } 
from '../../dialogs/success-ok-dialog/success-ok-dialog.component';

import { SearchBoxService } from "../../services/searchbox.service";

import { AuthService } from '../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators }from '@angular/forms';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})
export class BookingsComponent {
  
  searchword = '';
  filterDate:Date = undefined;
  
  shift: any;
  shiftslist:any;
  blist = [];

  sub:any;
  passedBookingId:any;
  activeTab = 'MyBooking';

  shiftsSubscription:Subscription;
  bookingsSubscription:Subscription;
  
  private searchBoxSubscription: Subscription;

  constructor(public dialog: MatDialog,
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    private route: ActivatedRoute,
    private searchBoxService: SearchBoxService
    ) {

     

    }

    ngOnInit() {
      // Reset search box in navigation
      this.searchBoxService.clearMessage();

      this.searchBoxSubscription = this.searchBoxService.getMessage().subscribe((msg) => {
        this.searchword = msg.searchword;
      });

      this.sub = this.route.params.subscribe(params => {
        this.passedBookingId = params['bookingId']; 
        console.log(params);
      });

      this.updateShiftsList();
    }

    ngOnDestroy() {

      this.searchBoxSubscription.unsubscribe();
      
      if(this.bookingsSubscription != undefined) {
        this.bookingsSubscription.unsubscribe();
      }
      if(this.shiftsSubscription != undefined) {
        this.shiftsSubscription.unsubscribe();
      }

      this.sub.unsubscribe();
    }

    updateBookings(){
      const aCollection = this.firestore.collection('bookings',ref => 
        ref.where('companyId','==',this.authService.loggedInProfile.companyId)
      );
      // .snapshotChanges() returns a DocumentChangeAction[], which contains
      // a lot of information about "what happened" with each change. If you want to
      // get the data and the id use the map operator.
      aCollection.snapshotChanges().pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        }))
      ).subscribe(someList => {
        if(someList != undefined){
          this.blist = someList;
        }
        
        for(var j=0; j<this.blist.length; j++){
          for(var i=0; i<this.shiftslist.length; i++){
            if(this.blist[j].shiftId == this.shiftslist[i].id){
              this.blist[j].shift = this.shiftslist[i];
            }
          }
        }
        console.log('booking fetched');
        console.log(this.blist);

        if ((this.passedBookingId != undefined) && (this.passedBookingId.length > 0)) {
          var found = false;
          for (var i=0; i< this.blist.length; i++) {
            if (this.blist[i].id == this.passedBookingId) {
              const aBooking = this.blist[i];
              found = true;
              if ((aBooking.status != 'cancelled') && 
                  ((aBooking.workingStatus == undefined) || (aBooking.workingStatus != 'completed'))){
                this.activeTab = 'MyBooking';
                this.openMybookingDetailsDialog(aBooking);
              } else if (aBooking.workingStatus != 'cancelled') {  
                this.activeTab = 'CancelBooking';
                this.openCancelledBookingsDetailsDialog(aBooking);
              } 
              break;
            }
          }

          if (found == false) {
            this.openDialog('Oops!','Booking you are looking for no longer available here.\n\nPlease check the Timesheet page.');
          }
        }
  
      }
      );
    }
  
     updateShiftsList(){
      var aCollection = this.firestore.collection('shifts',ref => 
        ref.where('companyId','==',this.authService.loggedInProfile.companyId)
      );
      // .snapshotChanges() returns a DocumentChangeAction[], which contains
      // a lot of information about "what happened" with each change. If you want to
      // get the data and the id use the map operator.
      this.shiftsSubscription = aCollection.snapshotChanges().pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        }))
      ).subscribe(shiftslist => {
          this.shiftslist = shiftslist;
          console.log(this.shiftslist);

          this.updateBookings();
      });
       
     }
  

  // MyBooking Details

  openMybookingDetailsDialog(booking): void {
    const dialogRef = this.dialog.open(MybookingDetailsDialogComponent, {
      panelClass: 'my-panel',
      width: '800px',
    });

    dialogRef.componentInstance.booking = booking;
    dialogRef.componentInstance.hideCancelledReason = true;
     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
    });
  }

  // CancelledBooking Details
  
  openCancelledBookingsDetailsDialog(booking): void {
    const dialogRef = this.dialog.open(MybookingDetailsDialogComponent, {
      panelClass: 'my-panel',
      width: '800px',
    });

    dialogRef.componentInstance.booking = booking;
    dialogRef.componentInstance.hideCancelButton = true;
     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
    });
  }

  openDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
    
  }

}
