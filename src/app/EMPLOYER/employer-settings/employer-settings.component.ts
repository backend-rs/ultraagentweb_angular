import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';

import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { HttpClient, HttpParams } from '@angular/common/http';

import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { ApproveRejectDialogComponent } from '../../dialogs/approve-reject-dialog/approve-reject-dialog.component';

import * as firebase from 'firebase/app';

@Component({
  selector: 'app-employer-settings',
  templateUrl: './employer-settings.component.html',
  styleUrls: ['./employer-settings.component.css']
})
export class EmployerSettingsComponent implements OnInit {

  passwordChangeIsProcessing = false;
  newUserIsProcessing = false;

  checked = true;
  oldPassword = '';
  newPassword = '';
  confirmNewPassword = '';
  newUser= {
    firstName: "",
    lastName: "",
    email: "",
    phoneNumber: "",
    jobTitle: "",
    password: "",
    permsShifts:true,
    permsBookings: false,
    permsTimesheets: false,
    permsUsers: false,
  };

  sub: any;

  companyusers: any[];
  //companyadmins: any[];
  
  private usersCollection: AngularFirestoreCollection;
  //private adminsCollection: AngularFirestoreCollection;

  private usersSubscription:Subscription;
  //private adminSubscription:Subscription;

  constructor(public dialog: MatDialog,
    private authService: AuthService,
    private router: Router,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    private httpClient:HttpClient,
    private route: ActivatedRoute) { }

  ngOnInit() {

    console.log(this.authService.companyProfile);
    // Query for Company Users
    this.usersCollection = this.firestore.collection('profiles', ref => 
      ref.where('profileType','==','companyuser')
        .where('companyId','==',this.authService.companyProfile.id)
    );
    this.usersSubscription = this.usersCollection
    .snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(profileslist => {
        this.companyusers = profileslist;
        console.log(this.companyusers);
    });

    this.sub = this.route.params.subscribe(params => {
      console.log(params);
      if(params['show'] != undefined){
        if(params['show'] == 'users'){
          console.log('setting active tab');
          this.activeTab = 'users';
        }
      }
   });

  }

  ngOnDestroy() {
    this.usersSubscription.unsubscribe();
    //this.adminSubscription.unsubscribe();
  }

  activeTab = 'basic';

  basic(activeTab){
    this.activeTab = activeTab;
    console.log('onBasicTabSelected click');
  }

  users(activeTab){
    this.activeTab = activeTab;
    console.log('onUsersTabSelected click');
  }

  addusers(activeTab){
    this.activeTab = activeTab;
    console.log('onAddUsersTabSelected click');
  }

  onNewUserSaveClicked() {
    console.log("onNewUserSaveClicked called");
    console.log(this.newUser);

    if ((this.newUser.email == undefined) || (this.newUser.email.length < 1)) {
      this.openDialog("Email not specified","Please enter a valid email.");
      return;
    }
    if ((this.newUser.firstName == undefined) || (this.newUser.firstName.length < 1)) {
      this.openDialog("First name not specified","Please enter a valid first name.");
      return;
    }
    if (!/^[a-zA-Z ]*$/.test(this.newUser.firstName)) {
      this.openDialog("Invalid","Please enter a valid first name.");
      return;
    }
    if ((this.newUser.lastName == undefined) || (this.newUser.lastName.length < 1)) {
      this.openDialog("Last name not specified","Please enter a valid last name.");
      return;
    }
    if (!/^[a-zA-Z ]*$/.test(this.newUser.lastName)) {
      this.openDialog("Invalid","Please enter a valid last name.");
      return;
    }
    if ((this.newUser.phoneNumber == undefined) || (this.newUser.phoneNumber.length < 10)) {
      this.openDialog("Phone number not specified","Please enter a valid phone number.");
      return;
    }
    if ((this.newUser.password == undefined) || (this.newUser.password.length < 1)) {
      this.openDialog("Password not specified","Please enter a valid password.");
      return;
    }
    if ((!/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/.test(this.newUser.password))) {
      this.openDialog("Invalid","Password should be Minimum 8 characters, at least one uppercase letter, one lowercase letter, one digit and special character");
      return;
    }
    if ((this.newUser.jobTitle == undefined) || (this.newUser.jobTitle.length < 1)) {
      this.openDialog("Job title not specified","Please enter a valid job title.");
      return;
    }

    if ((this.newUser.permsShifts == false) && (this.newUser.permsBookings == false)
    && (this.newUser.permsTimesheets == false) && (this.newUser.permsUsers == false)){
      this.openDialog("Rights not checked","Please check Rights.");
      return;
    }

    if (this.newUserIsProcessing == true) { return; }
    this.newUserIsProcessing = true;

    this.httpClient.post("https://x784f653.ultraagent.co.uk/createuser",
    {
        "email": this.newUser.email.trim().toLowerCase(),
        "phoneNumber": this.newUser.phoneNumber,
        "displayName": this.newUser.firstName+' '+this.newUser.lastName,
        "password": this.newUser.password,
        "senderName": this.authService.loggedInProfile.firstName+' '+this.authService.loggedInProfile.lastName,
        "loginUrl":"https://employer.ultraagent.co.uk",
        "jobTitle":this.newUser.jobTitle,
    })
    .subscribe(
        data => {
            console.log("POST Request is successful ");
            console.log(data);
            if ((data == undefined) || (data["uid"] == undefined) || (data["uid"].length < 1)) {
              // show if any error message
              this.openDialog("Error", data["code"]+'\n'+data["message"]);
            } else {
              // user created. now create profile in firestore.

              this.firestore.collection("profiles").doc(data["uid"]).set({
                companyId:this.authService.companyProfile.id,
                email: this.newUser.email,
                firstName:this.newUser.firstName,
                lastName: this.newUser.lastName,
                profileType:'companyuser',
                approvalStatus:'approved',//Since, company admin creating, hence mark approved
                userId: data["uid"],
                phoneNumber: this.newUser.phoneNumber,
                city: this.authService.companyProfile.city,
                address: this.authService.companyProfile.address,
                jobTitle: this.newUser.jobTitle,
                permsShifts: this.newUser.permsShifts,
                permsBookings: this.newUser.permsBookings,
                permsTimesheets: this.newUser.permsTimesheets,
                permsUsers: this.newUser.permsUsers,
                sendNotifications: true,
                createdAt: firebase.firestore.FieldValue.serverTimestamp()
              }, {merge: true})
              .then(_ => {
                this.openDialog("SUCCESS","User added successfully.");
                this.newUserIsProcessing = false;

                this.newUser.email = '';
                this.newUser.firstName = '';
                this.newUser.lastName = '';
                this.newUser.jobTitle = '';
                this.newUser.phoneNumber = '';
                this.newUser.password = '';
                this.newUser.permsShifts = false;
                this.newUser.permsBookings = false;
                this.newUser.permsTimesheets = false;
                this.newUser.permsUsers = false;

                var usersTab = document.getElementById('users-tab'); 
                usersTab.click();
              })
              .catch((err) => {
                console.log(err);
                this.openDialog("Error",err.message);
                this.newUserIsProcessing = false;
              }); 
            
            }
        },
        error => {
            console.log("Error", error);
            this.openDialog("Error", error.message);
            this.newUserIsProcessing = false;
        }
    );   
    
  }

  clickOnChangePassword() {
    if ((this.oldPassword == undefined) || (this.oldPassword.length < 1)) {
      this.openDialog("Old password not specified","Please enter a valid old password.");
      return;
    }
    if ((this.newPassword == undefined) || (this.confirmNewPassword == undefined) ||
    (!/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/.test(this.newPassword)) || 
    (!/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/.test(this.confirmNewPassword))) {
      this.openDialog("Password not valid","Password and Confirm password should be Minimum 8 characters, at least one uppercase letter, one lowercase letter, one digit and one special character.");
      return;
    }

   
    // if ((this.newPassword == undefined) || (this.confirmNewPassword == undefined) || 
    //   (this.newPassword.length < 6) || (this.confirmNewPassword.length < 6)) 
    // {
    //   this.openDialog("Password not valid","Password should be minimum 6 chars long.");
    //   return;
    // }

    if (this.newPassword != this.confirmNewPassword) {
      this.openDialog("Confirm Password not valid","Password and Confirm password do not match.");
      return;
    }

    if (this.passwordChangeIsProcessing == true) { return; }
    this.passwordChangeIsProcessing = true;

    var cred = firebase.auth.EmailAuthProvider.credential(this.authService.loggedInProfile.email, this.oldPassword);
    this.afAuth.auth.currentUser.reauthenticateWithCredential(cred)
    .then(() => {
      this.afAuth.auth.currentUser.updatePassword(this.newPassword)
      .then(() => {
        this.openDialog("SUCCESS","Password changed successfully.");
        this.passwordChangeIsProcessing = false;
        this.oldPassword = '';
        this.newPassword = '';
        this.confirmNewPassword = '';
      })
      .catch((err) => {
        console.log(err);
        this.openDialog("Error", err.message);
        this.passwordChangeIsProcessing = false;
      });
    })
    .catch((err) => {
      console.log(err);
      this.openDialog("Error", "Old Password: "+err.message);
      this.passwordChangeIsProcessing = false;
    });


    // this.httpClient.post("https://x784f653.ultraagent.co.uk/57few674",
    // {
    //     "uid": this.authService.loggedInProfile.id,
    //     "password": this.newPassword
    // })
    // .subscribe(
    //     data => {
    //         console.log("POST Request is successful ");
    //         console.log(data);
    //         if ((data == undefined) || (data["uid"] == undefined) || (data["uid"].length < 1)) {
    //           // show if any error message
    //           this.openDialog("Error", data["code"]+'\n'+data["message"]);
    //         } else {
    //           // user created. now create profile in firestore.
    //           this.openDialog("SUCCESS","Password changed successfully.");
    //         }
    //     },
    //     error => {
    //         console.log("Error", error);
    //         this.openDialog("Error", error.message);
    //     }
    // ); 
  }

  clickOnRemoveUser(aUser) {
    
    const dialogRef = this.dialog.open(ApproveRejectDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = 'Are you sure?';
    dialogRef.componentInstance.descr = 
    "Do you really want to remove "+aUser.firstName+" "+aUser.lastName+" from the company?";

    dialogRef.componentInstance.approveTitle = 'Yes, Remove';
    dialogRef.componentInstance.rejectTitle = 'No, Cancel';

    dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      if (result == dialogRef.componentInstance.approveTitle) {

        this.httpClient.post("https://x784f653.ultraagent.co.uk/deleteuser",
        {
            "uid": aUser.id,
            "email": aUser.email,
            "displayName": aUser.firstName+' '+aUser.lastName
        })
        .subscribe(
            data => {
                console.log("POST Request is successful ");
                console.log(data);
                if ((data == undefined) || (data["message"] != undefined)) {
                  // show if any error message
                  this.openDialog("Error", data["code"]+'\n'+data["message"]);
                  // Now delete profile
                  this.firestore.collection('profiles').doc(aUser.id).delete()
                  .then(() => {
                    // user created. now create profile in firestore.
                   this.openDialog("SUCCESS","User removed successfully.");
                  })
                  .catch((error) => {
                   console.log("Error", error);
                   this.openDialog("Error", error.message);
                  });
                } else {
                   // Now delete profile
                   this.firestore.collection('profiles').doc(aUser.id).delete()
                   .then(() => {
                     // user created. now create profile in firestore.
                    this.openDialog("SUCCESS","User removed successfully.");
                   })
                   .catch((error) => {
                    console.log("Error", error);
                    this.openDialog("Error", error.message);
                   });
                  
                }
            },
            error => {
                console.log("Error", error);
                this.openDialog("Error", error.message);
            }
        ); 
      }
    });
  }

  clickOnEditUser(aUser) {
    this.router.navigate(['users_setting_edit', {profile:JSON.stringify(aUser), skipLocationChange: true}]);
  }


  openDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
    
  }
  
}
