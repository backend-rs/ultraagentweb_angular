import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessShiftDeletedDialogComponent } from './success-shift-deleted-dialog.component';

describe('SuccessShiftDeletedDialogComponent', () => {
  let component: SuccessShiftDeletedDialogComponent;
  let fixture: ComponentFixture<SuccessShiftDeletedDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccessShiftDeletedDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessShiftDeletedDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
