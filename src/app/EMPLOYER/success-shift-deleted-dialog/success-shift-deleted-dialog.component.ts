import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-success-shift-deleted-dialog',
  templateUrl: './success-shift-deleted-dialog.component.html',
  styleUrls: ['./success-shift-deleted-dialog.component.css']
})
export class SuccessShiftDeletedDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<SuccessShiftDeletedDialogComponent>, @Inject(MAT_DIALOG_DATA) public data:string) { }

  ngOnInit() {
  }

  cancelClick(): void {
    this.dialogRef.close();
  }

}
