import { Component, OnInit, Inject, ViewChild, NgZone } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { DeleteShiftDialogComponent } from '../delete-shift-dialog/delete-shift-dialog.component';

import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators }from '@angular/forms';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';
import { CdkTreeModule } from '@angular/cdk/tree';
import { MapsAPILoader, AgmMap } from '@agm/core';
import { GoogleMapsAPIWrapper } from '@agm/core/services';

declare var google: any;


 
interface Marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
 
interface Location {
  lat: number;
  lng: number;
  viewport?: Object;
  zoom: number;
  address_level_1?:string;
  address_level_2?: string;
  address_city?: string;
  address_country?: string;
  address_zip?: string;
  address_state?: string;
  marker?: Marker;
}

@Component({
  selector: 'app-myshift-edit-dialog',
  templateUrl: './myshift-edit-dialog.component.html',
  styleUrls: ['./myshift-edit-dialog.component.css'],
  providers: [GoogleMapsAPIWrapper]
})

export class MyshiftEditDialogComponent implements OnInit {

  title = '';
  shift: any;

  newShiftDateFromPicker:Date = undefined;
  hideMap:boolean = false;
  hideSaveButton:boolean = false;
  hidePostDeleteShiftButton:boolean = false;
  hideExperience:boolean = false;
  geocoder:any;
  public location:Location;

  searchInput = "";

  minShiftStartTime = new Date((new Date()).getTime() + 30*60000);

  @ViewChild(AgmMap) map: AgmMap;

  constructor(
    public dialog: MatDialog,
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    public dialogRef: MatDialogRef<MyshiftEditDialogComponent>, 
    public mapsApiLoader: MapsAPILoader,
    public zone: NgZone,
    private wrapper: GoogleMapsAPIWrapper,
    @Inject(MAT_DIALOG_DATA) public data:any) {
      
      console.log(data);
      this.shift = data.shift;

      this.newShiftDateFromPicker= this.shift.startDateTime.toDate();

      const latV = this.shift.lat;
      const lngV = this.shift.lng;
      
      this.location = {
        lat: latV,
        lng: lngV,
        marker: {
          lat: latV,
          lng: lngV,
          draggable: true
        },
        zoom: 12
      };
      this.mapsApiLoader = mapsApiLoader;
      this.zone = zone;
      this.wrapper = wrapper;

      this.mapsApiLoader.load().then(() => {
        this.geocoder = new google.maps.Geocoder();
        this.findAddressByCoordinates();
      });

      // update to 30 mins from now
      this.minShiftStartTime = new Date((new Date()).getTime() + 30*60000);
     }

     ngOnInit() {
      this.location.marker.draggable = true;

      // update to 30 mins from now
      this.minShiftStartTime = new Date((new Date()).getTime() + 30*60000);
    }
  
    ngOnDestroy() {
    }

    updateOnMap() {
      let full_address:string = this.location.address_level_1 || ""
      if (this.location.address_level_2) full_address = full_address + " " + this.location.address_level_2
      if (this.location.address_state) full_address = full_address + " " + this.location.address_state
      if (this.location.address_country) full_address = full_address + " " + this.location.address_country
   
      this.findLocation(full_address);
    }
  
    findLocation(address) {
      if (!this.geocoder) this.geocoder = new google.maps.Geocoder()
      this.geocoder.geocode({
        'address': address
      }, (results, status) => {
        //console.log(results);
        if (status == google.maps.GeocoderStatus.OK) {
          for (var i = 0; i < results[0].address_components.length; i++) {
            let types = results[0].address_components[i].types
   
            if (types.indexOf('locality') != -1) {
              this.location.address_level_2 = results[0].address_components[i].long_name
            }
            if (types.indexOf('country') != -1) {
              this.location.address_country = results[0].address_components[i].long_name
            }
            if (types.indexOf('postal_code') != -1) {
              this.location.address_zip = results[0].address_components[i].long_name
            }
            if (types.indexOf('administrative_area_level_1') != -1) {
              this.location.address_state = results[0].address_components[i].long_name
            }
          }
   
          if (results[0].geometry.location) {
            this.location.lat = results[0].geometry.location.lat();
            this.location.lng = results[0].geometry.location.lng();
            this.location.marker.lat = results[0].geometry.location.lat();
            this.location.marker.lng = results[0].geometry.location.lng();
            this.location.marker.draggable = true;
            this.location.viewport = results[0].geometry.viewport;
          }
          
          this.map.triggerResize()
        } else {
          alert("Sorry, this search produced no results.");
        }
      })
    }
  
    markerDragEnd(m: any) {
      this.location.marker.lat = m.coords.lat;
      this.location.marker.lng = m.coords.lng;
      this.findAddressByCoordinates();
    }
  
    findAddressByCoordinates() {
      //console.log('findAddressByCoordinates: '+this.location.marker.lat+', '+this.location.marker.lng);
      this.geocoder.geocode({
        'location': {
          lat: this.location.marker.lat,
          lng: this.location.marker.lng
        }
      }, (results, status) => {
        this.decomposeAddressComponents(results);
      })
    }
  
    decomposeAddressComponents(addressArray) {
      if (addressArray.length == 0) return false;
  
      this.location.lat = this.location.marker.lat;
      this.location.lng = this.location.marker.lng;
      this.location.address_level_1 = "";
      this.location.address_level_2 = "";
      this.location.address_city = "";
      this.location.address_state = "";
      this.location.address_country = "";
      this.location.address_zip = "";
  
      let address = addressArray[0].address_components;
      
      //console.log(address);
  
      for(let element of address) {
        if (element.length == 0 && !element['types']) continue
   
        if (element['types'].indexOf('street_number') > -1) {
          this.location.address_level_1 = element['long_name'];
          continue;
        }
        if (element['types'].indexOf('route') > -1) {
          this.location.address_level_1 += ', ' + element['long_name'];
          continue;
        }
        if (element['types'].indexOf('locality') > -1) {
          this.location.address_level_2 = element['long_name'];
          this.location.address_city = element['long_name'];
          continue;
        }
        if (element['types'].indexOf('postal_town') > -1) {
          this.location.address_city = element['long_name'];
          continue;
        }
        if (element['types'].indexOf('administrative_area_level_1') > -1) {
          this.location.address_state = element['long_name'];
          continue;
        }
        if (element['types'].indexOf('country') > -1) {
          this.location.address_country = element['long_name'];
          continue;
        }
        if (element['types'].indexOf('postal_code') > -1) {
          this.location.address_zip = element['long_name'];
          continue;
        }
      }
  
      this.showAddressOnInputField();
    }
  
    showAddressOnInputField() {
      let full_address:string = ""
      if (this.location.address_level_1) full_address = full_address + " " + this.location.address_level_1
      if (this.location.address_level_2) full_address = full_address + " " + this.location.address_level_2
      if (this.location.address_state) full_address = full_address + " " + this.location.address_state
      if (this.location.address_country) full_address = full_address + " " + this.location.address_country
      
      console.log('Found address: '+full_address);
      this.searchInput = full_address;
  
      // this will force update searchInput on view
      this.zone.run(() => { // <== added
        this.searchInput = full_address;
      });
    }
  
    searchButtonClicked() {
      //console.log("searchButtonClicked: "+this.searchInput);
      this.findLocation(this.searchInput);
    }
  
  cancelClick(): void {
    this.dialogRef.close();
  }

  saveShift(): void {
    //console.log(this.shift);
    this.shift.lat = this.location.lat;
    this.shift.lng = this.location.lng;
    this.shift.city = this.location.address_city;
    this.shift.address = this.searchInput;


    if (this.shift.subcategory == undefined) {
      this.openInformDialog("Subcategory not specified","Please select a valid subcategory.");
      return;
    }
    // if ((this.shift.duration == undefined) || (this.shift.duration.length < 1)) {
    //   this.openInformDialog("Duration not specified","Please enter a valid duration.");
    //   return;
    // }
    if ((this.shift.durationHH == undefined) || (this.shift.durationHH.length < 1)) {
      this.openInformDialog("Duration not specified","Please enter a valid duration.");
      return;
    }
    if ((this.shift.durationMM == undefined) || (this.shift.durationMM.length < 1)) {
      this.openInformDialog("Duration not specified","Please enter a valid duration.");
      return;
    }
    if ((this.shift.fee == undefined) || (this.shift.fee.length < 1)) {
      this.openInformDialog("Fee not specified","Please enter a valid fee.");
      return;
    }
    let feeMinMax = parseInt(this.shift.fee);
    if(feeMinMax < 8 || feeMinMax > 999){
      this.openInformDialog("Invalid","Fee should be minimum £8/h to maximum £999/h");
      return;
    }
    if ((this.shift.contactpername == undefined) || (this.shift.contactpername.length < 1)) {
      this.openInformDialog("Contact person not specified","Please enter a valid Contact person.");
      return;
    }
    if (!/^[a-zA-Z ]*$/.test(this.shift.contactpername)) {
      this.openInformDialog("Invalid","Please enter a valid Contact person");
      return;
    }
    if ((this.shift.phoneNumber == undefined) || (this.shift.phoneNumber.length < 10)) {
      this.openInformDialog("Phone number not specified","Please enter a valid Phone number.");
      return;
    }
    if ((this.shift.address == undefined) || (this.shift.address.length < 1)) {
      this.openInformDialog("Address not specified","Please enter a valid address.");
      return;
    }
    if ((this.shift.city == undefined) || (this.shift.city.length < 1)) {
      this.openInformDialog("City not specified","Please enter a valid city.");
      return;
    }
    if ((this.shift.postcode == undefined) || (this.shift.postcode.length < 1)) {
      this.openInformDialog("Postcode not specified","Please enter a valid Postcode.");
      return;
    }
    // if ((this.shift.expNeeded == undefined) || (this.shift.expNeeded.length < 1)) {
    //   this.openInformDialog("Experience not specified","Please enter a valid experience.");
    //   return;
    // }
    // if ((this.shift.description == undefined) || (this.shift.description.length < 1)) {
    //   this.openInformDialog("Description not specified","Please enter a valid description.");
    //   return;
    // }

    if (this.newShiftDateFromPicker != undefined) {
      this.shift.startDateTime = this.newShiftDateFromPicker;
    }

    this.shift.duration = this.shift.durationHH + ':' + this.shift.durationMM;


    this.firestore.collection("shifts").doc(this.shift.id).set({
      duration:this.shift.duration,
      startDateTime:this.shift.startDateTime,
      fee:this.shift.fee,
      contactpername:this.shift.contactpername,
      postcode:this.shift.postcode,
      phoneNumber:this.shift.phoneNumber == undefined? '': this.shift.phoneNumber,
      address:this.shift.address,
      city:this.shift.city,
      lat:this.shift.lat,
      lng: this.shift.lng,
      description:this.shift.description,
      expNeeded: this.shift.expNeeded == undefined? '': this.shift.expNeeded,
    }, {merge: true})
    .then((aObj) => {
      //this.router.navigate(['login'])
      this.dialogRef.close();
      this.openInformDialog("SUCCESS", "Shift updated successfully.");
    })
    .catch(err=> {
      console.log(err);
      this.dialogRef.close();
      this.openInformDialog("Error", err.message);
      
    }) 


  }


  openInformDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
      
  }

  openDeleteShiftDialog(): void {
   
    const aShift = this.shift;

    const dialogRef = this.dialog.open(DeleteShiftDialogComponent, {
      panelClass: 'my-panel',
      width: '750px',
    });

    dialogRef.componentInstance.title = "DELETE "+aShift.subcategory.toUpperCase()+" #"+aShift.shiftNo;
     dialogRef.afterClosed().subscribe(result => {
      if (result == true) {

        this.dialogRef.close();

        // Delete associated bookingrequests
        this.firestore.collection('bookingrequests',ref=>
          ref.where('companyId','==',this.authService.loggedInProfile.companyId)
        ).ref
        .get()
        .then((querySnapshot) => {
          
          let bookingIds = [] ;
          querySnapshot.forEach((doc) => {
              // doc.data() is never undefined for query doc snapshots
              if (doc.exists) {
                console.log("Deleting booking request "+doc.id);
                bookingIds.push(doc.id);
              }
          });
          for(let idx = 0; idx < bookingIds.length; idx++){
            const aId = bookingIds[idx];
              this.firestore.collection('bookingrequests').doc(aId).delete();

          }
        })
        .catch((err) => {
          console.log(err);
          this.openInformDialog("Error",err.message);
        });

        this.firestore.collection('shifts').doc(aShift.id).delete()
        .then(() => {
          this.openInformDialog("SUCCESS", "Shift deleted successfully.");
        })
        .catch((err) => {
          console.log(err);
          this.openInformDialog("Error", err.message);
        });

      }
    });
  }

}
