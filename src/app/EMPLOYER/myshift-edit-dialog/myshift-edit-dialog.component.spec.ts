import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyshiftEditDialogComponent } from './myshift-edit-dialog.component';

describe('MyshiftEditDialogComponent', () => {
  let component: MyshiftEditDialogComponent;
  let fixture: ComponentFixture<MyshiftEditDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyshiftEditDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyshiftEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
