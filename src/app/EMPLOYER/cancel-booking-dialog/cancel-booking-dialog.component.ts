import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { SuccessBookingCancelDialogComponent } from '../success-booking-cancel-dialog/success-booking-cancel-dialog.component';
import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';

import { AuthService } from '../../services/auth.service';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

import * as firebase from 'firebase/app';

@Component({
  selector: 'app-cancel-booking-dialog',
  templateUrl: './cancel-booking-dialog.component.html',
  styleUrls: ['./cancel-booking-dialog.component.css']
})
export class CancelBookingDialogComponent implements OnInit {
  
  // checked = true;

  materialsNotReady=false;
  personalTechnicalReason=false;
  shiftWillBeRescheduledToLaterDate=false;
  shiftHandledByEmployee = false;
  brief="";
  dialogResult="";
  booking:any;

  constructor(
    public dialog: MatDialog, 
    private authService: AuthService,
    private firestore: AngularFirestore,
    public dialogRef: MatDialogRef<SuccessBookingCancelDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data:string) { }

  ngOnInit() {
  }

  openSuccessBookingCancelledDialog(): void {
    const dialogRef = this.dialog.open(SuccessBookingCancelDialogComponent, {
      panelClass: 'my-panel',
      width: '700px',
    });

    const startInMillis = this.booking.shiftStartDateTime.toDate().getTime();
    const nowInMillis = (new Date()).getTime();
    const diff = Math.abs(startInMillis - nowInMillis);
    if((startInMillis > nowInMillis) && (diff < 24*60*60*1000)) {
      dialogRef.componentInstance.show24HrNotice = true;
    }
     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      this.dialogResult = result;
    });
  }
  
  materialsNotReady_Clicked() {
    console.log('materialsNotReady_Clicked');
    this.materialsNotReady=true;
    this.personalTechnicalReason=false;
    this.shiftWillBeRescheduledToLaterDate=false;
    this.shiftHandledByEmployee = false;
  }
  personalTechnicalReason_Clicked() {
    console.log('personalTechnicalReason_Clicked');
    this.materialsNotReady=false;
    this.personalTechnicalReason=true;
    this.shiftWillBeRescheduledToLaterDate=false;
    this.shiftHandledByEmployee = false;
  }
  shiftWillBeRescheduledToLaterDate_Clicked() {
    console.log('shiftWillBeRescheduledToLaterDate_Clicked');
    this.materialsNotReady=false;
    this.personalTechnicalReason=false;
    this.shiftWillBeRescheduledToLaterDate=true;
    this.shiftHandledByEmployee = false;
  }
  shiftHandledByEmployee_Clicked() {
    console.log('shiftHandledByEmployee_Clicked');
    this.materialsNotReady=false;
    this.personalTechnicalReason=false;
    this.shiftWillBeRescheduledToLaterDate=false;
    this.shiftHandledByEmployee = true;
  }

  cancelBookingClicked() {

    if ((this.brief == undefined) || (this.brief.length < 1)){
      this.openInformDialog("Brief not specified","Please enter a valid brief.");
      return;
    }

    if ((this.materialsNotReady == false) && 
        (this.personalTechnicalReason == false) && 
        (this.shiftWillBeRescheduledToLaterDate == false) &&
        (this.shiftHandledByEmployee == false)){
      this.openInformDialog("Reason not specified","Please select one of the reasons above.");
      return;
    }

    let cancelReasone = 'Materials not ready';
    if (this.personalTechnicalReason == true) {
      cancelReasone = 'Personal/Technical Reason';
    } else if (this.shiftWillBeRescheduledToLaterDate == true) {
      cancelReasone = 'Shift will be re-scheduled to a later date';
    } else if (this.shiftHandledByEmployee == true) {
      cancelReasone = 'Shift will be handled by our employee';
    }
    const reqId = this.booking.shiftId+'-'+this.booking.workerId;

    // Create new booking object
    // this.firestore.collection('bookings').doc(reqId).set(
    //   {
    //     status:'cancelled',
    //     cancelledBy:this.authService.loggedInProfile.userId,
    //     cancelReason:cancelReasone,
    //     cancelBrief:this.brief,
    //     cancelledAt:firebase.firestore.FieldValue.serverTimestamp()
    //   },
    //   {merge:true}
    // )
    this.authService.doCancelBookedWorker(reqId, cancelReasone, this.brief)
    .then(() => {
      //this.openInformDialog('SUCCESS', 'Worker booking cancelled successfully');
      this.openSuccessBookingCancelledDialog();
      this.dialogRef.close();

    })
    .catch((err) => {
      console.log(err.message);
      this.openInformDialog('Error', err.message);
    });
      
  }

  cancelClick(): void {
    this.dialogRef.close();
  }

  openInformDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
      
  }

}
