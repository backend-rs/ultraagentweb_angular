import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { SuccessShiftDeletedDialogComponent } from '../success-shift-deleted-dialog/success-shift-deleted-dialog.component'


@Component({
  selector: 'app-delete-shift-dialog',
  templateUrl: './delete-shift-dialog.component.html',
  styleUrls: ['./delete-shift-dialog.component.css']
})
export class DeleteShiftDialogComponent implements OnInit {
  dialogResult="";

  title="";
  constructor(public dialog: MatDialog, public dialogRef: MatDialogRef<DeleteShiftDialogComponent>, @Inject(MAT_DIALOG_DATA) public data:string) { }

  openYesDialog(): void {
    this.dialogRef.close(true);
  }
  ngOnInit() {
  }

  cancelClick(): void {
    this.dialogRef.close(false);
  }

}
