import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveShiftDialogComponent } from './approve-shift-dialog.component';

describe('ApproveShiftDialogComponent', () => {
  let component: ApproveShiftDialogComponent;
  let fixture: ComponentFixture<ApproveShiftDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveShiftDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveShiftDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
