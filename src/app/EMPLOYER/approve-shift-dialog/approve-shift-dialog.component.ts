import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { AmendDialogComponent } from '../../dialogs/amend-dialog/amend-dialog.component';
import { ApproveDialogComponent } from '../../dialogs/approve-dialog/approve-dialog.component';



@Component({
  selector: 'app-approve-shift-dialog',
  templateUrl: './approve-shift-dialog.component.html',
  styleUrls: ['./approve-shift-dialog.component.css']
})
export class ApproveShiftDialogComponent implements OnInit {
  
  dialogResult="";

  constructor(public dialog: MatDialog, public dialogRef: MatDialogRef<ApproveDialogComponent, AmendDialogComponent>, @Inject(MAT_DIALOG_DATA) public data:string) { }

  ngOnInit() {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ApproveDialogComponent, {
      panelClass: 'my-panel',
      width: '600px',
    });

     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      this.dialogResult = result;
    });
  }
  
  openAmendDialog(): void {
    const dialogRef = this.dialog.open(AmendDialogComponent, {
      panelClass: 'my-panel',
      width: '700px',
    });
     
    dialogRef.componentInstance.title = 'AMEND SHIFT';
     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      this.dialogResult = result;
    });
  }
  cancelClick(): void {
    this.dialogRef.close();
  }

}
