import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerNavigationComponent } from './employer-navigation.component';

describe('EmployerNavigationComponent', () => {
  let component: EmployerNavigationComponent;
  let fixture: ComponentFixture<EmployerNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
