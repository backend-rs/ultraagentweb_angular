import { Component, ViewChild, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { MessagingService } from "../../services/messaging.service";
import { SearchBoxService } from "../../services/searchbox.service";
import { AlertService } from "../../services/alert.service";

import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-employer-navigation',
  templateUrl: './employer-navigation.component.html',
  styleUrls: ['./employer-navigation.component.css']
})
export class EmployerNavigationComponent {

  message=null;
  public searchword='';

  @ViewChild('sidenav') sidenav: any;
  navMode = 'side';

  profile:any;
  company:any;

  unreadNoticesCount = 0;

  private noticeCollectionSub: Subscription;
  private searchBoxSubscription: Subscription;

  constructor(private router: Router, 
    public authService: AuthService,  
    private messagingService: MessagingService,
    private alertService: AlertService,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth, 
    private searchBoxService: SearchBoxService) {

    this.profile = this.authService.loggedInProfile;
    this.company = this.authService.companyProfile;

    console.log('EmployerNavigationComponent constructor');
    console.log(this.profile);
    console.log(this.company);
   
  }

  ngOnInit() {

    this.searchBoxSubscription = this.searchBoxService.getMessage().subscribe((msg) => {
      // Bharat: updating in a timeout function to avoid following error:
      // ExpressionChangedAfterItHasBeenCheckedError: Expression has changed after it was checked. Previous value: 'model: ggkbgi'. Current value: 'model: '.
      setTimeout(() => {
        this.searchword = msg.searchword;
      }, 200);
    });

    const userId = this.profile.userId;
    this.messagingService.requestPermission(userId)
    this.messagingService.receiveMessage()
    this.message = this.messagingService.currentMessage
    console.log(this.message);
    console.log(this.message.getValue());

    this.updateNoticesList();
  }

  ngOnDestroy() {
    this.noticeCollectionSub.unsubscribe();
    this.searchBoxSubscription.unsubscribe();
  }

  searchWordUpdated() {
    //console.log(this.searchword);
    this.searchBoxService.sendMessage(this.searchword);
  }

  clickOnLogOut(){
    // reset fcm token for webapp
    const userId = this.profile.userId;
    this.messagingService.unlinkTokenFromUserId(userId);
    
    this.authService.doLogout().then(() => {
      this.router.navigate(['employer_login_registration']);
    }).catch();

  }
  
  @HostListener('window:resize', ['$event'])
  onResize(event) {
      if (event.target.innerWidth < 768) {
          this.navMode = 'over';
          this.sidenav.close();
      }
      if (event.target.innerWidth > 768) {
         this.navMode = 'side';
         this.sidenav.open();
      }
  }

  updateNoticesList(){
    const aCollection = this.firestore.collection('profiles')
    .doc(this.authService.loggedInProfile.userId)
    .collection<any>('notices');
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.noticeCollectionSub = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(noticeslist => {
      let cnt = 0;
      if ((noticeslist != undefined) && (noticeslist.length > 0)) {
        for (const aNotice of noticeslist) {
          if (aNotice.isRead == false) {
            cnt++;
          }
        }
      }
      this.unreadNoticesCount = cnt;
    });
  }

  notificationClick() {
    console.log('notificationClick clicked');

    // this.alertService.success('Test Message');
    this.router.navigate(['employer_dashboard', {show: 'notifications'}]);

  }
  

}
