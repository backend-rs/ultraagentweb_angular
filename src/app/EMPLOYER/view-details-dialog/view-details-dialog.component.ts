import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-view-details-dialog',
  templateUrl: './view-details-dialog.component.html',
  styleUrls: ['./view-details-dialog.component.css']
})
export class ViewDetailsDialogComponent implements OnInit {

  shift: any;
  constructor(public dialogRef: MatDialogRef<ViewDetailsDialogComponent>, @Inject(MAT_DIALOG_DATA) public data:string) { }

  ngOnInit() {
  }
  
  cancelClick(): void {
    this.dialogRef.close();
  }
  
}
