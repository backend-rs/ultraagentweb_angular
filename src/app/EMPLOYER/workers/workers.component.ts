import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../../services/auth.service';
import { SearchBoxService } from "../../services/searchbox.service";
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';

import { MatDialog } from '@angular/material';
import { EmployerWorkersDialogComponent } from '../employer-workers-dialog/employer-workers-dialog.component';

import {UAGlobals} from '../../globals/uaglobals';

@Component({
  selector: 'app-workers',
  templateUrl: './workers.component.html',
  styleUrls: ['./workers.component.css']
})
export class WorkersComponent implements OnInit {

  dialogResult="";
  filterSearchText ="";
  filterWorkersText="";

  workerslist=[];

  subcategories= [];


  private workersSubscription:Subscription;
  private searchBoxSubscription: Subscription;

  constructor(public dialog: MatDialog, 
    private authService: AuthService,
    private router: Router,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    private searchBoxService: SearchBoxService
  ) { 
    this.subcategories = UAGlobals.MASTER_CATEGORIES[this.authService.companyProfile.industry];
  }

  ngOnInit() {
    // Reset search box in navigation
    this.searchBoxService.clearMessage();

    this.searchBoxSubscription = this.searchBoxService.getMessage().subscribe((msg) => {
      this.filterSearchText = msg.searchword;
    });

    this.updateWorkersList();
  }

  ngOnDestroy() {
    this.searchBoxSubscription.unsubscribe();

    this.workersSubscription.unsubscribe();
  }

  filterWorkersSelected(selectedSubCat) {
    console.log('filterWorkersSelected='+selectedSubCat);
    if (selectedSubCat.toLowerCase() == 'none') {
      this.filterWorkersText = "";
    } else {
      this.filterWorkersText = selectedSubCat;
    }
    return true;
  }

  updateWorkersList(){
    //console.log('updateWorkersList');
    //console.log(this.authService.companyProfile);
    var aCollection = this.firestore.collection('profiles', ref => 
    ref.where('profileType','==','worker')
      .where('approvalStatus','==','approved')
      .where('wProfession','==',this.authService.companyProfile.industry)
  );
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    this.workersSubscription = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(workerslist => {
      if (workerslist != undefined) {
       this.workerslist = workerslist;
       //console.log('workers');
       //console.log(workerslist);
      } else {
        this.workerslist = [];
      }
    });
     
   }

   openWorkerDialog(aprofile): void {
    const dialogRef = this.dialog.open(EmployerWorkersDialogComponent, {
      panelClass: 'my-panel',
      width: '700px',
    });

    dialogRef.componentInstance.profile = aprofile;
    dialogRef.componentInstance.updateBookingsList();
    dialogRef.afterClosed().subscribe(result => {
      // console.log(`The dialog was closed: ${result}`);
      // this.dialogResult = result;
         
       
    });
  }

}
