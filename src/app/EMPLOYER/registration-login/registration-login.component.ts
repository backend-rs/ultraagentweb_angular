import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators }from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { NewshiftDoneDialogComponent } from '../newshift-done-dialog/newshift-done-dialog.component';
import { ApproveRejectDialogComponent } from '../../dialogs/approve-reject-dialog/approve-reject-dialog.component';

//import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
import { AngularFirestore } from 'angularfire2/firestore';

import { Profile } from '../../models/profile.model';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';
import { Title } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { EmployerGuard } from 'src/app/services/employerGuard.service';

@Component({
  selector: 'app-registration-login',
  templateUrl: './registration-login.component.html',
  styleUrls: ['./registration-login.component.css']
})
export class RegistrationLoginComponent implements OnInit {

  public email='';
  public password = '';

  uaWebVersion = environment.uaWebVersion;
  
  constructor(
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    public dialog: MatDialog,
    private employerGuard: EmployerGuard,
  ) { 
    
   
  }

  ngOnInit() {
    //console.log('calling employerGuard.canActivate');
    this.employerGuard.canActivate(null,null)
    .then((val) => {
      //console.log('employerGuard.canActivate returned '+val);
      if (val == true) {
        // User already logged-in why, show the login page
        this.router.navigate(['employer_dashboard']);
      }
    });
  }

  ngOnDestroy(){
  }

  clickForgotPassword(){
    this.router.navigate(['forgot_password', {
      employer:true,
      skipLocationChange:true
    }]);
  }

  clickTermsOfServices(){
    this.router.navigate(['terms_of_services']);
  }

  clickOnLogin(){
    // For Empty email and password
     if ((this.email.trim().length < 1 && this.password.length < 1)) {
       this.openDialog('Invalid', "Please enter a valid email address and password.");
       return;
     }

    // For Empty email or lessthan 3 character
    if ((this.email == undefined) || (this.email.trim().length < 3)) {
      this.openDialog('Invalid', "Please enter a valid email address.");
      return;
    }

    // For Empty email or lessthan 8 character
    if ((this.password == undefined) || (this.password.length < 1)) {
      this.openDialog('Invalid', "Please enter a valid password.");
      return;
    }

    // For Empty password or lessthan 8 character
    // if (!/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/.test(this.password)) {
    //   this.openDialog('Error', "The username or password you entered is incorrect");
    //   return;
    // }

    this.authService.doLogin(this.email.trim(), this.password)
    .then((auser) => {
      console.log(auser);
      if((auser != undefined) && (auser.uid != undefined )){
        console.log('checking if email verified');
        if (auser.emailVerified == false) {
          // Well well email NOT verified.

          const dialogRef = this.dialog.open(ApproveRejectDialogComponent, {
            width: '600px',
          });
          dialogRef.componentInstance.title = 'Email not verified';
          dialogRef.componentInstance.descr = 
          "Your email address "+auser.email+" has not been verified yet.\n"+
          "Choose 'Start Verification' so that we can send you an email with verification link.\n"+
          "You can then open the verification link from your mailbox and follow the instructions.";

          dialogRef.componentInstance.approveTitle = 'Start Verification';
          dialogRef.componentInstance.rejectTitle = 'Cancel';

          dialogRef.afterClosed().subscribe(result => {
            console.log(`The dialog was closed: ${result}`);

            if (result == dialogRef.componentInstance.approveTitle) {
              firebase.auth().currentUser.sendEmailVerification()
              .then(() => {
                this.openDialog("Verification link sent",
                "An email with verification link has been sent to your email address "+auser.email);
                this.authService.doLogout().then().catch();
              })
              .catch((err) => {
                this.authService.doLogout().then().catch();
                console.log(err);
                this.openDialog('Error',err.message);
              });

            } else {
              this.authService.doLogout().then().catch();
              return;
            }
          });


        } else {
          console.log('email is verified');
          this.firestore.collection('profiles').doc(auser.uid).ref.get()
          .then((adoc) => {
            if (!adoc.exists) { 
              this.openDialog('Error', 'Account has been deleted. Contact support.');
              return; 
            }
            var aprofile = adoc.data();
            aprofile.id = adoc.id;

            console.log("Fetching Here1");
            console.log(aprofile);
            //aprofile:Profile = aObject as Profile;
            if(aprofile != undefined){
              if(aprofile.approvalStatus != 'approved'){
                //dialog 
                if(aprofile.approvalStatus == 'pending'){
                  this.openDialog('Oops!', "Your profile is in pending state.\n"+
                  "We are verifying your documents. Once ready, we will intimate you via email.\n"+
                  "Please contact admin if you have any queries.\nadmin@ultraagent.co.uk");
                }else{
                  this.openDialog('Error', "Your account is in " + aprofile.approvalStatus + " state. Please contact support.");
                }
                this.authService.doLogout().then().catch();
                return;
              }
              if(aprofile.profileType == 'worker'){
                //dialog 
                this.openDialog('Error', "You don't have access to this portal. Please login from the worker app.");
                this.authService.doLogout().then().catch();
                return;
              }
              if((aprofile.profileType == 'adminsuper') || 
                (aprofile.profileType == 'adminuser') || 
                (aprofile.profileType == 'admin')){
                //dialog 
                this.openDialog('Error', "You are a admin user. Please login from admin portal.");
                this.authService.doLogout().then().catch();
                return;
              }
              if((aprofile.profileType == 'companyadmin') || 
                (aprofile.profileType == 'companyuser') || 
                (aprofile.profileType == 'company')){

                this.firestore.collection('companies').doc(aprofile.companyId).ref.get()
                .then((cdoc) => {
                  if (!cdoc.exists) { 
                    this.openDialog('Error', 'Could not load associated company. Contact support.');
                    return; 
                  }
                  this.router.navigate(['employer_dashboard']);
                });

                return;
              }
              this.openDialog("Oops!", "Unsupported account type.\nPlease contact support.");
            }

          }).catch((err) => {
            console.log(err);
            this.openDialog('Error', err.message);
          });
        }
      }
    })
    .catch((err) => {
      console.log(err);
      if ((err.code != undefined) && (err.code == "auth/wrong-password")) {
        this.openDialog('Error', 'The username or password you entered is incorrect');
      } else if ((err.code != undefined) && (err.code == "auth/user-not-found")) {
        this.openDialog('Error', 'There is no record corresponding to this username. Please contact your administrator to request access');
      } else {
        this.openDialog('Error', err.message);
      }
      
    });
  }

  openDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(NewshiftDoneDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    
  }

}
