import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerDetailsDialogComponent } from './employer-details-dialog.component';

describe('EmployerDetailsDialogComponent', () => {
  let component: EmployerDetailsDialogComponent;
  let fixture: ComponentFixture<EmployerDetailsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerDetailsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerDetailsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
