import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ApproveShiftDialogComponent } from '../approve-shift-dialog/approve-shift-dialog.component';

@Component({
  selector: 'app-employer-details-dialog',
  templateUrl: './employer-details-dialog.component.html',
  styleUrls: ['./employer-details-dialog.component.css']
})
export class EmployerDetailsDialogComponent implements OnInit {


  constructor(public dialog: MatDialog, public dialogRef: MatDialogRef<EmployerDetailsDialogComponent>, @Inject(MAT_DIALOG_DATA) public data:string) { }

  ngOnInit() {
  }
  
  openDialog(): void {
    const dialogRef = this.dialog.open(ApproveShiftDialogComponent, {
      panelClass: 'my-panel',
      width: '700px',
    });

     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
    });

    this.dialogRef.close();

  }

  cancelClick(): void {
    this.dialogRef.close();
  }

}
