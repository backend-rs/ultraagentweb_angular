import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerTimesheetComponent } from './employer-timesheet.component';

describe('EmployerTimesheetComponent', () => {
  let component: EmployerTimesheetComponent;
  let fixture: ComponentFixture<EmployerTimesheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerTimesheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerTimesheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
