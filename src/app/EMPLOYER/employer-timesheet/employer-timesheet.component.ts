import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CompletedPendingDisputedViewdetailsDialogComponent } from '../../dialogs/completed-pending-disputed-viewdetails-dialog/completed-pending-disputed-viewdetails-dialog.component';

import { SearchBoxService } from "../../services/searchbox.service";

import { AuthService } from '../../services/auth.service';
import { Router, ActivatedRoute} from '@angular/router';
import { FormBuilder, FormGroup, Validators }from '@angular/forms';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';

import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';

@Component({
  selector: 'app-employer-timesheet',
  templateUrl: './employer-timesheet.component.html',
  styleUrls: ['./employer-timesheet.component.css']
})
export class EmployerTimesheetComponent {

  searchword = '';
  filterDate:Date = undefined;
  bookingsSubscription:Subscription;

  private searchBoxSubscription: Subscription;

  blist = [];
  Math: any;
  sub:any;
  passedBookingId:any;


  activeTab = 'all';

  constructor(public dialog: MatDialog,
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    private route: ActivatedRoute,
    private searchBoxService: SearchBoxService
    ) {
      this.Math = Math;
     

    }

    ngOnInit() {

      // Reset search box in navigation
      this.searchBoxService.clearMessage();

      this.searchBoxSubscription = this.searchBoxService.getMessage().subscribe((msg) => {
        this.searchword = msg.searchword;
      });

      this.sub = this.route.params.subscribe(params => {
        this.passedBookingId = params['bookingId']; 
        console.log(params);
      });

      this.updateBookings();

     
    }

    ngOnDestroy() {
      this.searchBoxSubscription.unsubscribe();
      
      if(this.bookingsSubscription != undefined) {
        this.bookingsSubscription.unsubscribe();
      }

      this.sub.unsubscribe();
    }

  updateBookings(){
    const aCollection = this.firestore.collection('bookings',ref => 
      ref.where('companyId','==',this.authService.loggedInProfile.companyId)
      .where('workingStatus','==','completed')
    );
    // .snapshotChanges() returns a DocumentChangeAction[], which contains
    // a lot of information about "what happened" with each change. If you want to
    // get the data and the id use the map operator.
    aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(someList => {
      if(someList != undefined){
        this.blist = someList;
      }
      
      // console.log('booking fetched');
      // console.log(this.blist);
      // console.log(this.passedBookingId);

      if ((this.passedBookingId != undefined) && (this.passedBookingId.length > 0)) {
        var found = false;
        for (var i=0; i< this.blist.length; i++) {
          if (this.blist[i].id == this.passedBookingId) {
            const aBooking = this.blist[i];
            found = true;
            if ((aBooking.timesheetStatus == undefined) || (aBooking.timesheetStatus == 'pending')){
              //console.log('Programmatically opening pending');
              this.activeTab = 'pending';
              this.openPendingViewDetailsDialog(aBooking);
              return;
            } else if ((aBooking.timesheetStatus == 'approved')){
              //console.log('Programmatically opening completed');

              this.activeTab = 'completed';
              this.openCompletedViewDetailsDialog(aBooking);
              return;
            }  else  if ((aBooking.timesheetStatus != undefined) && (aBooking.timesheetStatus != 'approved') && 
            (aBooking.timesheetStatus != 'pending')){
              //console.log('Programmatically opening disputed');

              this.activeTab = 'disputed';
              this.openDisputedViewDetailsDialog(aBooking);
              return;
            }
            break;
          }
        }

        if (found == false) {
          this.openDialog('Oops!','Booking you are looking for no longer available here.\n\nIt might have been deleted or archived.');
        }

      }

    }
    );
  }


  openCompletedViewDetailsDialog(aBooking): void {

    const dialogRef = this.dialog.open(CompletedPendingDisputedViewdetailsDialogComponent, {
      panelClass: 'my-panel',
      width: '800px',
    });

    dialogRef.componentInstance.title='APPROVED SHIFT';
    dialogRef.componentInstance.subtitle='SHIFT NO: '+aBooking.shiftSubCategory+' #'+aBooking.shiftNo;
    dialogRef.componentInstance.approveTitle = 'approve';
    dialogRef.componentInstance.amendTitle = 'amend';
    dialogRef.componentInstance.rateworkerbtnTitle = 'rate worker';

    dialogRef.componentInstance.booking = aBooking;
    dialogRef.componentInstance.status = 'APPROVED';
    dialogRef.componentInstance.hideProposedChangesSection = true;
    dialogRef.componentInstance.hideRateWorkerButton = false;
    dialogRef.componentInstance.hideAmendApproveButtons = true;

     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
    });
  }

  openPendingViewDetailsDialog(aBooking): void {

    const dialogRef = this.dialog.open(CompletedPendingDisputedViewdetailsDialogComponent, {
      panelClass: 'my-panel',
      width: '800px',
    });

    dialogRef.componentInstance.title='pending shift';
    dialogRef.componentInstance.subtitle='SHIFT NO: '+aBooking.shiftSubCategory+' #'
    +aBooking.shiftNo;
    dialogRef.componentInstance.approveTitle = 'approve';
    dialogRef.componentInstance.amendTitle = 'amend';
    dialogRef.componentInstance.rateworkerbtnTitle = 'rate worker';
    
    dialogRef.componentInstance.booking = aBooking;
    dialogRef.componentInstance.status = 'PENDING';
    dialogRef.componentInstance.hideProposedChangesSection = true;
    dialogRef.componentInstance.hideRateWorkerButton = true;
    dialogRef.componentInstance.hideAmendApproveButtons = false;
    
     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
    });
  }

  openDisputedViewDetailsDialog(aBooking): void {

    const dialogRef = this.dialog.open(CompletedPendingDisputedViewdetailsDialogComponent, {
      panelClass: 'my-panel',
      width: '800px',
    });


    dialogRef.componentInstance.title='disputed shift';
    dialogRef.componentInstance.subtitle='SHIFT NO: '+aBooking.shiftSubCategory+' #'
    +aBooking.shiftNo;
    //dialogRef.componentInstance.approveTitle = 'approve';
    //dialogRef.componentInstance.amendTitle = 'amend';
    //dialogRef.componentInstance.rateworkerbtnTitle = 'rate worker';
    
    dialogRef.componentInstance.booking = aBooking;
    dialogRef.componentInstance.status = 'DISPUTED';
    dialogRef.componentInstance.hideProposedChangesSection = false;
    dialogRef.componentInstance.hideRateWorkerButton = true;
    dialogRef.componentInstance.hideAmendApproveButtons = true;

    this.firestore.collection('bookings').doc(aBooking.id)
    .collection('amends').doc(aBooking.activeAmendId)
    .ref
    .get()
    .then((adoc) => {
      if (!adoc.exists) { 
        this.openDialog('Error', 'Could not find amend object');
        return; 
      }
      var aAmend = adoc.data();
      aAmend.id = adoc.id;

      dialogRef.componentInstance.amend = aAmend;
      
    })
    .catch((err) => {
      console.log(err.message);
      this.openDialog('Error', err.message);
    })

     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
    });
  }

  openDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
    
  }

}
