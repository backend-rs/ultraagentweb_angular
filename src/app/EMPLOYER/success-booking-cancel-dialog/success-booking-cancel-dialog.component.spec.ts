import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessBookingCancelDialogComponent } from './success-booking-cancel-dialog.component';

describe('SuccessBookingCancelDialogComponent', () => {
  let component: SuccessBookingCancelDialogComponent;
  let fixture: ComponentFixture<SuccessBookingCancelDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccessBookingCancelDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessBookingCancelDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
