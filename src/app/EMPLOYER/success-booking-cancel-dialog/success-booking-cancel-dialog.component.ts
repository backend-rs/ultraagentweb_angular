import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-success-booking-cancel-dialog',
  templateUrl: './success-booking-cancel-dialog.component.html',
  styleUrls: ['./success-booking-cancel-dialog.component.css']
})
export class SuccessBookingCancelDialogComponent implements OnInit {

  public show24HrNotice = false;
  constructor(public dialogRef: MatDialogRef<SuccessBookingCancelDialogComponent>, @Inject(MAT_DIALOG_DATA) public data:string) { }

  ngOnInit() {
  }

  cancelClick(): void {
    this.dialogRef.close();
  }

}
