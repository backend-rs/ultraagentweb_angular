import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAdvanceBookingDetailsDialogComponent } from './view-advance-booking-details-dialog.component';

describe('ViewAdvanceBookingDetailsDialogComponent', () => {
  let component: ViewAdvanceBookingDetailsDialogComponent;
  let fixture: ComponentFixture<ViewAdvanceBookingDetailsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAdvanceBookingDetailsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAdvanceBookingDetailsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
