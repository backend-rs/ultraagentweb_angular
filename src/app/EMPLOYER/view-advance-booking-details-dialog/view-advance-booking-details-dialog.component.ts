import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { ApproveRejectDialogComponent } from '../../dialogs/approve-reject-dialog/approve-reject-dialog.component';
import { AngularFirestore } from 'angularfire2/firestore';

import { AuthService } from '../../services/auth.service';
import * as firebase from 'firebase/app';


@Component({
  selector: 'app-view-advance-booking-details-dialog',
  templateUrl: './view-advance-booking-details-dialog.component.html',
  styleUrls: ['./view-advance-booking-details-dialog.component.css']
})
export class ViewAdvanceBookingDetailsDialogComponent implements OnInit {

  hideneedExperience = false;


  shift:any;
  public shiftDate = new Date();
  declinedWorkers:any[];
  bookedWorkers:any[];
  interestedWorkers:any[];
  requestedWorkers:any[];
  hideDeclinedWorkersList:boolean = true;
  hideBookedWorkersList:boolean = false;
  hideRequestedWorkersList:boolean = false;
  hideInterestedWorkersList:boolean = false;
  isDeclinedWorkersListActionable:boolean = false;
  isBookedWorkersListActionable:boolean = true;
  isInterestedWorkersListActionable:boolean = true;
  isRequestedWorkersListActionable:boolean = true;
  hideWaitingApproval:boolean = false;
  hideWorkerDeclined:boolean = false;

  constructor(
    public dialog: MatDialog, 
    public authService: AuthService,
    private firestore: AngularFirestore,
    public dialogRef: MatDialogRef<ViewAdvanceBookingDetailsDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data:string) { 

  }

  ngOnInit() {
  }
  
  cancelClick(): void {
    this.dialogRef.close();
  }

  clickRequestedWorker(refWorker={}): void {
    if(this.isRequestedWorkersListActionable == false){
      return;
    }

    const dialogRef = this.dialog.open(ApproveRejectDialogComponent, {
      panelClass: 'my-panel',
      width: '600px',
    });

    dialogRef.componentInstance.title='confirmation';
    dialogRef.componentInstance.descr = 'description';
    dialogRef.componentInstance.approveTitle = 'approve';
    dialogRef.componentInstance.rejectTitle = 'reject';

     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
    });
  }

  clickCancelBookedWorker(refWorker): void {
    if(this.isBookedWorkersListActionable == false){
      return;
    }
    const dialogRef = this.dialog.open(ApproveRejectDialogComponent, {
      panelClass: 'my-panel',
      width: '600px',
    });

    dialogRef.componentInstance.title='Confirmation';
    dialogRef.componentInstance.descr = 'Do you really want to cancel booking with '+
      refWorker.firstName+' '+refWorker.lastName+'?';
    dialogRef.componentInstance.approveTitle = 'Yes, Cancel Booking';
    dialogRef.componentInstance.rejectTitle = 'No, Keep Booking';

     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);

      if (result == dialogRef.componentInstance.approveTitle) {
        // Change approvalStatus to pending on bookingrequest
        const reqId = this.shift.id+'-'+refWorker.userId;
        let cancelReasone = 'Materials not ready';
        let cancelBrief = 'Materials not ready yet. Hence, cancelling.';
        this.authService.doCancelBookedWorker(reqId, cancelReasone, cancelBrief)
        .then((aObj) => {
          this.openInformDialog('SUCCESS', 'Worker booking cancelled successfully');
          this.dialogRef.close();
        })
        .catch((err) => {
          console.log(err.message);
          this.openInformDialog('Error', err.message);
        });
        // this.firestore.collection('bookingrequests').doc(reqId).set(
        //   {approvalStatus:'pending'},
        //   {merge:true}
        // )
        // .then((aObj) => {
        //   // Create new booking object
        //   this.firestore.collection('bookings').doc(reqId).set(
        //     {status:'cancelled'},
        //     {merge:true}
        //   )
        //   .then(() => {
        //     this.openInformDialog('SUCCESS', 'Worker booking cancelled successfully');
        //     this.dialogRef.close();
        //   })
        //   .catch((err) => {
        //     console.log(err.message);
        //     this.openInformDialog('Error', err.message);
        //   });
          
        // })
        // .catch((err) => {
        //   console.log(err.message);
        //   this.openInformDialog('Error', err.message);
        // });

      }

    });
  }

  clickAcceptInterestedWorker(refWorker): void {
    if(this.isInterestedWorkersListActionable == false){
      return;
    }
    const dialogRef = this.dialog.open(ApproveRejectDialogComponent, {
      panelClass: 'my-panel',
      width: '600px',
    });

    dialogRef.componentInstance.title='Confirmation';
    dialogRef.componentInstance.descr = 'Do you really want to confirm booking with '+
      refWorker.firstName+' '+refWorker.lastName+'?';
    dialogRef.componentInstance.approveTitle = 'Yes, Confirm';
    dialogRef.componentInstance.rejectTitle = 'No, Cancel';

     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      if (result == dialogRef.componentInstance.approveTitle) {
        // Change approvalStatus to approved on bookingrequest
        const reqId = this.shift.id+'-'+refWorker.userId;
        this.authService.doAcceptWorkerForBookingRequest(
          reqId, 
          this.shift.id, 
          refWorker.userId, 
          this.authService.companyProfile.id)
        .then((aObj) => {
          this.openInformDialog('SUCCESS', 'Worker booked successfully');
          this.dialogRef.close();
        }).catch((err)=>{
          console.log(err);
          this.openInformDialog("Error", err.message);
        }) ;
      }
    });
  }
  clickDeclineInterestedWorker(refWorker): void {
    if(this.isInterestedWorkersListActionable == false){
      return;
    }
    const dialogRef = this.dialog.open(ApproveRejectDialogComponent, {
      panelClass: 'my-panel',
      width: '600px',
    });

    dialogRef.componentInstance.title='Confirmation';
    dialogRef.componentInstance.descr = 'Do you really want to decline booking with '+
      refWorker.firstName+' '+refWorker.lastName+'?';
    dialogRef.componentInstance.approveTitle = 'Yes, Decline';
    dialogRef.componentInstance.rejectTitle = 'No, Cancel';

     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      if (result == dialogRef.componentInstance.approveTitle) {
        // Change approvalStatus to approved on bookingrequest
        const reqId = this.shift.id+'-'+refWorker.userId;
        this.authService.doDeclineWorkerForBookingRequest(reqId)
        .then((aObj) => {
          this.openInformDialog('SUCCESS', 'Worker declined successfully');
          this.dialogRef.close();
        }).catch((err)=>{
          console.log(err);
          this.openInformDialog("Error", err.message);
        }) ;
      }
    });
  }

  
  editShift(aShift):void{
    this.dialogRef.close();
  }

  openInformDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
      
  }


}
