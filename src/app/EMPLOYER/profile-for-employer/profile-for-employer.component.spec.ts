import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileForEmployerComponent } from './profile-for-employer.component';

describe('ProfileForEmployerComponent', () => {
  let component: ProfileForEmployerComponent;
  let fixture: ComponentFixture<ProfileForEmployerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileForEmployerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileForEmployerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
