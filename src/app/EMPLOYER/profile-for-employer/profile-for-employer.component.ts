import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { UploadService } from '../../services/upload.service';
import {Upload} from '../../models/upload.model';

import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { HttpClient, HttpParams } from '@angular/common/http';

import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { ApproveRejectDialogComponent } from '../../dialogs/approve-reject-dialog/approve-reject-dialog.component';

import * as firebase from 'firebase/app';

@Component({
  selector: 'app-profile-for-employer',
  templateUrl: './profile-for-employer.component.html',
  styleUrls: ['./profile-for-employer.component.css']
})
export class ProfileForEmployerComponent implements OnInit {

  profile: any;
  profilePicUpload: Upload;
  profilePic: any;

  constructor(public dialog: MatDialog,
    public authService: AuthService,
    private router: Router,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    private httpClient:HttpClient,
    private upSvc: UploadService,) { 
      this.profile = this.authService.loggedInProfile;
      console.log(this.profile);
      if(this.profile.profilePicture != undefined){
        this.profilePicUpload = new Upload(undefined);
        this.profilePicUpload.name = this.profile.profilePicture.name;
        this.profilePicUpload.uniqId = this.profile.profilePicture.uniqId;
        this.profilePicUpload.totalBytes = this.profile.profilePicture.totalBytes;
        this.profilePicUpload.url = this.profile.profilePicture.url;
        this.profilePicUpload.progress = 100;

        this.profilePic = this.profile.profilePicture.url;

      }
    }

  ngOnInit() {
  }

  readProfilePicURL(event) {
    if (event.target.files && event.target.files[0]) {
        const file = event.target.files[0];

        const reader = new FileReader();
        reader.onload = e => this.profilePic = reader.result;

        reader.readAsDataURL(file);

        if ((this.profilePicUpload != undefined) && (this.profilePicUpload.url != undefined) &&
            (this.profilePicUpload.url.length > 0) && (this.profilePicUpload.uniqId != undefined) &&
            (this.profilePicUpload.uniqId.length > 0)) {
              // Delete existing entry
              this.upSvc.deleteUpload(this.profilePicUpload, 'companyimages');
        }
        this.profilePicUpload = new Upload(file);
        this.upSvc.pushUpload(this.profilePicUpload, 'companyimages');
    }
}


  onProfileSave() {

    if ((this.profile.email == undefined) || (this.profile.email.length < 1)) {
      this.openDialog("Email not specified","Please enter a valid email.");
      return;
    }
    if ((this.profile.firstName == undefined) || (this.profile.firstName.length < 1)) {
      this.openDialog("First name not specified","Please enter a valid first name.");
      return;
    }
    if ((this.profile.lastName == undefined) || (this.profile.lastName.length < 1)) {
      this.openDialog("Last name not specified","Please enter a valid last name.");
      return;
    }
    if ((this.profile.phoneNumber == undefined) || (this.profile.phoneNumber.length < 1)) {
      this.openDialog("Phone number not specified","Please enter a valid phone number.");
      return;
    }
    if ((this.profile.phoneNumber == undefined) || (this.profile.phoneNumber.length < 10)) {
      this.openDialog("Invalid","Phone number should be minimum 10 digits.");
      return;
    }
   
    if ((this.profile.jobTitle == undefined) || (this.profile.jobTitle.length < 1)) {
      this.openDialog("Job title not specified","Please enter a valid job title.");
      return;
    }
    if ((this.profile.city == undefined) || (this.profile.city.length < 1)) {
      this.openDialog("City not specified","Please enter a valid city.");
      return;
    }
    if ((this.profile.address == undefined) || (this.profile.address.length < 1)) {
      this.openDialog("Address not specified","Please enter a valid address.");
      return;
    }
    // if ((this.profile.postcode == undefined) || (this.profile.postcode.length < 1)) {
    //   this.openDialog("postcode not specified","Please enter a valid postcode.");
    //   return;
    // }

    const dialogRef = this.dialog.open(ApproveRejectDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = 'Are you sure?';
    dialogRef.componentInstance.descr = 
    "Do you really want to save change to this profile?";

    dialogRef.componentInstance.approveTitle = 'Yes, Save';
    dialogRef.componentInstance.rejectTitle = 'No, Cancel';

    dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      if (result == dialogRef.componentInstance.approveTitle) {

        this.firestore.collection("profiles").doc(this.profile.userId).set({
          firstName:this.profile.firstName,
          lastName: this.profile.lastName,
          phoneNumber: this.profile.phoneNumber,
          city: this.profile.city,
          address: this.profile.address,
          // postcode: this.profile.postcode,
          jobTitle: this.profile.jobTitle,
          permsShifts: this.profile.permsShifts,
          permsBookings: this.profile.permsBookings,
          permsTimesheets: this.profile.permsTimesheets,
          permsUsers: this.profile.permsUsers,
          createdAt: firebase.firestore.FieldValue.serverTimestamp()
        }, {merge: true})
        .then(_ => {
          
          if ((this.profilePicUpload != undefined) && 
          (this.profilePicUpload.progress != undefined) && 
          (this.profilePicUpload.progress == 100) && 
          (this.profilePicUpload.uniqId != undefined) && 
          (this.profilePicUpload.uniqId.length > 0)) {
            var tempArr = new Array();
    
            tempArr.push({
              name: this.profilePicUpload.name != undefined? this.profilePicUpload.name:'',
              url: this.profilePicUpload.url != undefined? this.profilePicUpload.url:'',
              totalBytes: this.profilePicUpload.totalBytes,
              uniqId: this.profilePicUpload.uniqId
            });
            var profilePicObj=tempArr[0];
            this.firestore.collection("profiles").doc(this.profile.userId).set({
              profilePicture: profilePicObj,
            }, {merge: true})
            .then(() => {
              // If the user is a companyadmin, then apply the profile pic changes to the company as well
              if (this.profile.profileType == 'companyadmin') {
                this.firestore.collection("companies").doc(this.profile.userId).set({
                  profilePicture: profilePicObj,
                }, {merge: true})
                .then(() => {
                  this.openDialog("SUCCESS","Profile updated successfully.");
                })
                .catch((err) => {
                  console.log(err);
                  this.openDialog("Error",err.message);
                }); 
              } else {
                this.openDialog("SUCCESS","Profile updated successfully.");
              }
            })
            .catch((err) => {
              console.log(err);
              this.openDialog("Error",err.message);
            }); 
          }else{
            this.openDialog("SUCCESS","Profile updated successfully.");
          }
    
        })
        .catch((err) => {
          console.log(err);
          this.openDialog("Error",err.message);
        }); 

      }
    });
  }

  openDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
    
  }

}
