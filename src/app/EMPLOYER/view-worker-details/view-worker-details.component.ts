import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-view-worker-details',
  templateUrl: './view-worker-details.component.html',
  styleUrls: ['./view-worker-details.component.css']
})
export class ViewWorkerDetailsComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ViewWorkerDetailsComponent>, @Inject(MAT_DIALOG_DATA) public data:string) { }

  ngOnInit() {
  }
  
  cancelClick(): void {
    this.dialogRef.close();
  }

  generatePrint(){}
  generatePDF(){}

}
