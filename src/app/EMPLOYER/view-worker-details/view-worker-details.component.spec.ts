import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewWorkerDetailsComponent } from './view-worker-details.component';

describe('ViewWorkerDetailsComponent', () => {
  let component: ViewWorkerDetailsComponent;
  let fixture: ComponentFixture<ViewWorkerDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewWorkerDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewWorkerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
