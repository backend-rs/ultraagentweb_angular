import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { RegistrationDialogComponent } from '../registration-dialog/registration-dialog.component';
import { NewshiftDoneDialogComponent } from '../../EMPLOYER/newshift-done-dialog/newshift-done-dialog.component';

import { LocationLookupDialogComponent } from '../../dialogs/location-lookup-dialog/location-lookup-dialog.component';

import { AuthService } from '../../services/auth.service';
import { UploadService } from '../../services/upload.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators }from '@angular/forms';

import {Upload} from '../../models/upload.model';

//import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
import { AngularFirestore } from 'angularfire2/firestore';
import { UAGlobals } from '../../globals/uaglobals';
import { UAHelpers } from '../../globals/uahelpers';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';


@Component({
  selector: 'app-company-registration',
  templateUrl: './company-registration.component.html',
  styleUrls: ['./company-registration.component.css']
})
export class CompanyRegistrationComponent {

  selectedFiles: FileList;
  currentUploads = new Array<Upload>();

  profilePicUpload: Upload;
  profilePic: any;

  dialogResult="";
  registerForm: FormGroup;
  errorMessage: string = '';
  successMessage: string = '';
  profileAlreadyCreated = false;

  categories: any[];
  citySelectionData: any[];

  profile = {
    emailId:'',
    companycategory:'',
    firstname:'',
    lastname:'',
    companyname:'',
    phoneNumber:'',
    regnumber:'',
    city:'',
    address:'',
    postcode:'',
    cdescription:'',
    approvalStatus:'',
    profileType:'',
    lat:51.509865,
    lng:-0.118092
  }
 
  constructor(private authService: AuthService,
    private router: Router,
    public dialog: MatDialog,
    private fb: FormBuilder,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    private upSvc: UploadService,
    ) { 
      this.createForm();
      
      this.categories = Object.keys(UAGlobals.MASTER_CATEGORIES);
      this.citySelectionData = UAHelpers.cityDropdownData();
  }

  readProfilePicURL(event) {
    if (event.target.files && event.target.files[0]) {
        const file = event.target.files[0];

        const reader = new FileReader();
        reader.onload = e => this.profilePic = reader.result;

        reader.readAsDataURL(file);

        if ((this.profilePicUpload != undefined) && (this.profilePicUpload.url != undefined) &&
            (this.profilePicUpload.url.length > 0)) {
              // Delete existing entry
              this.upSvc.deleteUpload(this.profilePicUpload, 'companyimages');
        }
        this.profilePicUpload = new Upload(file);
        this.upSvc.pushUpload(this.profilePicUpload, 'companyimages');
    }
}

 
detectFiles(event) {
    this.selectedFiles = event.target.files;
    if ((this.selectedFiles != undefined) && (this.selectedFiles.length > 0)) {
      // Start file uplaod
      this.uploadMulti();
    }
}

uploadSingle() {
  let file = this.selectedFiles.item(0)
  var aUpload:Upload = new Upload(file);
  this.currentUploads.push(aUpload);
  this.upSvc.pushUpload(aUpload, 'companydocs')
}

uploadMulti() {
  console.log(this.currentUploads);
  let files = this.selectedFiles
  for (var i=0; i < files.length; i++) {
    var aUpload:Upload = new Upload(files[i]);
    this.currentUploads.push(aUpload);
    this.upSvc.pushUpload(aUpload, 'companydocs')
  }
}

  createForm() {
    this.registerForm = this.fb.group({
      email: ['', Validators.required ],
      password: ['', Validators.required ],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      companycategory: ['', Validators.required ],
      companyname: ['', Validators.required ],
      phoneno: ['', Validators.required ],
      regdno: ['', Validators.required ],
      city: ['', Validators.required ],
      address: ['', Validators.required ],
      postcode: ['', Validators.required ],
      cdesc: ['', Validators.required ],
    });
  }

  tryRegister(value){

    if ((value.email == undefined) || (value.email.trim().length < 3)) {
      this.openInformDialog('Invalid', "Please enter a valid email address.");
      return;
    }
   
    // if ((value.password == undefined) || (value.password.length < 8)){
    //   this.openInformDialog('Invalid', "Password should be minimum 8 characters and one uppercase and lowercase characters.");
    //   return;
    // }
    if ((value.password == undefined) || (!/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/.test(value.password))) {
      this.openInformDialog('Invalid', "Password should be Minimum 8 characters, at least one uppercase letter, one lowercase letter, one digit and one special character.");
      return;
    }
    
    if ((value.phoneno == undefined) || (value.phoneno.length < 10)) {
      this.openInformDialog("Invalid","Please enter a valid phone number.");
      return;
    }
    if ((value.firstname == undefined) || (value.firstname.length < 1)) {
      this.openInformDialog("Invalid","Please enter a valid firstname.");
      return;
    }
    if (!/^[a-zA-Z ]*$/.test(this.profile.firstname)) {
      this.openInformDialog("Invalid","Please enter a valid First Name");
      return;
    }
    if ((value.lastname == undefined) || (value.lastname.length < 1)) {
      this.openInformDialog("Invalid","Please enter a valid lastname.");
      return;
    }
    if (!/^[a-zA-Z ]*$/.test(this.profile.lastname)) {
      this.openInformDialog("Invalid","Please enter a valid Last Name");
      return;
    }
    if ((value.postcode == undefined) || (value.postcode.length < 1)) {
      this.openInformDialog("Invalid","Please enter a valid postcode.");
      return;
    }
    
    this.authService.doRegister(value.email.toLowerCase().trim(), value.password).
    then((aUser) => {
      this.createProfile(aUser.user);
    }).catch((err)=>{
      console.log('Error in this.authService.doRegister');
      console.log(err);
      this.openInformDialog("Error", err.message);
    }) ;
  }
  createProfile(createdUser){
      console.log(createdUser);
      if(this.profileAlreadyCreated == true){
        return;
      }

      var docs = new Array();
      for (var i=0; i< this.currentUploads.length; i++) {
        const aUpload = this.currentUploads[i];
        if (aUpload.progress == 100) {
          docs.push({
            name:aUpload.name,
            url:aUpload.url,
            totalBytes: aUpload.totalBytes,
            uniqId: aUpload.uniqId
          });
        }
      }

      var profilePicObj={
        url:"https://placehold.it/180",
      };
      if ((this.profilePicUpload != undefined) && (this.profilePicUpload.progress == 100)){
        var tempArr = new Array();

        tempArr.push({
          name: this.profilePicUpload.name,
          url: this.profilePicUpload.url,
          totalBytes: this.profilePicUpload.totalBytes,
          uniqId: this.profilePicUpload.uniqId
        });
        profilePicObj=tempArr[0];
      }

      console.log(docs);

      // Use the userid to create company
      this.firestore.collection("companies").doc(createdUser.uid).set({
        email: this.profile.emailId,
        approvalStatus:'pending',
        createdBy: createdUser.uid,
        industry: this.profile.companycategory,
        name: this.profile.companyname,
        phoneNumber: this.profile.phoneNumber,
        regdNumber: this.profile.regnumber,
        city: this.profile.city,
        address: this.profile.address,
        postcode: this.profile.postcode,
        descr: this.profile.cdescription,
        documents:docs,
        profilePicture: profilePicObj,
        lat:this.profile.lat,
        lng:this.profile.lng, 
        createdAt: firebase.firestore.FieldValue.serverTimestamp()
      }, {merge:true})
      .then((createdCompany) => {
        this.firestore.collection("profiles").doc(createdUser.uid).set({
          companyId:createdUser.uid,
          email: this.profile.emailId,
          firstName: this.profile.firstname,
          lastName: this.profile.lastname,
          profileType:'companyadmin',
          approvalStatus:'pending',
          userId: createdUser.uid,
          phoneNumber: this.profile.phoneNumber,
          city: this.profile.city,
          address: this.profile.address,
          postcode: this.profile.postcode,
          jobTitle: 'Admin',
          permsShifts: true,
          permsBookings: true,
          permsTimesheets: true,
          permsUsers: true,
          profilePicture: profilePicObj,
          sendNotifications: true,
          createdAt: firebase.firestore.FieldValue.serverTimestamp()
        }, {merge: true})
        .then(_ => {
          this.profileAlreadyCreated = true;
          this.authService.doLogout().then(() =>{
            this.openDialog();
            //this.router.navigate(['login']);
          }).catch((err)=>{
            console.log(err);
            this.openInformDialog("Error",err.message);
          })
        })
        .catch((err) => {
          console.log(err);
          this.openInformDialog("Error",err.message);
        }); 
      })
      .catch((err) => {
        console.log(err);
        this.openInformDialog("Error",err.message);
      });
      


    
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(RegistrationDialogComponent, {
      width: '600px',
      data: 'this text will display in dialog'
    });

     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      this.dialogResult = result;
    });
  }

  openInformDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(NewshiftDoneDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    
  }

  clickOnClear(){
    // this.registerForm.value.email = '';
    // this.registerForm.value.password = '';
    // this.profile.emailId='';
    // this.profile.companyname = '';
    // this.profile.regnumber = '';
    // this.profile.companycategory = '';
    // this.profile.phoneNumber = '';
    // this.profile.address = '';
    // this.profile.city = '';
    // this.profile.cdescription = '';
    // this.currentUploads = [];
    this.registerForm.reset();
    this.currentUploads = new Array<Upload>();
    this.profile.companycategory = '';
    this.profile.city = '';

  }

  openLocationLookupDialog(): void {
    const dialogRef = this.dialog.open(LocationLookupDialogComponent, {
      panelClass: 'my-panel',
      width: '750px',
      data: {
        lat:this.profile.lat,
        lng:this.profile.lng
      }
    });
   
     dialogRef.afterClosed().subscribe(result => {
      //console.log(`The dialog was closed: ${result}`);
      this.dialogResult = result;
      this.profile.address = result.address;
      this.profile.city = result.city;
      this.profile.lat = result.lat;
      this.profile.lng = result.lng;
      
    });
  }
  
}
