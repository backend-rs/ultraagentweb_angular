import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppRoutes } from './app.routes';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { AsyncPipe } from '../../node_modules/@angular/common';

import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';

import { HttpClientModule } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { AngularFireFunctionsModule } from 'angularfire2/functions';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireMessagingModule } from 'angularfire2/messaging';
import { UploadService } from './services/upload.service';
import { AuthService } from './services/auth.service';
import { MessagingService } from './services/messaging.service';
import { SearchBoxService } from './services/searchbox.service';
import { EmployerGuard } from './services/employerGuard.service';
import { AdminGuard } from './services/adminGuard.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { environment } from '../environments/environment';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatBadgeModule } from '@angular/material/badge';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';

import { ChartsModule } from 'ng2-charts/ng2-charts';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './ADMIN/dashboard/dashboard.component';
import { AdminLoginComponent } from './ADMIN/admin-login/admin-login.component';
import { ForgotPasswordComponent } from './ADMIN/forgot-password/forgot-password.component';
import { RegistrationLoginComponent } from './EMPLOYER/registration-login/registration-login.component';
import { ForgotPasswordDialogComponent } from './ADMIN/forgot-password-dialog/forgot-password-dialog.component';
import { CompanyRegistrationComponent } from './EMPLOYER/company-registration/company-registration.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { NewPasswordComponent } from './ADMIN/new-password/new-password.component';
import { RegistrationDialogComponent } from './EMPLOYER/registration-dialog/registration-dialog.component';
import { EmployeeUserExperienceComponent } from './EMPLOYER/employee-user-experience/employee-user-experience.component';
import { NavigationComponent } from './ADMIN/navigation/navigation.component';
import { AdminProfileComponent } from './ADMIN/admin-profile/admin-profile.component';
import { RegistrationComponent } from './ADMIN/registration/registration.component';
import { ShiftsComponent } from './ADMIN/shifts/shifts.component';
import { TimesheetComponent } from './ADMIN/timesheet/timesheet.component';
import { UsersComponent } from './ADMIN/users/users.component';
import { ReportsComponent } from './ADMIN/reports/reports.component';
import { DetailsDialogBoxComponent } from './ADMIN/details-dialog-box/details-dialog-box.component';
import { PostedShiftsComponent } from './EMPLOYER/posted-shifts/posted-shifts.component';
import { BookingsComponent } from './EMPLOYER/bookings/bookings.component';
import { WorkersComponent } from './EMPLOYER/workers/workers.component';
import { EmployerProfileComponent } from './EMPLOYER/employer-profile/employer-profile.component';
import { EmployerNavigationComponent } from './EMPLOYER/employer-navigation/employer-navigation.component';
import { EmployerDashboardComponent } from './EMPLOYER/employer-dashboard/employer-dashboard.component';
import { EmployerTimesheetComponent } from './EMPLOYER/employer-timesheet/employer-timesheet.component';
import { EmployerReportsComponent } from './EMPLOYER/employer-reports/employer-reports.component';
import { WorkersDialogComponent } from './ADMIN/workers-dialog/workers-dialog.component';
import { CompaniesDialogComponent } from './ADMIN/companies-dialog/companies-dialog.component';
import { EmployerDetailsDialogComponent } from './EMPLOYER/employer-details-dialog/employer-details-dialog.component';
import { ApproveShiftDialogComponent } from './EMPLOYER/approve-shift-dialog/approve-shift-dialog.component';
import { NewshiftAddDialogComponent } from './EMPLOYER/newshift-add-dialog/newshift-add-dialog.component';
import { NewshiftDoneDialogComponent } from './EMPLOYER/newshift-done-dialog/newshift-done-dialog.component';
import { DeleteShiftDialogComponent } from './EMPLOYER/delete-shift-dialog/delete-shift-dialog.component';
import { SuccessShiftDeletedDialogComponent } from './EMPLOYER/success-shift-deleted-dialog/success-shift-deleted-dialog.component';
import { ViewDetailsDialogComponent } from './EMPLOYER/view-details-dialog/view-details-dialog.component';
import { ViewWorkerDetailsComponent } from './EMPLOYER/view-worker-details/view-worker-details.component';
import { ViewAdvanceBookingDetailsDialogComponent } from './EMPLOYER/view-advance-booking-details-dialog/view-advance-booking-details-dialog.component';
import { EmployerWorkersDialogComponent } from './EMPLOYER/employer-workers-dialog/employer-workers-dialog.component';
import { MyshiftEditDialogComponent } from './EMPLOYER/myshift-edit-dialog/myshift-edit-dialog.component';
import { CancelBookingDialogComponent } from './EMPLOYER/cancel-booking-dialog/cancel-booking-dialog.component';
import { SuccessBookingCancelDialogComponent } from './EMPLOYER/success-booking-cancel-dialog/success-booking-cancel-dialog.component';
import { AdvanceBookingPageComponent } from './EMPLOYER/advance-booking-page/advance-booking-page.component';
import { PayTimesheetPageComponent } from './ADMIN/pay-timesheet-page/pay-timesheet-page.component';
import { ProfileComponent } from './ADMIN/profile/profile.component';
import { EmployerSettingsComponent } from './EMPLOYER/employer-settings/employer-settings.component';
import { AdminSettingsComponent } from './ADMIN/admin-settings/admin-settings.component';
import { ProfileForEmployerComponent } from './EMPLOYER/profile-for-employer/profile-for-employer.component';
import { AdminSettingEditPageComponent } from './ADMIN/admin-setting-edit-page/admin-setting-edit-page.component';
import { EmployerUsersEditPageComponent } from './EMPLOYER/employer-users-edit-page/employer-users-edit-page.component';
import { ReviewWorkerComponent } from './EMPLOYER/review-worker/review-worker.component';
import { TermsOfServicesComponent } from './EMPLOYER/terms-of-services/terms-of-services.component';


//Dialogs
import { ApproveRejectDialogComponent } from './dialogs/approve-reject-dialog/approve-reject-dialog.component';
import { SuccessOkDialogComponent } from './dialogs/success-ok-dialog/success-ok-dialog.component';
import { CompletedPendingDisputedViewdetailsDialogComponent } from './dialogs/completed-pending-disputed-viewdetails-dialog/completed-pending-disputed-viewdetails-dialog.component';
import { MybookingDetailsDialogComponent } from './dialogs/mybooking-details-dialog/mybooking-details-dialog.component';
import { PostedBookingsCancelledOngoingApprovedDisputedDetailsDialogComponent } from './dialogs/posted-bookings-cancelled-ongoing-approved-disputed-details-dialog/posted-bookings-cancelled-ongoing-approved-disputed-details-dialog.component';
import { PendingDetailDialogComponent } from './dialogs/pending-detail-dialog/pending-detail-dialog.component';
import { AdminRegistrationFullProfileDialogboxComponent } from './dialogs/admin-registration-full-profile-dialogbox/admin-registration-full-profile-dialogbox.component';
import { AdminRegistrationDeleteDialogboxComponent } from './dialogs/admin-registration-delete-dialogbox/admin-registration-delete-dialogbox.component';
import { AdminRegistrationApproveDialogboxComponent } from './dialogs/admin-registration-approve-dialogbox/admin-registration-approve-dialogbox.component';
import { AdminRegistrationRejectDialogboxComponent } from './dialogs/admin-registration-reject-dialogbox/admin-registration-reject-dialogbox.component';
import { LocationLookupDialogComponent } from './dialogs/location-lookup-dialog/location-lookup-dialog.component';
import { EmployerDateAdvanceBookingDialogComponent } from './dialogs/employer-date-advance-booking-dialog/employer-date-advance-booking-dialog.component';
import { ApproveDialogComponent } from './dialogs/approve-dialog/approve-dialog.component';
import { AmendDialogComponent } from './dialogs/amend-dialog/amend-dialog.component';
import { NoticeDetailsDialogComponent } from './dialogs/notice-details-dialog/notice-details-dialog.component';

//report design
import { UsersReportComponent } from './report_design/users-report/users-report.component';
import { TimesheetReportComponent } from './report_design/timesheet-report/timesheet-report.component';
import { ShiftsReportComponent } from './report_design/shifts-report/shifts-report.component';
import { RegistrationReportComponent } from './report_design/registration-report/registration-report.component';
import { EmployerBookingsComponent } from './report_design/employer-bookings/employer-bookings.component';
import { EmployerPostedshiftComponent } from './report_design/employer-postedshift/employer-postedshift.component';
import { EmployerTimesheetReportComponent } from './report_design/employer-timesheet-report/employer-timesheet-report.component';

// Filters
// For Admin Registrations Filters
import { RegistrationsCompanyFilterPipe } from './filters/registrationscompany-filter.pipe';
import { RegistrationsWorkerFilterPipe } from './filters/registrationsworker-filter.pipe';
import { RegistrationsAgentFilterPipe } from './filters/registrationsagent-filter.pipe';
import { RegistrationsOrderFilterPipe } from './filters/registrationsorder-filter.pipe';
// For Admin Users Filters
import { UsersCompanyFilterPipe } from './filters/userscompany-filter.pipe';
import { UsersWorkerFilterPipe } from './filters/usersworker-filter.pipe';
// For Employer Shifts Filters
import { ShiftsMyFilterPipe } from './filters/shiftsmy-filter.pipe';
import { ShiftsAdvancedBookingFilterPipe } from './filters/shiftsadvancedbooking-filter.pipe';
import { ShiftsOrderFilterPipe } from './filters/shiftsorder-filter.pipe';

import { BookingsOrderFilterPipe } from './filters/bookingsorder-filter.pipe';

// For Employer Workers Filters
import { EmployerWorkersFilterPipe } from './filters/employerworkers-filter.pipe';
import { EmployerWorkersOrderFilterPipe } from './filters/employerworkersorder-filter.pipe';
// For Admin Shifts Filters
import { AdminShiftsPostedFilterPipe } from './filters/adminshiftsposted-filter.pipe';
import { AdminShiftsBookingsFilterPipe } from './filters/adminshiftsbookings-filter.pipe';
import { AdminShiftsCancelledFilterPipe } from './filters/adminshiftscancelled-filter.pipe';
import { AdminShiftsOnGoingFilterPipe } from './filters/adminshiftsongoing-filter.pipe';
import { AdminShiftsApprovedFilterPipe } from './filters/adminshiftsapproved-filter.pipe';
import { AdminShiftsDisputesOpenFilterPipe } from './filters/adminshiftsdisputesopen-filter.pipe';
import { AdminShiftsDisputesResolvedFilterPipe } from './filters/adminshiftsdisputesresolved-filter.pipe';
import { AdminShiftsOrderFilterPipe } from './filters/adminshiftsorder-filter.pipe';
import { OrderByPipe } from './filters/orderby';
import { AdminBookingsOrderFilterPipe } from './filters/adminbookingsorder-filter.pipe';
//For Admin Timesheet Filters
import { AdminTimesheetPendingFilterPipe } from './filters/admintimesheetpending-filter.pipe';
import { AdminTimesheetPaidFilterPipe } from './filters/admintimesheetpaid-filter.pipe';

//For Employer Booking Filters
import { EmployerBookingsFilterPipe } from './filters/employerbookings-filter.pipe';
import { EmployerCancelledBookingsFilterPipe } from './filters/employercancelledbookings-filter.pipe';

import { BookingRequestFromWorkerFilterPipe } from './filters/bookingrequestfromworker-filter.pipe';
import { BookingRequestToWorkerFilterPipe } from './filters/bookingrequesttoworker-filter.pipe';

import { EmployerTimesheetCompletedFilterPipe } from './filters/employertimesheetcompleted-filter.pipe';
import { EmployerTimesheetDisputedFilterPipe } from './filters/employertimesheetdisputed-filter.pipe';
import { EmployerTimesheetPendingFilterPipe } from './filters/employertimesheetpending-filter.pipe';
import { EmployerTimesheetAllFilterPipe } from './filters/employertimesheetall-filter.pipe';
import { EmployerTimesheetOrderFilterPipe } from './filters/employertimesheetorder-filter.pipe';


// Missing declarations
import { ApproveDialogboxComponent } from './ADMIN/approve-dialogbox/approve-dialogbox.component';
import { DeleteDialogboxComponent } from './ADMIN/delete-dialogbox/delete-dialogbox.component';
import { FullProfileDialogboxComponent } from './ADMIN/full-profile-dialogbox/full-profile-dialogbox.component';
import { PendingDetailDialogComponent as PendingDetailDialogComponentADMIN } from './ADMIN/pending-detail-dialog/pending-detail-dialog.component';
import { RejectDialogboxComponent } from './ADMIN/reject-dialogbox/reject-dialogbox.component';
import { MybookingDetailsDialogComponent as MybookingDetailsDialogComponentEMPLOYER } from './EMPLOYER/mybooking-details-dialog/mybooking-details-dialog.component';
import { WorkersAdvanceBookingDialogComponent } from './EMPLOYER/workers-advance-booking-dialog/workers-advance-booking-dialog.component';

import { AlertComponent } from './directives/alert.component';
import { AlertService } from './services/alert.service';

import { UANumberOnlyDirective } from './directives/uanumberonly-directive';

import { UACustomDataAdapter } from './adapters/uacustomdate.adapter';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { Alert } from './models/alert';

//Star Rating
import { StarRatingModule } from 'angular-star-rating';



export const APP_DATE_FORMATS =
{
  parse: {
    dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
  },
  display: {
    dateInput: 'input',
    monthYearLabel: { year: 'numeric', month: 'numeric' },
    dateA11yLabel: { year: 'numeric', month: 'long', day: 'numeric' },
    monthYearA11yLabel: { year: 'numeric', month: 'long' },
  }
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    AdminLoginComponent,
    ForgotPasswordComponent,
    RegistrationLoginComponent,
    ForgotPasswordDialogComponent,
    CompanyRegistrationComponent,
    VerifyEmailComponent,
    NewPasswordComponent,
    RegistrationDialogComponent,
    EmployeeUserExperienceComponent,
    NavigationComponent,
    AdminProfileComponent,
    RegistrationComponent,
    ShiftsComponent,
    TimesheetComponent,
    UsersComponent,
    ReportsComponent,
    DetailsDialogBoxComponent,
    PostedShiftsComponent,
    BookingsComponent,
    WorkersComponent,
    EmployerProfileComponent,
    EmployerNavigationComponent,
    EmployerDashboardComponent,
    EmployerTimesheetComponent,
    EmployerReportsComponent,
    AdminRegistrationFullProfileDialogboxComponent,
    WorkersDialogComponent,
    CompaniesDialogComponent,
    PendingDetailDialogComponent,
    EmployerDetailsDialogComponent,
    ApproveShiftDialogComponent,
    ApproveDialogComponent,
    AmendDialogComponent,
    NewshiftAddDialogComponent,
    NewshiftDoneDialogComponent,
    DeleteShiftDialogComponent,
    SuccessShiftDeletedDialogComponent,
    ViewDetailsDialogComponent,
    AdminRegistrationDeleteDialogboxComponent,
    AdminRegistrationApproveDialogboxComponent,
    AdminRegistrationRejectDialogboxComponent,
    RegistrationsCompanyFilterPipe,
    RegistrationsWorkerFilterPipe,
    RegistrationsAgentFilterPipe,
    RegistrationsOrderFilterPipe,
    UsersCompanyFilterPipe,
    UsersWorkerFilterPipe,
    ViewWorkerDetailsComponent,
    ViewAdvanceBookingDetailsDialogComponent,
    EmployerWorkersDialogComponent,
    ShiftsMyFilterPipe,
    ShiftsAdvancedBookingFilterPipe,
    ShiftsOrderFilterPipe,
    BookingsOrderFilterPipe,
    EmployerWorkersFilterPipe,
    EmployerWorkersOrderFilterPipe,
    AdminShiftsPostedFilterPipe,
    AdminShiftsBookingsFilterPipe,
    AdminShiftsCancelledFilterPipe,
    AdminShiftsOnGoingFilterPipe,
    AdminShiftsApprovedFilterPipe,
    AdminShiftsDisputesOpenFilterPipe,
    AdminShiftsDisputesResolvedFilterPipe,
    AdminShiftsOrderFilterPipe,
    AdminBookingsOrderFilterPipe,
    AdminTimesheetPendingFilterPipe,
    AdminTimesheetPaidFilterPipe,
    MyshiftEditDialogComponent,
    MybookingDetailsDialogComponent,
    CancelBookingDialogComponent,
    SuccessBookingCancelDialogComponent,
    ApproveRejectDialogComponent,
    AdvanceBookingPageComponent,
    SuccessOkDialogComponent,
    CompletedPendingDisputedViewdetailsDialogComponent,
    PostedBookingsCancelledOngoingApprovedDisputedDetailsDialogComponent,
    PayTimesheetPageComponent,
    EmployerBookingsFilterPipe,
    EmployerCancelledBookingsFilterPipe,
    BookingRequestFromWorkerFilterPipe,
    BookingRequestToWorkerFilterPipe,
    EmployerTimesheetCompletedFilterPipe,
    EmployerTimesheetDisputedFilterPipe,
    EmployerTimesheetPendingFilterPipe,
    EmployerTimesheetOrderFilterPipe,
    EmployerTimesheetAllFilterPipe,
    ProfileComponent,
    EmployerSettingsComponent,
    AdminSettingsComponent,
    ProfileForEmployerComponent,
    AdminSettingEditPageComponent,
    EmployerUsersEditPageComponent,
    ApproveDialogboxComponent,
    DeleteDialogboxComponent,
    FullProfileDialogboxComponent,
    RejectDialogboxComponent,
    WorkersAdvanceBookingDialogComponent,
    MybookingDetailsDialogComponentEMPLOYER,
    PendingDetailDialogComponentADMIN,
    LocationLookupDialogComponent,
    EmployerDateAdvanceBookingDialogComponent,
    ReviewWorkerComponent,
    UANumberOnlyDirective,
    AlertComponent,
    NoticeDetailsDialogComponent,
    TermsOfServicesComponent,
    UsersReportComponent,
    TimesheetReportComponent,
    ShiftsReportComponent,
    RegistrationReportComponent,
    EmployerBookingsComponent,
    EmployerPostedshiftComponent,
    EmployerTimesheetReportComponent,
    OrderByPipe,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    RouterModule,
    AppRoutes,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCardModule,
    MatSidenavModule,
    MatFormFieldModule,
    MatListModule,
    MatIconModule,
    MatInputModule,
    MatDialogModule,
    MatMenuModule,
    MatToolbarModule,
    MatTabsModule,
    MatButtonToggleModule,
    MatBadgeModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    AngularFireFunctionsModule,
    AgmCoreModule.forRoot({
      //apiKey: 'AIzaSyBCVyhT9XtFXv5dzGOnQajhGv8cbdDfBhI' // by Bhagi
      apiKey: 'AIzaSyC6tw95R_WsmfUfqEhI8IF8TCbAf0kzeaE' // by Bharat on appsmartdev2018@gmail.com
    }),
    AngularFireMessagingModule,
    NgxMaterialTimepickerModule.forRoot(),
    ChartsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    StarRatingModule.forRoot()
  ],
  entryComponents: [
    ForgotPasswordDialogComponent,
    RegistrationDialogComponent,
    DetailsDialogBoxComponent,
    AdminRegistrationFullProfileDialogboxComponent,
    WorkersDialogComponent,
    CompaniesDialogComponent,
    PendingDetailDialogComponent,
    EmployerDetailsDialogComponent,
    ApproveShiftDialogComponent,
    ApproveDialogComponent,
    AmendDialogComponent,
    NewshiftAddDialogComponent,
    NewshiftDoneDialogComponent,
    DeleteShiftDialogComponent,
    SuccessShiftDeletedDialogComponent,
    ViewDetailsDialogComponent,
    AdminRegistrationDeleteDialogboxComponent,
    AdminRegistrationApproveDialogboxComponent,
    AdminRegistrationRejectDialogboxComponent,
    ViewWorkerDetailsComponent,
    ViewAdvanceBookingDetailsDialogComponent,
    EmployerWorkersDialogComponent,
    MyshiftEditDialogComponent,
    MybookingDetailsDialogComponent,
    CancelBookingDialogComponent,
    SuccessBookingCancelDialogComponent,
    ApproveRejectDialogComponent,
    SuccessOkDialogComponent,
    CompletedPendingDisputedViewdetailsDialogComponent,
    PostedBookingsCancelledOngoingApprovedDisputedDetailsDialogComponent,
    LocationLookupDialogComponent,
    EmployerDateAdvanceBookingDialogComponent,
    NoticeDetailsDialogComponent
  ],
  providers: [
    {
      provide: DateAdapter, useClass: UACustomDataAdapter
    },
    {
      provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    },
    {
      // use british locale
      // date display format will be dd/mm/yyyy
      provide: OWL_DATE_TIME_LOCALE, useValue: 'en-UK'
    },
    EmployerGuard, AdminGuard, AuthService, UploadService,
    GoogleMapsAPIWrapper, MessagingService, AsyncPipe, AlertService, SearchBoxService],
  bootstrap: [AppComponent]
})
export class AppModule { }
