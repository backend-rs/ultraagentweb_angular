import { Injectable, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { map } from 'rxjs/operators';

import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable, Subscription } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnInit {

  // persist observable
  private userAuthState: Observable<firebase.User>;
  public firebaseAuthUserData: any = undefined;
  public loggedInProfile: any = undefined;
  public companyProfile: any = undefined;

  private profileSubscription: Subscription = undefined;
  private companySubscription: Subscription = undefined;
  private authStateSubscription: Subscription = undefined;


  constructor(private _firebaseAuth: AngularFireAuth, private router: Router, private firestore: AngularFirestore){
    this.loggedInProfile = undefined;
    this.userAuthState = _firebaseAuth.authState;
    this.authStateSubscription = this.userAuthState.subscribe((auser) => {
      console.log('User auth state updated');
      console.log(auser);
      if (this.profileSubscription != undefined) {
        this.profileSubscription.unsubscribe();
        this.profileSubscription = undefined;
      }

      this.firebaseAuthUserData = auser;
      if((auser != undefined) && (auser != null) && (auser.uid != undefined ) && (auser.uid != null )){
        //console.log('from constructor');
        //console.log(auser);
        
        const aCollection = this.firestore.collection<any>('profiles');
        this.profileSubscription = aCollection.doc(auser.uid).snapshotChanges().pipe(
          map(action => {
            const data = action.payload.data();
            const id = action.payload.id;
            return { id, ...data };
          })
        ).subscribe(aprofile => {
          //console.log('AuthService: Updated profile:');
          
          this.loggedInProfile = aprofile;
          //console.log(this.loggedInProfile);
          this.fetcheLoggedInUserAssociatedCompanyIfAny();
        });
      } else {
        //console.log('AuthService: Profile reset to undefined');
        this.loggedInProfile = undefined;
      }
    });
  }

  ngOnInit() {
  }

  ngOnDestroy(){
    //console.log('AuthService: ngOnDestroy CALLED. SHUTTING DOWN!!');
    // this._firebaseAuth.authState.subscribe().unsubscribe();
    this.authStateSubscription.unsubscribe();
  }
 
  fetcheLoggedInUserAssociatedCompanyIfAny() {
    if (this.companySubscription != undefined) {
      this.companySubscription.unsubscribe();
      this.companySubscription = undefined;
    }
    
    if ((this.loggedInProfile != undefined) && (this.loggedInProfile.companyId != undefined) && 
        (this.loggedInProfile.companyId.length > 0)) {
     
      const aCollection = this.firestore.collection<any>('companies');
      this.companySubscription = aCollection.doc(this.loggedInProfile.companyId).snapshotChanges().pipe(
        map(action => {
          const data = action.payload.data();
          const id = action.payload.id;
          return { id, ...data };
        })
      ).subscribe(acompany => {
        //console.log('AuthService: Updated profile:');
        
        this.companyProfile = acompany;
        //console.log('associated company found')
        //console.log(this.companyProfile);
        //console.log(this.loggedInProfile);
      });
    }
  }
  // For User Login
  doLogin(email, password){
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(email, password)
      .then(res => {
        //console.log('doLogin');
        //console.log(res);
        resolve(res.user);
      }, err => reject(err))
    })
  }

  // For User Logout
  doLogout(){
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signOut()
      .then(res => {
       // console.log(res);
        // return undefined
        resolve(res);
        this.loggedInProfile = undefined;
        this.companyProfile = undefined;
      }, err => reject(err))
    })
  }

  // For User Register
  doRegister(email, password){
    return new Promise<any>((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(res => {
        //console.log(res.user.uid);
        resolve(res);
      }, err => reject(err))
    })
  }

  doAcceptWorkerForBookingRequest(bookingRequestId, shiftId, workerId, companyId) {
    return new Promise<any>((resolve, reject) => {
      if ((shiftId != undefined) && (shiftId.length > 0) && 
      (workerId != undefined) && (workerId.length > 0) &&
      (companyId != undefined) && (companyId.length > 0) &&
      (bookingRequestId != undefined) && (bookingRequestId.length > 0)) {
        var refCompany:any;
        var refShift:any;
        var refWorker:any;
        const companySubscripton = this.firestore.collection('companies').doc(companyId).snapshotChanges().pipe(
          map(action => {
            const data = action.payload.data();
            const id = action.payload.id;
            return { id, ...data };
          })
        ).subscribe(acompany => {
          refCompany = acompany;
          companySubscripton.unsubscribe();
          
          const workerSubscripton = this.firestore.collection('profiles').doc(workerId).snapshotChanges().pipe(
            map(action => {
              const data = action.payload.data();
              const id = action.payload.id;
              return { id, ...data };
            })
          ).subscribe(aworker => {
            refWorker = aworker;
            workerSubscripton.unsubscribe();

            const shiftSubscripton = this.firestore.collection('shifts').doc(shiftId).snapshotChanges().pipe(
              map(action => {
                const data = action.payload.data();
                const id = action.payload.id;
                return { id, ...data };
              })
            ).subscribe(ashift => {
              refShift = ashift;
              shiftSubscripton.unsubscribe();

              var refShiftStartTimeInMillis = refShift.startDateTime.toDate().getTime();
              var hhmmArr = refShift.duration.split(':');
              var refShiftEndTimeInMillis = refShiftStartTimeInMillis+
                (hhmmArr[0]*60*60 + hhmmArr[1]*60)*1000;
              
              // Fetch existing bookings of the worker to validate time overlap
              const aValiColl = this.firestore.collection('bookings',ref=> 
                ref.where('workerId','==',workerId)
              )
              const validateSubscription = aValiColl.snapshotChanges().pipe(
                map(actions => actions.map(action => {
                  const data = action.payload.doc.data();
                  const id = action.payload.doc.id;
                  return { id, ...data };
                }))
              ).subscribe(someList => {
                // Check if any existing booking conflicts with this shift
                var aList: any;
                aList = someList;
                for (const aBooking of aList) {

                  if (aBooking.shiftId == shiftId) { continue; }

                  var refBookStartTimeInMillis = aBooking.shiftStartDateTime.toDate().getTime();
                  var hhmmArr = aBooking.duration.split(':');
                  var refBookEndTimeInMillis = refBookStartTimeInMillis+
                    (hhmmArr[0]*60*60 + hhmmArr[1]*60)*1000;

                  if (((refShiftStartTimeInMillis >= refBookStartTimeInMillis) && 
                      (refShiftStartTimeInMillis < refBookEndTimeInMillis)) || 
                      ((refShiftEndTimeInMillis > refBookStartTimeInMillis) && 
                      (refShiftEndTimeInMillis <= refBookEndTimeInMillis)))
                      {
                        // reject({message:
                        //   'Worker is already booked for '+
                        //   aBooking.shiftSubCategory+'#'+aBooking.shiftNo+' during same working times.\n'+
                        // 'Hence, can not be booked for this shift.'});

                        // 22-05-2019:Albert
                        //update this error message - just write - `Worker not available for this shift`

                         reject({message:
                          'Worker not available for this shift.'});
                        return;
                  }
                }

                 // Now Approve booking request
                this.firestore.collection('shifts').doc(shiftId).set(
                  {recruitmentStatus:'recruited'},
                  {merge:true}
                )
                .then((aObj) => {
                  this.firestore.collection('bookingrequests').doc(bookingRequestId).set(
                    {approvalStatus:'approved'},
                    {merge:true}
                  )
                  .then((aObj) => {
                    // Create new booking object
                    this.firestore.collection('bookings').doc(bookingRequestId).set(
                      {
                        status:'active',
                        createdBy:this.loggedInProfile.userId,
          
                        shiftId: refShift.id,
                        shiftCategory:refShift.category,
                        shiftSubCategory:refShift.subcategory,
                        shiftNo:refShift.shiftNo,
                        shiftStartDateTime: refShift.startDateTime,
                        fee:refShift.fee,
                        description:refShift.description,
                        city:refShift.city,
                        lat:refShift.lat,
                        lng:refShift.lng,
                        duration:refShift.duration,
                        address:refShift.address,
                        contactpername:refShift.contactpername,
                        contactphonenumber: refShift.phoneNumber,
          
                        workerFirstName:refWorker.firstName,
                        workerLastName:refWorker.lastName,
                        workerPhoneNumber:refWorker.phoneNumber,
                        workerProfilePicture:refWorker.profilePicture,
                        workerId: refWorker.userId,
          
                        companyProfilePicture:refCompany.profilePicture,
                        companyName:refCompany.name,
                        companyId:companyId,
          
                        createdAt: firebase.firestore.FieldValue.serverTimestamp(),
                      },
                      {merge:true}
                    )
                    .then((aObj) => {
                      resolve(aObj);
                    })
                    .catch((err) => {
                      reject(err);
                    });
                    
                  })
                  .catch((err) => {
                    reject(err);
                  });
                })
                .catch((err) => {
                  reject(err);
                });

              });
             
            });

          });

        });
      } else {
        reject(undefined);
      }
    });
  }

  doDeclineWorkerForBookingRequest(bookingRequestId) {
    return new Promise<any>((resolve, reject) => {
      if ((bookingRequestId != undefined) && (bookingRequestId.length > 0)) {

        this.firestore.collection('bookingrequests').doc(bookingRequestId).set(
          {approvalStatus:'rejected'},
          {merge:true}
        )
        .then((aObj) => {
          resolve(aObj);
          
        })
        .catch((err) => {
          reject(err);
        });

      } else {
        reject(undefined);
      }
    });
  }

  doCancelBookedWorker(bookingRequestId, cancelReasone, brief) {
    return new Promise<any>((resolve, reject) => {
      this.firestore.collection('bookingrequests').doc(bookingRequestId).set(
        {approvalStatus:'pending'},
        {merge:true}
      )
      .then((aObj) => {
        // Create new booking object
        this.firestore.collection('bookings').doc(bookingRequestId).set(
          {status:'cancelled',
          cancelledBy:this.loggedInProfile.userId,
          cancelReason:cancelReasone,
          cancelBrief:brief,
          cancelledAt:firebase.firestore.FieldValue.serverTimestamp()
        },
          {merge:true}
        )
        .then((aObj) => {
          resolve(aObj);
        })
        .catch((err) => {
          reject(err);
        });
      })
      .catch((err) => {
        reject(err);
      });
    });
  }


  doApproveShift(bookingId, approvedMins, approvedRate) {
    return new Promise<any>((resolve, reject) => {
      this.firestore.collection("bookings").doc(bookingId).set({
        timesheetStatus: 'approved',
        approvedMins: approvedMins,
        approvedRate: approvedRate
      }, {merge: true})
      .then(_ => {
        resolve();
      })
      .catch((err) => {
        reject(err);
      });
    });
  }
}
