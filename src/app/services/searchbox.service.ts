import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
 
@Injectable({ providedIn: 'root' })
export class SearchBoxService {
    private subject = new Subject<any>();
 
    sendMessage(searchword: string) {
        this.subject.next({ searchword: searchword });
    }
 
    clearMessage() {
        this.subject.next({ searchword: '' });
    }
 
    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}
