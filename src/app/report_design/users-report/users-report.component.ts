import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Location } from '@angular/common';
import {MatDialog} from '@angular/material';

import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { HttpClient, HttpParams } from '@angular/common/http';

import {UAGlobals} from '../../globals/uaglobals';
import {UAHelpers} from '../../globals/uahelpers';
import * as moment from 'moment';

@Component({
  selector: 'app-users-report',
  templateUrl: './users-report.component.html',
  styleUrls: ['./users-report.component.css']
})
export class UsersReportComponent implements OnInit {

  categories=[];

  sub:any;
  startStr='';
  endStr='';
  type='';

  generatedAtStr='';
  displayStartStr='';
  displayEndStr='';

  usersSubscription:Subscription;
  companiesSubscription:Subscription;
  bookingsSubscription:Subscription;

  workersCount = 0;
  companiesCount = 0;

  workersByCat = {};
  companiesByCat = {};

  constructor(
    private router: Router, 
    public dialog: MatDialog, private location: Location, 
    private authService: AuthService, 
    private afAuth: AngularFireAuth, 
    private firestore: AngularFirestore,
    private route: ActivatedRoute,
    private httpClient:HttpClient
  ) { 
    this.categories = (Object.keys(UAGlobals.MASTER_CATEGORIES)).sort();
    for (const aCat of this.categories) {
      this.workersByCat[aCat] = [];
      this.companiesByCat[aCat] = [];
    }

  }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.type = params['type']; 
      this.startStr = params['start']; 
      this.endStr = params['end']; 

      this.reloadData();

   });

  }

  ngOnDestroy() {

    this.sub.unsubscribe();

    if (this.usersSubscription != undefined) {
      this.usersSubscription.unsubscribe();
      this.usersSubscription = undefined;
    }
    if (this.companiesSubscription != undefined) {
      this.companiesSubscription.unsubscribe();
      this.companiesSubscription = undefined;
    }
    if (this.bookingsSubscription != undefined) {
      this.bookingsSubscription.unsubscribe();
      this.bookingsSubscription = undefined;
    }

  }

  reloadData() {

    var startDate = moment(this.startStr).toDate();
    startDate.setHours(0,0,0,0);
    var beginningOfStartDay = startDate.getTime();
    //console.log(this.startStr);
    //console.log(startDate);
    //console.log(beginningOfStartDay);

    var endDate = moment(this.endStr).toDate();
    endDate.setHours(23,59,59,999);
    var endOfEndDay = endDate.getTime();
    //console.log(this.endStr);
    //console.log(endDate);
    //console.log(endOfEndDay);

    var aCollection = this.firestore.collection('profiles', ref => 
      ref.where('approvalStatus','==','approved')
      .where('profileType', '==', 'worker')
    );
  
    this.usersSubscription = aCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(aList => {
      var workers = [];

      workers = aList;
      // for (const aWorker of aList) {
      //   var timeStamp = aWorker['createdAt'].toDate().getTime();
      //   if ((timeStamp >= beginningOfStartDay) && (timeStamp <= endOfEndDay)) {
      //     workers.push(aWorker);
      //   }
      // }

      if (this.usersSubscription != undefined) {
        this.usersSubscription.unsubscribe();
        this.usersSubscription = undefined;
      }

      var bCollection = this.firestore.collection('companies', ref => 
        ref.where('approvalStatus','==','approved')
      );
    
      this.companiesSubscription = bCollection.snapshotChanges().pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        }))
      ).subscribe(bList => {

        var companies = [];

        companies = bList;
        // for (const aCompany of bList) {
        //   var timeStamp = aCompany['createdAt'].toDate().getTime();
        //   if ((timeStamp >= beginningOfStartDay) && (timeStamp <= endOfEndDay)) {
        //     companies.push(aCompany);
        //   }
        // }

        if (this.companiesSubscription != undefined) {
          this.companiesSubscription.unsubscribe();
          this.companiesSubscription = undefined;
        }

        var cCollection = this.firestore.collection('bookings', ref => 
          ref.where('paymentStatus','==','paid')
        );
      
        this.bookingsSubscription = cCollection.snapshotChanges().pipe(
          map(actions => actions.map(a => {
            const data = a.payload.doc.data();
            const id = a.payload.doc.id;
            return { id, ...data };
          }))
        ).subscribe(cList => {

          var bookings = [];

          for (const aBooking of cList) {
            var timeStamp = aBooking['paidAt'].toDate().getTime();
            if ((timeStamp >= beginningOfStartDay) && (timeStamp <= endOfEndDay)) {
              bookings.push(aBooking);
            }
          }

          if (this.bookingsSubscription != undefined) {
            this.bookingsSubscription.unsubscribe();
            this.bookingsSubscription = undefined;
          }
          
          this.formatDataForDisplay(workers, companies, bookings);

          this.generatedAtStr = (moment(new Date()).format('DD/MM/YYYY hh:mm a')).toUpperCase();
          this.displayStartStr = (moment(this.startStr).format('DD/MM/YYYY'));
          this.displayEndStr = (moment(this.endStr).format('DD/MM/YYYY'));

        });

       

      });

    });

  }

  formatDataForDisplay(workers, companies, bookings) {

    var workerByCategoriesMap = {};
    var companiesByCategoriesMap = {};

    for (const aCat of this.categories) {
      workerByCategoriesMap[aCat] = [];
      companiesByCategoriesMap[aCat] = [];
    }




    if ((workers != undefined) && (workers.length > 0)) {

      for (const aWorker of workers) {

        // associate earnings with workers
        var minsWorked = 0;
        var shiftsWorked = 0;
        var earnings = 0;
        for (const aBooking of bookings) {
          if (aBooking.workerId == aWorker.userId) {
            shiftsWorked += 1;
            minsWorked += parseInt(aBooking.workedMins+"");
            earnings += parseFloat(aBooking.calculatedWorkerFee+"");
          }
        }
        aWorker.shiftsWorked = shiftsWorked;
        var hours = Math.floor(minsWorked/60);
        var mins = minsWorked%60;
        var displaySt = '';
        if (hours > 0) {
          displaySt += hours; displaySt += 'h ';
        }
        if (mins > 0) {
          displaySt += mins; displaySt += 'm';
        }
        aWorker.hoursWorked = displaySt;
        aWorker.earnings = earnings.toFixed(2);

        // group workers by categories
        const category = aWorker.wProfession;
        var arra = workerByCategoriesMap[category];
        if (arra == undefined) {
          arra = [];
        }
        arra.push(aWorker);
        workerByCategoriesMap[category] = arra;
      }
    } 

    if ((companies != undefined) && (companies.length > 0)) {
      for (const aCompany of companies) {

        // associate earnings with workers
        var minsWorked = 0;
        var shiftsWorked = 0;
        var spent = 0;
        for (const aBooking of bookings) {
          if (aBooking.companyId == aCompany.id) {
            shiftsWorked += 1;
            minsWorked += parseInt(aBooking.workedMins+"");
            spent += parseFloat(aBooking.calculatedEmployerFee+"");
          }
        }
        aCompany.shiftsWorked = shiftsWorked;
        var hours = Math.floor(minsWorked/60);
        var mins = minsWorked%60;
        var displaySt = '';
        if (hours > 0) {
          displaySt += hours; displaySt += 'h ';
        }
        if (mins > 0) {
          displaySt += mins; displaySt += 'm';
        }
        aCompany.hoursWorked = displaySt;
        aCompany.spent = spent.toFixed(2);


        const category = aCompany.industry;
        var arra = companiesByCategoriesMap[category];
        if (arra == undefined) {
          arra = [];
        }
        arra.push(aCompany);
        companiesByCategoriesMap[category] = arra;
      }
    } 


    //console.log(workerByCategoriesMap);
    //console.log(companiesByCategoriesMap);   
    
    this.workersByCat = workerByCategoriesMap;
    this.companiesByCat = companiesByCategoriesMap;

    var count = 0;
    var catArr = Object.keys(this.workersByCat);
    for (const aCat of catArr) {
      const dataArr = this.workersByCat[aCat];
      if (dataArr != undefined) {
        count += dataArr.length;
      }
    }
    this.workersCount = count;


    count = 0;
    catArr = Object.keys(this.companiesByCat);
    for (const aCat of catArr) {
      const dataArr = this.companiesByCat[aCat];
      if (dataArr != undefined) {
        count += dataArr.length;
      }
    }
    this.companiesCount = count;
  }

}
