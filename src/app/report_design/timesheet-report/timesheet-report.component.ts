import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Location } from '@angular/common';
import {MatDialog} from '@angular/material';

import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { HttpClient, HttpParams } from '@angular/common/http';

import {UAGlobals} from '../../globals/uaglobals';
import {UAHelpers} from '../../globals/uahelpers';
import * as moment from 'moment';

@Component({
  selector: 'app-timesheet-report',
  templateUrl: './timesheet-report.component.html',
  styleUrls: ['./timesheet-report.component.css']
})

export class TimesheetReportComponent implements OnInit {

  categories=[];

  sub:any;
  startStr='';
  endStr='';
  type='';

  generatedAtStr='';
  displayStartStr='';
  displayEndStr='';

  bookingsSubscription:Subscription;

  pendingBookings = [];
  paidBookings = [];

  constructor(
    private router: Router, 
    public dialog: MatDialog, private location: Location, 
    private authService: AuthService, 
    private afAuth: AngularFireAuth, 
    private firestore: AngularFirestore,
    private route: ActivatedRoute,
    private httpClient:HttpClient
  ) { 

  }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.type = params['type']; 
      this.startStr = params['start']; 
      this.endStr = params['end']; 

      this.reloadData();

   });

  }

  ngOnDestroy() {

    this.sub.unsubscribe();

    if (this.bookingsSubscription != undefined) {
      this.bookingsSubscription.unsubscribe();
      this.bookingsSubscription = undefined;
    }

  }

  reloadData() {

    var startDate = moment(this.startStr).toDate();
    startDate.setHours(0,0,0,0);
    var beginningOfStartDay = startDate.getTime();
   
    var endDate = moment(this.endStr).toDate();
    endDate.setHours(23,59,59,999);
    var endOfEndDay = endDate.getTime();
    
  
    
    var cCollection = this.firestore.collection('bookings');
  
    this.bookingsSubscription = cCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(cList => {

      var bookings = [];

      for (const aBooking of cList) {
        if (aBooking['workEndTime'] == undefined) { continue; }
        var timeStamp = aBooking['workEndTime'].toDate().getTime();
        if ((timeStamp >= beginningOfStartDay) && (timeStamp <= endOfEndDay)) {
          bookings.push(aBooking);
        }
      }

      if (this.bookingsSubscription != undefined) {
        this.bookingsSubscription.unsubscribe();
        this.bookingsSubscription = undefined;
      }
      
      this.formatDataForDisplay(bookings);

      this.generatedAtStr = (moment(new Date()).format('DD/MM/YYYY hh:mm a')).toUpperCase();
      this.displayStartStr = (moment(this.startStr).format('DD/MM/YYYY'));
      this.displayEndStr = (moment(this.endStr).format('DD/MM/YYYY'));

    });

       

    
  }

  formatDataForDisplay(bookings) {

    var pending = [];
    var paid = [];

    for (const aBooking of bookings) {
      if (((aBooking.timesheetStatus == 'approved') || 
            (aBooking.timesheetStatus == 'amendresolved') || 
            (aBooking.timesheetStatus == 'disputeresolved')) && 
          ((aBooking.paymentStatus == undefined) || (aBooking.paymentStatus == 'pending'))
          )
      {
        pending.push(aBooking);
      } else if ((aBooking.paymentStatus != undefined) && (aBooking.paymentStatus == 'paid'))
      {
        paid.push(aBooking);
      }
    }

    this.pendingBookings = pending;
    this.paidBookings = paid;

   
  }

}
