import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerTimesheetReportComponent } from './employer-timesheet-report.component';

describe('EmployerTimesheetReportComponent', () => {
  let component: EmployerTimesheetReportComponent;
  let fixture: ComponentFixture<EmployerTimesheetReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerTimesheetReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerTimesheetReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
