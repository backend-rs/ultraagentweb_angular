import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerPostedshiftComponent } from './employer-postedshift.component';

describe('EmployerPostedshiftComponent', () => {
  let component: EmployerPostedshiftComponent;
  let fixture: ComponentFixture<EmployerPostedshiftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerPostedshiftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerPostedshiftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
