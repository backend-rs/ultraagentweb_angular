import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerBookingsComponent } from './employer-bookings.component';

describe('EmployerBookingsComponent', () => {
  let component: EmployerBookingsComponent;
  let fixture: ComponentFixture<EmployerBookingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerBookingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerBookingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
