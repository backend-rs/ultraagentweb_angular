import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Location } from '@angular/common';
import {MatDialog} from '@angular/material';

import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { HttpClient, HttpParams } from '@angular/common/http';

import {UAGlobals} from '../../globals/uaglobals';
import {UAHelpers} from '../../globals/uahelpers';
import * as moment from 'moment';

@Component({
  selector: 'app-employer-bookings',
  templateUrl: './employer-bookings.component.html',
  styleUrls: ['./employer-bookings.component.css']
})


export class EmployerBookingsComponent implements OnInit {

  categories=[];

  sub:any;
  startStr='';
  endStr='';
  type='';
  companyId='';

  generatedAtStr='';
  displayStartStr='';
  displayEndStr='';

  companyName = '';

  bookingsSubscription:Subscription;

  totalBookings = 0;

  mybookings = [];
  cancelledbookings = [];

  constructor(
    private router: Router, 
    public dialog: MatDialog, private location: Location, 
    private authService: AuthService, 
    private afAuth: AngularFireAuth, 
    private firestore: AngularFirestore,
    private route: ActivatedRoute,
    private httpClient:HttpClient
  ) { 

  }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.type = params['type']; 
      this.startStr = params['start']; 
      this.endStr = params['end']; 
      this.companyId = params['companyId']; 

      this.reloadData();

   });

  }

  ngOnDestroy() {

    this.sub.unsubscribe();

    if (this.bookingsSubscription != undefined) {
      this.bookingsSubscription.unsubscribe();
      this.bookingsSubscription = undefined;
    }

  }

  reloadData() {

    var startDate = moment(this.startStr).toDate();
    startDate.setHours(0,0,0,0);
    var beginningOfStartDay = startDate.getTime();
   
    var endDate = moment(this.endStr).toDate();
    endDate.setHours(23,59,59,999);
    var endOfEndDay = endDate.getTime();
    
  
    var bCollection = this.firestore.collection('bookings', ref => 
      ref.where('companyId','==',this.companyId) 
    );
  
    this.bookingsSubscription = bCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(bList => {
      var bs = [];
      bs = bList;

      var books = [];
      var cancs = [];
      var total = bs.length;
      for (const aBooking of bs) {
        var timeStamp = aBooking['createdAt'].toDate().getTime();
        if ((timeStamp >= beginningOfStartDay) && (timeStamp <= endOfEndDay)) {

          if ((aBooking.status != 'cancelled') && 
              ((aBooking.workingStatus == undefined) || (aBooking.workingStatus != 'completed'))){
            books.push(aBooking);
          } else if (aBooking.status == 'cancelled') {  
            cancs.push(aBooking);
          } 
         
        }
      }
      
      if (this.bookingsSubscription != undefined) {
        this.bookingsSubscription.unsubscribe();
        this.bookingsSubscription = undefined;
      }
    

      this.mybookings = books;
      this.cancelledbookings = cancs;
      this.totalBookings = total;

      this.generatedAtStr = (moment(new Date()).format('DD/MM/YYYY hh:mm a')).toUpperCase();
      this.displayStartStr = (moment(this.startStr).format('DD/MM/YYYY'));
      this.displayEndStr = (moment(this.endStr).format('DD/MM/YYYY'));


    });

    this.firestore.collection('companies').doc(this.companyId).ref.get()
    .then((adoc) => {
      if (!adoc.exists) { 
        return; 
      }         
      var aprofile = adoc.data();
      //console.log(aprofile);
      this.companyName = aprofile.name;
      
    })
    .catch((err) => {
      console.log(err);
    });

    
  }



}

