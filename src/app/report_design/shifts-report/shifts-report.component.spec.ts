import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftsReportComponent } from './shifts-report.component';

describe('ShiftsReportComponent', () => {
  let component: ShiftsReportComponent;
  let fixture: ComponentFixture<ShiftsReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftsReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
