import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Location } from '@angular/common';
import {MatDialog} from '@angular/material';

import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { HttpClient, HttpParams } from '@angular/common/http';

import {UAGlobals} from '../../globals/uaglobals';
import {UAHelpers} from '../../globals/uahelpers';
import * as moment from 'moment';

@Component({
  selector: 'app-shifts-report',
  templateUrl: './shifts-report.component.html',
  styleUrls: ['./shifts-report.component.css']
})


export class ShiftsReportComponent implements OnInit {

  categories=[];

  sub:any;
  startStr='';
  endStr='';
  type='';

  generatedAtStr='';
  displayStartStr='';
  displayEndStr='';

  bookingsSubscription:Subscription;
  shiftsSubscription:Subscription;

  postedshifts = [];
  bookings = [];
  cancels = [];
  ongoings = [];
  approveds = [];
  opendisputes = [];

  constructor(
    private router: Router, 
    public dialog: MatDialog, private location: Location, 
    private authService: AuthService, 
    private afAuth: AngularFireAuth, 
    private firestore: AngularFirestore,
    private route: ActivatedRoute,
    private httpClient:HttpClient
  ) { 

  }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.type = params['type']; 
      this.startStr = params['start']; 
      this.endStr = params['end']; 

      this.reloadData();

   });

  }

  ngOnDestroy() {

    this.sub.unsubscribe();

    if (this.shiftsSubscription != undefined) {
      this.shiftsSubscription.unsubscribe();
      this.shiftsSubscription = undefined;
    }

    if (this.bookingsSubscription != undefined) {
      this.bookingsSubscription.unsubscribe();
      this.bookingsSubscription = undefined;
    }

  }

  reloadData() {

    var startDate = moment(this.startStr).toDate();
    startDate.setHours(0,0,0,0);
    var beginningOfStartDay = startDate.getTime();
   
    var endDate = moment(this.endStr).toDate();
    endDate.setHours(23,59,59,999);
    var endOfEndDay = endDate.getTime();
    
  
    var bCollection = this.firestore.collection('shifts', ref => 
      ref.where('startDateTime','>',startDate) 
      .where('startDateTime','<',endDate) 
    );
  
    this.shiftsSubscription = bCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).subscribe(bList => {
      var shifts = [];
      shifts = bList;
      
      if (this.shiftsSubscription != undefined) {
        this.shiftsSubscription.unsubscribe();
        this.shiftsSubscription = undefined;
      }
    
      var cCollection = this.firestore.collection('bookings', ref => 
        ref.where('shiftStartDateTime','>',startDate) 
        .where('shiftStartDateTime','<',endDate) 
      );
    
      this.bookingsSubscription = cCollection.snapshotChanges().pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        }))
      ).subscribe(cList => {

        var bookings = [];
        bookings = cList;
        
        if (this.bookingsSubscription != undefined) {
          this.bookingsSubscription.unsubscribe();
          this.bookingsSubscription = undefined;
        }
        
        this.formatDataForDisplay(shifts, bookings);

        this.generatedAtStr = (moment(new Date()).format('DD/MM/YYYY hh:mm a')).toUpperCase();
        this.displayStartStr = (moment(this.startStr).format('DD/MM/YYYY'));
        this.displayEndStr = (moment(this.endStr).format('DD/MM/YYYY'));

      });

    });

    
  }

  formatDataForDisplay(shifts, bookings) {

    // console.log('formatDataForDisplay');
    // console.log(shifts);
    // console.log(bookings);

    var posted = [];
    for (const aShift of shifts) {
      var found = false;
      for (const aBooking of bookings) {
        if (aBooking.id == aShift.id) {
          found = true;
          break;
        }
      }

      if(found == false) {
        posted.push(aShift);
      }
    }

    var books = [];
    var canc = [];
    var gois = [];
    var apprs = [];
    var opens = [];

    for (const aBooking of bookings) {

      if ((aBooking.status != undefined) && (aBooking.status == 'cancelled')){
        canc.push(aBooking);
      } 
      
      if ((aBooking.timesheetStatus != undefined) && (aBooking.timesheetStatus == 'disputed')) {
        opens.push(aBooking);
      }
      
      if ((aBooking.timesheetStatus != undefined) && (
        (aBooking.timesheetStatus == 'approved') || (aBooking.timesheetStatus == 'amendresolved')))
      {
        apprs.push(aBooking);
      }
      
      if ((aBooking.workingStatus != undefined) && (
        (aBooking.workingStatus == 'working') || (aBooking.workingStatus == 'paused')))
      {
        gois.push(aBooking);
      }
      
      if ((aBooking.status != undefined) && (aBooking.status == 'active')){
        books.push(aBooking);
      } 
      
    }

    this.postedshifts = posted;
    this.bookings = books;
    this.cancels = canc;
    this.ongoings = gois;
    this.approveds = apprs;
    this.opendisputes = opens;

   
  }

}

