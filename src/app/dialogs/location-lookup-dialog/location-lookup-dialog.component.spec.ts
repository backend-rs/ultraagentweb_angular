import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationLookupDialogComponent } from './location-lookup-dialog.component';

describe('LocationLookupDialogComponent', () => {
  let component: LocationLookupDialogComponent;
  let fixture: ComponentFixture<LocationLookupDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocationLookupDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationLookupDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
