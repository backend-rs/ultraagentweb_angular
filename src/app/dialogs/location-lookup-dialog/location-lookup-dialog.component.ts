import { Component, Input, ViewChild, NgZone, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MapsAPILoader, AgmMap } from '@agm/core';
import { GoogleMapsAPIWrapper } from '@agm/core/services';
import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { Router, ActivatedRoute } from '@angular/router';


declare var google: any;
 
interface Marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
 
interface Location {
  lat: number;
  lng: number;
  viewport?: Object;
  zoom: number;
  address_level_1?:string;
  address_level_2?: string;
  address_city?: string;
  address_country?: string;
  address_zip?: string;
  address_state?: string;
  marker?: Marker;
}

@Component({
  selector: 'app-location-lookup-dialog',
  templateUrl: './location-lookup-dialog.component.html',
  styleUrls: ['./location-lookup-dialog.component.css'],
  providers: [GoogleMapsAPIWrapper]
})

export class LocationLookupDialogComponent implements OnInit {

  geocoder:any;
  public location:Location;

  searchInput = "";

  @ViewChild(AgmMap) map: AgmMap;

  constructor(public dialog: MatDialog, public mapsApiLoader: MapsAPILoader,
    public zone: NgZone,
    private wrapper: GoogleMapsAPIWrapper,
    public dialogRef: MatDialogRef<LocationLookupDialogComponent>, 
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public data:any) {

      // Disable close on clicking outside,
      // only close button. So that parameters can be sent to parent.
      dialogRef.disableClose = true;

      console.log(data);
      const latV = data.lat;
      const lngV = data.lng;
      
      this.location = {
        lat: latV,
        lng: lngV,
        marker: {
          lat: latV,
          lng: lngV,
          draggable: true
        },
        zoom: 12
      };
      this.mapsApiLoader = mapsApiLoader;
      this.zone = zone;
      this.wrapper = wrapper;

      this.mapsApiLoader.load().then(() => {
        this.geocoder = new google.maps.Geocoder();
        this.findAddressByCoordinates();
      });
    
  }

  
  

  ngOnInit() {
    this.location.marker.draggable = true;
  }

  ngOnDestroy() {
  }

  updateOnMap() {
    let full_address:string = this.location.address_level_1 || ""
    if (this.location.address_level_2) full_address = full_address + " " + this.location.address_level_2
    if (this.location.address_state) full_address = full_address + " " + this.location.address_state
    if (this.location.address_country) full_address = full_address + " " + this.location.address_country
 
    this.findLocation(full_address);
  }

  findLocation(address) {
    if (!this.geocoder) this.geocoder = new google.maps.Geocoder()
    this.geocoder.geocode({
      'address': address
    }, (results, status) => {
      //console.log(results);
      if (status == google.maps.GeocoderStatus.OK) {
        for (var i = 0; i < results[0].address_components.length; i++) {
          let types = results[0].address_components[i].types
 
          if (types.indexOf('locality') != -1) {
            this.location.address_level_2 = results[0].address_components[i].long_name
          }
          if (types.indexOf('country') != -1) {
            this.location.address_country = results[0].address_components[i].long_name
          }
          if (types.indexOf('postal_code') != -1) {
            this.location.address_zip = results[0].address_components[i].long_name
          }
          if (types.indexOf('administrative_area_level_1') != -1) {
            this.location.address_state = results[0].address_components[i].long_name
          }
        }
 
        if (results[0].geometry.location) {
          this.location.lat = results[0].geometry.location.lat();
          this.location.lng = results[0].geometry.location.lng();
          this.location.marker.lat = results[0].geometry.location.lat();
          this.location.marker.lng = results[0].geometry.location.lng();
          this.location.marker.draggable = true;
          this.location.viewport = results[0].geometry.viewport;
        }
        
        this.map.triggerResize()
      } else {
        alert("Sorry, this search produced no results.");
      }
    })
  }

  markerDragEnd(m: any) {
    this.location.marker.lat = m.coords.lat;
    this.location.marker.lng = m.coords.lng;
    this.findAddressByCoordinates();
  }

  findAddressByCoordinates() {
    //console.log('findAddressByCoordinates: '+this.location.marker.lat+', '+this.location.marker.lng);
    this.geocoder.geocode({
      'location': {
        lat: this.location.marker.lat,
        lng: this.location.marker.lng
      }
    }, (results, status) => {
      this.decomposeAddressComponents(results);
    })
  }

  decomposeAddressComponents(addressArray) {
    if (addressArray.length == 0) return false;

    this.location.lat = this.location.marker.lat;
    this.location.lng = this.location.marker.lng;
    this.location.address_level_1 = "";
    this.location.address_level_2 = "";
    this.location.address_city = "";
    this.location.address_state = "";
    this.location.address_country = "";
    this.location.address_zip = "";

    let address = addressArray[0].address_components;
    
    //console.log(address);

    for(let element of address) {
      if (element.length == 0 && !element['types']) continue
 
      if (element['types'].indexOf('street_number') > -1) {
        this.location.address_level_1 = element['long_name'];
        continue;
      }
      if (element['types'].indexOf('route') > -1) {
        this.location.address_level_1 += ', ' + element['long_name'];
        continue;
      }
      if (element['types'].indexOf('locality') > -1) {
        this.location.address_level_2 = element['long_name'];
        this.location.address_city = element['long_name'];
        continue;
      }
      if (element['types'].indexOf('postal_town') > -1) {
        this.location.address_city = element['long_name'];
        continue;
      }
      if (element['types'].indexOf('administrative_area_level_1') > -1) {
        this.location.address_state = element['long_name'];
        continue;
      }
      if (element['types'].indexOf('country') > -1) {
        this.location.address_country = element['long_name'];
        continue;
      }
      if (element['types'].indexOf('postal_code') > -1) {
        this.location.address_zip = element['long_name'];
        continue;
      }
    }

    this.showAddressOnInputField();
  }

  showAddressOnInputField() {
    let full_address:string = ""
    if (this.location.address_level_1) full_address = full_address + " " + this.location.address_level_1
    if (this.location.address_level_2) full_address = full_address + " " + this.location.address_level_2
    if (this.location.address_state) full_address = full_address + " " + this.location.address_state
    if (this.location.address_country) full_address = full_address + " " + this.location.address_country
    
    console.log('Found address: '+full_address);
    this.searchInput = full_address;

    // this will force update searchInput on view
    this.zone.run(() => { // <== added
      this.searchInput = full_address;
    });
  }

  searchButtonClicked() {
    //console.log("searchButtonClicked: "+this.searchInput);
    this.findLocation(this.searchInput);
  }
  
  cancelClick(): void {
    const refData = {
      lat: this.location.lat,
      lng: this.location.lng,
      city: this.location.address_city,
      address: this.searchInput
    }
    this.dialogRef.close(refData);
  }

  openDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
    
  }


}
