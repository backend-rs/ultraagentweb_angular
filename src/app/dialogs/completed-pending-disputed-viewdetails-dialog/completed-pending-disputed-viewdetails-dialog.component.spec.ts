import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedPendingDisputedViewdetailsDialogComponent } from './completed-pending-disputed-viewdetails-dialog.component';

describe('CompletedPendingDisputedViewdetailsDialogComponent', () => {
  let component: CompletedPendingDisputedViewdetailsDialogComponent;
  let fixture: ComponentFixture<CompletedPendingDisputedViewdetailsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletedPendingDisputedViewdetailsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedPendingDisputedViewdetailsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
