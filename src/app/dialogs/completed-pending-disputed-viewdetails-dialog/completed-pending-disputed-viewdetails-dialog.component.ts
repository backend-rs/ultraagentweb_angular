import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { AmendDialogComponent } from '../../dialogs/amend-dialog/amend-dialog.component';
import { ApproveDialogComponent } from '../../dialogs/approve-dialog/approve-dialog.component';


import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';


import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { ApproveRejectDialogComponent } from '../../dialogs/approve-reject-dialog/approve-reject-dialog.component';


@Component({
  selector: 'app-completed-pending-disputed-viewdetails-dialog',
  templateUrl: './completed-pending-disputed-viewdetails-dialog.component.html',
  styleUrls: ['./completed-pending-disputed-viewdetails-dialog.component.css']
})
export class CompletedPendingDisputedViewdetailsDialogComponent implements OnInit {

  title:string = '';
  subtitle:string = '';
  approveTitle:string = '';
  amendTitle:string = '';
  rateworkerbtnTitle:string = '';
  hideProposedChangesSection:boolean = false;
  hideRateWorkerButton:boolean = false;
  hideAmendApproveButtons:boolean = false;
  status:string = '';

  booking:any;

  amend:any;


   constructor(public dialog: MatDialog,
    private authService: AuthService,
    private router: Router,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    private route: ActivatedRoute,
    public dialogRef: MatDialogRef<CompletedPendingDisputedViewdetailsDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data:string, 

    ) { 
    }

  ngOnInit() {
  }

  approveClick(): void {

    var dialogRef = this.dialog.open(ApproveRejectDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = 'Are you sure?';
    dialogRef.componentInstance.descr = 
    "Do you really want to approve this timesheet?";

    dialogRef.componentInstance.approveTitle = 'Yes, Approve';
    dialogRef.componentInstance.rejectTitle = 'No, Cancel';

    dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      if (result == dialogRef.componentInstance.approveTitle) {

        this.authService.doApproveShift(this.booking.id, this.booking.workedMins, this.booking.fee)
        .then((aObj) => {
          this.dialogRef.close(this.approveTitle);
          this.openDialog("SUCCESS","Shift completed.");
        })
        .catch((err) => {
          console.log(err);
          this.openDialog("Error",err.message);
        });

      }
    });


    // dialogRef = this.dialog.open(ApproveDialogComponent, {
    //   panelClass: 'my-panel',
    //   width: '600px',
    // });

    //  dialogRef.afterClosed().subscribe(result => {
    //   console.log(`The dialog was closed: ${result}`);
    // });
  }

  amendClick(): void {

    const dialogRef = this.dialog.open(AmendDialogComponent, {
      panelClass: 'my-panel',
      width: '800px',
    });

    dialogRef.componentInstance.booking = this.booking;
    dialogRef.componentInstance.amend = undefined;
    dialogRef.componentInstance.hidePreviousProposalSection = true;
    dialogRef.componentInstance.hideApprovedProposalSection = true;
    dialogRef.componentInstance.hideProposalSubmitSection = false;
    dialogRef.componentInstance.title = 'AMEND SHIFT';
    
     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      if (result == 'back') {
        // do not close this dialog
      } else {
        this.dialogRef.close(this.amendTitle);
      }
    });
  }


  rateWorkerClick(booking): void {
    this.dialogRef.close(this.rateworkerbtnTitle);
    this.router.navigate(['review_worker', {
    //   'workerFirstName':booking.workerFirstName,
    // 'workerLastName':booking.workerLastName,
    // 'workerProfilePictureUrl':booking.workerProfilePicture.url,
    'workerId':booking.workerId,
    'bookingId':booking.id,
  }]);
  }

  cancelClick(): void {
    this.dialogRef.close();
  }

  openDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
    
  }

}
