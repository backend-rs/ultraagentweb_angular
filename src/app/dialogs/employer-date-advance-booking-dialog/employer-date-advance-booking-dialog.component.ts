import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-employer-date-advance-booking-dialog',
  templateUrl: './employer-date-advance-booking-dialog.component.html',
  styleUrls: ['./employer-date-advance-booking-dialog.component.css']
})
export class EmployerDateAdvanceBookingDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<EmployerDateAdvanceBookingDialogComponent>, @Inject(MAT_DIALOG_DATA) public data:string) { }

  ngOnInit() {
  }

  cancelClick(): void {
    this.dialogRef.close();
  }
  
}
