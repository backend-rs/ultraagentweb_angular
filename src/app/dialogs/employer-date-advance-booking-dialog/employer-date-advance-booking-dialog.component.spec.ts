import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerDateAdvanceBookingDialogComponent } from './employer-date-advance-booking-dialog.component';

describe('EmployerDateAdvanceBookingDialogComponent', () => {
  let component: EmployerDateAdvanceBookingDialogComponent;
  let fixture: ComponentFixture<EmployerDateAdvanceBookingDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerDateAdvanceBookingDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerDateAdvanceBookingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
