import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
//import { CompletedPendingDisputedViewdetailsDialogComponent } from '../../dialogs/completed-pending-disputed-viewdetails-dialog/completed-pending-disputed-viewdetails-dialog.component';


import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';


import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { ApproveRejectDialogComponent } from '../../dialogs/approve-reject-dialog/approve-reject-dialog.component';

import * as firebase from 'firebase/app';

@Component({
  selector: 'app-amend-dialog',
  templateUrl: './amend-dialog.component.html',
  styleUrls: ['./amend-dialog.component.css']
})
export class AmendDialogComponent implements OnInit {

  amend:any;
  booking:any;
  title:string = '';
  proposalTitle:string = '';
  inputStartTime: Date;
  inputEndTime:Date;
  inputFee:any;
  inputExpl:any;
  hidePreviousProposalSection:boolean = true;
  hideApprovedProposalSection:boolean = true;
  hideProposalSubmitSection:boolean = true;

  constructor(public dialog: MatDialog,
    private authService: AuthService,
    private router: Router,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    private route: ActivatedRoute,
     public dialogRef: MatDialogRef<AmendDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data:string,
    ) { }

  ngOnInit() {
  }

  cancelClick(): void {
    this.dialogRef.close();
  }

  amendClick(): void {

    if ((this.inputFee == undefined) || (this.inputFee.length < 1)) {
      this.openDialog("Fee not specified","Please enter a valid fee.");
      return;
    }
    let feeMinMax = parseInt(this.inputFee);
    if(feeMinMax < 8 || feeMinMax > 999){
      this.openDialog("Invalid","Fee should be minimum £8/h to maximum £999/h");
      return;
    }
    if ((this.inputExpl == undefined) || (this.inputExpl.length < 1)) {
      this.openDialog("Explanation not specified","Please enter a valid explanation.");
      return;
    }
    if (this.inputStartTime == undefined) {
      this.openDialog("Start Time not specified","Please select a valid date/time.");
      return;
    }
    if (this.inputEndTime == undefined) {
      this.openDialog("End Time not specified","Please select a valid date/time.");
      return;
    }
    if (this.inputStartTime.getTime() >= this.inputEndTime.getTime()) {
      this.openDialog("Invalid times","Start time should be before end time");
      return;
    }

    var dialogRef = this.dialog.open(ApproveRejectDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = 'Are you sure?';

    if ((this.authService.loggedInProfile.profileType == 'adminsuper') ||
    (this.authService.loggedInProfile.profileType == 'adminuser')) {
      dialogRef.componentInstance.descr = 
        "Do you really want to submit these changes?\n\n"+
        "Your changes will be final and binding on worker and employer.\n";
        dialogRef.componentInstance.approveTitle = 'Yes, Submit';
    } else {
      dialogRef.componentInstance.descr = 
      "Do you really want to propose changes to this timesheet?";
      dialogRef.componentInstance.approveTitle = 'Yes, Amend';
    }

    dialogRef.componentInstance.rejectTitle = 'No, Cancel';

    dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      if (result == dialogRef.componentInstance.approveTitle) {

        if ((this.authService.loggedInProfile.profileType == 'adminsuper') ||
        (this.authService.loggedInProfile.profileType == 'adminuser')) {
          // admin user will set it to dispute resolved
          this.firestore.collection("bookings").doc(this.booking.id)
          .collection('amends').doc(this.amend.id).set({
            adminApprovedBy:this.authService.loggedInProfile.userId,
            approvalStatus:'approved',
            adminApprovedStartTime:this.inputStartTime,
            adminApprovedEndTime:this.inputEndTime,
            adminApprovedFee:this.inputFee,
            adminApprovedBrief:this.inputExpl,
            resolvedAt: firebase.firestore.FieldValue.serverTimestamp(),
          }, {merge: true})
          .then(_ => {

            let approvedMins = parseInt(((this.inputEndTime.getTime() - 
                this.inputStartTime.getTime())/(60*1000))+'');
            let approvedRate = this.inputFee;

            this.firestore.collection("bookings").doc(this.booking.id).set({
              timesheetStatus: 'disputeresolved',
              approvedMins: approvedMins,
              approvedRate: approvedRate
            }, {merge: true})
            .then(_ => {
              this.dialogRef.close('disputeresolved');
              this.openDialog("SUCCESS","Your changes have been submitted.");
            })
            .catch((err) => {
              console.log(err);
              this.openDialog("Error",err.message);
            });
          })
          .catch((err) => {
            console.log(err);
            this.openDialog("Error",err.message);
          });
        } else {
          // employer will set it to amended
          const amendId = (new Date()).getTime()+'';
          this.firestore.collection("bookings").doc(this.booking.id)
          .collection('amends').doc(amendId).set({
            proposedBy:this.authService.loggedInProfile.userId,
            companyId:this.authService.loggedInProfile.companyId,
            workerId:this.booking.workerId,
            approvalStatus:'pending',
            proposedStartTime:this.inputStartTime,
            proposedEndTime:this.inputEndTime,
            proposedFee:this.inputFee,
            brief:this.inputExpl,
            createdAt: firebase.firestore.FieldValue.serverTimestamp(),
          }, {merge: true})
          .then(_ => {
            this.firestore.collection("bookings").doc(this.booking.id).set({
              timesheetStatus: 'amended',
              activeAmendId:amendId,
            }, {merge: true})
            .then(_ => {
              this.dialogRef.close('amended');
              this.openDialog("SUCCESS","Your changes have been submitted.");
            })
            .catch((err) => {
              console.log(err);
              this.openDialog("Error",err.message);
            });
          })
          .catch((err) => {
            console.log(err);
            this.openDialog("Error",err.message);
          });
        }
      }
    });


    
  }

  amendBackClick(): void {
    this.dialogRef.close('back');

    // Commenting out to prevent WARINING 
//     WARNING in Circular dependency detected:
// src/app/dialogs/completed-pending-disputed-viewdetails-dialog/completed-pending-disputed-viewdetails-dialog.component.ts -> src/app/dialogs/amend-dialog/amend-dialog.component.ts -> src/app/dialogs/completed-pending-disputed-viewdetails-dialog/completed-pending-disputed-viewdetails-dialog.component.ts

  //   const dialogRef = this.dialog.open(CompletedPendingDisputedViewdetailsDialogComponent, {
  //   panelClass: 'my-panel',
  //      width: '800px',
  //   });

  //   dialogRef.componentInstance.title='pending shift';
  //   dialogRef.componentInstance.subtitle='SHIFT NO: EQUIPMENT OPERATOR 12345';
  //   dialogRef.componentInstance.approveTitle = 'approve';
  //   dialogRef.componentInstance.amendTitle = 'amend';
  //   dialogRef.componentInstance.rateworkerbtnTitle = 'rate worker';
    
  //  dialogRef.afterClosed().subscribe(result => {
  //     console.log(`The dialog was closed: ${result}`);
  //   });
  }

  openDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
    
  }

}
