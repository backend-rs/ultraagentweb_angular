import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmendDialogComponent } from './amend-dialog.component';

describe('AmendDialogComponent', () => {
  let component: AmendDialogComponent;
  let fixture: ComponentFixture<AmendDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmendDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmendDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
