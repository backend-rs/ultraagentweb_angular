import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRegistrationApproveDialogboxComponent } from './admin-registration-approve-dialogbox.component';

describe('ApproveDialogboxComponent', () => {
  let component: AdminRegistrationApproveDialogboxComponent;
  let fixture: ComponentFixture<AdminRegistrationApproveDialogboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRegistrationApproveDialogboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRegistrationApproveDialogboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
