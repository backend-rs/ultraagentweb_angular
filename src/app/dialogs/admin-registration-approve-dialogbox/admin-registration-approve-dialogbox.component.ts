import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-admin-registration-approve-dialogbox',
  templateUrl: './admin-registration-approve-dialogbox.component.html',
  styleUrls: ['./admin-registration-approve-dialogbox.component.css']
})
export class AdminRegistrationApproveDialogboxComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<AdminRegistrationApproveDialogboxComponent>, @Inject(MAT_DIALOG_DATA) public data:string) { }

  ngOnInit() {
  }

  cancelClick(): void {
    this.dialogRef.close();
  }

}
