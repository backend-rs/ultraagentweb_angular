import { Component, OnInit, ElementRef, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import * as jspdf from 'jspdf';  
import html2canvas from 'html2canvas';  

@Component({
  selector: 'app-admin-registration-full-profile-dialogbox',
  templateUrl: './admin-registration-full-profile-dialogbox.component.html',
  styleUrls: ['./admin-registration-full-profile-dialogbox.component.css']
})
export class AdminRegistrationFullProfileDialogboxComponent implements OnInit{

  title:string = '';
  buttonTitle:string = '';
  buttonTitle2:string = '';
  profile:any = undefined;
  company: any = undefined;
  constructor(public dialogRef: MatDialogRef<AdminRegistrationFullProfileDialogboxComponent>, @Inject(MAT_DIALOG_DATA) public data:string) { }

  ngOnInit() {
  }

  cancelClick(): void {
    this.dialogRef.close();
  }

  onButtonTitleClicked(aTitle) {
    this.dialogRef.close(aTitle);
  }

  generatePDF() {

    var data = document.getElementById('printableContainerId'); 

    var pdfName = "MYPdf.pdf";
    if (this.profile != undefined) {
      // profile exists
      pdfName = this.profile.firstName.replace(/\s/g,'')+"-"+this.profile.lastName.replace(/\s/g,'')+".pdf";
    } else if (this.company != undefined) {
      // company exists
      pdfName = this.company.name.replace(/\s/g,'')+".pdf";
    }

    html2canvas(data).then(canvas => {  
      // Few necessary setting options  
      var imgWidth = 208;   
      var pageHeight = 295;    
      var imgHeight = canvas.height * imgWidth / canvas.width;  
      var heightLeft = imgHeight;  
  
      const contentDataURL = canvas.toDataURL('image/png')  
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;  
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
      pdf.save(pdfName); // Generated PDF   
    });  

  }

  generatePrint() {

    var title = "";
    if (this.profile != undefined) {
      // profile exists
      title = this.profile.firstName+"-"+this.profile.lastName;
    } else if (this.company != undefined) {
      // company exists
      title = this.company.name;
    }

    var innerContents = document.getElementById("printableContainerId").innerHTML;
    var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
    popupWinindow.document.open();
    popupWinindow.document.write(`
      <html>
        <head>
          <title>${title}</title>
          <style>
          .example-spacer {
            flex: 1 1 auto;
        }
        
        .dialog-toolbar{
            background-color: #c3ccf7;
            padding: 0px 30px;
        }
        .dialog-toolbar-heading h3{
            font-family: Montserrat;
            font-size: 25px;
            font-weight: 600;
            margin-bottom: 0;
        }
        .personal-detail-heading h5{
            font-family: Montserrat;
            font-size: 23px;
            font-weight: 400;
            margin-bottom: 0;
            padding: 10px 0;
        }
        .document-heading h5{
            font-family: Montserrat;
            font-size: 23px;
            font-weight: 400;
            margin-bottom: 0;
            padding: 10px 0;
        }
        .personal-detail-sec{
            border-bottom: 1px solid #e2e2e2;
        }
        .document-sec{
            border-bottom: 1px solid #e2e2e2;
        }
        .personal-detail-content{
            padding: 5px 0px;
            margin: 0 70px;
        }
        .personal-detail-content p{
            font-size: 16px;
            font-weight: 600;
            color: #000;
        }
        
        .personal-detail-content span{
            font-size: 14px;
            font-weight: 400;
            color: rgb(92, 92, 92);
            padding-left: 10px;
        }
        
        .mat-dialog-content{
            margin: 0;
            padding: 0;
        }
        
        .mat-toolbar-row{
            height: 75px;
        }
        .clear-icon{
            cursor: pointer;
            font-size: 25px;
            font-weight: 700;
        }
        .professional-detail-content{
            padding: 5px 0px;
            margin: 0 70px;
        }
        .text-bold{
           font-size: 16px;
           font-weight: 600;
           color: #000;
        }
        .text-bold span{
            padding: 0 5px;
        }
        .breaking-word{
            word-break: break-word;
        }
        .document-content-sec{
            padding: 5px 0px;
            margin: 0 70px;
        }
        .document-content-1{
            color: #b44634;
        }
        
        .document-content-2{
            color: #0f38f7;
            text-decoration: underline;
        }
        .button-sec{
            padding: 5px 0px;
            margin: 0 70px;
        }
        .print-pdf-btn{
            border: 0;
            outline: 0;
            background-color: #465f9e;
            color: #fff;
            font-size: 12px;
            font-weight: 100;
            padding: 3px 8px;
            cursor: pointer;
            letter-spacing: 1px;
        }
        .activate-worker button{
            background-color: #DF8C40;
            color: #fff;
            border-radius: 3px;
            padding: 8px 25px;
            box-shadow: 0 0 20px rgba(223,140,64, .3);
            border: 0;
            outline: none;
            margin: 10px 50px 30px;
            font-size: 15px;
            font-weight: 300;
            letter-spacing: 1px;
            cursor: pointer;
        }
        .professional-detail-heading{
            font-family: Montserrat;
            font-size: 25px;
            font-weight: 400;
            margin-bottom: 0;
            padding: 20px 0;
        }
        .professional-detail-sec{
            border-bottom: 1px solid #e2e2e2;
        }
        .professional-detail-content span{
            padding-left: 30px;
        }
          </style>
        </head>
    <body onload="window.print();window.close()">${innerContents}</body>
      </html>`
    );    
    popupWinindow.document.close();

  }

}
