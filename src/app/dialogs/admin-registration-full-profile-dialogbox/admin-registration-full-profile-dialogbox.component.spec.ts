import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRegistrationFullProfileDialogboxComponent } from './admin-registration-full-profile-dialogbox.component';

describe('FullProfileDialogboxComponent', () => {
  let component: AdminRegistrationFullProfileDialogboxComponent;
  let fixture: ComponentFixture<AdminRegistrationFullProfileDialogboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRegistrationFullProfileDialogboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRegistrationFullProfileDialogboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
