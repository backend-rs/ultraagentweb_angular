import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Router } from '@angular/router';

import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../../services/auth.service';

import { SuccessOkDialogComponent } from '../../dialogs/success-ok-dialog/success-ok-dialog.component';
import { ApproveRejectDialogComponent } from '../../dialogs/approve-reject-dialog/approve-reject-dialog.component';

import * as firebase from 'firebase/app';

import * as jspdf from 'jspdf';  
import html2canvas from 'html2canvas'; 

@Component({
  selector: 'app-pending-detail-dialog',
  templateUrl: './pending-detail-dialog.component.html',
  styleUrls: ['./pending-detail-dialog.component.css']
})
export class PendingDetailDialogComponent implements OnInit {

  title:string = '';
  subtitle:string = '';

  hidePayTimesheetButton:boolean = false;
  hidePaidTimeText:boolean = true;
  
  booking:any;
  employerRating:any;
  averageRating:any;

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<PendingDetailDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data:string, 
    private authService: AuthService,
    private router: Router,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    ) { }

  ngOnInit() {
  }

  paytimesheetButton(): void {
   

    const dialogRef = this.dialog.open(ApproveRejectDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = 'Are you sure?';
    dialogRef.componentInstance.descr = 
    "Do you really want to proceed with payment for this timesheet?";

    dialogRef.componentInstance.approveTitle = 'Yes, Proceed';
    dialogRef.componentInstance.rejectTitle = 'No, Cancel';

    dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      if (result == dialogRef.componentInstance.approveTitle) {
        this.dialogRef.close();

        // let invoiceNo = this.booking.shiftNo;
        // let paymentNo = this.booking.shiftNo;
        this.firestore.collection("bookings").doc(this.booking.id).set({
          calculatedWorkerFee: this.booking.calculatedWorkerFee,
          calculatedUAMargin: this.booking.calculatedUAMargin,
          calculatedEmployerFee: this.booking.calculatedEmployerFee,
          
        }, {merge:true})
        .then(() => {
          this.router.navigate(['pay-timesheet-page',{bookingId:this.booking.id}]);
        })
        .catch((err) => {
          console.log(err.message);
          this.openDialog('Error', err.message);
        });
      }
    });

  }

  cancelClick(): void {
    this.dialogRef.close();
  }

  openDialog(title='', msg=''): void {
    const dialogRef = this.dialog.open(SuccessOkDialogComponent, {
      width: '600px',
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.descr = msg;
    dialogRef.componentInstance.buttonTitle="OK";
    
  }

  generatePDF() {

    var data = document.getElementById('printableContainerId'); 

    var pdfName = "MYPdf.pdf";
    if (this.booking != undefined) {
      // profile exists
      pdfName = this.booking.shiftSubCategory.replace(/\s/g,'')+"-"+this.booking.shiftNo.replace(/\s/g,'')+".pdf";
    }

    html2canvas(data).then(canvas => {  
      // Few necessary setting options  
      var imgWidth = 208;   
      var pageHeight = 295;    
      var imgHeight = canvas.height * imgWidth / canvas.width;  
      var heightLeft = imgHeight;  
  
      const contentDataURL = canvas.toDataURL('image/png')  
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;  
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
      pdf.save(pdfName); // Generated PDF   
    });  

  }

  generatePrint() {

    var title = "";
    if (this.booking != undefined) {
      // profile exists
      title = this.booking.shiftSubCategory.replace(/\s/g,'')+"-"+this.booking.shiftNo.replace(/\s/g,'');
    }

    var innerContents = document.getElementById("printableContainerId").innerHTML;
    var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
    popupWinindow.document.open();
    popupWinindow.document.write(`
      <html>
        <head>
          <title>${title}</title>
          <style>
          .example-spacer {
            flex: 1 1 auto;
        }
        .word-wrap-break{
            word-wrap: break-word;
        }
        .mat-dialog-content{
            margin: 0;
            padding: 0;
        }
        .dialog-toolbar{
            background-color: #c3ccf7;
            padding: 20px 30px;
        }
        .clear-icon{
            cursor: pointer;
            font-size: 30px;
            font-weight: 600;
            margin-bottom: 40px;
        }
        .toolbar-heading{
            font-size: 25px;
            font-weight: 600;
            font-family: Montserrat;
            text-transform: uppercase;
        }
        .toolbar-subheading{
            font-size: 18px;
            font-weight: 500;
            font-family: Roboto;
            text-transform: uppercase;
        }
        .mat-toolbar-row{
            height: 30px;
        }
        .dialog-subject{
            padding: 30px 0px;
        }
        .details_content{
            background-color: #f4f4f4;
            padding: 35px 20px 35px;
            margin: 0 40px;
        }
        .description-heading{
            font-size: 20px;
        }
        .description-text{
            font-size: 14px;
            line-height: 18px;
            margin-bottom: 0;
        }
        .shift-description-below-content{
            margin: 20px 40px;
        }
        .text-bold{
            font-size: 16px;
            font-weight: 600;
            color: #000;
        }
        .text-bold span{
            padding: 0 5px;
        }
        .bottom-margin{
            margin-bottom: 0;
        }
        .breaking-word{
            word-break: break-word;
            font-size: 14px;
            font-weight: 500;
            color: #000;
        }
        .print-pdf-sec{
            padding-bottom: 30px;
        }
        .print-pdf-btn{
            border: 0;
            outline: 0;
            background-color: #465f9e;
            /* box-shadow: 0 0 40px rgba(223,140,64, .3); */
            color: #fff; 
            font-size: 12px;
            font-weight: 100;
            padding: 3px 7px;
            cursor: pointer;
        }
        .inline{
            display: inline-block;
        }
        .pay-timesheet button{
            background-color: #DF8C40;
            color: #fff;
            border-radius: 3px;
            padding: 9px 25px;
            box-shadow: 0 0 40px rgba(223,140,64, .2);
            border: 0;
            outline: none;
            margin: 0 5px;
            font-size: 14px;
            font-weight: 300;
            letter-spacing: 1px;
            cursor: pointer;
        }
          </style>
        </head>
    <body onload="window.print();window.close()">${innerContents}</body>
      </html>`
    );    
    popupWinindow.document.close();

  }

}
