import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingDetailDialogComponent } from './pending-detail-dialog.component';

describe('PendingDetailDialogComponent', () => {
  let component: PendingDetailDialogComponent;
  let fixture: ComponentFixture<PendingDetailDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingDetailDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingDetailDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
