import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-admin-registration-delete-dialogbox',
  templateUrl: './admin-registration-delete-dialogbox.component.html',
  styleUrls: ['./admin-registration-delete-dialogbox.component.css']
})
export class AdminRegistrationDeleteDialogboxComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<AdminRegistrationDeleteDialogboxComponent>, @Inject(MAT_DIALOG_DATA) public data:string) { }

  ngOnInit() {
  }

  cancelClick(): void {
    this.dialogRef.close();
  }

}
