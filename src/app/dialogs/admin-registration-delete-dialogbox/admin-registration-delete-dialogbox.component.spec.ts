import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRegistrationDeleteDialogboxComponent } from './admin-registration-delete-dialogbox.component';

describe('DeleteDialogboxComponent', () => {
  let component: AdminRegistrationDeleteDialogboxComponent;
  let fixture: ComponentFixture<AdminRegistrationDeleteDialogboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRegistrationDeleteDialogboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRegistrationDeleteDialogboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
