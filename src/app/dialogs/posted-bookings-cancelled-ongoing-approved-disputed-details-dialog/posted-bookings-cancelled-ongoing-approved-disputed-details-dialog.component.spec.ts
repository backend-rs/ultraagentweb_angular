import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostedBookingsCancelledOngoingApprovedDisputedDetailsDialogComponent } from './posted-bookings-cancelled-ongoing-approved-disputed-details-dialog.component';

describe('PostedBookingsCancelledOngoingApprovedDisputedDetailsDialogComponent', () => {
  let component: PostedBookingsCancelledOngoingApprovedDisputedDetailsDialogComponent;
  let fixture: ComponentFixture<PostedBookingsCancelledOngoingApprovedDisputedDetailsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostedBookingsCancelledOngoingApprovedDisputedDetailsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostedBookingsCancelledOngoingApprovedDisputedDetailsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
