import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-posted-bookings-cancelled-ongoing-approved-disputed-details-dialog',
  templateUrl: './posted-bookings-cancelled-ongoing-approved-disputed-details-dialog.component.html',
  styleUrls: ['./posted-bookings-cancelled-ongoing-approved-disputed-details-dialog.component.css']
})
export class PostedBookingsCancelledOngoingApprovedDisputedDetailsDialogComponent 
implements OnInit {

  title:string = '';
  subtitle:string = '';
  buttonTitle:string = '';
  penaliseWorkerbutton:string = '';
  penaliseEmployerbutton:string = '';
  shift:any;
  booking:any;
  bookedWorker:any;
  employerRating:any;
  averageRating:any;

  expectedEndTime:any;

  interestedWorkersHidden = false;
  selectedWorkersHidden = false;
  workerDetailHidden = false;
  startEndTimeHidden = false;
  reasonForCancellationHidden = false;
  employerReviewHidden = false;
  penaliseWorkerHidden = false;
  tracktimesheetHidden = false;
  
  constructor(public dialogRef: 
    MatDialogRef<PostedBookingsCancelledOngoingApprovedDisputedDetailsDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data:string) {
    // this.title = data.title;
    // this.descr = data.descr;
   }

  ngOnInit() {
  }

  cancelClick(): void {
    this.dialogRef.close();
  }

  buttonClick(){
    this.dialogRef.close(this.buttonTitle);
  }

}
