import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRegistrationRejectDialogboxComponent } from './admin-registration-reject-dialogbox.component';

describe('RejectDialogboxComponent', () => {
  let component: AdminRegistrationRejectDialogboxComponent;
  let fixture: ComponentFixture<AdminRegistrationRejectDialogboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRegistrationRejectDialogboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRegistrationRejectDialogboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
