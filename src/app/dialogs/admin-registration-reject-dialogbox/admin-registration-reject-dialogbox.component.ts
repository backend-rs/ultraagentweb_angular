import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-admin-registration-reject-dialogbox',
  templateUrl: './admin-registration-reject-dialogbox.component.html',
  styleUrls: ['./admin-registration-reject-dialogbox.component.css']
})
export class AdminRegistrationRejectDialogboxComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<AdminRegistrationRejectDialogboxComponent>, @Inject(MAT_DIALOG_DATA) public data:string) { }

  ngOnInit() {
  }

  cancelClick(): void {
    this.dialogRef.close();
  }
}
