import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { CancelBookingDialogComponent } from '../../EMPLOYER/cancel-booking-dialog/cancel-booking-dialog.component';


@Component({
  selector: 'app-mybooking-details-dialog',
  templateUrl: './mybooking-details-dialog.component.html',
  styleUrls: ['./mybooking-details-dialog.component.css']
})
export class MybookingDetailsDialogComponent implements OnInit {
  
  dialogResult="";
  booking:any;

  hideCancelledReason: boolean = false;
  hideCancelButton: boolean = false;

  constructor(public dialog: MatDialog, public dialogRef: MatDialogRef<MybookingDetailsDialogComponent>, @Inject(MAT_DIALOG_DATA) public data:string) { }

  ngOnInit() {
  }

  openCancelBookingDialog(): void {
    const dialogRef = this.dialog.open(CancelBookingDialogComponent, {
      panelClass: 'my-panel',
      width: '800px',
    });
    
    
    dialogRef.componentInstance.booking = this.booking;
     dialogRef.afterClosed().subscribe(result => {
      console.log(`The dialog was closed: ${result}`);
      this.dialogResult = result;
    });

    this.dialogRef.close();

  }
  cancelClick(): void {
    this.dialogRef.close();
  }
}
