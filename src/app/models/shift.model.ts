export class Shift {
    $key: string;
    category:string;
    subcategory:string;
    duration:string;
    date:string;
    time:string;
    fee:string;
    contactpername:string;
    phoneNumber:number;
    address:string;
    city:string;
    description:string;
    shiftNo:string;
}
