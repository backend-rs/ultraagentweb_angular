export class BookingRequest {
    $key: string;
    companyId: string;
    workerId: string;
    approvalStatus: string;
    shiftId: string;
    createdAt: string;
    createdBy: string;
    isAdvanceBooking: boolean;
}
