export class Profile {
    $key: string;
    id:string;
    emailId:string;
    companycategory:string;
    companyname:string;
    phoneNumber:number;
    regnumber:string;
    city:string;
    address:string;
    cdescription:string;
    approvalStatus:string;
    profileType:string;
}
