import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { LoginComponent } from './login/login.component';
import { AdminLoginComponent } from './ADMIN/admin-login/admin-login.component';
import { ForgotPasswordComponent } from './ADMIN/forgot-password/forgot-password.component';
import { DashboardComponent } from './ADMIN/dashboard/dashboard.component';
import { RegistrationLoginComponent } from './EMPLOYER/registration-login/registration-login.component';
import { CompanyRegistrationComponent } from './EMPLOYER/company-registration/company-registration.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { NewPasswordComponent } from './ADMIN/new-password/new-password.component';
import { EmployeeUserExperienceComponent } from './EMPLOYER/employee-user-experience/employee-user-experience.component';
import { AdminProfileComponent } from './ADMIN/admin-profile/admin-profile.component';
import { RegistrationComponent } from './ADMIN/registration/registration.component';
import { ShiftsComponent } from './ADMIN/shifts/shifts.component';
import { TimesheetComponent } from './ADMIN/timesheet/timesheet.component';
import { UsersComponent } from './ADMIN/users/users.component';
import { ReportsComponent } from './ADMIN/reports/reports.component';
import { EmployerProfileComponent } from './EMPLOYER/employer-profile/employer-profile.component';
import { PostedShiftsComponent } from './EMPLOYER/posted-shifts/posted-shifts.component';
import { BookingsComponent } from './EMPLOYER/bookings/bookings.component';
import { WorkersComponent } from './EMPLOYER/workers/workers.component';
import { EmployerDashboardComponent } from './EMPLOYER/employer-dashboard/employer-dashboard.component';
import { EmployerTimesheetComponent } from './EMPLOYER/employer-timesheet/employer-timesheet.component';
import { EmployerReportsComponent } from './EMPLOYER/employer-reports/employer-reports.component';
import { EmployerGuard } from './services/employerGuard.service';
import { AdminGuard } from './services/adminGuard.service';
import { AdvanceBookingPageComponent } from './EMPLOYER/advance-booking-page/advance-booking-page.component';
import { PayTimesheetPageComponent } from './ADMIN/pay-timesheet-page/pay-timesheet-page.component';
import { ProfileComponent } from './ADMIN/profile/profile.component';
import { EmployerSettingsComponent } from './EMPLOYER/employer-settings/employer-settings.component';
import { ProfileForEmployerComponent } from './EMPLOYER/profile-for-employer/profile-for-employer.component';
import { AdminSettingsComponent } from './ADMIN/admin-settings/admin-settings.component';
import { AdminSettingEditPageComponent } from './ADMIN/admin-setting-edit-page/admin-setting-edit-page.component';
import { EmployerUsersEditPageComponent } from './EMPLOYER/employer-users-edit-page/employer-users-edit-page.component';
import { ReviewWorkerComponent } from './EMPLOYER/review-worker/review-worker.component';
import { TermsOfServicesComponent } from './EMPLOYER/terms-of-services/terms-of-services.component';
import { UsersReportComponent } from './report_design/users-report/users-report.component';
import { TimesheetReportComponent } from './report_design/timesheet-report/timesheet-report.component';
import { ShiftsReportComponent } from './report_design/shifts-report/shifts-report.component';
import { RegistrationReportComponent } from './report_design/registration-report/registration-report.component';
import { EmployerBookingsComponent } from './report_design/employer-bookings/employer-bookings.component';
import { EmployerPostedshiftComponent } from './report_design/employer-postedshift/employer-postedshift.component';
import { EmployerTimesheetReportComponent } from './report_design/employer-timesheet-report/employer-timesheet-report.component';


const appRoutes : Routes = [

  /* Employer part*/
  
  { path: '', redirectTo:'employer_login_registration', pathMatch:'full'},
  // {path: 'login', component: LoginComponent},
  { path: 'employer_login_registration', component: RegistrationLoginComponent},
  { path: 'forgot_password', component: ForgotPasswordComponent},
  { path: 'company_registration', component: CompanyRegistrationComponent},
  { path: 'employee_user_experience', component: EmployeeUserExperienceComponent},
  { path: 'Employer_bookings_report', component: EmployerBookingsComponent},
  { path: 'Employer_postedshift_report', component: EmployerPostedshiftComponent},
  { path: 'Employer_timesheet_report', component: EmployerTimesheetReportComponent},
  

  /* Admin part*/
  /*
  //{ path: '', redirectTo:'Admin_login', pathMatch:'full'},
  { path: 'Admin_login', component: AdminLoginComponent},
  { path: 'Admin_new_password', component: NewPasswordComponent},
  { path: 'Users_report', component: UsersReportComponent},
  { path: 'Timesheet_report', component: TimesheetReportComponent},
  { path: 'Shifts_report', component: ShiftsReportComponent},
  { path: 'Registration_report', component: RegistrationReportComponent},
 */

  /* Common part */
  { path: 'authaction', component: VerifyEmailComponent},
  { path: 'terms_of_services', component: TermsOfServicesComponent},

  { 
    path: '', 
    component: AdminProfileComponent,
    children: [
      { path: 'admin_dashboard', component: DashboardComponent, canActivate: [AdminGuard]},
      { path: 'registration', component: RegistrationComponent, canActivate: [AdminGuard]},
      { path: 'shifts', component: ShiftsComponent, canActivate: [AdminGuard]},
      { path: 'timesheet', component: TimesheetComponent, canActivate: [AdminGuard] },
      { path: 'users', component: UsersComponent, canActivate: [AdminGuard]},
      { path: 'reports', component: ReportsComponent, canActivate: [AdminGuard]},
      { path: 'pay-timesheet-page', component: PayTimesheetPageComponent, canActivate: [AdminGuard]},
      { path: 'profile', component: ProfileComponent, canActivate: [AdminGuard]},
      { path: 'admin_settings', component: AdminSettingsComponent, canActivate: [AdminGuard]},
      { path: 'admin_setting_edit', component: AdminSettingEditPageComponent, canActivate: [AdminGuard]}
    ]
  },
  {
    path: '',
    component: EmployerProfileComponent,
    children: [
      { path: 'employer_dashboard', component: EmployerDashboardComponent, canActivate: [EmployerGuard]},
      { path: 'posted_shifts', component: PostedShiftsComponent, canActivate: [EmployerGuard]},
      { path: 'bookings', component: BookingsComponent, canActivate: [EmployerGuard]},
      { path: 'employer_timesheet', component: EmployerTimesheetComponent, canActivate: [EmployerGuard]},
      { path: 'workers', component: WorkersComponent, canActivate: [EmployerGuard]},
      { path: 'employer_report', component: EmployerReportsComponent, canActivate: [EmployerGuard]},
      { path: 'worker_advance_booking', component: AdvanceBookingPageComponent, canActivate: [EmployerGuard]},
      { path: 'settings', component: EmployerSettingsComponent, canActivate: [EmployerGuard]},
      { path: 'profile_for_employer', component: ProfileForEmployerComponent, canActivate: [EmployerGuard]},
      { path: 'users_setting_edit', component: EmployerUsersEditPageComponent, canActivate: [EmployerGuard]},
      { path: 'review_worker', component: ReviewWorkerComponent, canActivate: [EmployerGuard]}
    ]
  }
];

// export const AppRoutes = RouterModule.forRoot(appRoutes);
// Using AuthGuard provider now
export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(appRoutes, {useHash: true});