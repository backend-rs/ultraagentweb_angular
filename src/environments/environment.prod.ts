import { uaWebVersion } from './uaWebVersion';

export const environment = {
  uaWebVersion: uaWebVersion,
  production: true,
    // Demo account from bharat.biswal@gmail.com
  // firebase: {
  //   apiKey: "AIzaSyACYpu6ZLbmZVzxBcEaNCAsc_Slb5zCo3o",
  //   authDomain: "ultraagentproject.firebaseapp.com",
  //   databaseURL: "https://ultraagentproject.firebaseio.com",
  //   projectId: "ultraagentproject",
  //   storageBucket: "ultraagentproject.appspot.com",
  //   messagingSenderId: "198484758520" 
  // }

  // Real Account from appsmartdev2018@gmail.com
  firebase: {
    apiKey: "AIzaSyCvmCqDwonLEj4o6p0MCNfU20tDPiA48IU",
    authDomain: "ultraagentcouk.firebaseapp.com",
    databaseURL: "https://ultraagentcouk.firebaseio.com",
    projectId: "ultraagentcouk",
    storageBucket: "ultraagentcouk.appspot.com",
    messagingSenderId: "667463728840"
  }

};
