
import { uaWebVersion } from './uaWebVersion';

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  uaWebVersion: uaWebVersion,
  production: false,
  // Demo account from bharat.biswal@gmail.com
  // firebase: {
  //   apiKey: "AIzaSyACYpu6ZLbmZVzxBcEaNCAsc_Slb5zCo3o",
  //   authDomain: "ultraagentproject.firebaseapp.com",
  //   databaseURL: "https://ultraagentproject.firebaseio.com",
  //   projectId: "ultraagentproject",
  //   storageBucket: "ultraagentproject.appspot.com",
  //   messagingSenderId: "198484758520" 
  // }

  // Test account from debasmitasamantaray123@gmail.com
  // firebase: {
  //   apiKey: "AIzaSyB9O-Slzi2qB37IfpV2wdZTamxBXcxnTNc",
  //   authDomain: "ultraagentweb.firebaseapp.com",
  //   databaseURL: "https://ultraagentweb.firebaseio.com",
  //   projectId: "ultraagentweb",
  //   storageBucket: "ultraagentweb.appspot.com",
  //   messagingSenderId: "301191053836" 
  // }

  // Real Account from appsmartdev2018@gmail.com
  firebase: {
    apiKey: "AIzaSyCvmCqDwonLEj4o6p0MCNfU20tDPiA48IU",
    authDomain: "ultraagentcouk.firebaseapp.com",
    databaseURL: "https://ultraagentcouk.firebaseio.com",
    projectId: "ultraagentcouk",
    storageBucket: "ultraagentcouk.appspot.com",
    messagingSenderId: "667463728840"
  }


};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
